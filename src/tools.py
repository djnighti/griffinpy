import openpyxl
import pandas as pd
import numpy as np
from parameters import Parameters


class FileTool:
    """

    """
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)
        self.file_input = self.parameters.file_pegasus_input_data
        self.file_location_gtd = self.parameters.file_pegasus_input_location
        self.label_column_one = []
        self.label_column_two = []
        self.title_list = []
        self.data_column_1 = None
        self.data_column_2 = None
        self.data_walk = None
        self.data_step_size = None
        self.sheet_name_walk = "walk"
        self.sheet_name_power_history = "power_history"
        self.sheet_name_axial_power_shapes = "axial_power_shapes"
        self.sheet_name_temperature_history = "temperature_history"
        self.sheet_prefix_z = 'z'
        self.num_z_dep_gtd = 0


    def convert_xlsx_to_gtd(self):
        """

        """
        data_column_1 = []
        data_column_2 = []
        data_column_all_z = []

        xlsx_dict = pd.read_excel(
            f"{self.file_input}",
            sheet_name=None)
        
        for sheet_name, sheet_data in xlsx_dict.items():
            # Doing list comp to convert vals to floats because np could not convert?
            
            if sheet_name != self.sheet_name_walk:
                headers_list = xlsx_dict[sheet_name].columns.ravel()
                self.num_z_pos = len(headers_list) - 1
                if self.num_z_pos > 1:
                    self.num_z_dep_gtd+=1
                    data_column_z = []
                    data_column_time_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[0]].values.tolist()]
                    z_pos_data = list(headers_list[1:])
                    for z_pos in range(1, self.num_z_pos+1):
                        data_column_var_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[z_pos]].values.tolist()]
                        data_column_z.append([data_column_var_raw, data_column_time_raw])
                    data_column_all_z.append([data_column_time_raw, z_pos_data, data_column_z])
                    label_column_one = headers_list[0].split('_', 1)[0]
                    label_column_two = headers_list[0].split('_', 1)[1]

                else:
                    data_column_1_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[0]].values.tolist()]
                    data_column_2_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[1]].values.tolist()]
                    data_column_1.append(data_column_1_raw)
                    data_column_2.append(data_column_2_raw)
                    label_column_one = headers_list[0]
                    label_column_two = headers_list[1]

                    
                self.label_column_one.append(label_column_one)
                self.label_column_two.append(label_column_two)
                self.title_list.append(sheet_name)

        self.data_column_1 = np.array(data_column_1, dtype=object)
        self.data_column_2 = np.array(data_column_2, dtype=object)
        self.data_column_all_z = np.array(data_column_all_z, dtype=object)
        self.num_gtd_files = len(self.title_list)
        # print(
        #     f"self.title_list: {self.title_list}\n"
        #     f"self.num_gtd_files: {self.num_gtd_files}\n"
        #     f"self.label_column_one: {len(self.label_column_one)}\n"
        #     f"self.label_column_two: {len(self.label_column_two)}\n"
        #     )
        

        # iterate over all sheets to turn each sheet into a new gtd file
        gtd_sheet_count = 0
        for sheet in range(0, self.num_gtd_files):
            gtd_name = f"/{self.title_list[sheet]}.gtd"
            sheet_prefix = self.title_list[sheet].split('_', 1)[0]
            # print(
            #     f"sheet_prefix: {sheet_prefix}\n"
            #     f"self.title_list[sheet]: {self.title_list[sheet]}\n"
            # )
            
            # if data is a function of position
            if sheet_prefix==self.sheet_prefix_z:
                # name_sheet = sheet_name.split('_', 1)[1]
                # print("heres")
                z_combined = []
                for z_gtd in range(0, self.num_z_dep_gtd):
                    # space and time 
                    zn_time = np.transpose([self.data_column_all_z[z_gtd][0]])
                    zn_space = np.transpose([self.data_column_all_z[z_gtd][1]])
                    empty_time_str_array = np.full_like(zn_time, "", dtype=object)
                    empty_space_str_array = np.full_like(zn_space, "", dtype=object)
                    zn_time = np.concatenate((zn_time, empty_time_str_array), axis=1)
                    zn_space = np.concatenate((zn_space, empty_space_str_array), axis=1)
                    spacer = np.array([[":", '']], dtype=object)
                    
                    # print(
                    #     f"zn_time: {zn_time}\n"
                    #     f"zn_space: {zn_space}\n"
                    #     f"spacer: {spacer}\n"
                    # )
                    z_gtd_data = np.concatenate(
                        ([[str(self.label_column_one[sheet]),'']], 
                        zn_time, 
                        [[str(self.label_column_two[sheet]),'']], 
                        zn_space,
                        spacer),
                        axis=0)

                gtd_name = f"/{self.title_list[sheet].split('_', 1)[1]}.gtd"
                with open(self.file_location_gtd+gtd_name, 'w') as f:
                    # f.writelines(a + '\n')
                    np.savetxt(f, z_gtd_data, fmt='%s')

                    # Actual space dependent variable
                    for z_pos in range(0, self.num_z_pos):
                        zn_variable = np.transpose(self.data_column_all_z[z_gtd][2][z_pos])
                        # print(
                        #     f"zn_variable: {zn_variable}\n"
                        # )
                        # zn_combined = np.concatenate((zn_variable, zn_time), axis=1)
                        # z_combined.append(zn_combined)
                        z_gtd_data = np.concatenate(
                            (z_gtd_data, 
                            zn_variable), 
                            axis=0)
                # self.z_combined = np.array(z_combined, dtype=object)
                        np.savetxt(f, zn_variable, delimiter = ",", fmt='%s')
                        f.close
                gtd_data = z_gtd_data

                
                
                # gtd_data_var_time = np.concatenate((), axis=1)
                # gtd_data = np.concatenate(
                #     ([str(label_time)], 
                #     self.data_column_1[sheet], 
                #     [":"], 
                #     [str(label_space)], 
                #     self.data_column_1[sheet], 
                #     [":"], 
                #     self.data_column_2[sheet]), 
                #     axis=0)

            # regular gtd
            else:
                # print(
                #     f"self.data_column_1: {self.data_column_1}\n"
                #     f"sheet: {sheet}"
                #     )
                label_time_or_space = self.label_column_one[sheet]
                gtd_data = np.concatenate(
                    ([str(label_time_or_space)], 
                    self.data_column_1[sheet], 
                    [":"], 
                    self.data_column_2[sheet]), 
                    axis=0)
                gtd_sheet_count+=1
                np.savetxt(self.file_location_gtd+gtd_name, gtd_data, fmt='%s')
            
            # restart count 
            gtd_sheet_count = 0
            # print(
            #     f"gtd_data: {gtd_data}"
            #     )

            # save gtd
            
            # print(f"gtd_name: {gtd_name}")
            # np.savetxt(self.file_location_gtd+gtd_name, gtd_data, delimiter = ",", fmt='%s')
            
            gtd_data = 0


    def get_time_steps(self):
        """

        """
        data_walk = []
        data_step_size = []
        xlsx_dict = pd.read_excel(
            f"{self.file_input}",
            sheet_name=None)
        
        for sheet_name, sheet_data in xlsx_dict.items():
            # Doing list comp to convert vals to floats because np could not convert?
            if sheet_name == self.sheet_name_walk:
                headers_list = xlsx_dict[sheet_name].columns.ravel()
                data_walk_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[0]].values.tolist()]
                data_step_size_raw = [float(val) for val in xlsx_dict[sheet_name][headers_list[1]].values.tolist()]
                data_walk.append(data_walk_raw)
                data_step_size.append(data_step_size_raw)
        
        self.data_walk = np.array(data_walk,dtype=object)
        self.data_step_size = np.array(data_step_size,dtype=object)
        self.data_walk = self.data_walk[0]
        self.data_step_size = self.data_step_size[0]
        
        data_walk_and_size = np.concatenate(
            (self.data_walk, 
            self.data_step_size), 
            axis=0)


if __name__ == '__main__':
    param_file_test = "/home/projects/griffinpy/scrip_gen_tests/scrip_gen_input/generator_input_gtd_test.yaml"
    csv_to_xlsx_converter = FileTool(param_file_test)
    csv_to_xlsx_converter.convert_xlsx_to_gtd()
    csv_to_xlsx_converter.get_time_steps()
