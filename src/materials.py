from parameters import Parameters
import yaml


class CreateMaterials:
    """
    
    This class allows for the creation of the psf formatted strings that all material classes can access
    and manages the creation of the materials.

    """
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)
        self.prefix_power = "power"
        self.prefix_property = "property"
        self.prefix_material = "material"
        self.prefix_function = "function"
        self.prefix_retrieve = "retrieve"
        self.object_inventory = "inventory"
        self.object_material = "material"
        self.state_variable_type_node = "node"
        self.state_variable_type_element = "element"

        # common retrievable state variables
        self.retrieve_heat_flux_x = self.parameters.standard_retrieve_heat_flux_x
        self.retrieve_heat_flux_y = self.parameters.standard_retrieve_heat_flux_y
        self.retrieve_heat_flux_z = self.parameters.standard_retrieve_heat_flux_z
        self.retrieve_internal_energy = self.parameters.standard_retrieve_internal_energy
        self.retrieve_cauchy_stress_xx = self.parameters.standard_retrieve_cauchy_stress_xx
        self.retrieve_cauchy_stress_yy = self.parameters.standard_retrieve_cauchy_stress_yy
        self.retrieve_cauchy_stress_zz = self.parameters.standard_retrieve_cauchy_stress_zz
        self.retrieve_cauchy_stress_thetatheta = self.parameters.standard_retrieve_cauchy_stress_thetatheta
        self.retrieve_cauchy_stress_rr = self.parameters.standard_retrieve_cauchy_stress_rr
        self.retrieve_temperature = self.parameters.standard_retrieve_temperature
        self.retrieve_displacement_x = self.parameters.standard_retrieve_displacement_x
        self.retrieve_displacement_y = self.parameters.standard_retrieve_displacement_y
        self.retrieve_displacement_z = self.parameters.standard_retrieve_displacement_z

    def create_material(self, material_index=None):
        """

        :param material_index:
        :return:
        """
        params_dict = vars(self)
        props_dict = self.create_material_dicts()
        num_props = len(props_dict)
        material_string = []
        string_post = []

        # Begining part of creating material
        if self.object_type == self.object_material:
            string_pre = (
                f"CREATE MATERIAL \\ \n"
                f"  {self.material_name.upper()} \\ \n"
                f"  {self.material_type.upper()} \\ \n"
                )
        elif self.object_type == self.object_inventory:
            string_pre = (
                f"CREATE {self.material_type.upper()} \\ \n"
                f"  {self.material_name.upper()} \\ \n"
                )
        
        # Last part of string (can vary size)
        count = 1
        # print(props_dict)
        if material_index is None:
            material_index = ""

        for key in props_dict:
            if type(props_dict[key])==float:
                _, mat_real = self.parameters.create_real(key, props_dict[key])
                if count!=num_props:
                    single_string_post = (
                    f"  {mat_real+material_index} \\ \n"
                    )
                else:
                    single_string_post = (
                    f"  {mat_real+material_index} \n"
                    f"\n"
                    )
                string_post.append(single_string_post)
            
            elif type(props_dict[key])==str:
                if count!=num_props:
                    single_string_post = (
                    f'  "{props_dict[key]}" \\ # {key} \n'
                    )
                else:
                    single_string_post = (
                    f'  "{props_dict[key]}" # {key} \n'
                    f"\n"
                    )
                string_post.append(single_string_post)
            
            # adding this quick fix for power dist object (PDO) becasue its supposed to be an object and not string
            # so putting PDO as type=list
            elif type(props_dict[key])==list:
                if count!=num_props:
                    single_string_post = (
                    f"  {props_dict[key][0]} \\ # {key} \n"
                    )
                else:
                    single_string_post = (
                    f"  {props_dict[key][0]} # {key} \n"
                    f"\n"
                    )
                string_post.append(single_string_post)
            count+=1
        
        material_string.extend(string_pre)
        material_string.extend(string_post)
        return material_string
    
    def create_material_dicts(self):
        """

        :return properties_dict:
        """
        params_dict = vars(self)
        properties_dict = {}
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            if key_prefix == self.prefix_property:
                key_suffix = key.lower().split('_', 1)[1]
                properties_dict[key_suffix] = params_dict[key]
            elif key_prefix == self.prefix_power:
                key_suffix = key.lower().split('_', 1)[1]
                properties_dict[key_suffix] = [params_dict[key]]
            elif key_prefix == self.prefix_function:
                key_suffix = key.lower().split('_', 1)[1]
                properties_dict[key_suffix] = [params_dict[key]]
        return properties_dict

    def create_material_state_var_dict(self):
        """
        
        This function creates a dictionary of the possible retrievable state variables for a material
        to later be used in `retrieve.`
        

        :return retrieve_dict: where the keys -> state variables -> object type (nodal, element)
        """
        params_dict = vars(self)
        retrieve_dict = {}
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            if key_prefix == self.prefix_retrieve:
                key_suffix = key.lower().split('_', 1)[1]
                retrieve_dict[key_suffix.upper()] = params_dict[key]
        return retrieve_dict

    def create_reals(self, material_index=None):
        """

        :param material_index:
        :return:
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        if material_index is None:
            material_index = ""
        for key in params_dict:
            if type(params_dict[key])==float:
                if key.lower().split('_', 1)[0] == self.prefix_property:
                    create_an_real, _ = self.parameters.create_real(key.lower().split('_', 1)[1]+material_index, params_dict[key])
                    create_params_string_list.extend(create_an_real)
                
        create_params_string_list.extend(new_line)
        return create_params_string_list

    def print_atts(self):
        """

        """
        params_dict = vars(self)
        for key in params_dict:
            print(f"{key} : {params_dict[key]}")

class GenericZircNonfuel(CreateMaterials):
    """
    
    This class holds material attributes for zirc material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(GenericZircNonfuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_cladding_list
        self.material_class_name = self.__class__.__name__
        self.material_type = "GENERIC_ZIRC_NONFUEL"
        self.property_zirc_density = 6560.0
        self.property_zirc_default_internal_energy = 0.0
        self.property_zirc_default_temperature = 298.0
        self.property_zirc_poisson_ratio = 0.37
        self.property_zirc_SIG_C_0 = 0.0
        self.property_zirc_ALPHA_C = 0.0
        self.property_zirc_EPS_C_0 = 1.0
        self.property_zirc_SIG_P_0 = 0.0
        self.property_zirc_ALPHA_P = 0.0
        self.property_zirc_EPS_P_0 = 1.0
        self.property_zirc_INITIAL_YIELD_STRESS = 400.0E6
        self.property_zirc_INITIAL_YIELD_SLOPE_MULTIPLIER = 1.0
        self.property_zirc_HARD_EXP = 0.25
        self.property_zirc_cold_work_factor = 0.0
        self.property_zirc_initial_oxide_layer_thickness = 0.0
        self.property_zirc_initial_additional_oxygen_concentration = 0.0
        self.property_zirc_oxide_conductivity = 1.7
        self.property_zirc_surface_roughness = 1.8E-05
        self.property_zirc_material_axis_origin_x = 0.0
        self.property_zirc_material_axis_origin_y = 0.0
        self.property_zirc_material_axis_origin_z = 0.0
        self.property_zirc_material_axis_direction_x = 0.0
        self.property_zirc_material_axis_direction_y = 0.0
        self.property_zirc_material_axis_direction_z = 1.0
        self.property_zirc_creep_rate_multiplier = 1.2
        self.property_zirc_irradiation_growth_strain_multiplier = 1.0
        self.property_zirc_reactor_type = "PWR"
        self.property_zirc_creep_rate_model = "LIMBACK_SECONDARY"
        
        # unique zirc
        self.retrieve_von_mises_stress = self.state_variable_type_element
        self.retrieve_fast_neutron_flux = self.state_variable_type_element
        self.retrieve_fast_neutron_fluence = self.state_variable_type_element
        self.retrieve_eqv_plastic_strain = self.state_variable_type_element
        self.retrieve_primary_creep_strain = self.state_variable_type_element
        self.retrieve_oxide_lyr_thickness = self.state_variable_type_element

class Uo2Fuel(CreateMaterials):
    """
    
    This class holds material attributes for uo2 material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(Uo2Fuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_fuel
        self.material_class_name = self.__class__.__name__
        self.material_type = "UO2_FUEL"
        self.property_uo2_density = 11000.0
        self.property_uo2_default_internal_energy = 0.0
        self.property_uo2_default_temperature = 298.0
        self.property_uo2_poisson_ratio = 0.33
        self.property_uo2_initial_fission_density = 0.0
        self.property_uo2_reference_pressure = 500.0E6
        self.property_uo2_dil_temperature_rate = 0.002
        self.property_uo2_dil_temperature_factor = 0.01
        self.property_uo2_dil_creep_exponent = 3.0
        self.property_uo2_fracture_strain_ratio = -0.2
        self.property_uo2_closure_factor = 0.5
        self.property_uo2_open_porosity_ratio = 0.05
        self.property_uo2_manufactured_porosity = 0.015
        self.property_uo2_initial_gadolinium_enrichment = 0.04
        self.property_uo2_initial_PuO2_weight_fraction = 0.0
        self.property_uo2_initial_Oxygen_to_metal_ratio = 2.0
        self.property_uo2_grain_size = 1.0E-5
        self.property_uo2_surface_roughness = 1.0E-5
        self.property_uo2_material_axis_origin_x = 0.0
        self.property_uo2_material_axis_origin_y = 0.0
        self.property_uo2_material_axis_origin_z = 0.0
        self.property_uo2_material_axis_direction_x = 0.0
        self.property_uo2_material_axis_direction_y = 0.0
        self.property_uo2_material_axis_direction_z = 1.0
        self.property_uo2_as_manufactured_gap_width = self.parameters.geometry_gap_thickness
        self.property_uo2_as_manufactured_pellet_diameter = self.parameters.geometry_fuel_diameter
        self.property_uo2_max_densification_strain = 0.01
        self.property_uo2_swelling_multiplier = 1.0
        self.property_uo2_relocation_threshold = 2.0
        self.property_uo2_relocation_strain_multiplier = 1.0
        self.property_uo2_fast_flux_multiplier = 1.0
        self.property_uo2_fgr_parameter = 1.0
        self.property_uo2_creep_strain_multiplier = 1.0
        self.property_uo2_initial_fuel_composition = "DEFAULT_UO2"
        self.property_uo2_fission_gas_release_model = "SIMPLE_FGR"

class PelletCladGapPseudo(CreateMaterials):
    """

    This class holds material attributes for pellet-clad-gap material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(PelletCladGapPseudo, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_fuel_cladding_gap
        self.material_class_name = self.__class__.__name__
        self.material_type = "PELLET_CLAD_GAP_PSEUDO_MAT"
        self.property_gap_density = 0.1
        self.property_gap_default_internal_energy = 0.0
        self.property_gap_default_temperature = 298.0
        self.property_gap_closed_gap_stiffness = 72.0E9
        self.property_gap_gap_stiffness_rate = 1.0
        self.property_gap_friction_coefficient = 0.25
        self.property_gap_gas_heat_capacity = 5.0E3
        self.property_gap_clad_surface_roughness = 1.0E-6
        self.property_gap_fuel_surface_roughness = 1.0E-6
        self.property_gap_clad_oxide_layer_thickness = 0.0
        self.property_gap_gap_condutance_multiplier = 1.0

class GapPseudo(CreateMaterials):
    """
    
    This class holds material attributes for gap material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(GapPseudo, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_fuel_cladding_gap
        self.material_class_name = self.__class__.__name__
        self.material_type = "GAP_PSEUDO_MATERIAL"
        self.property_gap_density = 0.1
        self.property_gap_default_internal_energy = 0.0
        self.property_gap_default_temperature = 298.0
        self.property_gap_closed_gap_stiffness = 72.0E9
        self.property_gap_gap_stiffness_rate = 1.0
        self.property_gap_friction_coefficient = 0.25
        self.property_gap_open_conductance = 1.5E3
        self.property_gap_closed_conductance = 6.0E3
        self.property_gap_gas_heat_capacity = 5.0E3

class Gas(CreateMaterials):
    """
    
    This class holds material attributes for gas material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(Gas, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_gas
        self.material_class_name = self.__class__.__name__
        self.material_type = "GAS"
        self.property_gas_density = 0.1
        self.property_gas_default_internal_energy = 0.0
        self.property_gas_default_temperature = 298.0

class J2ThermoElasticPlastic(CreateMaterials):
    """
    
    This class holds material attributes for J2 material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(J2ThermoElasticPlastic, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_general
        self.material_class_name = self.__class__.__name__
        self.material_type = "J2_THERMO_ELASTIC_PLASTIC"
        self.property_j2_density = 6560.0
        self.property_j2_default_internal_energy = 0.0
        self.property_j2_default_temperature = 298.0
        self.property_j2_elastic_modulus = 20.0E9
        self.property_j2_poisson_ratio = 0.37
        self.property_j2_hardening_exponent = 0.4
        self.property_j2_yield_stress_coeff_0 = 300.0E12
        self.property_j2_yield_stress_coeff_1 = 800.0
        self.property_j2_reference_strain = 1.0
        self.property_j2_heat_capacity = 500.0
        self.property_j2_conductivity = 200.0
        self.property_j2_thermal_expansivity = 1.0E-6

class UcoFuel(CreateMaterials):
    """
    
    This class holds material attributes for uco material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(UcoFuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_general
        self.material_class_name = self.__class__.__name__
        self.material_type = "UCO_FUEL"
        self.property_uco_density = 3200.0
        self.property_uco_default_internal_energy = 0.0
        self.property_uco_default_temperature = 298.0
        self.property_uco_thermal_expansivity = 15.0e-6
        self.property_uco_porosity = 0.05
        self.property_uco_initial_oxygen_to_uranium_ratio = 1.5
        self.property_uco_initial_carbon_to_uranium_ratio = 0.4
        self.property_uco_initial_uo2_enrichment_wt_percent = 0.155
        self.property_uco_reference_pressure = 500.0E6
        self.property_uco_dil_temperature_rate = 0.002
        self.property_uco_dil_temperature_factor = 0.01
        self.property_uco_dil_creep_exponent = 3.0
        self.property_uco_creep_multiplier = 1.0
        self.property_uco_swelling_multiplier = 1.0
        self.property_uco_material_axis_origin_x = 0.0
        self.property_uco_material_axis_origin_y = 0.0
        self.property_uco_material_axis_origin_z = 0.0
        self.property_uco_material_axis_direction_x = 0.0
        self.property_uco_material_axis_direction_y = 0.0
        self.property_uco_material_axis_direction_z = 1.0
        self.property_uco_fission_gas_release_model = "NO_FGR"

class SicNonfuel(CreateMaterials):
    """
    
    This class holds material attributes for sic material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(SicNonfuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_general
        self.material_class_name = self.__class__.__name__
        self.material_type = "SIC_NONFUEL"
        self.property_sic_density = 3200.0
        self.property_sic_default_internal_energy = 0.0
        self.property_sic_default_temperature = 298.0
        self.property_sic_poisson_ratio = 0.13
        self.property_sic_thermal_expansivity = 4.9e-6
        self.property_sic_initial_yield_stress = 400.0e6
        self.property_sic_initial_yield_slope_multiplier = 1.0
        self.property_sic_hard_exp = 0.25
        self.property_sic_fast_flux_multiplier = 1.0
        self.property_sic_creep_rate_model = "DEFAULT"

class PycNonfuel(CreateMaterials):
    """
    
    This class holds material attributes for pyc material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(PycNonfuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_general
        self.material_class_name = self.__class__.__name__
        self.material_type = "PYC_NONFUEL"
        self.property_pyc_density = 1900.0
        self.property_pyc_default_internal_energy = 0.0
        self.property_pyc_default_temperature = 298.0
        self.property_pyc_poisson_ratio = 0.13
        self.property_pyc_thermal_conductivity = 4.0
        self.property_pyc_specific_heat_capacity = 720.0
        self.property_pyc_as_fab_bacon_anisotropy_factor = 1.05 
        self.property_pyc_crystallite_diameter = 30.0
        self.property_pyc_initial_yield_stress = 400.0e6
        self.property_pyc_initial_yield_slope_multiplier = 1.0
        self.property_pyc_hard_exp = 0.25
        self.property_pyc_irradiation_strain_multiplier = 1.0
        self.property_pyc_creep_rate_multiplier = 1.0
        self.property_pyc_fast_flux_value = 0.0
        self.property_pyc_creep_rate_model = "DEFAULT"

class BufferNonfuel(CreateMaterials):
    """
    
    This class holds material attributes for buffer material
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    
    def __init__(self, input_file_path=None, material_name=None):
        super(BufferNonfuel, self).__init__(input_file_path)
        self.object_type = "material"
        if material_name is None:
            self.material_name = self.parameters.material_alias_name_general
        self.material_class_name = self.__class__.__name__
        self.material_type = "BUFFER_NONFUEL"
        self.property_buffer_density = 2250.0
        self.property_buffer_default_internal_energy = 0.0
        self.property_buffer_default_temperature = 298.0
        self.property_buffer_poisson_ratio = 0.13
        self.property_buffer_thermal_conductivity_theoretical = 4.0
        self.property_buffer_thermal_conductivity_initial = 0.5
        self.property_buffer_specific_heat_capacity = 720.0
        self.property_buffer_density_initial = 1000.0
        self.property_buffer_initial_yield_stress = 400.0e6
        self.property_buffer_initial_yield_slope_multiplier = 1.0
        self.property_buffer_hard_exp = 0.25
        self.property_buffer_irradiation_strain_multiplier = 1.0
        self.property_buffer_creep_rate_multiplier = 1.0
        self.property_buffer_fast_flux_value = 0.0
        self.property_buffer_creep_rate_model = "DEFAULT"

class CreateDynamicClass(CreateMaterials):
    """
    
    This class is the manager for materials made dynamically (at run time) from user input.
        
    Supporting Classes Used:
        :class: `CreateMaterials`
    """
    def __init__(self, input_file_path=None, material_name=None):
        super(CreateDynamicClass, self).__init__(input_file_path)
        if input_file_path is not None:
            self.param_file = input_file_path
            self.dict_all_dynamic_materials = self.class_from_yaml()
            self.num_dyn_mats = len(self.dict_all_dynamic_materials)

    def class_from_yaml(self):
        """
        
        A method for creating a material (at run time) from a yaml file.
        
        :return obj_list: a dictionary with key-> material index and value-> list of all attribute values
        """
        with open(self.param_file, 'r') as file:
            params = yaml.safe_load(file)
        
        obj_list = {}
        try:
            dyn_mat_params = params['MATERIAL']['DYNAMIC']['INDEX']
            num_ids = len(dyn_mat_params)
            for i in range(num_ids):
                id = dyn_mat_params[i]['MATERIAL_CLASS_NAME']
                dynamic_params = {key: value for key, value in dyn_mat_params[i].items() if key != 'ID'}
                obj_list[i] = self.DynamicMaterial(self.flatten_params(dynamic_params))
        except KeyError:
            pass
        return obj_list
      
    def flatten_params(self, params, parent_key=''):
        """
        
        A method for reading dynmaic materials from a yaml file.
        
        :return obj_list: a dictionary with key-> material index and value-> list of all attribute values
        """
        items = []
        try:
            for key in params:
                new_key = parent_key + '_' + key if parent_key else key
                value = params[key]
                if isinstance(value, dict):
                    items.extend(self.flatten_params(value, new_key).items())
                elif isinstance(value, list):
                    for i, v in enumerate(value):
                        items.extend(self.flatten_params(v, new_key + f'_{i+1}').items())
                else:
                    items.append((new_key, value))
        except TypeError:
            pass
        return dict(items)
 
    class DynamicMaterial(CreateMaterials):
        """
        
        This class holds material attributes for a single dynamically made material
        
        Supporting Classes Used:
            :class: `CreateMaterials`
        """
        def __init__(self, params):
            super().__init__()
            self.object_type = "material"
            self.set_atts(params)
            
        def set_atts(self, params):
            """
            
            A method for updating class attributes.
            
            :param params: a dictionary with key-> attribute name and value-> attribute value
            """
            for key in params:
                value = params[key]
                setattr(self, key.lower(), value)

class RodMaterialsTool(CreateMaterials):
    """
    
    This class combines all classes in the `materials` module and can be imported into other python files to 
    easily access all material features.

    Supporting Classes Used:
        :class: `Uo2Fuel`,
        :class: `PelletCladGapPseudo`,
        :class: `GapPseudo`,
        :class: `GenericZircNonfuel`,
        :class: `Gas`,
        :class: `J2ThermoElasticPlastic`,
        :class: `UcoFuel`,
        :class: `SicNonfuel`,
        :class: `PycNonfuel`,
        :class: `BufferNonfuel`,
        :class: `CreateDynamicClass`
    """
    def __init__(self, input_file_path=None):
        super(RodMaterialsTool, self).__init__(input_file_path)
        self.input_provided = False
        self.all_mat_dict = {}
        self.used_mat_dict = {}
        self.mat_uo2 = Uo2Fuel(input_file_path)
        self.mat_pellet_clad_gap_pseudo = PelletCladGapPseudo(input_file_path)
        self.mat_gap_pseudo = GapPseudo(input_file_path)
        self.mat_gen_zirc = GenericZircNonfuel(input_file_path)
        self.mat_gas = Gas(input_file_path)
        self.mat_j2 = J2ThermoElasticPlastic(input_file_path)
        self.mat_uco = UcoFuel(input_file_path)
        self.mat_sic = SicNonfuel(input_file_path)
        self.mat_pyc = PycNonfuel(input_file_path)
        self.mat_buffer = BufferNonfuel(input_file_path)
        if input_file_path is not None:
            self.input_provided = True
            self.mat_dynamic_manager = CreateDynamicClass(input_file_path)
        
    def create_all_mat_dictionary(self):
        """
        
        This function creates a dictionary for all the possible materials materials.
        
        :return all_mat_dict: key-> material_class_name, value-> entire material object
        """
        all_mat_dict = {}
        all_mat_dict[self.mat_uo2.material_class_name] = self.mat_uo2
        all_mat_dict[self.mat_pellet_clad_gap_pseudo.material_class_name] = self.mat_pellet_clad_gap_pseudo
        all_mat_dict[self.mat_gap_pseudo.material_class_name] = self.mat_gap_pseudo
        all_mat_dict[self.mat_gen_zirc.material_class_name] = self.mat_gen_zirc
        all_mat_dict[self.mat_gas.material_class_name] = self.mat_gas
        all_mat_dict[self.mat_j2.material_class_name] = self.mat_j2
        all_mat_dict[self.mat_uco.material_class_name] = self.mat_uco
        all_mat_dict[self.mat_sic.material_class_name] = self.mat_sic
        all_mat_dict[self.mat_pyc.material_class_name] = self.mat_pyc
        all_mat_dict[self.mat_buffer.material_class_name] = self.mat_buffer
        if self.input_provided:
            for mats in range(self.mat_dynamic_manager.num_dyn_mats):
                all_mat_dict[self.mat_dynamic_manager.dict_all_dynamic_materials[mats].material_class_name] = self.mat_dynamic_manager.dict_all_dynamic_materials[mats]
        return all_mat_dict
    
    def determine_create_material(self, material_class_name, material_alias, material_index=None):
        """
        
        This function creates the specified material which will have the given alias name and 
        create all the real variables needed for the material.

        :param material_class_name: name of actual material class ex. (`GenericZircNonfuel`)
        :param material_alias: alias name of material ex. (`MATERIAL_CLADDING`)
        :param material_index: index of material if using layers in cladding or triso ex. (`1`)
        :return 1: mat_string: psf formatted string of created material, 2: reals_string: psf formatted list of real variables for material
        """
        if material_index is None:
            material_index = ""
        self.all_mat_dict = self.create_all_mat_dictionary()
        self.all_mat_dict.get(material_class_name).material_name = material_alias
        mat_string = self.all_mat_dict.get(material_class_name).create_material(material_index)
        reals_string = self.all_mat_dict.get(material_class_name).create_reals(material_index)
        return mat_string, reals_string