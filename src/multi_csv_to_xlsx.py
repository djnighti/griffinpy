import glob
import os
import sys
import pandas as pd
from pathlib import Path
from progress_bar import ProgressBar
import time
# sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

'''
Intended Usage: This data processing tool is intended to convert a collection of csv files into a single XLSX workbook.
The names of each csv file will become the names of each sheet in the workbook.

User Defined Inputs: 
1. CSV files location (provide absolute path)
2. Name of XLSX workbook to be created

Program Defined input
3. Location to put the XLSX workbook - will place new XLSX workbook in the same location as where the csv files are.

Outputs:
1. Creates a data file in .xlsx format

How to Run:

From the terminal enter the following:
python3 /home/YOUR_USER_NAME/Pegasus/tools/multi_csv_to_xlsx.py

Then 2 questions will be prompted (the 3 user defined inputs described above) 
EX. 
Enter where csv files are located (paste the absolute path here): /home/dnightingale/pegasus_demos/PCI/MPS_with_crack/runs/08_03_21/tabular_data_1
Enter name of output xlsx file: rod_data

If the data was found, the terminal will print (#: number of csv files found):
"Found # csv files. Creating Movie, please wait..." and display a progress bar underneath it.

If the provided path does not exist, the terminal will print:
"Could not find directory. Check that the provided path is correct."

If the provided path does not contain any csv files, the terminal will print:
"Directory does not contain any csv files. Check that the provided path is correct."

NOTE: The amount of time for a movie to be created depends on how much data there is to be processed!

After a few moments the XLSX workbook will be available in the location specified above.
'''


class MultiCsvToXlsx:
    def __init__(self, external_call=False):
        self.file_location = None
        self.name_of_xlsx_file = None
        self.xlsx_writer = None
        self.progress_status = ProgressBar()
        self.bool_found_files = True
        self.call_type = external_call
        self.determine_call_type()


    def determine_call_type(self):
        if not self.call_type:
            self.file_location = input("Enter where csv files are located (paste the absolute path here): ")
            self.name_of_xlsx_file = str(input("Enter name of output xlsx file: ")+'.xlsx')


    def single_csv_to_xlsx_converter(self):
        self.xlsx_writer = pd.ExcelWriter(
            self.file_location + '/' + self.name_of_xlsx_file,
            engine='xlsxwriter',
            options={'strings_to_numbers': True})
        df = pd.read_csv(self.name_and_path_of_csv_file, header=None)
        df.to_excel(
            self.xlsx_writer,
            sheet_name=str(self.name_of_csv_file),
            index=False,
            header=False)
        self.xlsx_writer.save()
        print("\nXLSX workbook created!")


    def multi_csv_to_xlsx_converter(self):
        if self.file_location is not None:
            self.xlsx_writer = pd.ExcelWriter(
                self.file_location + '/' + self.name_of_xlsx_file,
                engine='xlsxwriter',
                options={'strings_to_numbers': True})
        else:
            self.xlsx_writer = pd.ExcelWriter(
                self.name_of_xlsx_file,
                engine='xlsxwriter',
                options={'strings_to_numbers': True})
        try:
            print(
                f"self.file_location: {self.file_location}\n"
                f"self.name_of_xlsx_file: {self.name_of_xlsx_file}\n"
            )
            Path(self.file_location).resolve(strict=True)
            csv_file_names = [os.path.basename(x) for x in glob.glob(self.file_location + '/*.csv')]
            sheet_name_list = []
            if len(csv_file_names) != 0:
                print(f"Found {len(csv_file_names)} csv files, with names: {csv_file_names}")
                print(f"Creating new XLSX workbook...")
                for a_csv_file in csv_file_names:
                    a_csv_file = a_csv_file.replace("_SPACIAL", "")
                    sheet_name_list.append(a_csv_file.split(".csv", 1)[0])
                csv_files_location = glob.glob(self.file_location + '/*.csv')
                number_of_files = len(csv_files_location)
                for csv_file in range(number_of_files):
                    df = pd.read_csv(csv_files_location[csv_file], header=None)
                    df.to_excel(
                        self.xlsx_writer,
                        sheet_name=str(sheet_name_list[csv_file]),
                        index=False,
                        header=False)
                    self.progress_status.print_progress_bar(
                        csv_file + 1,
                        number_of_files,
                        prefix='Progress:',
                        suffix='Complete',
                        length=50)
                    time.sleep(0.1)
                self.xlsx_writer.save()
                print("\nXLSX workbook created!")
            else:
                print("Directory does not contain any csv files. Check that the provided path is correct.")

        except FileNotFoundError:
            print("Could not find directory. Check that the provided path is correct.")


if __name__ == '__main__':
    csv_to_xlsx_converter = MultiCsvToXlsx()
    csv_to_xlsx_converter.multi_csv_to_xlsx_converter()
