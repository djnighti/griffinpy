import math
from parameters import Parameters
from control.matlab import *
import numpy as np
import matplotlib.pyplot as plt
import copy


class MeshUtilities:
    """
    
    This class is the book-keeper of any quad patch or hex mesh created by the user which
    is useful for automating the snapping, deleting and transforming processes. It also contains the core transformation PSF commands.

    """

    def __init__(self, input_file_path=None):
        # super(MeshUtilities, self).__init__(input_file_path)
        self.parameters = Parameters(input_file_path)
        self.object_type_quad_patch = "QP"
        self.object_type_hex = "HEX"
        self.object_type_region = "REGION"
        self.object_type_sel_vol = "SELVOL"
        self.creation_dict_quad_patch = {}
        self.creation_dict_hex_mesh = {}
        self.creation_dict_region = {}
        self.creation_dict_sel_vol = {}
        self.creation_dict_bc_set = {}
        self.creation_dict_mat_set = {}
        self.creation_count_quad_patch = 0
        self.creation_count_hex_mesh = 0
        self.creation_count_region = 0
        self.creation_count_sel_vol = 0

    def delete_object_string(self, object_name):
        """

        :param object_name: name of object to delete
        :return psf_string: psf formatted string to delete
        """
        psf_string = (
            f"DELETE {{{object_name.upper()}}} \n"
            f"\n")
        return psf_string

    def delete_object(self, object_name):
        """

        this function uses the delete string function but then also removes the object from the
        griffinpy inventory of created objects
        
        Supporting Methods Used:
            :method: `delete_object_string`,
            :method: `determine_object_type`
        :param object_name: name of object to delete
        :return psf_string: psf formatted string to delete
        """

        psf_string = self.delete_object_string(object_name)
        object_type = self.determine_object_type(object_name)
        self.remove_object(object_name, object_type)
        return psf_string

    def delete_list_of_objects(self, object_list, excluded_object=None):
        """
        
        Same purpose as `delete_object` but now for entire list and the option to exclude an object from that list.
        
        Supporting Methods Used:
            :method: `delete_object_string`,
            :method: `determine_object_type`,
            :method: `remove_object`
        :param object_list: of objects to delete
        :param excluded_object: objects to exclude from being deleted
        :return psf_string: psf formatted string to delete a list of objects
        """
        object_list_copy = object_list.copy()
        if type(object_list) == dict:
            if excluded_object is not None:
                if type(excluded_object) == list:
                    for obj in excluded_object:
                        if obj in object_list_copy:
                            object_list_copy.pop(obj)
                else:
                    object_list_copy.pop(excluded_object)
            object_list_copy = list(object_list_copy.keys())
        object_name = ', '.join(object_list_copy)
        object_type = self.determine_object_type(object_list_copy[0])
        for qp in object_list_copy:
            self.remove_object(qp, object_type)
        psf_string = self.delete_object_string(object_name)
        return psf_string

    def rotate(self, object_name, degrees_rotated, axis_of_rotation):
        """
        
        :param object_name: name of object to rotate
        :param degrees_rotated: degree to rotate object
        :param axis_of_rotation: axis to rotate object about
        :return psf_string: psf formatted string that rotates a object
        """
        if type(axis_of_rotation) == list and len(axis_of_rotation) == 3:
            rotation_vector = axis_of_rotation
        elif type(axis_of_rotation) == str:
            if axis_of_rotation.upper() == "X":
                rotation_vector = [1.0, 0.0, 0.0]
            elif axis_of_rotation.upper() == "Y":
                rotation_vector = [0.0, 1.0, 0.0]
            elif axis_of_rotation.upper() == "Z":
                rotation_vector = [0.0, 0.0, 1.0]
        psf_string = (
            f"ROTATE {object_name.upper()} [0.0,0.0,0.0] {rotation_vector} {float(degrees_rotated)}\n"
            f"\n")
        return psf_string

    def mirror(self, object_name, mirrored_axis):
        """
        
        :param object_name: name of object to mirror
        :param mirrored_axis: axis to mirror quad patch about
        :return psf_string: psf formatted string that mirrors an object
        """
        if type(mirrored_axis) == list and len(mirrored_axis) == 3:
            mirrored_vector = mirrored_axis
        elif type(mirrored_axis) == str:
            if mirrored_axis.upper() == "X":
                mirrored_vector = [1.0, 0.0, 0.0]
            elif mirrored_axis.upper() == "Y":
                mirrored_vector = [0.0, 1.0, 0.0]
            elif mirrored_axis.upper() == "Z":
                mirrored_vector = [0.0, 0.0, 1.0]
        psf_string = (
            f"MIRROR {object_name.upper()} [0.0,0.0,0.0] {mirrored_vector} \n"
            f"\n")
        return psf_string

    def translate(self, object_name, direction, magnitude_of_translation):
        """
        
        :param object_name: name of object
        :param direction: direction to translate quad patch
        :param magnitude_of_translation: distance to translate quad patch
        :return: psf formatted string that translates an object
        """
        if type(direction) == list and len(direction) == 3:
            translate_vector = []
            for element in direction:
                translate_vector.append(magnitude_of_translation * element)
        elif type(direction) == str:
            if direction.upper() == "X":
                translate_vector = [float(magnitude_of_translation), 0.0, 0.0]
            elif direction.upper() == "Y":
                translate_vector = [0.0, float(magnitude_of_translation), 0.0]
            elif direction.upper() == "Z":
                translate_vector = [0.0, 0.0, float(magnitude_of_translation)]
            elif direction.upper() == "XYZ":
                translate_vector = [
                    round(float(magnitude_of_translation[0]), self.parameters.tolerance_rounding),
                    round(float(magnitude_of_translation[1]), self.parameters.tolerance_rounding),
                    round(float(magnitude_of_translation[2]), self.parameters.tolerance_rounding)
                ]
        psf_string = (
            f"TRANSLATE \\ \n"
            f"  {object_name.upper()} \\ # Name of object to translate \n"
            f"  {translate_vector} # Vector representing translation\n"
            f"\n")
        return psf_string

    def mark(self, object_name, mark_name, boundary_type, complete_or_partial, inside_or_outside, selection_volume_name):
        """

        :param object_name: Name of object to mark
        :param mark_name: Name of marked surface
        :param boundary_type: options:(`"BOUNDARY_ONLY"`, `"SURFACE_ONLY"`, `"BOUNDARY_AND_SURFACE"`)
        :param complete_or_partial: options:(`"COMPLETELY"`, `"PARTIALLY"`)
        :param inside_or_outside: options:(`"INSIDE"`, `"OUTSIDE"`)
        :param selection_volume_name: Name of selection volume to use for marking
        :return psf_string: psf formatted string that marks a boundary type
        """
        psf_string = (
            f"MARK \\ \n"
            f"  {object_name.upper()} \\ # Name of object to mark \n"
            f"  {mark_name.upper()} \\ # Name of marked surface\n"
            f"  {boundary_type.upper()} \\ # options:BOUNDARY_ONLY, SURFACE_ONLY, BOUNDARY_AND_SURFACE \n"
            f"  {complete_or_partial.upper()} \\ # options:COMPLETELY, PARTIALLY \n"
            f"  {inside_or_outside.upper()} \\ # options:INSIDE, OUTSIDE \n"
            f"  {selection_volume_name.upper()} # Name of selection volume to use for marking \n"
            f" \n"
            f"\n")
        return psf_string

    def copy(self, original_object_name, clone_object_name):
        """

        this function calls the copy string function and also adds new object to griffinpy inventory of objects
        
        Supporting Methods Used:
            :method: `determine_object_type`,
            :method: `add_object`
        :param original_object_name: name of object to be copied
        :param clone_object_name: name of object to be created
        :return psf_string: psf formatted string that copies an object
        """
        object_type = self.determine_object_type(original_object_name)
        psf_string = (
            f"COPY {original_object_name.upper()} {clone_object_name.upper()} \n"
            f"\n")
        self.add_object(clone_object_name.upper(), object_type)
        return psf_string

    def snap_string(self, parent_object_name_1, parent_object_name_2, child_object_name):
        """

        :param parent_object_name_1: name of first parent object
        :param parent_object_name_2: name of second parent object
        :param child_object_name: name of resultant object
        :return psf_string: psf formatted string that snaps two objects together
        """
        psf_string = (
            f"{child_object_name.upper()} = {parent_object_name_1.upper()} SNAP {parent_object_name_2.upper()} \n"
            f"\n")
        return psf_string

    def snap_2_objects(self, parent_object_name_1, parent_object_name_2, child_object_name):
        """

        This function calls the `snap_string` function and adds new object to the griffinpy inventory of objects
        
        Supporting Methods Used:
            :method: `determine_object_type`,
            :method: `add_object`,
            :method: `snap_string`
        :param parent_object_name_1: name of first parent object to snap
        :param parent_object_name_2: name of second parent object to snap
        :param child_object_name: name of resultant object
        :return psf_string: psf formatted string that snaps 2 quad objects together
        """
        object_type = self.determine_object_type(parent_object_name_1)
        if parent_object_name_1 == child_object_name or parent_object_name_2 == child_object_name:
            # print("already in list")
            pass
        else:
            self.add_object(child_object_name.upper(), object_type)
        psf_string = self.snap_string(parent_object_name_1, parent_object_name_2, child_object_name)
        return psf_string

    def snap_list_objects(self, object_list, child_object_name,  exclude_object_name=None):
        """

        This function is a list version of snap_2_objects
        
        Supporting Methods Used:
            :method: `snap_2_objects`
        :param object_list: list of all parent object names to snap
        :param child_object_name: name of resultant object
        :return psf_string_list: psf formatted list of strings that snaps multiple objects together
        """
        psf_string_list = []

        # determine if snapping items from dict or list
        
        if type(object_list) == list:
            object_list_copy = object_list.copy()
        elif type(object_list) == dict:
            object_list_copy = object_list.copy()
            if exclude_object_name is not None:
                
                # check if exclude object is a list of things to exclude
                if type(exclude_object_name)==list:
                    
                    # loop over each object in list
                    for obj in exclude_object_name:
                        
                        # making sure requested excluded object has been created
                        if obj in object_list_copy:
                            object_list_copy.pop(obj)
                
                # else remove the single requested object if it exists
                else:
                    if exclude_object_name in object_list_copy:
                        object_list_copy.pop(exclude_object_name)
            object_list_copy = list(object_list.copy().keys())

        # snap first 2 copies to create first known child
        psf_string_list.append(self.snap_2_objects(
            object_list_copy[0],
            object_list_copy[1],
            child_object_name)
        )
        object_list_copy.pop(0)
        object_list_copy.pop(0)

        # iterate over remaining list
        for parent in object_list_copy:
            psf_string_list.append(self.snap_2_objects(child_object_name, parent, child_object_name))
        return psf_string_list

    def snap_and_delete_list_objects(self, object_list, child_object_name):
        """

        This function combines snap_list_objects and delete_list_objects
        
        Supporting Methods Used:
            :method: `snap_2_objects`,
            :method: `delete_object`,
            :method: `delete_list_of_objects`
        :param object_list: list of all parent object names to snap
        :param child_object_name: name of resultant object
        :return psf_string_list: psf formatted list of strings that snaps multiple objects together
        """
        psf_string_list = []

        # determine if snapping items from dict or list
        if type(object_list) == list:
            object_list_copy = object_list.copy()
        elif type(object_list) == dict:
            object_list_copy = list(object_list.copy().keys())

        try:
            # snap first 2 copies to create first known child
            psf_string_list.append(self.snap_2_objects(
                object_list_copy[0],
                object_list_copy[1],
                child_object_name)
            )
            psf_string_list.append(self.delete_list_of_objects(
                [object_list_copy[0], object_list_copy[1]])
            )

            object_list_copy.pop(0)
            object_list_copy.pop(0)
        except IndexError:
            pass

        # iterate over remaining list
        for parent in object_list_copy:
            # print(f"sd: child_object_name: {child_object_name}, parent: {parent}")
            psf_string_list.append(self.snap_2_objects(
                child_object_name,
                parent,
                child_object_name))
            psf_string_list.append(self.delete_object(parent))
        return psf_string_list

    def add_object(self, object_name, object_type, region_name=None):
        """

        this function adds an object to the griffinpy object inventory and organizes by type.

        :param object_name: name of object to add
        :param object_type: type of object options:(`"QP"`, `"HEX"`, `"REGION"`, `"SELVOL"`)
        :param region_name: region to assign to object
        """
        try:
            # quad patches
            if object_type.upper() == self.object_type_quad_patch:
                self.creation_count_quad_patch += 1
                self.creation_dict_quad_patch[object_name.upper()] = self.creation_count_quad_patch

            # hex mesh
            elif object_type.upper() == self.object_type_hex:
                self.creation_count_hex_mesh += 1
                self.creation_dict_hex_mesh[object_name.upper()] = self.creation_count_hex_mesh

            # regions
            elif object_type.upper() == self.object_type_region:
                self.creation_count_region += 1
                object_name = object_name.lower().split('_', 1)[0]
                self.creation_dict_region[self.creation_count_region] = region_name.upper()
                result = {}
                count = 1
                for key, value in self.creation_dict_region.items():
                    if value not in result.values():
                        result[count] = value
                        count += 1
                self.creation_dict_region = result
                self.parameters.region_dict = self.creation_dict_region

            # selection volumes
            elif object_type.upper() == self.object_type_sel_vol:
                self.creation_count_sel_vol += 1
                self.creation_dict_sel_vol[object_name.upper()] = self.creation_count_sel_vol

        except AttributeError:
            pass

    def remove_object(self, object_name, object_type):
        """

        this function removes object from the griffinpy object inventory.

        :param object_name: name of object to remove
        :param object_type: type of object options:(`"QP"`, `"HEX"`, `"REGION"`, `"SELVOL"`)
        """
        try:
            # quad patches
            if object_type.upper() == self.object_type_quad_patch:
                self.creation_dict_quad_patch.pop(str(object_name.upper()))

            # hex mesh
            elif object_type.upper() == self.object_type_hex:
                self.creation_dict_hex_mesh.pop(str(object_name.upper()))

            # selection volumes
            elif object_type.upper() == self.object_type_sel_vol:
                self.creation_dict_sel_vol.pop(str(object_name.upper()))
        except AttributeError:
            pass

    def determine_object_type(self, object_name):
        """
        
         determines object type via prefix naming convention on string

        :param object_name: name of object
        :return: type of object options:(`"QP"`, `"HEX"`, `"REGION"`, `"SELVOL"`)
        """
        if object_name in self.creation_dict_quad_patch:
            object_type = self.object_type_quad_patch
            return object_type
        elif object_name in self.creation_dict_hex_mesh:
            object_type = self.object_type_hex
            return object_type
        elif object_name in self.creation_dict_sel_vol:
            object_type = self.object_type_sel_vol
            return object_type
        else:
            pass
            print(
                f"Error (From function {self.determine_object_type.__name__} in {MeshUtilities.__name__} class): {object_name} has not been created yet.")

    def auto_stack(self, object_name, quantity, translation_direction, translation_magnitude):
        """

        this function is a simple mesh generator by taking a specified object and doing a series of
        copying, translations, and snaps to form a mesh. example usage is taking a mesh for single discrete
        pellet and then forming a rod with n number of pellets.
        
        Supporting Methods Used:
            :method: `translate`,
            :method: `snap_2_objects`,
            :method: `delete_object`,
        :param object_name: name of object to duplicate and perform stack
        :param quantity: number of copies
        :param translation_direction: direction of stacking
        :param translation_magnitude: amount to translate a single copy in translated direction
        :return psf_string_list: psf formatted list of strings for the auto stacking process
        """
        auto_string_list = []
        
        # Copy object to stack
        auto_string_list.append(self.copy(
            original_object_name=object_name, 
            clone_object_name=self.parameters.temporary_object_auto_stack_name))
        
        # Begin auto-stacking loop
        for a_copy in range(0, quantity):
            auto_string_list.append(self.translate(
                object_name=self.parameters.temporary_object_auto_stack_name,
                direction=translation_direction,
                magnitude_of_translation=translation_magnitude))
            auto_string_list.append(self.snap_2_objects(
                parent_object_name_1=object_name,
                parent_object_name_2=self.parameters.temporary_object_auto_stack_name,
                child_object_name=object_name))
            
        # delete copy
        auto_string_list.append(self.delete_object(
            object_name=self.parameters.temporary_object_auto_stack_name))
        return auto_string_list
    
    def auto_translate_assemblies(self, object_dictionary, position_dictionary=None, result_object_name=None):
        """

        this function similar to `auto_stack` will auto assemble a set of objects by comparing the object_dictionary
        with the position_dictionary to then translate assemblies to correct locations.
        
        Supporting Methods Used:
            :method: `translate`,
            :method: `snap_list_objects`,
            :method: `delete_object`,
        :param object_dictionary: keys- values-
        :param position_dictionary: keys- values-
        :param result_object_name: result object name
        :return psf_string_list: psf formatted list of strings for the auto stacking process
        """
        
        # set defaults
        if position_dictionary is None:
            position_dictionary = self.parameters.geometry_mesh_positions_dict
        if result_object_name is None:
            result_object_name = self.parameters.hex_rod_name
            
        psf_string_list = []
            
        # auto translate all assemblies to correct position
        for key in object_dictionary:
            key_suffix = key.split('_',1)[1]
            if key_suffix in position_dictionary:
                psf_string_list.extend(self.translate(
                    object_name=key,
                    direction="z",
                    magnitude_of_translation=position_dictionary[key_suffix]))

        # snap all assemblies
        psf_string_list.extend(self.snap_list_objects(
                object_list=object_dictionary,
                child_object_name=result_object_name))
        
        # Delete everything excluding the final resulting object
        psf_string_list.append(self.delete_list_of_objects(
            object_dictionary,
            result_object_name))
        return psf_string_list


    def auto_revolve(self, object_name, quantity, rotation_axis, rotation_degrees):
        """

        this function is a simple mesh generator by taking a specified object and doing a series of
        copying, rotations, and snaps to form a mesh. example usage is taking a mesh for a single sector
        of a circular polyhedron and then forming a larger sector (spherical shell).
        
        Supporting Methods Used:
            :method: `copy`,
            :method: `rotate`,
            :method: `snap_2_objects`,
            :method: `delete_object`
        :param object_name: name of object to duplicate and perform revolve
        :param quantity: number of copies
        :param rotation_axis: axis to perform rotation about
        :param rotation_degrees: amount to rotate a single copy
        :return psf_string_list: psf formatted string for the auto revolving process
        """
        psf_string_list = []
        
        # Copy object to revolve
        psf_string_list.append(self.copy(
            original_object_name=object_name,
            clone_object_name=self.parameters.temporary_object_auto_revolve_name))
        
        # Begin auto-revolve loop
        for a_copy in range(0, quantity):
            psf_string_list.append(self.rotate(
                object_name=self.parameters.temporary_object_auto_revolve_name,
                degrees_rotated=rotation_degrees,
                axis_of_rotation=rotation_axis))
            psf_string_list.append(self.snap_2_objects(
                parent_object_name_1=object_name,
                parent_object_name_2=self.parameters.temporary_object_auto_revolve_name,
                child_object_name=object_name))
        
        # delete copy
        psf_string_list.append(self.delete_object(
            object_name=self.parameters.temporary_object_auto_revolve_name))
        return psf_string_list

    def auto_triso_grid_gen(self, object_names, quantity, translation_direction, translation_magnitude):
        """

        this function is a simple mesh generator similar to auto_stack but now keeps track of the index of the
        created object and returns a list of all the copies made. Its intended specifically for creating a grid
        of voids and particles for triso fuel.
        
        Supporting Methods Used:
            :method: `copy`,
            :method: `translate`,
            :method: `snap_and_delete_list_objects`
        :param object_names: name of object to duplicate and perform translation
        :param quantity: number of copies to make
        :param translation_direction: direction vector to perform translation
        :param translation_magnitude: amount to translate a single copy in translated direction
        :return psf_string_list: psf formatted string for the auto grid generation process and object list of the copies made
        """
        psf_string_list = []
        object_list = []
        final_object_name = object_names[2]
        temp_obj_list = []
        
        for a_copy in range(0, quantity):
            translation_vec = translation_magnitude[a_copy][0:3]
            is_particle = translation_magnitude[a_copy][3]
            
            if is_particle:
                temp_obj = self.parameters.temporary_object_triso_name + '_' + str(int(a_copy))
                object_name = object_names[0]
            else:
                temp_obj = self.parameters.temporary_object_void_name + '_' + str(int(a_copy))
                object_name = object_names[1]

            norm_of_translation = np.linalg.norm(translation_vec)
            
            # create copy of void or particle
            psf_string_list.append(self.copy(
                original_object_name=object_name,
                clone_object_name=temp_obj))
            
            # begin auto-triso grid gen
            # edge case: if void/particle doesnt move from current location dont translate else, translate it.
            if norm_of_translation != 0.0:
                psf_string_list.append(self.translate(
                    object_name=temp_obj,
                    direction=translation_direction,
                    magnitude_of_translation=translation_vec))
            
            # print(f"creating pause: {self.parameters.model_flag_triso_plot_build}")
            if self.parameters.model_flag_triso_plot_build:
                # print(f"creating pause true: {self.parameters.model_flag_triso_plot_build}")
                psf_string_list.extend(self.parameters.create_pause())

            # add object to lists
            object_list.append(temp_obj)
            temp_obj_list.append(temp_obj)

            # begin auto-snap and delete
            if a_copy >= 1:
                psf_string_list.extend(self.snap_and_delete_list_objects(
                    object_list=temp_obj_list,
                    child_object_name=final_object_name))
                temp_obj_list = []
        return psf_string_list, object_list

    def create_phys_mesh(self, mesh_list):
        """

        :param mesh_list: list of objects to convert to a physical mesh
        :return psf_string: psf formatted string for resulting physical mesh(es)
        """
        psf_string = (
            f"CREATE PHYSICAL_MESH {self.parameters.physical_mesh_name} {mesh_list} \n"
            f"ASSIGN DEFORMABLE_QUASISTATIC_FINITE_DEFORMATION TRANSIENT_THERMAL TO {self.parameters.physical_mesh_name} \n"
            f"\n"
        )
        return psf_string

    def print_dict(self, dict_to_print):
        """

        :param dict_to_print: print a dictionary contents
        """
        for key in dict_to_print:
            print(f"{key}, {dict_to_print[key]}")

class SelectionVolume(MeshUtilities):
    """

    This class inherits the class `MeshUtilities` for creating selection volumes for creating element and node sets
    
    Class inherits:
        :class: `MeshUtilities`
    """

    def __init__(self, input_file_path=None):
        super(SelectionVolume, self).__init__(input_file_path)

    def create_cube_sel_vol(self, selection_volume_name, starting_point, extrude_vector):
        """
        
        Create a cube-like selection volume.
        
        Supporting Methods Used:
            :method: `add_object`
        :param selection_volume_name: name of result cube
        :param starting_point: starting coordinate of cube [x0,y0,z0]
        :param extrude_vector: extrusion vector in each direction [x_len, y_len, z_len]
        :return psf_string: psf formatted string creating cube selection volume
        """
        x_start = starting_point[0]
        y_start = starting_point[1]
        z_start = starting_point[2]
        x_len = extrude_vector[0]
        y_len = extrude_vector[1]
        z_len = extrude_vector[2]
        sign_x = float(1)
        sign_y = float(1)
        sign_z = float(1)
        if x_len < 0:
            sign_x=-1*sign_x
        if y_len < 0:
            sign_y=-1*sign_y
        if z_len < 0:
            sign_z=-1*sign_z

        psf_string = (
            f"CREATE CUBE \\ \n"
            f"  {selection_volume_name.upper()} \\ \n"
            f"  [{float(x_start)},{float(y_start)},{float(z_start)}] \\  \n"
            f"  [{sign_x},0.0,0.0] \\  \n"
            f"  [0.0,{sign_y},0.0] \\  \n"
            f"  [0.0,0.0,{sign_z}] \\  \n"
            f"  {float(abs(x_len))} \\ \n"
            f"  {float(abs(y_len))} \\ \n"
            f"  {float(abs(z_len))} \\ \n"
            f"  1 \\ \n"
            f"  1 \\ \n"
            f"  1 \\ \n"
            f"  {self.parameters.geometry_tolerance} \n"
            f"\n")
        self.add_object(selection_volume_name, self.object_type_sel_vol)
        return psf_string

    def create_torus_sel_vol(self, selection_volume_name, radius_major, radius_minor, circumferential_divisions_major, circumferential_divisions_minor):
        """
        
        Create a torus (donut) selection volume.
        
        Supporting Methods Used:
            :method: `add_object`
        :param selection_volume_name: name of resulting selection volume
        :param radius_major: major radius of torus
        :param radius_minor: minor  radius of torus
        :param circumferential_divisions_major: number of circumferential divisions along major axis
        :param circumferential_divisions_minor: number of circumferential divisions along minor axis
        :return psf_string: psf formatted string creating torus selection volume
        """
        psf_string = (
            "CREATE TORUS \\ \n"
            f"  {selection_volume_name.upper()} \\ # name of selection volume \n"
            f"  [0.0, 0.0, 0.0] \\ # starting point \n"
            f"  [0.0, 0.0, 1.0] \\ # direction vector\n"
            f"  {radius_major} \\ # radius_major \n"
            f"  {radius_minor} \\ # radius_minor \n"
            f"  {circumferential_divisions_major} \\ # circumferential divisions major\n"
            f"  {circumferential_divisions_minor} \\ # circumferential divisions minor\n"
            f"  {self.parameters.geometry_selection_volume_threshold} # geometry tolerance \n"
            f" \n")
        self.add_object(selection_volume_name, self.object_type_sel_vol)
        return psf_string

    def create_sphere_sel_vol(self, selection_volume_name, radius, circumferntial_divisions):
        """
        
        Create a spherical selection volume.
        
        Supporting Methods Used:
            :method: `add_object`
        :param selection_volume_name: name of resulting selection volume
        :param radius: radius of sphere
        :param circumferntial_divisions: number of circumferential divisions
        :return psf_string: psf formatted string creating sphere selection volume
        """
        max_facet_length = 1.0
        max_facet_width = 1.0
        max_facet_angle = 360.0 / circumferntial_divisions
        psf_string = (
            "CREATE SPHERE \\ \n"
            f"  {selection_volume_name.upper()} \\ # name of selection volume \n"
            f"  [0.0, 0.0, 0.0] \\ # starting point \n"
            f"  [0.0, 0.0, 1.0] \\ # direction vector\n"
            f"  {radius} \\ # radius \n"
            f"  {max_facet_length} \\ # max_facet_length (phi divisions)\n"
            f"  {max_facet_width} \\ # max_facet_width (theta divisions)\n"
            f"  {max_facet_angle} \\ \n"
            f"  {self.parameters.geometry_selection_volume_threshold} # geometry tolerance \n"
            f" \n")
        self.add_object(selection_volume_name, self.object_type_sel_vol)
        return psf_string

    def create_cylinder_sel_vol(self, selection_volume_name, radius, height, circumferential_divisions, origin=None):
        """
        
        Create a cylindrical selection volume.
        
        Supporting Methods Used:
            :method: `add_object`
        :param selection_volume_name: name of resulting selection volume
        :param radius: radius of sphere
        :param circumferntial_divisions: number of circumferential divisions
        :return psf_string: psf formatted string creating sphere selection volume
        """
        if origin is None:
            x0=0.0
            y0=0.0
            z0=0.0
        else:
            x0=origin[0]
            y0=origin[1]
            z0=origin[2]

        max_facet_length = 1.0
        max_facet_width = 1.0
        max_facet_angle = 360.0 / circumferential_divisions
        psf_string = (
            "CREATE CYLINDER \\ \n"
            f"  {selection_volume_name.upper()} \\ # name of selection volume \n"
            f"  [{x0}, {y0}, {z0}] \\ # starting point \n"
            f"  [0.0, 0.0, 1.0] \\ # direction vector\n"
            f"  {radius} \\ # radius \n"
            f"  {height} \\ # radius \n"
            f"  {max_facet_length} \\ # max_facet_length (phi divisions)\n"
            f"  {max_facet_width} \\ # max_facet_width (theta divisions)\n"
            f"  {max_facet_angle} \\ \n"
            f"  {self.parameters.geometry_tolerance} # geometry tolerance \n"
            f" \n")
        self.add_object(selection_volume_name, self.object_type_sel_vol)
        return psf_string

    def create_linear_radial_polyline(self, poly_name, start_point, end_point, num_points):
        """

        Creates polyline in the x-direction
        
        Supporting Methods Used:
            :method: `create_real`
        :param poly_name: name of resulting polyline
        :param start_point: initial point on polyline
        :param end_point: final point on polyline
        :param num_points: total number of points in polyline
        :return psf_string_list: psf formatted list of strings creating a polyline of evenly spaced points
        """
        point_1_x = start_point[0]
        point_1_y = start_point[1]
        point_1_z = start_point[2]
        point_n_x = end_point[0]
        x_vec = linspace(point_1_x, point_n_x, num_points)
        create_points_string = []
        point_n_vec_string = []
        psf_string_list = []

        # Begining part of polyline creation
        polyline_string_pre = (
            f"CREATE POLYLINE \\ \n"
            f"  {poly_name.upper()} \\ \n"
            f"  [{point_1_x},{point_1_y},{point_1_z}] \\ \n"
            f"  [ \\ \n")

        # create variable points and use them in polyline
        for point in range(0, len(x_vec)):
            point_n_str = "point_" + str(point + 1) + "_x"
            create_point, real_point_str = self.parameters.create_real(point_n_str, x_vec[point])
            point_str = (f"  [{real_point_str},{point_1_y},{point_1_z}],\\ \n")
            point_n_vec_string.append(point_str)
            create_points_string.append(create_point)
        create_points_string.append("\n")

        # Ending part of polyline creation
        polyline_string_post = (
            f"  ] \\ \n"
            f"  {self.parameters.geometry_tolerance} \\ \n"
            f"  AS_IS ABSOLUTE \n")
        
        psf_string_list.extend(create_points_string)
        psf_string_list.extend(polyline_string_pre)
        psf_string_list.extend(point_n_vec_string)
        psf_string_list.extend(polyline_string_post)
        return psf_string_list

    def create_linear_axial_polyline(self, poly_name, start_point, end_point, num_points):
        """

        Creates polyline in the z-direction
        
        Supporting Methods Used:
            :method: `create_real`
        :param poly_name: name of resulting polyline
        :param start_point: initial point on polyline
        :param end_point: final point on polyline
        :param num_points: total number of points in polyline
        :return psf_string_list: psf formatted list of strings creating a polyline of evenly spaced points
        """
        point_1_x = start_point[0]
        point_1_y = start_point[1]
        point_1_z = start_point[2]
        point_n_z = end_point[2]
        z_vec = linspace(point_1_z, point_n_z, num_points)
        create_points_string = []
        point_n_vec_string = []
        psf_string_list = []

        # Begining part of polyline creation
        polyline_string_pre = (
            f"CREATE POLYLINE \\ \n"
            f"  {poly_name.upper()} \\ \n"
            f"  [{point_1_x},{point_1_y},{point_1_z}] \\ \n"
            f"  [ \\ \n")

        # create variable points and use them in polyline
        for point in range(0, len(z_vec)):
            point_n_str = "point_" + str(point + 1) + "_z"
            create_point, real_point_str = self.parameters.create_real(point_n_str, z_vec[point])
            point_str = (f"  [{point_1_x},{point_1_y},{real_point_str}],\\ \n")
            point_n_vec_string.append(point_str)
            create_points_string.append(create_point)
        create_points_string.append("\n")

        # Ending part of polyline creation
        polyline_string_post = (
            f"  ] \\ \n"
            f"  {self.parameters.geometry_tolerance} \\ \n"
            f"  AS_IS ABSOLUTE \n")
        
        psf_string_list.extend(create_points_string)
        psf_string_list.extend(polyline_string_pre)
        psf_string_list.extend(point_n_vec_string)
        psf_string_list.extend(polyline_string_post)
        return psf_string_list

    def create_element_set_from_selection_volume(self, element_set_name, boundary_or_continuum, phys_mesh, coverage_flag, side_flag, selection_volume_name, bc_set_name=None):
        """

        ch.4 p.128/132

        :param element_set_name: resulting element set name
        :param boundary_or_continuum: BOUNDARY_ELEMENT_SET, CONTINUUM_ELEMENT_SET
        :param phys_mesh: physical mesh that was created
        :param coverage_flag: COMPLETELY, PARTIALLY
        :param side_flag: INSIDE, OUTSIDE
        :param selection_volume_name: name of selection volume to be used to create element set
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of element set
        """
        psf_string = (
            f"CREATE {boundary_or_continuum.upper()} \\ \n"
            f"  {element_set_name.upper()} \\ \n"
            f"  {phys_mesh.upper()} \\ \n"
            f"  {coverage_flag.upper()} \\ \n"
            f"  {side_flag.upper()} \\ \n"
            f"  {selection_volume_name.upper()} \n"
            f" \n")
        
        # if boundary condition set is provided, associate it with the element set
        if bc_set_name is not None:
            self.creation_dict_bc_set[element_set_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_node_set_from_selection_volume(self, node_set_name, phys_mesh, side_flag, selection_volume_name, bc_set_name=None):
        """
        
        ch.4 p.136
        
        :param node_set_name: resulting node set name
        :param phys_mesh: physical mesh that was created
        :param side_flag: INSIDE, OUTSIDE
        :param selection_volume_name: name of selection volume to be used to create element set
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of node set
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_node} \\ \n"
            f"  {node_set_name.upper()} \\ \n"
            f"  {phys_mesh.upper()}\\ \n"
            f"  {side_flag.upper()}\\ \n"
            f"  {selection_volume_name.upper()} \n"
            f"\n")
        
        # if boundary condition set is provided, associate it with the node set
        if bc_set_name is not None:
            self.creation_dict_bc_set[node_set_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_element_set_from_mark(self, element_set_name, phys_mesh, with_flag, mark_name, bc_set_name=None):
        """

        :param element_set_name: any name
        :param phys_mesh: physical mesh that was created
        :param with_flag: WITH, WITHOUT
        :param mark_name: Name of mark to use
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of created element set
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_element_boundary} \\ \n"
            f"  {element_set_name.upper()} \\ \n"
            f"  {phys_mesh.upper()} \\  \n"
            f"  {with_flag.upper()} \\  \n"
            f"  {mark_name.upper()} \n"
            f" \n")
        
        # if boundary condition set is provided, associate it with the element set
        if bc_set_name is not None:
            self.creation_dict_bc_set[element_set_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_node_set_from_mark(self, node_set_name, phys_mesh, with_flag, mark_name, bc_set_name=None):
        """
        
        :param node_set_name: any name
        :param phys_mesh: physical mesh that was created
        :param with_flag: WITH, WITHOUT
        :param mark_name: Name of mark to use
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of created node set
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_node} \\ \n"
            f"  {node_set_name.upper()} \\ \n"
            f"  {phys_mesh.upper()} \\  \n"
            f"  {with_flag.upper()} \\  \n"
            f"  {mark_name.upper()} \n"
            f"\n")
        
        # if boundary condition set is provided, associate it with the node set
        if bc_set_name is not None:
            self.creation_dict_bc_set[node_set_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_node_set_from_element_set(self, node_set_name, element_set_name, bc_set_name=None):
        """
        
        :param node_set_name: name of resulting node set
        :param element_set_name: name of element set create from
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of created node set
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_node} \\ \n"
            f"  {node_set_name.upper()} \\ \n"
            f"  {element_set_name}\\  \n")
        
        # if boundary condition set is provided, associate it with the node set
        if bc_set_name is not None:
            self.creation_dict_bc_set[node_set_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_single_node(self, node_name, point, bc_set_name=None):
        """
        
        :param node_name: name of resulting node
        :param point: coordinate of node
        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of created node
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_node} \\ \n"
            f"  {node_name.upper()} \\ \n"
            f"  {self.parameters.physical_mesh_name} \\  \n"
            f"  CLOSEST_TO \\ \n"
            f"  {point} \n"
            f"\n")
        
        # if boundary condition set is provided, associate it with the node
        if bc_set_name is not None:
            self.creation_dict_bc_set[node_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_all_elements_set(self):
        """

        :return psf_string: psf formatted string of creating all elements
        """
        psf_string = (
            f"CREATE CONTINUUM_ELEMENT_SET \\ \n"
            f"  {self.parameters.elements_all_name} \\ \n"
            f"  {self.parameters.physical_mesh_name}  \n"
            f"\n")
        return psf_string

    def create_all_nodes_set(self, bc_set_name=None):
        """

        :param bc_set_name: optionally include the name of the boundary condition that should be associated
        :return psf_string: psf formatted string of creating all nodes
        """
        psf_string = (
            f"CREATE {self.parameters.set_type_node} \\ \n"
            f"  {self.parameters.nodes_all_name} \\ \n"
            f"  {self.parameters.physical_mesh_name}  \n"
            f"\n")
        
        # if boundary condition set is provided, associate it with the node set
        if bc_set_name is not None:
            self.creation_dict_bc_set[self.parameters.nodes_all_name.upper()] = bc_set_name.upper()
        return psf_string

    def create_material_set(self, element_set_name, material_region, material_alias_name=None):
        """
        
        :param element_set_name: any name
        :param material_region: name of region to associate material to
        :param material_alias_name: optionally include the name (alias) of the material that should be associated
        :return psf_string: psf formatted string of created material element sets
        """
        psf_string = (
            f"CREATE CONTINUUM_ELEMENT_SET \\ \n"
            f"  {element_set_name.upper()} \\ \n"
            f"  {self.parameters.physical_mesh_name}\\  \n"
            f"  IN \\  \n"
            f"  {material_region} \n"
            f"  \n")
        
        # if material alias name is provided, associate it with the element set
        if material_alias_name is not None:
            self.creation_dict_mat_set[element_set_name.upper()] = material_alias_name.upper()
        return psf_string

    def create_material_sets_for_rod(self, regions_dict=None):
        """

        This function automates that material element creation process by accessing the griffinpy inventory of
        created regions and associated materials determined from materials module (determine_create_material).
        
        Supporting Methods Used:
            :method: `create_material_set`
        :param regions_dict: dictionary of all created regions (keys) and desired material types (values)
        :return psf_string_list: psf formatted list of strings creating all material elements
        """
        psf_string_list = []
        
        
        if regions_dict is not None:
            for key in regions_dict:
                # print(
                #     f"value: {key} {regions_dict[key]}"
                # )
                value_suffix = (regions_dict[key].lower().split('_', 1)[-1]).upper()
                psf_string_list.append(self.create_material_set(
                    self.parameters.elements_prefix_name + value_suffix,
                    self.parameters.region_prefix_name + value_suffix))
                
                self.parameters.region_dict[
                    self.parameters.region_prefix_name + value_suffix] = self.parameters.elements_prefix_name + value_suffix
        else:
            psf_string_list.append(self.create_material_set(
                self.parameters.elements_fuel,
                self.parameters.region_fuel))
            
            psf_string_list.append(self.create_material_set(
                self.parameters.elements_fuel_cladding_gap,
                self.parameters.region_fuel_cladding_gap))
            
            psf_string_list.append(self.create_material_set(
                self.parameters.elements_cladding,
                self.parameters.region_cladding))
        return psf_string_list

    def create_material_sets_for_triso_particle(self, regions_dict):
        """

        this function automates that material element creation process by accessing the griffinpy inventory of
        created regions and associated materials determined from materials module (determine_create_material).

        :param regions_dict: dictionary of all created regions (keys) and desired material types (values)
        :return psf_string_list: psf formatted list of strings creating all material elements
        """
        psf_string_list = []
        for key in regions_dict:
            region_name = regions_dict[key]
            value_suffix = (region_name.lower().split('_', 1)[-1]).upper()
            if key == 1:
                element_name = self.parameters.elements_fuel
            else:
                element_name = self.parameters.elements_prefix_name + value_suffix

            psf_string_list.append(self.create_material_set(
                element_name,
                region_name)
            )
            self.parameters.region_dict[region_name] = element_name
        return psf_string_list

class QuadPatch(SelectionVolume):
    """

    This class inherits the class `SelectionVolume` for creating quad patches as well as basic shapes such as arcs, rings and disks
    and some more advanced geometric shapes such as MPS
    
    Class inherits:
        :class: `SelectionVolume`
    """

    def __init__(self, input_file_path=None):
        super(QuadPatch, self).__init__(input_file_path)
        self.element_kind = "QUAD_4_NODE_ELEM_KIND"
        self.division_ratios = [1.0, 1.0, 1.0, 1.0]
        self.center_circle_radius = None

    def create_quad_patch(self, name, point_1, point_2, point_3, point_4, divisions_from_1_and_3, divisions_from_2_and_4, curvature_radii, region):
        """
        
        This function creates any arbitrary quad patch and add it to the inventory of created quad patches.
        
        Supporting Methods Used:
            :method: `add_object`
        :param name: name of quad patch to be created
        :param point_1: x-y coordinates of point 1
        :param point_2: x-y coordinates of point 2
        :param point_3: x-y coordinates of point 3
        :param point_4: x-y coordinates of point 4
        :param divisions_from_1_and_3: mesh resolution from edges 1 to 3
        :param divisions_from_2_and_4: mesh resolution from edges 2 to 4
        :param curvature_radii: curvature of each edge
        :param region: region for quad patch to be assigned
        :return psf_string: psf formatted string that creates a quad patch
        """
        quad_patch_name = str(name).upper()
        psf_string = (
            f"CREATE QUAD_PATCH \\ \n"
            f"  {quad_patch_name} \\ # Arbitrary name of quad patch\n"
            f"  [{point_1},\\ # Point 1\n"
            f"  {point_2}, \\ # Point 2\n"
            f"  {point_3}, \\ # Point 3\n"
            f"  {point_4}] \\ # Point 4\n"
            f"  {int(divisions_from_1_and_3)} \\ # Divisions along edge 1 and 3 (radial) \n"
            f"  {int(divisions_from_2_and_4)} \\ # Divisions along edge 2 and 4 (axial) \n"
            f"  {curvature_radii} \\ # Curvatures along each edge\n"
            f"  {self.division_ratios} \\ # Division ratios along each edge\n"
            f"  {self.parameters.geometry_tolerance} \\ # Geometric tolerance\n"
            f"  AS_IS \\ # Orientation of provided points\n"
            f"  {self.element_kind} \\ # Element kind (4/9 node options)\n"
            f"  {region.upper()} # Region associated with quad patch\n"
            f"\n")
        self.add_object(quad_patch_name, self.object_type_quad_patch)
        self.add_object(region.upper(), self.object_type_region, region.upper())
        return psf_string

    def rectangular_quad_patch(self, width, height, radial_divisions, axial_divisions, quad_patch_region, quad_patch_name=None):
        """
        
        This function creates a simple rectangular quad patch.
        
        Supporting Methods Used:
            :method: `create_quad_patch`
        :param quad_patch_name: name of quad patch to be created
        :param width: width of quad patch
        :param height: height of quad patch
        :param radial_divisions: mesh resolution in the radial-direction
        :param axial_divisions: mesh resolution in the axial-direction
        :param quad_patch_region: region for quad patch to be assigned
        :return psf_string: psf formatted string that creates a rectangular quad patch
        """
        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_rectangle_name
        point_1 = [0.0, 0.0]
        point_2 = [float(width), 0.0]
        point_3 = [float(width), float(height)]
        point_4 = [0.0, float(height)]
        divisions_from_1_and_3 = int(radial_divisions)
        divisions_from_2_and_4 = int(axial_divisions)
        curvature_radii = [0.0, 0.0, 0.0, 0.0]
        region = str(quad_patch_region)
        psf_string = self.create_quad_patch(
            quad_patch_name,
            point_1,
            point_2,
            point_3,
            point_4,
            divisions_from_1_and_3,
            divisions_from_2_and_4,
            curvature_radii,
            region)
        return psf_string

    def arc_sector_quad_patch(self, inner_radius, outer_radius, theta, radial_divisions, quad_patch_region, quad_patch_name=None):
        """
        
        This function creates a 2D single element partial-annulus (arc sector) quad patch.
        
        Supporting Methods Used:
            :method: `create_quad_patch`
        :param quad_patch_name: name of quad patch to be created
        :param inner_radius: inner radius of arc
        :param outer_radius: outer radius of arc
        :param theta: angle (degrees) that defines the sector of the arc
        :param radial_divisions: mesh resolution in the radial-direction
        :param quad_patch_region: region for quad patch to be assigned
        :return psf_string: psf formatted string that creates a arc quad patch
        """
        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_arc_name
        point_1 = [inner_radius * (math.cos(math.radians(theta / 2))),
                   -1 * inner_radius * (math.sin(math.radians(theta / 2)))]
        point_2 = [outer_radius * (math.cos(math.radians(theta / 2))),
                   -1 * outer_radius * (math.sin(math.radians(theta / 2)))]
        point_3 = [outer_radius * (math.cos(math.radians(theta / 2))),
                   outer_radius * (math.sin(math.radians(theta / 2)))]
        point_4 = [inner_radius * (math.cos(math.radians(theta / 2))),
                   inner_radius * (math.sin(math.radians(theta / 2)))]
        divisions_from_1_and_3 = int(radial_divisions)
        divisions_from_2_and_4 = int(1)
        curvature_radii = [0.0, float(outer_radius), 0.0, -1 * float(inner_radius)]
        region = str(quad_patch_region)
        psf_string = self.create_quad_patch(
            quad_patch_name,
            point_1,
            point_2,
            point_3,
            point_4,
            divisions_from_1_and_3,
            divisions_from_2_and_4,
            curvature_radii,
            region)
        return psf_string

    def ring_quad_patch(self, inner_radius, outer_radius, radial_divisions, circumferential_divisions, quad_patch_region, quad_patch_name=None):
        """
        
        This function creates a multi-element full 2D annulus (ring) quad patch.
        
        Supporting Methods Used:
            :method: `arc_sector_quad_patch`,
            :method: `copy`,
            :method: `rotate`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param quad_patch_name: name of quad patch to be created
        :param inner_radius: inner radius of ring
        :param outer_radius: outer radius of ring
        :param radial_divisions: mesh resolution in the radial-direction
        :param circumferential_divisions: mesh resolution in the circumferential-direction
        :param quad_patch_region: region for quad patch to be assigned
        :return psf_string_list: psf formatted list of strings that creates a ring quad patch
        """

        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_ring_name
        total_number_of_arcs = circumferential_divisions
        theta = 360 / total_number_of_arcs
        sector_quad_patch_name = ("SECTOR_" + quad_patch_name).upper()
        copy_sector_quad_patch_name = ("COPY_" + sector_quad_patch_name).upper()

        psf_string_list = []

        # creating first arc
        psf_string_list.append(self.arc_sector_quad_patch(
            inner_radius,
            outer_radius,
            theta,
            radial_divisions,
            quad_patch_region,
            sector_quad_patch_name))

        #
        psf_string_list.append(self.copy(sector_quad_patch_name, copy_sector_quad_patch_name))
        psf_string_list.append(self.rotate(copy_sector_quad_patch_name, theta, "z"))
        psf_string_list.append(self.snap_2_objects(
            sector_quad_patch_name,
            copy_sector_quad_patch_name,
            quad_patch_name))

        # Creating full mesh
        # if symmetry_type==self.parameters.geometry_quarter_symmetry:
        #     total_number_of_arcs = int(total_number_of_arcs/4)
        # elif symmetry_type==self.parameters.geometry_half_symmetry:
        #     total_number_of_arcs = int(total_number_of_arcs/2)
        for arc in range(0, total_number_of_arcs - 2):
            psf_string_list.append(self.rotate(
                copy_sector_quad_patch_name,
                theta,
                "z"))
            psf_string_list.append(self.snap_2_objects(
                quad_patch_name,
                copy_sector_quad_patch_name,
                quad_patch_name))
        quad_patch_list = [sector_quad_patch_name, copy_sector_quad_patch_name]

        # delete
        psf_string_list.append(self.delete_list_of_objects(
            quad_patch_list,
            quad_patch_name))
        return psf_string_list

    def partial_ring_quad_patch(self, inner_radius, outer_radius, radial_divisions, circumferential_divisions, quad_patch_region, angle, quad_patch_name=None):
        """
        
        This function creates a multi-element 2D partial-annulus (partial ring) quad patch.
        
        Supporting Methods Used:
            :method: `arc_sector_quad_patch`,
            :method: `copy`,
            :method: `rotate`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param inner_radius: inner radius of ring
        :param outer_radius: outer radius of ring
        :param radial_divisions: mesh resolution in the radial-direction
        :param circumferential_divisions: mesh resolution in the circumferential-direction
        :param quad_patch_region: region for quad patch to be assigned
        :param angle: angle (in degrees) from 0 to create arc sector
        :param quad_patch_name: name of quad patch to be created
        :return psf_string_list: psf formatted list of strings that creates an arc quad patch
        """
        circumferential_divisions = int(
            circumferential_divisions * (self.parameters.model_symmetry_type / self.parameters.geometry_no_symmetry))

        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_ring_name
        total_number_of_arcs = circumferential_divisions
        theta = angle / total_number_of_arcs
        sector_quad_patch_name = ("SECTOR_" + quad_patch_name).upper()
        copy_sector_quad_patch_name = ("COPY_" + sector_quad_patch_name).upper()

        psf_string_list = []

        # creating first arc
        psf_string_list.append(self.arc_sector_quad_patch(
            inner_radius,
            outer_radius,
            theta,
            radial_divisions,
            quad_patch_region,
            sector_quad_patch_name))

        #
        psf_string_list.append(self.copy(sector_quad_patch_name, copy_sector_quad_patch_name))
        psf_string_list.append(self.rotate(copy_sector_quad_patch_name, theta, "z"))
        psf_string_list.append(self.snap_2_objects(
            sector_quad_patch_name,
            copy_sector_quad_patch_name,
            quad_patch_name))

        # Creating full mesh
        for arc in range(0, total_number_of_arcs - 2):
            psf_string_list.append(self.rotate(
                copy_sector_quad_patch_name,
                theta,
                "z"))
            psf_string_list.append(self.snap_2_objects(
                quad_patch_name,
                copy_sector_quad_patch_name,
                quad_patch_name))
        quad_patch_list = [sector_quad_patch_name, copy_sector_quad_patch_name]

        # delete
        psf_string_list.append(self.delete_list_of_objects(
            quad_patch_list,
            quad_patch_name))

        # Aligning orientation
        psf_string_list.extend(self.rotate(
            quad_patch_name,
            (11.25), "z"))
        return psf_string_list

    def center_disk(self, radius, circumferential_divisions, quad_patch_region, symmetry_type, quad_patch_name=None):
        """
        
        This function creates the core part of a 2D disk quad patch.
        
        Supporting Methods Used:
            :method: `create_quad_patch`,
            :method: `rectangular_quad_patch`,
            :method: `copy`,
            :method: `mirror`,
            :method: `rotate`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param radius: radius of disk
        :param circumferential_divisions:  mesh resolution in the circumferential-direction
        :param quad_patch_region: region for quad patch to be assigned
        :param symmetry_type: symmetry type options:(quarter->`90`, half->`180`, none->`360`)
        :param quad_patch_name: name of quad patch to be created
        :return psf_string_list: psf formatted list of strings that creates a disk quad patch
        """

        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_center_disk_name
        # Names
        center_square_qp_name = "FUEL_CENTER_SQUARE_QP"
        center_circle_qp_name = "FUEL_CENTER_CIRCLE_QP"
        center_circle_copy_qp_name = "COPY_FUEL_CENTER_CIRCLE_QP"
        center_qp_name = "FUEL_CENTER_QP"
        center_qp_copy_name = "COPY_FUEL_CENTER_QP"
        ring_qp_name = "FUEL_RING_QP"

        # Dimensions
        center_circle_radius = radius
        center_square_half_width = center_circle_radius / 2

        # Divisions
        rectangular_divisions = (circumferential_divisions / 2) / 4
        center_circle_radial_divisions = rectangular_divisions / 2
        center_circle_circumferential_divisions = rectangular_divisions

        # Curvature
        center_circle_curvature_radii = [0.0, float(center_circle_radius), 0.0, 0.0]

        # Points
        center_circle_point_1 = [center_square_half_width, 0.0]
        center_circle_point_2 = [center_circle_radius, 0.0]
        center_circle_point_3 = [center_circle_radius * math.cos(math.radians(45)),
                                 center_circle_radius * math.cos(math.radians(45))]
        center_circle_point_4 = [center_square_half_width, center_square_half_width]
        psf_string_list = []

        # center "square"
        psf_string_list.append(self.rectangular_quad_patch(
            center_square_half_width,
            center_square_half_width,
            rectangular_divisions,
            rectangular_divisions,
            quad_patch_region,
            center_square_qp_name))

        # "arc" that wraps around center "square"
        psf_string_list.append(self.create_quad_patch(
            center_circle_qp_name,
            center_circle_point_1,
            center_circle_point_2,
            center_circle_point_3,
            center_circle_point_4,
            center_circle_radial_divisions,
            center_circle_circumferential_divisions,
            center_circle_curvature_radii,
            quad_patch_region))

        # Copy, mirror and rotate to get entire center
        psf_string_list.append(self.copy(center_circle_qp_name, center_circle_copy_qp_name))
        psf_string_list.append(self.mirror(center_circle_copy_qp_name, [1.0, 0.0, 0.0]))
        psf_string_list.append(self.rotate(center_circle_copy_qp_name, -90, "z"))

        # snapping
        psf_string_list.append(self.snap_2_objects(
            center_circle_qp_name,
            center_circle_copy_qp_name,
            center_circle_qp_name))
        psf_string_list.append(self.snap_2_objects(
            center_square_qp_name,
            center_circle_qp_name,
            quad_patch_name))
        psf_string_list.append(self.copy(
            quad_patch_name,
            center_qp_copy_name))

        # dont do any mirroring
        if symmetry_type == self.parameters.geometry_quarter_symmetry:
            mirror_axis_sequence_list = []
        # mirroring accross y axis to get half mesh
        elif symmetry_type == self.parameters.geometry_half_symmetry:
            mirror_axis_sequence_list = ["x"]
        # mirroring accross all axes to get full 360
        elif symmetry_type == self.parameters.geometry_no_symmetry:
            mirror_axis_sequence_list = ["y", "x", "y"]

        # Begin auto-mirror
        for transformation in range(0, len(mirror_axis_sequence_list)):
            psf_string_list.append(self.mirror(
                center_qp_copy_name,
                mirror_axis_sequence_list[transformation]))
            psf_string_list.append(self.snap_2_objects(
                quad_patch_name,
                center_qp_copy_name,
                quad_patch_name))

        # deleting 
        psf_string_list.append(self.delete_list_of_objects(
            self.creation_dict_quad_patch, quad_patch_name))

        return psf_string_list

    def disk_quad_patch(self, radius, radial_divisions, circumferential_divisions, symmetry_type, quad_patch_region, quad_patch_name=None):
        """
        
        This function creates the entire 2D disk quad patch.
        
        Supporting Methods Used:
            :method: `center_disk`,
            :method: `partial_ring_quad_patch`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param radius: radius of disk
        :param radial_divisions: mesh resolution in the radial-direction
        :param circumferential_divisions:  mesh resolution in the circumferential-direction
        :param quad_patch_region: region for quad patch to be assigned
        :param symmetry_type: symmetry type, eigth, quarter, half
        :param quad_patch_name: name of quad patch to be created
        :return psf_string_list: psf formatted list of strings that creates a disk quad patch
        """
        if quad_patch_name is None:
            quad_patch_name = self.parameters.qp_disk_name

        psf_string_list = []

        center_circle_radius = radius / 4
        
        # create center disk
        psf_string_list.extend(self.center_disk(
            center_circle_radius,
            circumferential_divisions,
            quad_patch_region,
            symmetry_type,
            self.parameters.qp_center_disk_name)
        )

        # create partial ring
        psf_string_list.extend(self.partial_ring_quad_patch(
            center_circle_radius,
            radius,
            radial_divisions,
            circumferential_divisions,
            quad_patch_region,
            symmetry_type,
            self.parameters.qp_ring_name))

        # snap core disk with annulus
        psf_string_list.append(self.snap_2_objects(
            self.parameters.qp_center_disk_name,
            self.parameters.qp_ring_name,
            quad_patch_name))

        # deleting 
        psf_string_list.append(self.delete_list_of_objects(
            self.creation_dict_quad_patch, quad_patch_name))
        return psf_string_list

    def create_chip_ring_region(self, qp_suffix_name, points_list, radius_inner, radius_outer, curvature_radii_list, divisions_radial, divisions_circumferential, region, symmetry_type):
        """

        This function takes list of points to form a 2D mps mesh.
        
        Supporting Methods Used:
            :method: `create_quad_patch`,
            :method: `partial_ring_quad_patch`,
            :method: `copy`,
            :method: `rotate`,
            :method: `mirror`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param qp_suffix_name:
        :param points_list: list of points of each quad patch of MPS elements
        :param radius_inner: inner radius of mps
        :param radius_outer: outer radius of mps
        :param curvature_radii_list: curvatures of each quad patch
        :param divisions_radial: mesh resolution in the radial-direction
        :param divisions_circumferential: mesh resolution in the circumferential-direction
        :param region: name of resulting region
        :param symmetry_type: symmetry type options:(quarter->`90`, half->`180`, none->`360`)
        :return psf_string: psf formatted string of a created mps mesh
        """
        psf_string_list = []
        
        # making 4 quad patches for chipped region
        qp_name = (qp_suffix_name + "_outer_chipped_").upper()
        for qp in range(0, 2):
            psf_string_list.append(self.create_quad_patch(
                qp_name + str(qp + 1),
                points_list[qp][0],
                points_list[qp][1],
                points_list[qp][2],
                points_list[qp][3],
                divisions_radial,
                divisions_circumferential / 16,
                curvature_radii_list[qp],
                region)
            )

        # Snapping all chipped pieces
        qp_chipped_name = (qp_suffix_name + "_chipped").upper()
        psf_string_list.append(self.snap_2_objects(
            qp_name + str(1),
            qp_name + str(2),
            qp_chipped_name))

        # dont do any mirroring
        if symmetry_type == self.parameters.geometry_quarter_symmetry:
            mirror_axis_sequence_list = []
            ring_symmetry_type = symmetry_type - 45
            ring_divisions_circumferential = divisions_circumferential / 2
            rotation_angle = 0
        
        # mirroring accross x axis to get half mesh
        elif symmetry_type == self.parameters.geometry_half_symmetry:
            mirror_axis_sequence_list = []
            ring_symmetry_type = symmetry_type - 45
            ring_divisions_circumferential = divisions_circumferential * (3 / 4)
            rotation_angle = -90
        
        # mirroring accross all axes to get full 360
        elif symmetry_type == self.parameters.geometry_no_symmetry:
            mirror_axis_sequence_list = ["x"]
            ring_symmetry_type = symmetry_type - 90
            ring_divisions_circumferential = divisions_circumferential * (6 / 8)
            rotation_angle = 90 + 45

        psf_string_list.append(self.copy(
            qp_chipped_name,
            self.parameters.temporary_object_1_name))

        for transformation in range(0, len(mirror_axis_sequence_list)):
            psf_string_list.append(self.mirror(
                self.parameters.temporary_object_1_name,
                mirror_axis_sequence_list[transformation]))
            psf_string_list.append(self.snap_2_objects(
                qp_chipped_name,
                self.parameters.temporary_object_1_name,
                qp_chipped_name))

        # making unchipped ring portion
        qp_unchipped_name = (qp_suffix_name + "_outer_unchipped").upper()
        center_circle_radius = self.parameters.geometry_fuel_radius / 4
        psf_string_list.extend(self.partial_ring_quad_patch(
            radius_inner,
            radius_outer,
            divisions_radial,
            ring_divisions_circumferential,
            region,
            ring_symmetry_type,
            qp_unchipped_name))

        # Aligning orientation
        psf_string_list.extend(self.rotate(
            qp_unchipped_name,
            -(11.25/2),
            "z"))
        
        psf_string_list.extend(self.rotate(
            qp_unchipped_name,
            rotation_angle,
            "z"))

        # snapping
        psf_string_list.append(self.snap_2_objects(
            qp_chipped_name,
            qp_unchipped_name,
            qp_suffix_name))

        # deleting
        quad_patch_list = [
            qp_name + str(1), 
            qp_name + str(2), 
            qp_chipped_name, 
            qp_unchipped_name,
            self.parameters.temporary_object_1_name
            ]
        psf_string_list.append(self.delete_list_of_objects(
            quad_patch_list))
        return psf_string_list

    def todo_create_mesh_from_list_of_quads(self, xyr, quad_patch_name, name_region):
        """

        :param xyr:
        :param quad_patch_name:
        :param name_region:
        :return psf_string_list:
        """
        psf_string_list = []
        for k in range(xyr.shape[0]):
            psf_string_list.extend(self.create_quad_patch(
                name=(quad_patch_name + "_" + str(k)),
                point_1=xyr[k][0, 0:2],
                point_2=xyr[k][1, 0:2],
                point_3=xyr[k][2, 0:2],
                point_4=xyr[k][3, 0:2],
                divisions_from_1_and_3=1,
                divisions_from_2_and_4=1,
                curvature_radii=[xyr[k][0, 2], xyr[k][1, 2], xyr[k][2, 2], xyr[k][3, 2]],
                region=name_region))

        # # Snap components together
        # psf_string_list.extend(self.snap_list_objects(
        #     object_list=self.creation_dict_quad_patch, 
        #     child_object_name=quad_patch_name))

        # # deleting
        # psf_string_list.extend(self.delete_list_of_objects(
        #     object_list=self.creation_dict_quad_patch, 
        #     excluded_object=quad_patch_name))

        return psf_string_list

class HexMesh(QuadPatch):
    """

    This class inherits the class `QuadPatch` for creating several hex mesh objects such as cylinders, pipes and spheres and some 
    more advanced geometric shapes such as embedded spheres in rectangular block.
    
    Class inherits:
        :class: `QuadPatch`
    """

    def __init__(self, input_file_path=None):
        super(HexMesh, self).__init__(input_file_path)
        self.element_kind_mesh = "HEX_8_NODE_ELEM_KIND"
        self.prism_type = "PRISM"
        self.body_revolution_type = "BODY_OF_REVOLUTION"
        self.simple_extrude_taper_origin = [0.0, 0.0, 0.0]
        self.x_extruded_direction = [1.0, 0.0, 0.0]
        self.y_extruded_direction = [0.0, 1.0, 0.0]
        self.z_extruded_direction = [0.0, 0.0, 1.0]
        self.simple_division_ratio = 1.0
        self.simple_taper_scale_factor = 1.0
        self.simple_twist_angle = 0.0

    def extrude(self, extruded_hex_name, taper_xyz_origin, extruded_direction, length_of_extrusion, axial_divisions, division_ratios, taper_scale_factor, twist_angle, quad_patch, region=None, delete_source=True):
        """

        This function does a linear extrusion to create a 3D hex mesh from a 2D quad patch and adds it to the inventory of created hex meshes.
        
        Supporting Methods Used:
            :method: `add_object`
            :method: `delete_object`
        :param extruded_hex_name: The resultant extruded hexahedron mesh
        :param taper_xyz_origin: The base coordinates for the tapering of extrusion cross-section
        :param extruded_direction: Describes the direction of extrusion
        :param length_of_extrusion: Length of extrusion along axis
        :param axial_divisions: Number of increments (hexahedron layers) along the extrusion length
        :param division_ratios: The ratio of the length of the last increment to the first one along the extrusion length
        :param taper_scale_factor: Scale factor of the end cross-section when a tapered shape is desired
        :param twist_angle: Rotation of the end cross-section (in degrees) with respect to the base cross-section when a twisted shape is desired.
        :param quad_patch: The base quadrilateral patch to be extruded along the prism axis
        :param region: Region with which to define the resultant geometric objects interior
        :param delete_source: option to delete source quad patch options: True (default), False
        :return psf_string_list: psf formatted list of strings for extruding parent quad patch
        """
        psf_string_list = []
        extrude_string = (f"\n"
                          f"EXTRUDE {self.prism_type}\\ \n"
                          f"  {extruded_hex_name} \\ \n"
                          f"  {taper_xyz_origin} \\ \n"
                          f"  {extruded_direction} \\ \n"
                          f"  {length_of_extrusion} \\ \n"
                          f"  {axial_divisions} \\ \n"
                          f"  {division_ratios} \\ \n"
                          f"  {taper_scale_factor} \\ \n"
                          f"  {twist_angle} \\ \n")
        if region is not None:
            extruded_string_region_condition = (f"  {quad_patch} \\ \n"
                                                f"  {region} \n")
        else:
            extruded_string_region_condition = f"  {quad_patch} \n"
        extrude_string = extrude_string + extruded_string_region_condition

        # adding hex to inventory
        self.add_object(extruded_hex_name, self.object_type_hex)

        # output string
        psf_string_list.append(extrude_string)

        # delete base quad patch from inventory and string
        if delete_source:
            psf_string_list.append(self.delete_object(
                quad_patch.upper()))
        return psf_string_list

    def extrude_revolve(self, revolved_hex_name, revolved_axis_origin, revolved_direction, angle_of_extrusion, circumferential_divisions, division_ratios, quad_patch, region=None, delete_source=True):
        """

        This function does a circumferential extrusion (revolove) to create a 3D hex mesh from a 2D quad patch.
        
        Supporting Methods Used:
            :method: `add_object`,
            :method: `delete_object`
        :param revolved_hex_name:  The resultant extruded hexahedron mesh
        :param revolved_axis_origin: Coordinates of a point along the axis of revolution
        :param revolved_direction: Describes the axis of revolution
        :param angle_of_extrusion: Angle of extrusion (arc) about axis
        :param circumferential_divisions: Number of angular divisions along the extrusion arc
        :param division_ratios: The ratio of the last angular step to the first one along the extrusion arc
        :param quad_patch: The base quadrilateral patch that is revolved
        :param region: Region with which to define the resultant geometric objects interior
        :param delete_source: option to delete source quad patch options: True (default), False
        :return psf_string_list: psf formatted list of strings for revolve-extruding and deleting parent quad patch
        """
        psf_string_list = []
        revolve_string = (f"EXTRUDE {self.body_revolution_type} \\ \n"
                          f"    {revolved_hex_name} \\ \n"
                          f"    {revolved_axis_origin} \\ \n"
                          f"    {revolved_direction} \\ \n"
                          f"    {angle_of_extrusion} \\ \n"
                          f"    {circumferential_divisions} \\ \n"
                          f"    {division_ratios} \\ \n")
        if region is not None:
            revolve_string_region_condition = (f"    {quad_patch} \\ \n"
                                               f"    {region} \n")
        else:
            revolve_string_region_condition = f"    {quad_patch} \n"
        revolve_string = revolve_string + revolve_string_region_condition

        # adding hex to inventory
        self.add_object(revolved_hex_name, self.object_type_hex)

        # output string
        psf_string_list.append(revolve_string)

        # delete base quad patch from inventory and string
        if delete_source:
            psf_string_list.append(self.delete_object(
                quad_patch.upper()))
        return psf_string_list

    def simple_extrude(self, extruded_hex_name, extruded_direction, length_of_extrusion, axial_divisions, quad_patch):
        """
        
        This function does a simple linear extrusion to create a 3D hex mesh from a 2D quad patch.
        
        Supporting Methods Used:
            :method: `extrude`
        :param extruded_hex_name: The resultant extruded hexahedron mesh
        :param extruded_direction: Describes the direction of extrusion
        :param length_of_extrusion: Length of extrusion along axis
        :param axial_divisions: Number of increments (hexahedron layers) along the extrusion length
        :param quad_patch: The base quadrilateral patch to be extruded along the prism axis
        :return:
        :return psf_string: psf formatted string for extruding a quad patch
        """

        if type(extruded_direction) == list and len(extruded_direction) == 3 and float(
                sum(extruded_direction)) == 1.0:
            direction = [float(element) for element in extruded_direction]
        elif type(extruded_direction) == str:
            if extruded_direction.upper() == "X":
                direction = self.x_extruded_direction
            elif extruded_direction.upper() == "Y":
                direction = self.y_extruded_direction
            elif extruded_direction.upper() == "Z":
                direction = self.z_extruded_direction

        psf_string = self.extrude(
            extruded_hex_name,
            self.simple_extrude_taper_origin,
            direction,
            length_of_extrusion,
            axial_divisions,
            self.simple_division_ratio,
            self.simple_taper_scale_factor,
            self.simple_twist_angle,
            quad_patch)
        return psf_string

    def create_cylinder(self, radius, height, radial_divisions, circumferential_divisions, axial_divisions, symmetry_type, region=None, hex_name=None):
        """
        
        This function creates a simple 3D cylinder hex mesh from a 2D disk quad patch.
        
        Supporting Methods Used:
            :method: `disk_quad_patch`,
            :method: `rotate`,
            :method: `simple_extrude`
        :param radius:
        :param height:
        :param radial_divisions:
        :param circumferential_divisions:
        :param axial_divisions:
        :param symmetry_type: options:(quarter->`90`, half->`180`, none->`360`)
        :param region:
        :param hex_name:
        :return psf_string_list: psf formatted list of strings for creating a cylinder
        """
        if region is None:
            region = self.parameters.region_default
        if hex_name is None:
            hex_name = self.parameters.hex_cylinder_name
        psf_string_list = []

        psf_string_list.extend(self.disk_quad_patch(
            radius,
            radial_divisions,
            circumferential_divisions,
            symmetry_type,
            region,
            quad_patch_name=self.parameters.qp_disk_name)
            )
        
        # Aligning orientation
        psf_string_list.extend(self.rotate(
            self.parameters.qp_disk_name,
            (11.25 / 2), "z"))

        psf_string_list.extend(self.simple_extrude(
            hex_name,
            self.z_extruded_direction,
            height,
            axial_divisions,
            self.parameters.qp_disk_name))
        return psf_string_list

    def create_pipe(self, inner_radius,  outer_radius, height, radial_divisions, circumferential_divisions, axial_divisions, symmetry_type, region=None, hex_name=None):
        """
        
        This function creates a simple 3D annulus (pipe) hex mesh from a 2D ring quad patch.
        
        Supporting Methods Used:
            :method: `partial_ring_quad_patch`,
            :method: `simple_extrude`
        :param inner_radius:
        :param outer_radius:
        :param height:
        :param radial_divisions:
        :param circumferential_divisions:
        :param axial_divisions:
        :param symmetry_type: options:(quarter->`90`, half->`180`, none->`360`)
        :param region:
        :param hex_name:
        :return psf_string_list: psf formatted list of strings for creating a cylinder
        """
        if region is None:
            region = self.parameters.region_default
        if hex_name is None:
            hex_name = self.parameters.hex_pipe_name
        psf_string_list = []
        
        # creating ring
        psf_string_list.extend(self.partial_ring_quad_patch(
            inner_radius,
            outer_radius,
            radial_divisions,
            circumferential_divisions,
            region,
            symmetry_type,
            self.parameters.qp_ring_name))

        # extrude to create 3D
        psf_string_list.extend(self.simple_extrude(
            hex_name,
            self.z_extruded_direction,
            height,
            axial_divisions,
            self.parameters.qp_ring_name))

        return psf_string_list

    def create_hex_block(self, name, starting_coordinate, extrude_x, extrude_y, extrude_z, divisions_x, divisions_y, divisions_z, region):
        """
        
        This function creates a simple 3D cuboid (box) hex mesh and adds it to the inventory of created hex meshes.
        
        Supporting Methods Used:
            :method: `add_object`
        :param name:
        :param starting_coordinate:
        :param extrude_x:
        :param extrude_y:
        :param extrude_z:
        :param divisions_x:
        :param divisions_y:
        :param divisions_z:
        :param symmetry_type:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a cube
        """
        psf_string_list = []
        hex_name_output = str(name).upper()
        psf_string_list.append(
            f"CREATE HEX_BLOCK \\ \n"
            f"  {hex_name_output} \\ # Arbitrary name of mesh\n"
            f"  {starting_coordinate} \\ # Base Coordinate \n"
            f"  {extrude_x} \\ # Extrusion length along x direction\n"
            f"  {extrude_y} \\ # Extrusion length along y direction\n"
            f"  {extrude_z} \\ # Extrusion length along z direction\n"
            f"  {int(divisions_x)} \\ # Number of divisions along x direction\n"
            f"  {int(divisions_y)} \\ # Number of divisions along y direction\n"
            f"  {int(divisions_z)} \\ # Number of divisions along z direction\n"
            f"  {self.parameters.geometry_tolerance} \\ # Geometric tolerance\n"
            f"  {self.element_kind_mesh} \\ # Element kind (4/9 node options)\n"
            f"  {region.upper()} # Region associated with quad patch\n"
            f"\n")

        self.add_object(hex_name_output, self.object_type_hex)
        return psf_string_list

    def create_sphere_hex_block(self, name, length_inner, radius_inner, length_outer, radius_outer, divisions_circumferential, divisions_radial, region):
        """
        
        This function creates a simple 3D spherical-type 1/24 sector (solid or shell) hex mesh and adds it to the inventory of created hex meshes.
        
        Supporting Methods Used:
            :method: `add_object`
        :param name:
        :param length_inner:
        :param radius_inner:
        :param length_outer:
        :param radius_outer:
        :param divisions_circumferential:
        :param divisions_radial:
        :param region:
        :return:
        :return psf_string_list: psf formatted list of strings for creating a sphere hex block
        """
        mesh_name = str(name).upper()
        hex_mesh_string = (
            f"CREATE SPHERE_HEX_BLOCK \\ \n"
            f"  {mesh_name} \\ # Arbitrary name of mesh\n"
            f"  [0.0,0.0,0.0] \\ # Base Coordinate \n"
            f"  {length_inner} \\ # Inner length of bounding cube (use 0.0 when making a core block)\n"
            f"  {radius_inner} \\ # Inner radius of block\n"
            f"  {length_outer} \\ # Outer length of bounding cube (set to outer radius of block for perfect sphere)\n"
            f"  {radius_outer} \\ # Outer radius of block \n"
            f"  {int(divisions_circumferential)} \\ # Number of divisions along edges (circumferential) (that connect shell and core blocks) \n"
            f"  {int(divisions_radial)} \\ # Number of divisions along height (radial) (only for shell blocks, not core blocks)\n"
            f"  {self.parameters.geometry_tolerance} \\ # Geometric tolerance\n"
            f"  {self.element_kind_mesh} \\ # Element kind (4/9 node options)\n"
            f"  {region.upper()} # Region associated with quad patch\n"
            f"\n")

        self.add_object(mesh_name, self.object_type_hex)
        self.add_object(region.upper(), self.object_type_region, region.upper())
        return hex_mesh_string

    def auto_rotate_sphere_hex_block(self, hex_name_output, symmetry_type):
        """
        
        This function performs an automated process of copying and rotating a spherical sector to create a larger sector or full sphere.
        
        Supporting Methods Used:
            :method: `auto_revolve`,
            :method: `copy`,
            :method: `mirror`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :param hex_name_output:
        :param symmetry_type: options:(eigth->`45`,quarter->`90`, half->`180`, none->`360`)
        :return psf_string_list: psf formatted list of strings for creating a solid and or layered sphere
        """
        psf_string_list = []

        if symmetry_type == self.parameters.geometry_eigth_symmetry:
            mesh_quant = None
        elif symmetry_type == self.parameters.geometry_quarter_symmetry:
            mesh_quant = 1
        elif symmetry_type == self.parameters.geometry_half_symmetry:
            mesh_quant = 3
        elif symmetry_type == self.parameters.geometry_no_symmetry:
            mesh_quant = 3

        # auto revolve 1/8 sphere (only if user wants particular geometry)
        if mesh_quant is not None:
            sphere_axis_of_rotation = [0.0, 0.0, 1.0]
            sphere_degrees_rotated = 90
            psf_string_list.extend(self.auto_revolve(
                hex_name_output,
                mesh_quant,
                sphere_axis_of_rotation,
                sphere_degrees_rotated))
            
            # create full sphere (only if user wants)
            if symmetry_type == self.parameters.geometry_no_symmetry:
                psf_string_list.extend(self.copy(
                    hex_name_output,
                    self.parameters.temporary_object_1_name))
                
                psf_string_list.extend(self.mirror(
                    hex_name_output,
                    "Z"))
                
                # Snapping together --- result is full sphere
                psf_string_list.extend(self.snap_2_objects(
                    hex_name_output,
                    self.parameters.temporary_object_1_name,
                    hex_name_output))

                # delete 
                psf_string_list.append(self.delete_list_of_objects(
                    [self.parameters.temporary_object_1_name]))
        return psf_string_list

    def create_sphere_solid(self, hex_name_output, core_length, core_radius, divisions_circumferential, divisions_radial, region):
        """
        
        This function creates a simple 3D 1/24 sector solid sphere hex mesh.
        
        Supporting Methods Used:
            :method: `create_sphere_hex_block`
        :param hex_name_output:
        :param core_length:
        :param core_radius:
        :param divisions_circumferential:
        :param divisions_radial:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a solid sphere section
        """
        psf_string_list = []

        # create sphere "core" which is a curved cube
        psf_string_list.extend(self.create_sphere_hex_block(
            hex_name_output,
            0.0,
            0.0,
            core_length,
            core_radius,
            divisions_circumferential,
            divisions_radial,
            region))
        return psf_string_list

    def create_sphere_shell(self, hex_name_output, radius_inner, radius_outer, divisions_circumferential, divisions_radial, region):
        """
        
        This function creates a simple 3D 1/24 sector spherical shell hex mesh.
        
        Supporting Methods Used:
            :method: `create_sphere_hex_block`
        :param hex_name_output:
        :param radius_inner:
        :param radius_outer:
        :param divisions_circumferential:
        :param divisions_radial:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a spherical shell section
        """
        psf_string_list = []

        # create sphere shell that connects to core
        psf_string_list.extend(self.create_sphere_hex_block(
            hex_name_output,
            radius_inner,
            radius_inner,
            radius_outer,
            radius_outer,
            divisions_circumferential,
            divisions_radial,
            region))
        return psf_string_list

    def create_cube_hollow_sphere(self, hex_name_output, radius_inner, radius_outer, divisions_circumferential, divisions_radial, region):
        """
        
        This function creates a simple 3D 1/24 sector cuboid with a spherical hole hex mesh.
        
        Supporting Methods Used:
            :method: `create_sphere_hex_block`
        :param hex_name_output:
        :param radius_inner:
        :param radius_outer:
        :param divisions_circumferential:
        :param divisions_radial:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a cube section with spherical hole
        """
        psf_string_list = []

        # create hollow cube that connects to sphere
        psf_string_list.extend(self.create_sphere_hex_block(
            hex_name_output,
            radius_inner,
            radius_inner,
            radius_outer,
            "INFINITY",
            divisions_circumferential,
            divisions_radial,
            region))
        return psf_string_list

    def create_polyhedron(self, hex_name_output, point_1, point_2, point_3, point_4, extrude_height, divisions_circumferential, divisions_radial, divisions_axial, curvature_radii, region):
        """
        
        This function creates a an arbitrary polyhedron hex mesh.
        
        Supporting Methods Used:
            :method: `create_quad_patch`,
            :method: `simple_extrude`
        :param hex_name_output:
        :param point_1:
        :param point_2:
        :param point_3:
        :param point_4:
        :param extrude_height:
        :param divisions_circumferential:
        :param divisions_radial:
        :param divisions_axial:
        :param curvature_radii:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a polyhedron
        """
        psf_string_list = []

        # Create base quad patch
        qp_name = "qp_poly".upper()
        psf_string_list.extend(self.create_quad_patch(
            qp_name,
            point_1=point_1,
            point_2=point_2,
            point_3=point_3,
            point_4=point_4,
            divisions_from_1_and_3=divisions_radial,
            divisions_from_2_and_4=divisions_circumferential,
            curvature_radii=curvature_radii,
            region=region))

        # Extrude
        psf_string_list.extend(self.simple_extrude(
            hex_name_output,
            self.z_extruded_direction,
            length_of_extrusion=extrude_height,
            axial_divisions=divisions_axial,
            quad_patch=qp_name))
        return psf_string_list

    def create_circular_polyhedron(self, hex_name_output, point_1, point_2, point_3, point_4, extrude_height, divisions_circumferential, divisions_radial, divisions_axial, curvature_radii, region, mark=False, torus_radius=0.0):
        """
        
        This function creates a an arbitrary polyhedron hex mesh but with a spherical edge to be marked by.
        
        Supporting Methods Used:
            :method: `create_quad_patch`,
            :method: `create_torus_sel_vol`,
            :method: `mark`,
            :method: `simple_extrude`
        :param hex_name_output:
        :param point_1:
        :param point_2:
        :param point_3:
        :param point_4:
        :param extrude_height:
        :param divisions_circumferential:
        :param divisions_radial:
        :param divisions_axial:
        :param curvature_radii:
        :param region:
        :param mark:
        :param torus_radius:
        :return psf_string_list: psf formatted list of strings for creating a polyhedron with spherical edge
        """
        psf_string_list = []

        # Create base quad patch
        qp_name = "qp_poly".upper()
        psf_string_list.extend(self.create_quad_patch(
            qp_name,
            point_1=point_1,
            point_2=point_2,
            point_3=point_3,
            point_4=point_4,
            divisions_from_1_and_3=divisions_radial,
            divisions_from_2_and_4=divisions_circumferential,
            curvature_radii=curvature_radii,
            region=region))

        if mark:
            psf_string_list.extend(self.create_torus_sel_vol(
                selection_volume_name=self.parameters.selection_volume_temp_name,
                radius_major=torus_radius,
                radius_minor=(4 * self.parameters.geometry_selection_volume_threshold),
                circumferential_divisions_major=divisions_circumferential,
                circumferential_divisions_minor=divisions_circumferential))

            # create mark with selection volume above for bc's
            psf_string_list.extend(self.mark(
                object_name=qp_name,
                mark_name=self.parameters.mark_cladding_outer_surface_name,
                boundary_type=self.parameters.word_boundary_only,
                complete_or_partial=self.parameters.word_partially,
                inside_or_outside=self.parameters.word_inside,
                selection_volume_name=self.parameters.selection_volume_temp_name))

        # Extrude
        psf_string_list.extend(self.simple_extrude(
            hex_name_output,
            self.z_extruded_direction,
            length_of_extrusion=extrude_height,
            axial_divisions=divisions_axial,
            quad_patch=qp_name))
        return psf_string_list

    def create_layered_sphere_embedded_cube(self, hex_name_output, radius_list, divisions_circumferential, divisions_radial_list, symmetry_type, region_list, embed):
        """
        
        This function creates a full/partial (symmetry dependent) 3D sphere embedded in a cube hex mesh.
        
        Supporting Methods Used:
            :method: `create_sphere_solid`,
            :method: `create_sphere_hex_block`,
            :method: `create_sphere_shell`,
            :method: `create_cube_hollow_sphere`,
            :method: `auto_revolve`,
            :method: `snap_2_objects`
        :param hex_name_output:
        :param radius_list:
        :param divisions_circumferential:
        :param divisions_radial_list:
        :param symmetry_type: options:(eigth->`45`,quarter->`90`, half->`180`, none->`360`)
        :param region_list:
        :param embed:
        :return psf_string_list: psf formatted list of strings for creating a sphere embedded in a cube
        """
        psf_string_list = []

        num_layers = len(radius_list)
        divisions_circumferential_eigth = divisions_circumferential / 8

        # creating core of central sphere
        hex_name_core = "HEX_CORE"
        core_ratio = 0.3
        core_length = core_ratio * radius_list[0]
        core_radius = 2 * radius_list[0]
        center_region = region_list[0]

        # create core sphere
        psf_string_list.extend(self.create_sphere_solid(
            hex_name_core,
            core_length,
            core_radius,
            divisions_circumferential_eigth,
            divisions_radial_list[0],
            center_region))

        # create spherical shell that connects to core
        psf_string_list.extend(self.create_sphere_hex_block(
            hex_name_output,
            core_length,
            core_radius,
            radius_list[0],
            radius_list[0],
            divisions_circumferential_eigth,
            divisions_radial_list[0],
            center_region))

        # creating shell layers around sphere
        for layer in range(0, num_layers-1):
            layer_radius_inner = radius_list[layer]
            layer_radius_outer = radius_list[layer+1]
            layer_region = region_list[layer+1]
            layer_divisions_radial = divisions_radial_list[layer+1]
            
            # create single 1/24 spherical layer
            psf_string_list.extend(self.create_sphere_shell(
                hex_name_output + "_" + str(layer+1),
                layer_radius_inner,
                layer_radius_outer,
                divisions_circumferential_eigth,
                layer_divisions_radial,
                layer_region))

            # Snapping spherical layers together
            psf_string_list.extend(self.snap_2_objects(
                hex_name_output,
                hex_name_output + "_" + str(layer+1),
                hex_name_output))

            # delete 
            psf_string_list.append(self.delete_list_of_objects(
                [hex_name_output + "_" + str(layer+1)]))
        
        # adding support for emdedding particle
        if embed:
            # creating 1/24 section hollow cube for sphere
            hex_name_cube = "HEX_CUBE"
            psf_string_list.extend(self.create_cube_hollow_sphere(
                hex_name_cube,
                layer_radius_outer,
                radius_list[-1]*self.parameters.geometry_triso_graphite_block_scalar,
                divisions_circumferential_eigth,
                divisions_radial_list[-1],
                region_list[-1]))

            # Snapping hollow cube to resulting mesh
            psf_string_list.extend(self.snap_2_objects(
                hex_name_cube,
                hex_name_output,
                hex_name_output))
            mesh_delete = [hex_name_core, hex_name_cube]
        else:
            mesh_delete = [hex_name_core]

        # auto revolve 1/24 section mesh to get 1/8 section mesh
        shell_quant = 2
        shell_axis_of_rotation = [1.0, 1.0, 1.0]
        shell_degrees_rotated = 120
        psf_string_list.extend(self.auto_revolve(
            hex_name_output,
            shell_quant,
            shell_axis_of_rotation,
            shell_degrees_rotated))

        # Snapping solid sphere to resulting mesh
        psf_string_list.extend(self.snap_2_objects(
            hex_name_core,
            hex_name_output,
            hex_name_output))

        # delete 
        psf_string_list.append(self.delete_list_of_objects(mesh_delete))

        # Perform model symmetry operations
        psf_string_list.extend(self.auto_rotate_sphere_hex_block(
            hex_name_output, symmetry_type))
        return psf_string_list

class Rz2dRod(QuadPatch):
    """

    This class inherits the class `QuadPatch` for creating variants of a 2D RZ-type Rod
    
    Class inherits:
        :class: `QuadPatch`
    """

    def __init__(self, input_file_path=None):
        super(Rz2dRod, self).__init__(input_file_path)
        self.case_dimension_type = "2D"

    def create_fuel(self, height, axial_divisions, region=None, result_qp_name=None):
        """
        
        This function creates a 2D RZ fuel quad patch.
        
        Supporting Methods Used:
            :method: `rectangular_quad_patch`
        :param result_qp_name:
        :param height:
        :param axial_divisions:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a quad patch in the fuel region
        """
        if region is None:
            region = self.parameters.region_fuel
        if result_qp_name is None:
            result_qp_name = self.parameters.qp_fuel_name
        psf_string_list = []

        psf_string_list.extend(self.rectangular_quad_patch(
            self.parameters.geometry_fuel_radius,
            height,
            self.parameters.divisions_radial_fuel,
            axial_divisions,
            region,
            result_qp_name))
        return psf_string_list

    def create_gap(self, height, axial_divisions, region=None, result_qp_name=None):
        """
        
        This function creates a 2D RZ gap quad patch.
        
        Supporting Methods Used:
            :method: `translate`,
            :method: `rectangular_quad_patch`
        :param result_qp_name:
        :param height:
        :param axial_divisions:
        :param region:
        :return psf_string_list: psf formatted list of strings for creating a quad patch in the gap region
        """
        if region is None:
            region = self.parameters.region_fuel_cladding_gap
        if result_qp_name is None:
            result_qp_name = self.parameters.qp_fuel_cladding_gap_name
        psf_string_list = []

        psf_string_list.extend(self.rectangular_quad_patch(
            self.parameters.geometry_gap_thickness,
            height,
            self.parameters.divisions_radial_gap,
            axial_divisions,
            region,
            result_qp_name)
        )

        psf_string_list.extend(self.translate(
            result_qp_name,
            "x",
            self.parameters.geometry_fuel_radius)
        )

        return psf_string_list

    def create_cladding(self, thickness_list_in, divisions_radial_list, divisions_axial, height=None, region=None, name=None):
        """
        
        This function creates a 2D RZ multi layered cladding quad patch.
        
        Supporting Methods Used:
            :method: `translate`,
            :method: `rectangular_quad_patch`
        :param thickness_list_in:
        :param divisions_radial_list:
        :param divisions_axial:
        :param height:
        :param region:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating quad patch('s) in the cladding region('s)
        """
        
        if height is None:
            height = self.parameters.geometry_fuel_height_discrete
        if region is None:
            region = self.parameters.region_cladding
        if name is None:
            name = self.parameters.qp_cladding_name

        psf_string_list = []

        # supporting multi-layer cladding
        thickness_list = copy.deepcopy(thickness_list_in)
        num_layers = len(thickness_list)

        # inserting zero for indexing purposes
        thickness_list.insert(0, 0.0)

        # creating n number of cladding quad patch's
        cladding_inner_radius = self.parameters.geometry_cladding_inner_radius
        if thickness_list is not None:
            for thickness in range(0, num_layers):
                thickness_count = thickness + 1
                object_cladding_name = name + "_" + str(thickness_count)

                # allow for region(s) to be same or different 
                if region==self.parameters.region_cladding:
                    region_cladding = region + "_" + str(thickness_count)
                else:
                    region_cladding = region

                # updating radii on each iteration
                cladding_outer_radius = cladding_inner_radius + thickness_list[thickness + 1]

                # create quad patch
                psf_string_list.extend(self.rectangular_quad_patch(
                    thickness_list[thickness + 1],
                    height,
                    divisions_radial_list[thickness],
                    divisions_axial,
                    region_cladding,
                    object_cladding_name)
                )

                # translate to correct position
                psf_string_list.extend(self.translate(
                    object_cladding_name,
                    "x",
                    cladding_inner_radius)
                )

                # update radii
                cladding_inner_radius = cladding_outer_radius
        thickness_list = []
        return psf_string_list

    def create_unit_rod_assembly(self, height, axial_divisions, fuel_region=None, gap_region=None, cladding_region=None, result_mesh_name=None):
        """
        
        This function creates a 2D RZ unit rod assembly quad patch.
        
        Supporting Methods Used:
            :method: `create_fuel`,
            :method: `create_gap`,
            :method: `create_cladding`,
            :method: `snap_list_objects`,
            :method: `delete_list_of_objects`,
            :method: `rotate`
        :param height:
        :param axial_divisions:
        :param fuel_region:
        :param gap_region:
        :param cladding_region:
        :param result_mesh_name:
        :return psf_string_list: psf formatted list of strings for creating a unit rod quad patch
        """
        

        # updating default values
        if fuel_region is None:
            fuel_region = self.parameters.region_fuel
        if gap_region is None:
            gap_region = self.parameters.region_fuel_cladding_gap
        if cladding_region is None:
            cladding_region = self.parameters.region_cladding
        if result_mesh_name is None or result_mesh_name==self.parameters.qp_unit_rod_name or result_mesh_name==self.parameters.qp_rod_name:
            flag_assembly=False
            mesh_name_fuel = self.parameters.qp_fuel_name
            mesh_name_gap = self.parameters.qp_fuel_cladding_gap_name
            mesh_name_cladding = self.parameters.qp_cladding_name
            if result_mesh_name is None:
                result_mesh_name = self.parameters.qp_rod_name
        else:
            flag_assembly=True
            mesh_name_fuel = result_mesh_name+"_"+self.parameters.region_fuel
            mesh_name_gap = result_mesh_name+"_"+self.parameters.region_fuel_cladding_gap
            mesh_name_cladding = result_mesh_name+"_"+self.parameters.region_cladding

        # Multi-layer cladding support
        if len(self.parameters.geometry_cladding_thickness_list) > 1:
            thickness_list = self.parameters.geometry_cladding_thickness_list
            radial_divisions_list = self.parameters.divisions_radial_cladding_list
        else:
            thickness_list = [self.parameters.geometry_cladding_thickness_list[0]]
            radial_divisions_list = [self.parameters.divisions_radial_cladding_list[0]]

        psf_string_list = []

        # Fuel region
        psf_string_list.extend(self.create_fuel(
            height,
            axial_divisions,
            region=fuel_region,
            result_qp_name=mesh_name_fuel))

        # Gap region
        psf_string_list.extend(self.create_gap(
            height,
            axial_divisions,
            region=gap_region,
            result_qp_name=mesh_name_gap))

        # Cladding region
        psf_string_list.extend(self.create_cladding(
            thickness_list_in=thickness_list,
            divisions_radial_list=radial_divisions_list,
            divisions_axial=axial_divisions,
            height=height,
            region=cladding_region,
            name=mesh_name_cladding))

        # auto snapping
        assembly_list = []
        # snap component wise assemblies, not everything thats been created yet
        if not flag_assembly:
            for key in self.creation_dict_quad_patch:
                assembly_list.append(key)
        else:
            for key in self.creation_dict_quad_patch:
                if result_mesh_name in key:
                    assembly_list.append(key)

        psf_string_list.extend(self.snap_list_objects(
                object_list=assembly_list,
                child_object_name=result_mesh_name))

        # deleting 
        psf_string_list.append(self.delete_list_of_objects(object_list=assembly_list))

        # bring to xz plane
        psf_string_list.extend(self.rotate(
            result_mesh_name,
            90, 
            "x"))
        return psf_string_list

    def create_fuel_fuel_gap_assembly(self):
        """
        
        This function creates a 2D RZ fuel-fuel-gap quad patch.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating a fuel-fuel-gap quad patch
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=self.parameters.geometry_fuel_fuel_gap_height,
            axial_divisions=self.parameters.divisions_axial_fuel_fuel_gap,
            fuel_region=self.parameters.region_fuel_fuel_gap,
            result_mesh_name=self.parameters.qp_fuel_fuel_gap_assembly_name)
        return psf_string_list

    def create_plenum_assembly(self, height, divisions, name):
        """
        
        This function creates a 2D RZ plenum assembly quad patch.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :param height:
        :param divisions:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating a plenum assembly quad patch
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=height,
            axial_divisions=divisions,
            fuel_region=self.parameters.region_plenum,
            gap_region=self.parameters.region_plenum,
            cladding_region=self.parameters.region_cladding,
            result_mesh_name=name)
        return psf_string_list

    def create_end_cap_assembly(self):
        """
        
        This function creates a 2D RZ end-cap assembly quad patch.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating an end-cap assembly quad patch
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=self.parameters.geometry_end_cap_thickness,
            axial_divisions=self.parameters.divisions_axial_end_cap,
            fuel_region=self.parameters.region_end_cap,
            gap_region=self.parameters.region_end_cap,
            cladding_region=self.parameters.region_end_cap,
            result_mesh_name=self.parameters.qp_end_cap_top_name)
        return psf_string_list

    def create_simple_smeared_rod(self):
        """
        
        This function creates a 2D RZ simple smeared rod quad patch.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating a full-length smeared rod quad patch
        """

        psf_string_list = []
        # Make original assemblies
        psf_string_list.extend(self.create_unit_rod_assembly(
            height=self.parameters.geometry_fuel_height_smeared,
            axial_divisions=self.parameters.divisions_axial_fuel_smeared)
        )
        return psf_string_list

    def create_detailed_smeared_rod(self):
        """
        
        This function creates a 2D RZ smeared rod with end-caps and plenum quad patch.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`,
            :method: `create_plenum_assembly`,
            :method: `create_end_cap_assembly`,
            :method: `auto_translate_assemblies`,
            :method: `copy`,
            :method: `create_header_comment`
        :return psf_string_list: psf formatted list of strings for creating a full-length smeared rod with plenum and end-caps quad patch
        """
        self.creation_dict_hex_mesh

        psf_string_list = []
        # smeared rod assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Smeared Fuel rod assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_unit_rod_assembly(
            self.parameters.geometry_fuel_height_smeared,
            self.parameters.divisions_axial_fuel_smeared))

        # Top plenum
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_top,
            divisions=self.parameters.divisions_axial_plenum_top,
            name=self.parameters.qp_plenum_top_name))

        # Bottom plenum
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_bottom,
            divisions=self.parameters.divisions_axial_plenum_bottom,
            name=self.parameters.qp_plenum_bottom_name))

        # Top end cap
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_end_cap_assembly())

        # Bottom end cap
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.append(self.copy(
            self.parameters.qp_end_cap_top_name,
            self.parameters.qp_end_cap_bottom_name))

        # auto translate and snap all assemblies to correct position and delete parent objects
        psf_string_list.extend(self.auto_translate_assemblies(
            object_dictionary=self.creation_dict_quad_patch,
            position_dictionary=self.parameters.geometry_mesh_positions_dict,
            result_object_name=self.parameters.qp_rod_name))
        return psf_string_list

    def create_bc_sets_for_2drz_rod(self):
        """
        
        This function creates boundary condition element and node sets for 2D R-Z `create_simple_smeared_rod`.
        
        Supporting Methods Used:
            :method: `create_cube_sel_vol`,
            :method: `create_node_set_from_selection_volume`,
            :method: `delete_list_of_objects`,
            :method: `create_element_set_from_mark`
        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_simple_smeared_rod
        """

        psf_string_list = []

        # Bottom surface elements

        # Create cube to mark bottom of rod for fixed bc
        start_point = [
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold
        ]
        extrude_vec = [
            self.parameters.geometry_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create bottom surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects(
            [self.parameters.selection_volume_temp_name]))

        # Outer cladding surface elements
        psf_string_list.append(self.create_element_set_from_mark(
            self.parameters.elements_cladding_outer_surface,
            self.parameters.physical_mesh_name,
            self.parameters.word_with,
            self.parameters.mark_cladding_outer_surface_name))
        return psf_string_list

    def create_bc_sets_for_detailed_2drz_rod(self):
        """
        
        This function creates boundary condition element and node sets for 2D R-Z `create_detailed_smeared_rod`.
        
        Supporting Methods Used:
            :method: `create_single_node`,
            :method: `create_cube_sel_vol`,
            :method: `create_node_set_from_selection_volume`,
            :method: `create_element_set_from_selection_volume`,
            :method: `delete_list_of_objects`,
            :method: `create_element_set_from_mark`
        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_detailed_smeared_rod
        """
        psf_string_list = []

        ############################## Fixed ##############################
        psf_string_list.append(self.create_single_node(
            node_name=self.parameters.nodes_origin,
            point=self.parameters.position_coordinate_origin))
            
        psf_string_list.append(self.create_single_node(
            node_name=self.parameters.nodes_clad_od_pos_x,
            point=self.parameters.position_coordinate_x))

        psf_string_list.append(self.create_single_node(
            node_name=self.parameters.nodes_clad_od_pos_y,
            point=self.parameters.position_coordinate_y))

        ############################## Bottom end cap surface elements ##############################

        # Create cube to mark bottom of rod for fixed bc
        # Create cube at bottom of rod that extudes upwards into rod to mark bottom surface
        start_point = [
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_outside_bottom_rod_height
        ]
        extrude_vec = [
            self.parameters.geometry_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]
        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create bottom end cap surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ############################## Bottom fuel surface elements ##############################

        # Create cube to mark bottom of fuel for fixed bc
        # Create cube at bottom of rod that extudes upwards into rod to mark bottom surface
        start_point = [
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold
        ]
        extrude_vec = [
            (self.parameters.geometry_fuel_radius + self.parameters.geometry_times_2_selection_volume_threshold),
            self.parameters.geometry_times_2_selection_volume_threshold,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create bottom end cap surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_fuel,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ############################## Top surface elements ##############################

        # Create cube at top of rod that extudes downwards into rod to mark top surface
        start_point = [
            self.parameters.geometry_negative_selection_volume_threshold,
            self.parameters.geometry_negative_selection_volume_threshold,
            (self.parameters.geometry_outside_top_rod_height-self.parameters.geometry_selection_volume_threshold)
        ]
        extrude_vec = [
            self.parameters.geometry_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create top surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_top_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))
        
        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ############################## Outer cladding surface elements ##############################

        # Create cube at top of rod that extudes downwards into rod to mark top surface
        start_point = [
            self.parameters.geometry_inside_outer_clad_radius,
            self.parameters.geometry_negative_selection_volume_threshold,
            (self.parameters.geometry_outside_bottom_rod_height+self.parameters.geometry_times_2_selection_volume_threshold)
        ]

        extrude_vec = [
            self.parameters.geometry_times_2_selection_volume_threshold,
            self.parameters.geometry_times_2_selection_volume_threshold,
            (self.parameters.geometry_outside_top_rod_height -self.parameters.geometry_outside_bottom_rod_height - 2*self.parameters.geometry_times_2_selection_volume_threshold)
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create selection volume to mark outer surface for bc's    
        psf_string_list.append(self.create_element_set_from_selection_volume(
            element_set_name=self.parameters.elements_cladding_outer_surface,
            boundary_or_continuum=self.parameters.set_type_element_boundary,
            phys_mesh=self.parameters.physical_mesh_name,
            coverage_flag=self.parameters.word_partially,
            side_flag=self.parameters.word_inside,
            selection_volume_name=self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))
        return psf_string_list

class Rtheta2D(QuadPatch):
    """

    This class inherits the class `QuadPatch` for creating variants of a 2D RTheta-type Rod
    
    Class inherits:
        :class: `QuadPatch`
    """

    def __init__(self, input_file_path=None):
        super(Rtheta2D, self).__init__(input_file_path)
        self.case_dimension_type = "2D"

    def create_fuel(self):
        """
        
        This function creates a 2D R-Theta fuel quad patch.
        
        Supporting Methods Used:
            :method: `disk_quad_patch`
        :return psf_string_list: psf formatted list of strings for creating a quad patch in the fuel region
        """
        psf_string_list = self.disk_quad_patch(
            self.parameters.geometry_fuel_radius,
            self.parameters.divisions_radial_fuel,
            self.parameters.divisions_circumferential,
            self.parameters.model_symmetry_type,
            self.parameters.region_fuel,
            self.parameters.qp_fuel_name)
        return psf_string_list

    def create_gap(self):
        """
        
        This function creates a 2D R-Theta gap quad patch.
        
        Supporting Methods Used:
            :method: `partial_ring_quad_patch`
        :return psf_string_list: psf formatted list of strings for creating a quad patch in the gap region
        """

        psf_string_list = self.partial_ring_quad_patch(
            self.parameters.geometry_fuel_radius,
            self.parameters.geometry_cladding_inner_radius,
            self.parameters.divisions_radial_gap,
            self.parameters.divisions_circumferential,
            self.parameters.region_fuel_cladding_gap,
            self.parameters.model_symmetry_type,
            self.parameters.qp_fuel_cladding_gap_name)
        return psf_string_list

    def create_cladding(self, thickness_list_in, divisions_list, defect=None, region=None, name=None):
        """
        
        This function creates a 2D R-Theta cladding quad patch (which also supports MPS).
        
        Supporting Methods Used:
            :method: `partial_ring_quad_patch`
        :param thickness_list_in:
        :param divisions_list:
        :param defect:
        :param region:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating quad patch('s) in the cladding region('s)
        """
        if region is None:
            region = self.parameters.region_cladding
        if name is None:
            name = self.parameters.qp_cladding_name

        psf_string_list = []

        # supporting multi-layer cladding
        thickness_list = copy.deepcopy(thickness_list_in)
        num_layers = len(thickness_list)

        # inserting zero for indexing purposes
        thickness_list.insert(0, 0.0)

        # creating n number of cladding quad patch's
        cladding_inner_radius = self.parameters.geometry_cladding_inner_radius
        if thickness_list is not None:
            for thickness in range(0, num_layers):
                thickness_count = thickness + 1
                object_cladding_name = name + "_" + str(thickness_count)

                # allow for region(s) to be same or different
                if region==self.parameters.region_cladding:
                    region_cladding = region + "_" + str(thickness_count)
                else:
                    region_cladding = region

                # updating radii on each iteration
                cladding_outer_radius = cladding_inner_radius + thickness_list[thickness + 1]
                
                # creating non defected ring as default request
                if defect is None:
                    psf_string_list.extend(self.partial_ring_quad_patch(
                        cladding_inner_radius,
                        cladding_outer_radius,
                        divisions_list[thickness],
                        self.parameters.divisions_circumferential,
                        region_cladding,
                        self.parameters.model_symmetry_type,
                        object_cladding_name))

                # support mps defect if requested
                elif defect=="MPS":
                    cladding_radii = [cladding_inner_radius, cladding_outer_radius]
                    points_cladding, curvature_radii_cladding = self.calc_mps_points(region="CLADDING", radii=cladding_radii)
                    psf_string_list.extend(self.create_chip_ring_region(
                        object_cladding_name,
                        points_cladding,
                        cladding_inner_radius,
                        cladding_outer_radius,
                        curvature_radii_cladding,
                        divisions_list[thickness],
                        self.parameters.divisions_circumferential,
                        region_cladding,
                        self.parameters.model_symmetry_type))

                # update radii
                cladding_inner_radius = cladding_outer_radius

        # create selection volume to mark outer surface for bc's
        div_minor=max(int(3), int(self.parameters.divisions_circumferential / 4))
        psf_string_list.extend(self.create_torus_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            radius_major=self.parameters.geometry_outside_outer_clad_radius,
            radius_minor=(4 * self.parameters.geometry_selection_volume_threshold),
            circumferential_divisions_major=self.parameters.divisions_circumferential,
            circumferential_divisions_minor=div_minor))

        # create mark with selection volume above for bc's
        psf_string_list.extend(self.mark(
            object_name=object_cladding_name,
            mark_name=self.parameters.mark_cladding_outer_surface_name,
            boundary_type=self.parameters.word_boundary_only,
            complete_or_partial=self.parameters.word_completely,
            inside_or_outside=self.parameters.word_inside,
            selection_volume_name=self.parameters.selection_volume_temp_name))
        return psf_string_list

    def create_rod_disk(self):
        """
        
        This function creates a 2D R-Theta rod quad patch.
        
        Supporting Methods Used:
            :method: `create_fuel`,
            :method: `create_gap`,
            :method: `create_cladding`,
            :method: `snap_list_objects`,
            :method: `delete_list_of_objects`
        :return psf_string_list: psf formatted list of strings for creating a rod quad patch
        """

        psf_string_list = []
        if len(self.parameters.geometry_cladding_thickness_list) > 1:
            thickness_list = self.parameters.geometry_cladding_thickness_list
            divisions_list = self.parameters.divisions_radial_cladding_list
        else:
            thickness_list = [self.parameters.geometry_cladding_thickness_list[0]]
            divisions_list = [self.parameters.divisions_radial_cladding_list[0]]

        psf_string_list.extend(self.create_fuel())
        psf_string_list.extend(self.create_gap())
        psf_string_list.extend(self.create_cladding(thickness_list, divisions_list))
        psf_string_list.extend(self.snap_list_objects(
            self.creation_dict_quad_patch,
            self.parameters.qp_rod_name))

        # deleting
        psf_string_list.extend(self.delete_list_of_objects(
            self.creation_dict_quad_patch,
            self.parameters.qp_rod_name))
        return psf_string_list

    def calc_mps_points(self, region, radii):
        """
        
        :param region:
        :param radii:
        :return 1: points, 2: curvature_radii
        """

        # mps length/depth
        length_chip = self.parameters.geometry_fuel_mps_length
        radius_fuel = self.parameters.geometry_fuel_radius

        # angles
        theta_chip = math.asin(length_chip / (2 * radius_fuel))
        theta_desired = math.radians(11.25)
        theta_1 = math.radians(45)
        theta_5 = math.radians(90)
        theta_3 = theta_5 - theta_chip
        theta_2 = theta_1 + 0.5 * (theta_3 - theta_1)
        theta_4 = theta_3 + 0.5 * (theta_5 - theta_3)
        theta_6 = theta_1 + 2 * theta_desired

        if region=="FUEL" or region=="GAP":
            # MPS radius
            radius_chip = math.sqrt(radius_fuel ** 2 - (length_chip / 2) ** 2)
            radius_center = radii[0]
            radius_fuel = radii[1]
            if region=="FUEL":
                # verticies for both quads
                points = [
                    [
                        [radius_center * math.cos(theta_1), radius_center * math.sin(theta_1)],
                        [radius_fuel * math.cos(theta_1), radius_fuel * math.sin(theta_1)],
                        [radius_fuel * math.cos(theta_3), radius_fuel * math.sin(theta_3)],
                        [radius_center * math.cos(theta_6), radius_center * math.sin(theta_6)]
                    ],
                    [
                        [radius_center * math.cos(theta_6), radius_center * math.sin(theta_6)],
                        [radius_fuel * math.cos(theta_3), radius_fuel * math.sin(theta_3)],
                        [radius_chip * math.cos(theta_5), radius_chip * math.sin(theta_5)],
                        [radius_center * math.cos(theta_5), radius_center * math.sin(theta_5)]
                    ]
                    ]
                # curvatures along each edge for both quads
                curvature_radii = [
                    [0.0, radius_fuel, 0.0, -1 * radius_center],
                    [0.0, 0.0, 0.0, -1 * radius_center]
                    ]
            elif region=="GAP":
                radius_fuel = radii[0]
                radius_clad_ir = radii[1]
                # verticies for both quads
                points = [
                    [
                        [radius_fuel * math.cos(theta_1), radius_fuel * math.sin(theta_1)],
                        [radius_clad_ir * math.cos(theta_1), radius_clad_ir * math.sin(theta_1)],
                        [radius_clad_ir * math.cos(theta_3), radius_clad_ir * math.sin(theta_3)],
                        [radius_fuel * math.cos(theta_3), radius_fuel * math.sin(theta_3)]
                    ],
                    [
                        [radius_fuel * math.cos(theta_3), radius_fuel * math.sin(theta_3)],
                        [radius_clad_ir * math.cos(theta_3), radius_clad_ir * math.sin(theta_3)],
                        [radius_clad_ir * math.cos(theta_5), radius_clad_ir * math.sin(theta_5)],
                        [radius_chip * math.cos(theta_5), radius_chip * math.sin(theta_5)]
                    ]
                    ]
                # curvatures along each edge for both quads
                curvature_radii = [
                    [0.0, radius_clad_ir, 0.0, -1 * radius_fuel],
                    [0.0, radius_clad_ir, 0.0, 0.0]
                    ]
        elif region=="CLADDING":
            radius_clad_ir = radii[0]
            radius_clad_or = radii[1]
            # verticies for both quads
            points = [
                [
                    [radius_clad_ir * math.cos(theta_1), radius_clad_ir * math.sin(theta_1)],
                    [radius_clad_or * math.cos(theta_1), radius_clad_or * math.sin(theta_1)],
                    [radius_clad_or * math.cos(theta_3), radius_clad_or * math.sin(theta_3)],
                    [radius_clad_ir * math.cos(theta_3), radius_clad_ir * math.sin(theta_3)]
                ],
                [
                    [radius_clad_ir * math.cos(theta_3), radius_clad_ir * math.sin(theta_3)],
                    [radius_clad_or * math.cos(theta_3), radius_clad_or * math.sin(theta_3)],
                    [radius_clad_or * math.cos(theta_5), radius_clad_or * math.sin(theta_5)],
                    [radius_clad_ir * math.cos(theta_5), radius_clad_ir * math.sin(theta_5)]
                ]
                ]
            # curvatures along each edge for both quads
            curvature_radii = [
                [0.0, radius_clad_or, 0.0, -1 * radius_clad_ir],
                [0.0, radius_clad_or, 0.0, -1 * radius_clad_ir]
                ]
        return points, curvature_radii

    def create_fuel_mps(self):
        """
        
        This function creates a 2D R-Theta fuel quad patch with MPS.
        
        Supporting Methods Used:
            :method: `center_disk`,
            :method: `create_chip_ring_region`,
            :method: `calc_mps_points`,
            :method: `rotate`
        :return psf_string_list: psf formatted list of strings for creating a fuel with mps quad patch
        """
        
        psf_string_list = []
        radius_fuel = self.parameters.geometry_fuel_radius
        radius_center = radius_fuel / 4
        fuel_radii = [radius_center, radius_fuel]

        # Center Fuel disk
        psf_string_list.extend(self.center_disk(
            radius_center,
            self.parameters.divisions_circumferential,
            self.parameters.region_fuel,
            self.parameters.model_symmetry_type,
            self.parameters.qp_fuel_center_name))

        # only supports half symmetry
        if self.parameters.model_symmetry_type == self.parameters.geometry_half_symmetry:
            psf_string_list.extend(self.rotate(
                self.parameters.qp_fuel_center_name,
                (-90.0), 
                "z"))

        # Fuel mps
        points_fuel, curvature_radii_fuel = self.calc_mps_points(region="FUEL", radii=fuel_radii)
        psf_string_list.extend(self.create_chip_ring_region(
            self.parameters.qp_fuel_name,
            points_fuel,
            (self.parameters.geometry_fuel_radius / 4),
            self.parameters.geometry_fuel_radius,
            curvature_radii_fuel,
            self.parameters.divisions_radial_fuel,
            self.parameters.divisions_circumferential,
            self.parameters.region_fuel,
            self.parameters.model_symmetry_type))
        return psf_string_list

    def create_gap_mps(self):
        """
        
        This function creates a 2D R-Theta gap quad patch with MPS interface.
        
        Supporting Methods Used:
            :method: `create_chip_ring_region`,
            :method: `calc_mps_points`
        :return psf_string_list: psf formatted list of strings for creating a gap with mps quad patch
        """
        psf_string_list = []
        radius_fuel = self.parameters.geometry_fuel_radius
        radius_clad_ir = self.parameters.geometry_cladding_inner_radius
        gap_radii = [radius_fuel, radius_clad_ir]
        
        # gap mps
        points_gap, curvature_radii_gap = self.calc_mps_points(region="GAP", radii=gap_radii)
        psf_string_list.extend(self.create_chip_ring_region(
            self.parameters.qp_fuel_cladding_gap_name,
            points_gap,
            self.parameters.geometry_fuel_radius,
            self.parameters.geometry_cladding_inner_radius,
            curvature_radii_gap,
            self.parameters.divisions_radial_gap,
            self.parameters.divisions_circumferential,
            self.parameters.region_fuel_cladding_gap,
            self.parameters.model_symmetry_type))
        return psf_string_list

    def create_mps(self):
        """
        
        This function creates a 2D R-Theta rod quad patch with MPS.
        
        Supporting Methods Used:
            :method: `create_fuel_mps`,
            :method: `create_gap_mps`,
            :method: `create_cladding`,
            :method: `snap_list_objects`,
            :method: `delete_list_of_objects`
        :return psf_string_list: psf formatted list of strings for creating a rod with mps quad patch
        """
        
        psf_string_list = []
        if len(self.parameters.geometry_cladding_thickness_list) > 1:
            thickness_list = self.parameters.geometry_cladding_thickness_list
            divisions_list = self.parameters.divisions_radial_cladding_list
        else:
            thickness_list = [self.parameters.geometry_cladding_thickness_list[0]]
            divisions_list = [self.parameters.divisions_radial_cladding_list[0]]

        # Fuel mps
        psf_string_list.extend(self.create_fuel_mps())

        # gap mps
        psf_string_list.extend(self.create_gap_mps())

        # cladding mps
        # support multi-layer cladding
        psf_string_list.extend(self.create_cladding(
            thickness_list_in=thickness_list,
            divisions_list=divisions_list, 
            defect="MPS"))

        # snapping
        psf_string_list.extend(self.snap_list_objects(
            self.creation_dict_quad_patch,
            self.parameters.qp_rod_name))

        # deleting 
        psf_string_list.append(self.delete_list_of_objects(
            self.creation_dict_quad_patch,
            self.parameters.qp_rod_name))
        return psf_string_list

    def create_bc_sets_for_2d_rtheta_rod(self):
        """
        
        This function creates boundary condition element and node sets for `create_rod_disk`.
        
        Supporting Methods Used:
            :method: `create_single_node`,
            :method: `create_element_set_from_mark`
        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_rod_disk
        """

        psf_string_list = []

        # Fixed
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_origin,
            self.parameters.position_coordinate_origin
        )
        )
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_x,
            self.parameters.position_coordinate_x
        )
        )
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_y,
            self.parameters.position_coordinate_y
        )
        )

        # Outer cladding surface elements
        psf_string_list.append(self.create_element_set_from_mark(
            self.parameters.elements_cladding_outer_surface,
            self.parameters.physical_mesh_name,
            self.parameters.word_with,
            self.parameters.mark_cladding_outer_surface_name
        )
        )
        return psf_string_list

class Rtheta3D(HexMesh):
    """

    This class inherits the class `HexMesh` for creating variants of a 3D RTheta-type Rod.
    
    Class inherits:
        :class: `HexMesh`
    """

    def __init__(self, input_file_path=None):
        super(Rtheta3D, self).__init__(input_file_path)
        self.case_dimension_type = "3D"

    def create_unit_fuel_pellet(self, divisions_axial, height=None, region=None, name=None):
        """
        
        This function creates a 3D R-Theta-Z fuel hex mesh.
        
        Supporting Methods Used:
            :method: `create_cylinder`,
            :method: `rotate`
        :param divisions_axial:
        :param height:
        :param region:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating a hex mesh in the fuel region
        """

        if height is None:
            height = self.parameters.geometry_fuel_height_discrete
        if region is None:
            region = self.parameters.region_fuel
        if name is None:
            name = self.parameters.hex_fuel_name
        
        psf_string_list = []
        
        psf_string_list.extend(self.create_cylinder(
            radius=self.parameters.geometry_fuel_radius,
            height=height,
            radial_divisions=self.parameters.divisions_radial_fuel,
            circumferential_divisions=self.parameters.divisions_circumferential,
            axial_divisions=divisions_axial,
            symmetry_type=self.parameters.model_symmetry_type,
            region=region,
            hex_name=name))
        
        # Aligning orientation
        psf_string_list.extend(self.rotate(
            name,
            (-11.25 / 2), 
            "z"))
        return psf_string_list

    def create_unit_fuel_cladding_gap(self, divisions_axial, height=None, region=None, name=None):
        """
        
        This function creates a 3D R-Theta-Z gap hex mesh.
        
        Supporting Methods Used:
            :method: `create_pipe`
        :param divisions_axial:
        :param height:
        :param region:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating a hex mesh in the gap region
        """

        if height is None:
            height = self.parameters.geometry_fuel_height_discrete
        if region is None:
            region = self.parameters.region_fuel_cladding_gap
        if name is None:
            name = self.parameters.hex_fuel_cladding_gap_name

        psf_string_list = self.create_pipe(
            inner_radius=self.parameters.geometry_fuel_radius,
            outer_radius=self.parameters.geometry_cladding_inner_radius,
            height=height,
            radial_divisions=self.parameters.divisions_radial_gap,
            circumferential_divisions=self.parameters.divisions_circumferential,
            axial_divisions=divisions_axial,
            symmetry_type=self.parameters.model_symmetry_type,
            region=region,
            hex_name=name)
        return psf_string_list

    def create_unit_cladding(self, thickness_list_in, divisions_radial_list, divisions_axial, height=None, region=None, name=None):
        """
        
        This function creates a 3D R-Theta-Z cladding hex mesh.
        
        Supporting Methods Used:
            :method: `create_pipe`
        :param thickness_list_in:
        :param divisions_radial_list:
        :param divisions_axial:
        :param height:
        :param region:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating a hex mesh in the cladding region
        """

        if height is None:
            height = self.parameters.geometry_fuel_height_discrete
        if region is None:
            region = self.parameters.region_cladding
        if name is None:
            name = self.parameters.hex_cladding_name

        psf_string_list = []

        # supporting multi-layer cladding
        thickness_list = copy.deepcopy(thickness_list_in)
        num_layers = len(thickness_list)

        # inserting zero for indexing purposes
        thickness_list.insert(0, 0.0)

        # creating n number of cladding pipes
        cladding_inner_radius = self.parameters.geometry_cladding_inner_radius
        if thickness_list is not None:
            for thickness in range(0, num_layers):
                thickness_count = thickness + 1
                object_cladding_name = name + "_" + str(thickness_count)

                # allow for region(s) to be same or different
                if region==self.parameters.region_cladding:
                    region_cladding = region + "_" + str(thickness_count)
                else:
                    region_cladding = region

                # updating radii on each iteration
                cladding_outer_radius = cladding_inner_radius + thickness_list[thickness + 1]

                # creating pipe
                psf_string_list.extend(self.create_pipe(
                    inner_radius=cladding_inner_radius,
                    outer_radius=cladding_outer_radius,
                    height=height,
                    radial_divisions=divisions_radial_list[thickness],
                    circumferential_divisions=self.parameters.divisions_circumferential,
                    axial_divisions=divisions_axial,
                    symmetry_type=self.parameters.model_symmetry_type,
                    region=region_cladding,
                    hex_name=object_cladding_name)
                )
                # updating radii
                cladding_inner_radius = cladding_outer_radius
        thickness_list = []
        return psf_string_list

    def create_unit_rod_assembly(self, height=None, divisions_axial=None, fuel_region=None, gap_region=None, cladding_region=None, result_mesh_name=None):
        """
        
        This function creates a 3D R-Theta-Z rod hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_fuel_pellet`,
            :method: `create_unit_fuel_cladding_gap`,
            :method: `create_unit_cladding`,
            :method: `snap_list_objects`,
            :method: `delete_list_of_objects`
        :param height:
        :param divisions_axial:
        :param fuel_region:
        :param gap_region:
        :param cladding_region:
        :param result_mesh_name:
        :return psf_string_list: psf formatted list of strings for creating a unit rod hex mesh
        """

        # updating default values
        if height is None:
            height = self.parameters.geometry_fuel_height_discrete
        if fuel_region is None:
            fuel_region = self.parameters.region_fuel
        if gap_region is None:
            gap_region = self.parameters.region_fuel_cladding_gap
        if cladding_region is None:
            cladding_region = self.parameters.region_cladding
        if divisions_axial is None:
            divisions_axial = self.parameters.divisions_axial_fuel_discrete
        if result_mesh_name is None or result_mesh_name==self.parameters.hex_unit_rod_name or result_mesh_name==self.parameters.hex_rod_name:
            flag_assembly=False
            mesh_name_fuel = self.parameters.hex_fuel_name
            mesh_name_gap = self.parameters.hex_fuel_cladding_gap_name
            mesh_name_cladding = self.parameters.hex_cladding_name
            if result_mesh_name is None:
                result_mesh_name = self.parameters.hex_unit_rod_name
        else:
            flag_assembly=True
            mesh_name_fuel = result_mesh_name+"_"+self.parameters.region_fuel
            mesh_name_gap = result_mesh_name+"_"+self.parameters.region_fuel_cladding_gap
            mesh_name_cladding = result_mesh_name+"_"+self.parameters.region_cladding

        # Multi-layer cladding support
        if len(self.parameters.geometry_cladding_thickness_list) > 1:
            thickness_list = self.parameters.geometry_cladding_thickness_list
            divisions_list = self.parameters.divisions_radial_cladding_list
        else:
            thickness_list = [self.parameters.geometry_cladding_thickness_list[0]]
            divisions_list = [self.parameters.divisions_radial_cladding_list[0]]

        psf_string_list = []
        
        # fuel
        psf_string_list.extend(self.create_unit_fuel_pellet(
            divisions_axial=divisions_axial,
            height=height, 
            region=fuel_region,
            name=mesh_name_fuel))

        # gap
        psf_string_list.extend(self.create_unit_fuel_cladding_gap(
            divisions_axial=divisions_axial,
            height=height, 
            region=gap_region,
            name=mesh_name_gap))

        # cladding
        psf_string_list.extend(self.create_unit_cladding(
            thickness_list_in=thickness_list,
            divisions_radial_list=divisions_list,
            divisions_axial=divisions_axial,
            height=height, 
            region=cladding_region,
            name=mesh_name_cladding))

        # auto snapping
        assembly_list = []
        if not flag_assembly:
            for key in self.creation_dict_hex_mesh:
                assembly_list.append(key)
        else:
            for key in self.creation_dict_hex_mesh:
                if result_mesh_name in key:
                    assembly_list.append(key)

        psf_string_list.extend(self.snap_list_objects(
                object_list=assembly_list,
                child_object_name=result_mesh_name))

        # deleting 
        psf_string_list.append(self.delete_list_of_objects(object_list=assembly_list))
        return psf_string_list

    def create_fuel_fuel_gap_assembly(self):
        """
        
        This function creates a 3D R-Theta-Z fuel-fuel-gap assembly hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating a fuel-fuel-gap hex mesh
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=self.parameters.geometry_fuel_fuel_gap_height,
            fuel_region=self.parameters.region_fuel_fuel_gap,
            result_mesh_name=self.parameters.hex_unit_fuel_fuel_name)
        return psf_string_list

    def create_plenum_assembly(self, height, divisions, name):
        """
        
        This function creates a 3D R-Theta-Z plenum assembly hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :param height:
        :param divisions:
        :param name:
        :return psf_string_list: psf formatted list of strings for creating a plenum assembly hex mesh
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=height,
            divisions_axial=divisions,
            fuel_region=self.parameters.region_plenum,
            gap_region=self.parameters.region_plenum,
            result_mesh_name=name)
        return psf_string_list

    def create_end_cap_assembly(self):
        """
        
        This function creates a 3D R-Theta-Z end cap assembly hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating an end-dap assembly hex mesh
        """
        psf_string_list = self.create_unit_rod_assembly(
            height=self.parameters.geometry_end_cap_thickness,
            divisions_axial=self.parameters.divisions_axial_end_cap,
            fuel_region=self.parameters.region_end_cap,
            gap_region=self.parameters.region_end_cap,
            cladding_region=self.parameters.region_end_cap,
            result_mesh_name=self.parameters.hex_end_cap_top_name)
        return psf_string_list

    def create_simple_x_pellet_rod_assembly(self):
        """
        
        This function creates a 3D R-Theta-Z x# discrete pellets rod hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`,
            :method: `create_fuel_fuel_gap_assembly`,
            :method: `auto_stack`,
            :method: `copy`,
            :method: `translate`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :return psf_string_list: psf formatted list of strings for creating a discrete pellet rod hex mesh
        """

        psf_string_list = []

        # create single pellet fuel, gap and cladding mesh
        psf_string_list.extend(self.create_unit_rod_assembly())

        # create single fuel-fuel gap interface
        psf_string_list.extend(self.create_fuel_fuel_gap_assembly())

        # copy unit rod for auto-stacking
        psf_string_list.extend(self.copy(
            self.parameters.hex_unit_rod_name,
            self.parameters.temporary_object_1_name))

        # snap gap and copy of unit rod
        psf_string_list.extend(self.snap_2_objects(
            self.parameters.temporary_object_1_name,
            self.parameters.hex_fuel_fuel_gap_name,
            self.parameters.temporary_object_1_name))

        # perform auto-stack
        psf_string_list.extend(self.auto_stack(
            self.parameters.temporary_object_1_name,
            self.parameters.geometry_number_of_pellets - 1,
            self.parameters.position_vector_z,
            self.parameters.geometry_fuel_height_discrete))

        # translate original unit rod to top of rod
        psf_string_list.extend(self.translate(
            self.parameters.hex_unit_rod_name,
            self.parameters.position_vector_z,
            self.parameters.geometry_fuel_height_discrete * self.parameters.geometry_number_of_pellets))

        # snap stack and unit rod
        psf_string_list.extend(self.snap_2_objects(
            self.parameters.temporary_object_1_name,
            self.parameters.hex_unit_rod_name,
            self.parameters.hex_x_rod_name))

        # delete temporary objects
        hex_list = [self.parameters.temporary_object_1_name, self.parameters.hex_unit_rod_name]
        
        psf_string_list.extend(self.delete_list_of_objects(
            hex_list))
        return psf_string_list

    def create_simple_smeared_rod(self):
        """
        
        This function creates a 3D R-Theta-Z simple smeared rod hex mesh.
        
        Supporting Methods Used:
            :method: `create_unit_rod_assembly`
        :return psf_string_list: psf formatted list of strings for creating a full length smeared rod hex mesh
        """
        psf_string_list = []
        
        # create smeared pellet fuel, gap and cladding mesh
        psf_string_list.extend(self.create_unit_rod_assembly(
            height=self.parameters.geometry_fuel_height_smeared,
            divisions_axial=self.parameters.divisions_axial_fuel_smeared,
            result_mesh_name=self.parameters.hex_rod_name)
            )

        return psf_string_list

    def create_detailed_discrete_rod(self):
        """
        
        This function creates a 3D R-Theta-Z x# discrete pellets rod with plenum and end caps hex mesh.
        
        Supporting Methods Used:
            :method: `create_simple_x_pellet_rod_assembly`,
            :method: `create_plenum_assembly`,
            :method: `create_end_cap_assembly`,
            :method: `auto_translate_assemblies`,
            :method: `copy`,
            :method: `create_header_comment`
        :return psf_string_list: psf formatted list of strings for creating a full length discrete rod with plenum and end-caps hex mesh
        """
        psf_string_list = []
        # Create x number of pellets rod assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Discrete Fuel rod assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_simple_x_pellet_rod_assembly())
        
        # Create top plenum assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_top,
            divisions=self.parameters.divisions_axial_plenum_top,
            name=self.parameters.hex_plenum_top_name))

        # Create bottom plenum assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_bottom,
            divisions=self.parameters.divisions_axial_plenum_bottom,
            name=self.parameters.hex_plenum_bottom_name))

        # Create top end cap assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_end_cap_assembly())

        # Create bottom end cap assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.copy(
            self.parameters.hex_end_cap_top_name,
            self.parameters.hex_end_cap_bottom_name))
        
        # auto translate and snap all assemblies to correct position and delete parent objects
        psf_string_list.extend(self.auto_translate_assemblies(
            object_dictionary=self.creation_dict_hex_mesh,
            position_dictionary=self.parameters.geometry_mesh_positions_dict,
            result_object_name=self.parameters.hex_rod_name))
        return psf_string_list
    
    def create_detailed_smeared_rod(self):
        """
        
        This function creates a 3D R-Theta-Z smeared rod with plenum and end caps hex mesh.
        
        
        Supporting Methods Used:
            :method: `create_simple_smeared_rod`,
            :method: `create_plenum_assembly`,
            :method: `create_end_cap_assembly`,
            :method: `auto_translate_assemblies`,
            :method: `copy`,
            :method: `create_header_comment`
        :return psf_string_list: psf formatted list of strings for creating a full length smeared rod with plenum and end-caps hex mesh
        """
        psf_string_list = []

        # smeared rod assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Smeared Fuel rod assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_simple_smeared_rod())

        # Create top plenum assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_top,
            divisions=self.parameters.divisions_axial_plenum_top,
            name=self.parameters.hex_plenum_top_name))

        # Create bottom plenum assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_bottom,
            divisions=self.parameters.divisions_axial_plenum_bottom,
            name=self.parameters.hex_plenum_bottom_name))

        # Create top end cap assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.create_end_cap_assembly())

        # Create bottom end cap assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.copy(
            self.parameters.hex_end_cap_top_name,
            self.parameters.hex_end_cap_bottom_name))
        
        # auto translate and snap all assemblies to correct position and delete parent objects
        psf_string_list.extend(self.auto_translate_assemblies(
            object_dictionary=self.creation_dict_hex_mesh,
            position_dictionary=self.parameters.geometry_mesh_positions_dict,
            result_object_name=self.parameters.hex_rod_name))
        return psf_string_list

    def create_bc_sets_for_simple_3d_rtheta_rod(self):
        """
        
        This function creates boundary condition element and node sets for 3D R-Theta-Z `create_simple_smeared_rod`.
        
        Supporting Methods Used:
            :method: `create_single_node`,
            :method: `create_cube_sel_vol`,
            :method: `create_cylinder_sel_vol`,
            :method: `create_node_set_from_selection_volume`,
            :method: `create_element_set_from_selection_volume`,
            :method: `delete_list_of_objects`,
            :method: `create_element_set_from_mark`
        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_simple_smeared_rod
        """
        psf_string_list = []

        # Fixed
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_origin,
            self.parameters.position_coordinate_origin))
        
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_x,
            self.parameters.position_coordinate_x))
        
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_y,
            self.parameters.position_coordinate_y))

        # Bottom surface elements

        # Create cube to mark bottom of rod for fixed bc
        start_point = [
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_negative_selection_volume_threshold
        ]
        extrude_vec = [
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec)
        )

        # create bottom surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects(
            [self.parameters.selection_volume_temp_name]))

        # Outer cladding surface elements
        psf_string_list.extend(self.create_cylinder_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            radius=self.parameters.geometry_inside_outer_clad_radius,
            height=self.parameters.geometry_outside_top_rod_height,
            circumferential_divisions=self.parameters.divisions_circumferential,
            origin=[0.0,0.0,-self.parameters.geometry_selection_volume_threshold]))

        # create selection volume to mark outer surface for bc's    
        psf_string_list.append(self.create_element_set_from_selection_volume(
            element_set_name=self.parameters.elements_cladding_outer_surface,
            boundary_or_continuum=self.parameters.set_type_element_boundary,
            phys_mesh=self.parameters.physical_mesh_name,
            coverage_flag=self.parameters.word_completely,
            side_flag=self.parameters.word_outside,
            selection_volume_name=self.parameters.selection_volume_temp_name))
        return psf_string_list

    def create_bc_sets_for_detailed_3d_rtheta_rod(self):
        """
        
        This function creates boundary condition element and node sets for 3D R-Theta-Z `create_detailed_smeared_rod`.
        
        Supporting Methods Used:
            :method: `create_single_node`,
            :method: `create_cube_sel_vol`,
            :method: `create_cylinder_sel_vol`,
            :method: `create_node_set_from_selection_volume`,
            :method: `create_element_set_from_selection_volume`,
            :method: `rotate`,
            :method: `delete_list_of_objects`,
            :method: `create_element_set_from_mark`
        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_detailed_smeared_rod
        """
        psf_string_list = []

        # Fixed
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_origin,
            self.parameters.position_coordinate_origin))

        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_x,
            self.parameters.position_coordinate_x))

        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_y,
            self.parameters.position_coordinate_y))
        
        ############################## Symmetry Fixed ##############################
        # Create cube at top of rod that extudes downwards into rod to mark top surface
        # XZ plane
        start_point = [
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_negative_selection_volume_threshold,
            (self.parameters.geometry_outside_bottom_rod_height+self.parameters.geometry_times_2_selection_volume_threshold)
        ]

        extrude_vec = [
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold,
            (self.parameters.geometry_outside_top_rod_height -self.parameters.geometry_outside_bottom_rod_height - 2*self.parameters.geometry_times_2_selection_volume_threshold)
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))
  
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_plane_xz,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))
        
        # YZ plane
        psf_string_list.append(self.rotate(
            self.parameters.selection_volume_temp_name,
            90.0, 
            "z"))
        
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_plane_yz,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ############################## Bottom fuel surface elements ##############################

        psf_string_list.extend(self.create_cylinder_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            radius=(self.parameters.geometry_fuel_radius + self.parameters.geometry_selection_volume_threshold),
            height=(self.parameters.geometry_times_2_selection_volume_threshold),
            circumferential_divisions=self.parameters.divisions_circumferential,
            origin=[0.0,0.0,self.parameters.geometry_negative_selection_volume_threshold]))
        
        # create bottom end cap surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_fuel,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        ##############################  Bottom surface elements ############################## 

        # Create cube to mark bottom of rod for fixed bc
        # Create cube at bottom of rod that extudes upwards into rod to mark bottom surface
        start_point = [
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_outside_bottom_rod_height
        ]
        extrude_vec = [
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create bottom surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_bottom_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ############################## Top surface elements ############################## 

        # Create cube at top of rod that extudes downwards into rod to mark top surface
        start_point = [
            self.parameters.geometry_negative_outside_outer_clad_radius,
            self.parameters.geometry_negative_outside_outer_clad_radius,
            (self.parameters.geometry_outside_top_rod_height-self.parameters.geometry_selection_volume_threshold)
        ]
        extrude_vec = [
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_times_2_outside_outer_clad_radius,
            self.parameters.geometry_negative_times_2_selection_volume_threshold
        ]

        psf_string_list.append(self.create_cube_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            starting_point=start_point,
            extrude_vector=extrude_vec))

        # create top surface nodes
        psf_string_list.append(self.create_node_set_from_selection_volume(
            self.parameters.nodes_top_of_rod,
            self.parameters.physical_mesh_name,
            self.parameters.word_inside,
            self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))

        ###############################  Outer cladding surface elements ############################## 
        psf_string_list.extend(self.create_cylinder_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            radius=self.parameters.geometry_inside_outer_clad_radius,
            height=(self.parameters.geometry_outside_top_rod_height -self.parameters.geometry_outside_bottom_rod_height),
            circumferential_divisions=self.parameters.divisions_circumferential,
            origin=[0.0,0.0,self.parameters.geometry_outside_bottom_rod_height]))

        # create selection volume to mark outer surface for bc's    
        psf_string_list.append(self.create_element_set_from_selection_volume(
            element_set_name=self.parameters.elements_cladding_outer_surface,
            boundary_or_continuum=self.parameters.set_type_element_boundary,
            phys_mesh=self.parameters.physical_mesh_name,
            coverage_flag=self.parameters.word_completely,
            side_flag=self.parameters.word_outside,
            selection_volume_name=self.parameters.selection_volume_temp_name))

        # Delete selection volume
        psf_string_list.append(self.delete_list_of_objects([self.parameters.selection_volume_temp_name]))
        return psf_string_list

class TrisoMeshGridGen(HexMesh):
    """

    This class inherits the class `HexMesh` to create distributions of triso particles in a pellet matrix by giving the coordinates of each centroid
    of a particle and void block which can then be used to build spheres or cubes around.
    
    Class inherits:
        :class: `HexMesh`
    """

    def __init__(self, input_file_path=None):
        super(TrisoMeshGridGen, self).__init__(input_file_path)
        self.triso_particle_radius = self.parameters.geometry_triso_kernal_radius + sum(self.parameters.geometry_triso_thickness_list)
        self.pellet_radius = self.parameters.geometry_fuel_radius
        print(f"form grid: {self.triso_particle_radius}, {self.pellet_radius}")
        self.num_particles_across_pellet_diameter = self.parameters.geometry_triso_num_particles_across_diameter
        self.particle_bounding_box_half_width_percent = self.parameters.geometry_triso_graphite_block_scalar
        self.threshold_geometric = 4E-4
        self.output_csv_location = self.parameters.file_pegasus_input_location
        self.file_location = self.parameters.file_pegasus_input_location
        self.particle_output_csv = "triso_out.csv"
        self.void_output_csv = "void_out.csv"

        self.particle_bounding_box_half_width = 0.0
        self.pellet_bounding_box_width = 0.0
        self.a_start = 0.0
        self.grid_space_width = 0.0
        self.grid_lim = 0.0
        self.num_particles_to_remove = 0.0

        self.Xp = None
        self.Yp = None
        self.Zp = None
        self.Ip = None
        self.Xv = None
        self.Yv = None
        self.Zf = None
        self.Iv = None

        # debug flags
        self.debug_print_compute_num_particles = False
        self.debug_print_compute_mesh_properties = False
        self.debug_print_compute_mesh_offset_properties = True
        self.debug_print_compute_simple_random_grid = False
        self.debug_print_update_grid_for_offset = False
        self.debug_print_create_normal_out_vectors = False
        self.option_offset_mesh = True

        if self.parameters.model_flag_triso_pellet:
            # Calc mesh props
            if self.option_offset_mesh:
                self.compute_mesh_offset_properties()
            else:
                self.compute_mesh_properties()

    @property
    def option_3d_mesh(self):
        """

        :return:
        """
        if self.parameters.model_flag_dimmension == self.parameters.model_flag_dimmension_d3:
            return True
        else:
            return False

    @property
    def num_particles_to_remove_percent(self):
        """

        :return:
        """
        return (1 - self.parameters.geometry_triso_particles_density_percent)

    def compute_num_particles(self):
        """

        """
        pellet_scalar = 0.95
        particle_scalar_e = 1.1
        Bw = 2 * self.pellet_radius * math.cos(math.radians(45)) * pellet_scalar
        bw_e = 2 * self.triso_particle_radius * particle_scalar_e
        ne = Bw / bw_e
        ne_quotient, ne_remain = divmod(Bw, bw_e)
        particle_scalar = particle_scalar_e + ((ne_remain / bw_e) / ne_quotient)
        self.particle_bounding_box_width = self.triso_particle_radius * particle_scalar
        self.particle_bounding_box_half_width = (self.particle_bounding_box_width / 2)
        self.pellet_bounding_box_width = Bw

        self.particle_bounding_box_half_width = 0.5 * (
            self.parameters.geometry_triso_graphite_block_scalar) * self.triso_particle_radius
        # self.num_particles_across_pellet_diameter = int(Bw / self.particle_bounding_box_width)
        self.num_particles_across_pellet_diameter = 2 * self.parameters.geometry_triso_num_particles_across_diameter
        self.pellet_bounding_box_width = 2.0 * self.num_particles_across_pellet_diameter * self.particle_bounding_box_half_width

        if self.debug_print_compute_num_particles:
            print(
                f"compute_num_particles: \n"
                f"      self.pellet_bounding_box_width: {self.pellet_bounding_box_width}\n"
                # f"      old_pellet_bounding_box_width: {old_pellet_bounding_box_width}\n"
                f"      self.particle_bounding_box_width: {self.particle_bounding_box_width}\n"
                # f"      old_particle_bounding_box_half_width: {old_particle_bounding_box_half_width}\n"
                f"      self.num_particles_across_pellet_diameter: {self.num_particles_across_pellet_diameter}\n"
            )

    def compute_mesh_properties(self):
        """

        """
        self.particle_bounding_box_half_width = (
                                                    self.parameters.geometry_triso_graphite_block_scalar) * self.triso_particle_radius
        self.pellet_bounding_box_width = 2.0 * self.num_particles_across_pellet_diameter * self.particle_bounding_box_half_width
        self.a_start = (self.num_particles_across_pellet_diameter - 1) * self.particle_bounding_box_half_width
        self.grid_space_width = self.pellet_bounding_box_width / self.num_particles_across_pellet_diameter
        self.grid_lim = (self.pellet_bounding_box_width / 2.0) + self.grid_space_width
        self.num_particles_to_remove = int(
            self.num_particles_to_remove_percent * (self.num_particles_across_pellet_diameter ** 2))

        if self.debug_print_compute_mesh_properties:
            print(
                f"compute_mesh_properties: \n"
                f"      self.grid_lim: {self.grid_lim} \n"
                f"      triso_particle_radius: {self.triso_particle_radius} \n"
                f"      pellet_radius: {self.pellet_radius} \n"
                f"      self.a_start: {self.a_start} \n"
                f"      self.particle_bounding_box_half_width: {self.particle_bounding_box_half_width} \n"
                f"      self.grid_lim: {self.grid_lim} \n"
                f"      self.grid_space_width: {self.grid_space_width} \n"
                f"      self.num_particles_across_pellet_diameter: {self.num_particles_across_pellet_diameter} \n"
                f"      self.num_particles_to_remove: {self.num_particles_to_remove} \n"
            )

    def compute_mesh_offset_properties(self):
        """

        """
        self.compute_num_particles()
        self.a_start = (self.num_particles_across_pellet_diameter - 1) * self.particle_bounding_box_half_width
        self.grid_space_width = self.pellet_bounding_box_width / self.num_particles_across_pellet_diameter
        self.grid_lim = (self.pellet_bounding_box_width / 2.0) + self.grid_space_width
        self.num_particles_to_remove = int(
            self.num_particles_to_remove_percent * (self.num_particles_across_pellet_diameter ** 2))

        if self.debug_print_compute_mesh_offset_properties:
            print(
                f"compute_mesh_offset_properties: \n"
                f"      self.grid_lim: {self.grid_lim} \n"
                f"      triso_particle_radius: {self.triso_particle_radius} \n"
                f"      pellet_radius: {self.pellet_radius} \n"
                f"      self.a_start: {self.a_start} \n"
                f"      self.particle_bounding_box_half_width: {self.particle_bounding_box_half_width} \n"
                f"      self.grid_lim: {self.grid_lim} \n"
                f"      self.grid_space_width: {self.grid_space_width} \n"
                f"      self.num_particles_across_pellet_diameter: {self.num_particles_across_pellet_diameter} \n"
                f"      self.num_particles_to_remove: {self.num_particles_to_remove} \n"
            )

    def compute_simple_random_grid(self):
        """

        :param opt_3d:
        """
        n = self.num_particles_across_pellet_diameter
        m = self.num_particles_to_remove

        xu = np.linspace(-self.a_start, self.a_start, n)
        yu = np.linspace(-self.a_start, self.a_start, n)

        if self.parameters.model_flag_triso_plot_d3:
            zu = np.linspace(-self.a_start, self.a_start, n)
            x_u, y_u, z_u = np.meshgrid(xu, yu, zu, indexing='ij')
            x_us = x_u.reshape(-1, 1)
            y_us = y_u.reshape(-1, 1)
            z_us = z_u.reshape(-1, 1)
            remove_index = np.random.choice(range(n ** 3), m * n, replace=False).ravel()
            
            self.index_vec_t = np.linspace(1, len(zu) ** 3, len(zu) ** 3).reshape(
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter)
            
            self.index_vec = np.linspace(1, len(zu) ** 3, len(zu) ** 3).reshape(-1, 1)

            self.Xp = np.delete(x_us, remove_index, axis=0)
            self.Yp = np.delete(y_us, remove_index, axis=0)
            self.Zp = np.delete(z_us, remove_index, axis=0)
            self.Ip = np.delete(self.index_vec, remove_index, axis=0)
            self.Xv = x_us[remove_index]
            self.Yv = y_us[remove_index]
            self.Zv = z_us[remove_index]
            self.Iv = self.index_vec[remove_index]
        else:
            x_u, y_u = np.meshgrid(xu, yu)
            x_us = x_u.reshape(-1, 1)
            y_us = y_u.reshape(-1, 1)
            remove_index = np.random.choice(range(n ** 2), m, replace=False).ravel()
            
            self.index_vec_t = np.linspace(1, len(xu) ** 2, len(xu) ** 2).reshape(
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter)
            
            self.index_vec = np.linspace(1, len(xu) ** 2, len(xu) ** 2).reshape(-1, 1)

            self.Xp = np.delete(x_us, remove_index, axis=0)
            self.Yp = np.delete(y_us, remove_index, axis=0)
            self.Ip = np.delete(self.index_vec, remove_index, axis=0)
            self.Xv = x_us[remove_index]
            self.Yv = y_us[remove_index]
            self.Iv = self.index_vec[remove_index]
        
        if self.debug_print_compute_simple_random_grid:
            print(
                f"compute_simple_random_grid: \n"
                f"      n: {n} \n"
                f"      m: {m} \n"
                f"      remove_index: {remove_index}\n"
                f"      self.index_vec_t: {self.index_vec_t}\n")
        
        if self.option_offset_mesh:
            self.create_normal_out_vectors()
            self.update_grid_for_offset()
        else:
            self.create_normal_out_vectors()

    def create_normal_out_vectors(self):
        """
        
        """
        x_particle_final = self.Xp.reshape(-1, 1)
        y_particle_final = self.Yp.reshape(-1, 1)
        x_void_final = self.Xv.reshape(-1, 1)
        y_void_final = self.Yv.reshape(-1, 1)
        
        # setting number of triso particles
        self.number_of_particles = x_particle_final.shape[0]

        # setting number of voids
        self.number_of_voids = x_void_final.shape[0]

        # 3D mesh design
        if self.parameters.model_flag_triso_plot_d3:
            z_particle_final = self.Zp.reshape(-1, 1)
            z_void_final = self.Zv.reshape(-1, 1)
            self.particle_final = np.concatenate((x_particle_final, y_particle_final, z_particle_final, self.Ip), axis=1)
            self.void_final = np.concatenate((x_void_final, y_void_final, z_void_final, self.Iv), axis=1)

        # 2D mesh design
        else:
            # creating zero vector for z-direction particles (Zp) and voids (Zv)
            self.Zp = np.zeros((self.number_of_particles, 1), dtype=float)
            self.Zv = np.zeros((self.number_of_voids, 1), dtype=float)
            
            self.particle_final = np.concatenate((x_particle_final, y_particle_final, self.Zp, self.Ip), axis=1)
            self.void_final = np.concatenate((x_void_final, y_void_final, self.Zv, self.Iv), axis=1)
            
        # sorting data for particles and voids
        self.p_xyzi_coor = self.particle_final[np.argsort(self.particle_final[:, 3])]
        self.v_xyzi_coor = self.void_final[np.argsort(self.void_final[:, 3])]
        
        # x,y coordinates of particles and voids
        self.p_xyz_coor = self.p_xyzi_coor[:, [0, 1, 2]]
        self.v_xyz_coor = self.v_xyzi_coor[:, [0, 1, 2]]
        
        # creation index of particles and voids
        self.Ip = self.p_xyzi_coor[:, 3].astype(int)
        self.Iv = self.v_xyzi_coor[:, 3].astype(int)
            
        # translation vector to move particles and voids
        self.particle_trans_coor = np.concatenate((self.p_xyz_coor, self.Zp), axis=1)
        self.void_trans_coor = np.concatenate((self.v_xyz_coor, self.Zv), axis=1)


        # combining particle and void vectors. Doing this allows to snap the grid together sequentially based on
        # index number which means that we can delete the copies during the snapping process to improve run-time 
        # performance when building the mesh. number_of_pv
        self.number_of_pv = self.number_of_particles + self.number_of_voids
        index_part = 0
        index_void = 0
        pv_trans_coor = []
        pv_trans_coor_bool = []
        pv_index_vec = []
        for pv in range(0, self.number_of_pv):
            new_arr = np.array([1, 2, 3])
            pv_index = pv + 1
            result_particle = np.array_equal(self.Ip[index_part], pv_index)
            result_void = np.array_equal(self.Iv[index_void], pv_index)
            
            # inserting particle coordinates into particle-void translation vector
            if result_particle:
                pv_trans_coor.extend([self.particle_trans_coor[index_part]])
                pv_trans_coor_bool.append([result_particle])
                pv_index_vec.append(self.Ip[index_part])
                index_part += 1
                index_part = min(index_part, self.number_of_particles - 1)
            
            # inserting void coordinates into particle-void translation vector
            elif result_void:
                pv_trans_coor.extend([self.void_trans_coor[index_void]])
                pv_trans_coor_bool.append([result_particle])
                pv_index_vec.append(False)
                index_void += 1
                index_void = min(index_void, self.number_of_voids - 1)
            else:
                print("didnt work")
                
        pv_trans_coor = np.array(pv_trans_coor, dtype=object)
        pv_trans_coor_bool = np.array(pv_trans_coor_bool, dtype=object)
        self.pv_trans_coor = np.concatenate((pv_trans_coor, pv_trans_coor_bool), axis=1)
        
        if self.parameters.model_flag_triso_plot_d3:
            self.pv_index_vec = np.array(pv_index_vec).reshape(
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter)
        else:
            self.pv_index_vec = np.array(pv_index_vec).reshape(
                self.num_particles_across_pellet_diameter, 
                self.num_particles_across_pellet_diameter)
        
        if self.debug_print_create_normal_out_vectors:
            print(
                f"self.pv_trans_coor:{self.pv_trans_coor.shape}\n"
                f"self.pv_trans_coor:{self.pv_trans_coor}\n")
        
        # print(f"in create_normal_out_vectors: {self.number_of_pv}")

        if self.parameters.model_flag_triso_plot_design:
            self.plot_mesh()

    def update_grid_for_offset(self):
        """

        """
        # create new data structures to manage if particles have been assigned to their 4-particle struct
        is_assigned_vec = np.full(
            (self.num_particles_across_pellet_diameter, self.num_particles_across_pellet_diameter), False, dtype=bool)
        self.Ip_new = np.empty((self.num_particles_across_pellet_diameter, self.num_particles_across_pellet_diameter))
        self.Iv_new = []
        self.Ip_new[:] = False
        p_index_dict_new = {}
        particle_neighbors_dict_new = {}
        p_xyz_trans_coor_new = []

        # create new dict for
        for key, value in zip(self.index_vec_t.reshape(1, -1)[0], is_assigned_vec.reshape(1, -1)[0]):
            p_index_dict_new[int(key)] = value

        particle_created = 0
        
        # Looping over matrix of particles and voids
        for particle_row in range(0, self.num_particles_across_pellet_diameter):
            for particle_col in range(0, self.num_particles_across_pellet_diameter):
                index_particle = self.pv_index_vec[particle_row][particle_col]
                # if a void, pass
                # if np.isnan(index_particle):
                if not index_particle:
                    particle_neighbors_dict_new[int(self.index_vec_t[particle_row][particle_col])] = None

                # if a particle, begin creating 4-particle "clusters"
                else:
                    # try/except block for when at edge of array
                    try:
                        index_r_pm1 = particle_row + 1
                        index_c_pm1 = particle_col + 1
                        index_particle_r_pm1 = int(self.index_vec_t[index_r_pm1][particle_col])
                        index_particle_c_pm1 = int(self.index_vec_t[particle_row][index_c_pm1])
                        index_particle_rc_pm1 = int(self.index_vec_t[index_r_pm1][index_c_pm1])

                    except IndexError:
                        index_r_pm1 = particle_row - 1
                        index_c_pm1 = particle_col - 1
                        index_particle_r_pm1 = int(self.index_vec_t[index_r_pm1][particle_col])
                        index_particle_c_pm1 = int(self.index_vec_t[particle_row][index_c_pm1])
                        index_particle_rc_pm1 = int(self.index_vec_t[index_r_pm1][index_c_pm1])

                    is_assigned_list = [
                        p_index_dict_new[int(index_particle)],
                        p_index_dict_new[int(index_particle_r_pm1)],
                        p_index_dict_new[int(index_particle_c_pm1)],
                        p_index_dict_new[int(index_particle_rc_pm1)]
                    ]

                    # if none of the particles have NOT been assigned to a "cluster" then create a new cluster
                    if not any(is_assigned_list):
                        self.Ip_new[particle_row][particle_col] = index_particle
                        self.Ip_new[index_r_pm1][particle_col] = index_particle
                        self.Ip_new[particle_row][index_c_pm1] = index_particle
                        self.Ip_new[index_r_pm1][index_c_pm1] = index_particle
                        p_index_dict_new[int(index_particle)] = True
                        p_index_dict_new[int(index_particle_r_pm1)] = True
                        p_index_dict_new[int(index_particle_c_pm1)] = True
                        p_index_dict_new[int(index_particle_rc_pm1)] = True

                        # adding assigned particles to clustered group
                        particle_neighbors_dict_new[int(index_particle)] = [
                            int(index_particle_r_pm1),
                            int(index_particle_c_pm1),
                            int(index_particle_rc_pm1)
                        ]

                        # get particle pose by calculaitng mean of cluster
                        xyz_index_particle_cluster = np.array(
                            [
                                self.pv_trans_coor[int(index_particle - 1), 0:3],
                                self.pv_trans_coor[int(index_particle_r_pm1 - 1), 0:3],
                                self.pv_trans_coor[int(index_particle_c_pm1 - 1), 0:3],
                                self.pv_trans_coor[int(index_particle_rc_pm1 - 1), 0:3]
                            ]
                        )
                        p_xyz_trans_coor_new_single = np.mean(xyz_index_particle_cluster, axis=0)
                        p_xyz_trans_coor_new_single = np.around(np.array(p_xyz_trans_coor_new_single, dtype=float), 10)
                        p_xyz_trans_coor_new.append(p_xyz_trans_coor_new_single)
                        particle_created += 1
                        is_assigned_list = []

        # format arrays
        self.p_xyz_trans_coor_new = p_xyz_trans_coor_new
        self.Ip_new = np.array(self.Ip_new, dtype=int)
        self.Iv_new = np.argwhere((self.Ip_new.reshape(1, -1)[0]) < 1).reshape(1, -1)[0] + 1

        Ip_new_unique = np.unique(self.Ip_new)
        Ip_new_unique_index = np.argwhere(~Ip_new_unique).reshape(1, -1)[0]
        Ip_new_unique = Ip_new_unique[Ip_new_unique != 0]

        self.Ip_new = self.Ip_new.reshape(1, -1)[0]

        if self.debug_print_update_grid_for_offset:
            print(
                f"update_grid_for_offset: \n"
                f"      after: self.pv_index_vec:\n"
                f"      \n"
                f"      {self.pv_index_vec}\n"
                f"      \n"
                f"      self.Ip_new:\n"
                f"      \n"
                f"      {self.Ip_new}\n"
                f"      \n"
                #       f"{np.unique(self.Ip_new)}\n"
                f"      \n"
                f"      self.Ip_new.shape:\n"
                f"      \n"
                f"      {self.Ip_new.shape}\n"
                f"      \n"
                f"      self.Iv_new:\n"
                f"      \n"
                f"      {self.Iv_new}\n"
                f"      \n"
                f"      self.Iv_new.shape:\n"
                f"      \n"
                f"      {self.Iv_new.shape}\n"
                f"      \n"
                f"      Ip_new_unique:\n"
                f"      \n"
                f"      {Ip_new_unique}\n"
                f"      \n"
                f"      Ip_new_unique.shape:\n"
                f"      \n"
                f"      {Ip_new_unique.shape[0]}\n"
            )

        expected_num_particles = Ip_new_unique.shape[0]
        expected_num_voids = self.Iv_new.shape[0]
        expected_num_pv = expected_num_particles + expected_num_voids

        # create new dict for
        for key, value in zip(self.index_vec_t.reshape(1, -1)[0], is_assigned_vec.reshape(1, -1)[0]):
            p_index_dict_new[int(key)] = value

        self.v_xyz_trans_coor_new = []
        count_part = 0
        count_void = 0
        count_repeat = 0
        pv_trans_coor = []
        pv_trans_coor_bool = []
        pv_index_vec = []
        pv_index = 0
        new_num_pv = self.number_of_pv - particle_created * 3
        for pv in range(0, self.number_of_pv):
            index_particle = int(self.Ip_new[pv])
            pv_index = int(pv + 1)
            
            # inserting void coordinates into particle-void translation vector
            if not index_particle:
                if self.debug_print_update_grid_for_offset:
                    print(
                        f"update_grid_for_offset: \n"
                        f"      adding new void: {pv_index}, count_void: {count_void}\n"
                    )
                pv_trans_coor.extend([self.pv_trans_coor[pv, 0:3]])
                self.v_xyz_trans_coor_new.extend([self.pv_trans_coor[pv, 0:3]])
                pv_trans_coor_bool.append([False])
                count_void += 1

            # inserting particle coordinates into particle-void translation vector
            else:
                already_added = p_index_dict_new[int(index_particle)]
                if not already_added:
                    if self.debug_print_update_grid_for_offset:
                        print(
                            f"update_grid_for_offset: \n"
                            f"      adding new particle: {pv_index}, count_part: {count_part}\n")
                        
                    pv_trans_coor.extend([self.p_xyz_trans_coor_new[count_part]])
                    pv_trans_coor_bool.append([True])
                    count_part += 1
                    count_part = min(count_part, particle_created)
                    p_index_dict_new[int(index_particle)] = True
                else:
                    if self.debug_print_update_grid_for_offset:
                        print(
                            f"update_grid_for_offset: \n"
                            f"      skipping repeated particle: {pv_index}\n")
                    count_repeat += 1

        pv_trans_coor = np.array(pv_trans_coor, dtype=object)
        pv_trans_coor_bool = np.array(pv_trans_coor_bool, dtype=object)

        self.pv_trans_coor = np.concatenate((pv_trans_coor, pv_trans_coor_bool), axis=1)
        self.number_of_pv = count_void + count_part
        self.number_of_particles = particle_created

        self.p_xyz_trans_coor_new = np.array(self.p_xyz_trans_coor_new)
        self.v_xyz_trans_coor_new = np.array(self.v_xyz_trans_coor_new)

        if self.debug_print_update_grid_for_offset:
            print(
                f"update_grid_for_offset: \n"
                f"      self.pv_trans_coor: {self.pv_trans_coor.shape}\n"
                f"      particle_created: {particle_created}\n"
                f"      new_num_pv: {new_num_pv}\n"
                f"      count_repeat: {count_repeat}\n"
                f"      particle_count: {count_part}\n"
                f"      void_count: {count_void}\n"
                f"      total_count: {count_void + count_part + count_repeat}\n"
                f"      expected length of pv_trans_coor: {self.number_of_pv}\n")

        self.Xp = self.p_xyz_trans_coor_new[:, 0]
        self.Yp = self.p_xyz_trans_coor_new[:, 1]

        self.Xv = self.v_xyz_trans_coor_new[:, 0]
        self.Yv = self.v_xyz_trans_coor_new[:, 1]

        if self.parameters.model_flag_triso_plot_design:
            self.plot_mesh()

    def plot_mesh(self):
        """

        """
        # Figure 1 showing triso particles and voids
        fig = plt
        fig.figure()
        fig.rcParams['grid.color'] = "green"
        if self.parameters.model_flag_triso_plot_d3:
            ax = fig.gca(projection='3d')
        else:
            ax = fig.gca()

        ax.set_aspect("equal")
        ax.set_xticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
        ax.set_yticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
        # ax.axis([-self.pellet_radius, self.pellet_radius, -self.pellet_radius, self.pellet_radius])
        ax.grid(color='b', linestyle='--', linewidth=1)

        if self.parameters.model_flag_triso_plot_d3:
            ax.set_zticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
            ax.scatter(self.Xp, self.Yp, self.Zp, label="Triso Particle", c="b", alpha=1.0)
            ax.scatter(self.Xv, self.Yv, self.Zv, label="void", c="r", alpha=1.0)
        else:
            ax.scatter(self.Xp, self.Yp, label="Triso Particle", c="b", alpha=1.0)
            ax.scatter(self.Xv, self.Yv, label="void", c="r", alpha=1.0)
            # Labeling nodes
            node_num = 1
            ind = np.lexsort((self.Xp, self.Yp))
            for point in range(0, self.number_of_particles):
                node_i_label = self.Ip[point]
                ax.annotate(node_i_label, (self.Xp[point], self.Yp[point]))

        ax.legend(loc="upper left")
        fig.title('Random Triso-Particle Distribution with Voids')
        fig.tight_layout()
        fig.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')
        # fig.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
        # fig.gca().yaxis.set_major_locator(MaxNLocator(prune='lower'))

        # Figure 2 showing only trio particles
        fig2 = plt
        fig2.figure()
        fig2.rcParams['grid.color'] = "green"
        if self.parameters.model_flag_triso_plot_d3:
            ax2 = fig.gca(projection='3d')
        else:
            ax2 = fig.gca()

        ax2.set_aspect("equal")
        ax2.set_xticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
        ax2.set_yticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
        # ax2.axis([-self.grid_lim, self.grid_lim + self.grid_space_width, -self.grid_lim, self.grid_lim + self.grid_space_width])
        ax2.grid(color='b', linestyle='--', linewidth=1)

        if self.parameters.model_flag_triso_plot_d3:
            ax2.set_zticks(np.arange(-self.grid_lim, self.grid_lim + self.grid_space_width, self.grid_space_width))
            ax2.scatter(self.Xp, self.Yp, self.Zp, label="Triso Particle", c="b", alpha=1.0)
        else:
            ax2.scatter(self.Xp, self.Yp, label="Triso Particle", c="b", alpha=1.0)

        ax2.legend(loc="upper left")
        fig2.title('Random Triso-Particle Distribution')
        fig2.tight_layout()
        fig2.setp(ax2.get_xticklabels(), rotation=30, horizontalalignment='right')
        # fig2.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
        # fig2.gca().yaxis.set_major_locator(MaxNLocator(prune='lower'))

        # save and show
        # plt.grid()
        fig.savefig(self.file_location + 'node_id_with_voids.png')
        fig2.savefig(self.file_location + 'node_id_no_voids.png')
        plt.show(block=False)
        # input("press [enter] to close all open figures and continue using plotting tool.")
        # plt.close('all')

    def export_mesh_to_csv(self):
        """

        """
        self.create_normal_out_vectors()
        np.savetxt(self.output_csv_location + self.particle_output_csv, self.particle_final, delimiter=",")
        np.savetxt(self.output_csv_location + self.void_output_csv, self.void_final, delimiter=",")

    def load_mesh_from_csv(self):
        """

        """
        p_xyi_coor = np.loadtxt(self.output_csv_location + self.particle_output_csv, delimiter=",", dtype=float)
        p_xyi_coor = p_xyi_coor[np.argsort(p_xyi_coor[:, 2])]
        p_xy_coor = p_xyi_coor[:, [0, 1]]
        self.Xp = p_xy_coor[:, 0]
        self.Yp = p_xy_coor[:, 1]
        self.Ip = p_xyi_coor[:, 2].astype(int)
        self.number_of_particles = p_xy_coor.shape[0]
        self.Zp = np.zeros((self.umber_of_particles, 1), dtype=float)
        self.particle_trans_coor = np.concatenate((p_xy_coor, self.Zp), axis=1)

        v_xyi_coor = np.loadtxt(self.output_csv_location + self.void_output_csv, delimiter=",", dtype=float)
        v_xyi_coor = v_xyi_coor[np.argsort(v_xyi_coor[:, 2])]
        v_xy_coor = v_xyi_coor[:, [0, 1]]
        self.Xv = v_xy_coor[:, 0]
        self.Yv = v_xy_coor[:, 1]
        self.Iv = v_xyi_coor[:, 2].astype(int)
        self.number_of_voids = v_xy_coor.shape[0]
        self.Zv = np.zeros((number_of_voids, 1), dtype=float)
        self.void_trans_coor = np.concatenate((v_xy_coor, self.Zv), axis=1)

class Triso(TrisoMeshGridGen):
    """

    This class inherits the class `TrisoMeshGridGen` to be able to create distributions of triso particles in a pellet
    matrix but can also just create single triso particles
    
    Class inherits:
        :class: `TrisoMeshGridGen`
    """

    def __init__(self, input_file_path=None):
        super(Triso, self).__init__(input_file_path)
        self.case_dimension_type = "3D"

    def create_triso_particle(self):
        """
        
        This function creates a 3D triso particle (optional: embedded in a cuboid) (solid sphere and shells in box) hex mesh to be used in `create_triso_compact_core`.
        
        Supporting Methods Used:
            :method: `create_layered_sphere_embedded_cube`
        :return psf_string_list: psf formatted list of strings for creating a triso particle hex mesh
        """
        psf_string_list = []

        # supporting multi-layer triso
        thickness_list = self.parameters.geometry_triso_thickness_list
        num_layers = len(thickness_list)

        # inserting zero for indexing purposes
        thickness_list.insert(0, 0.0)

        # creating n number of triso layers
        radius_list = []
        region_list = []
        radius_layer_fuel = self.parameters.geometry_triso_kernal_radius
        # radius_particle = radius_layer_fuel + sum(thickness_list)
        # print(f"radius_particle: {radius_particle}")

        # for loop to calculate all radii and create region names
        for thickness in range(0, num_layers+1):
            # update regions
            region_n = self.parameters.region_triso_layer + "_" + str(thickness)
            region_list.append(region_n)
            
            # deffine fuel radius (solid sphere) as first radius
            if thickness==0:
                radius_layer_n = radius_layer_fuel
            # updating radii for layers (spherical shells) on each iteration
            else:
                radius_layer_n = round(radius_layer_n_previous + thickness_list[thickness], self.parameters.tolerance_rounding)
            
            # add radius to list and update previous radius
            radius_list.append(radius_layer_n)
            radius_layer_n_previous = radius_layer_n

        print(f"radius_list: {radius_list}\n")
        print(f"thickness_list: {thickness_list}\n")

        psf_string_list.extend(self.create_layered_sphere_embedded_cube(
            self.parameters.hex_triso_particle_name,
            radius_list=radius_list,
            divisions_circumferential=self.parameters.divisions_circumferential,
            divisions_radial_list=self.parameters.divisions_radial_triso_list,
            symmetry_type=self.parameters.model_symmetry_type,
            region_list=region_list,
            embed=self.parameters.model_flag_embed_triso))

        # define outer radius
        self.triso_particle_outer_radius = radius_layer_n
        return psf_string_list

    def create_void_block(self):
        """
        
        This function creates a 3D void (box) hex mesh to be used in `create_triso_compact_core`.
        
        Supporting Methods Used:
            :method: `create_hex_block`,
            :method: `copy`,
            :method: `mirror`,
            :method: `snap_2_objects`,
            :method: `delete_list_of_objects`
        :return psf_string_list: psf formatted list of strings for creating a block hex mesh
        """
        psf_string_list = []
        # for offset pellet formation
        # void_starting_coordinate = [
        #     -0.5 * self.parameters.geometry_triso_graphite_block_radius,
        #     -0.5 * self.parameters.geometry_triso_graphite_block_radius,
        #     0.0
        # ]
        # void_starting_coordinate = [
        #     -2*self.particle_bounding_box_half_width,
        #     -2*self.particle_bounding_box_half_width,
        #     0.0
        # ]
        void_starting_coordinate = [
            -(self.particle_bounding_box_half_width),
            -(self.particle_bounding_box_half_width),
            0.0
        ]
        # self.particle_bounding_box_half_width

        psf_string_list.extend(self.create_hex_block(
            self.parameters.hex_void_particle_name,
            starting_coordinate=void_starting_coordinate,
            extrude_x=2*(self.particle_bounding_box_half_width),
            extrude_y=2*(self.particle_bounding_box_half_width),
            extrude_z=2*(self.particle_bounding_box_half_width),
            divisions_x=(0.25 * self.parameters.divisions_circumferential) / 2,
            divisions_y=(0.25 * self.parameters.divisions_circumferential) / 2,
            divisions_z=(0.5 * 0.25 * self.parameters.divisions_circumferential),
            region=self.parameters.region_graphite))

        # psf_string_list.extend(self.create_hex_block(
        #     self.parameters.hex_void_particle_name,
        #     starting_coordinate=void_starting_coordinate,
        #     extrude_x=(self.parameters.geometry_triso_graphite_block_radius) / 2,
        #     extrude_y=(self.parameters.geometry_triso_graphite_block_radius) / 2,
        #     extrude_z=(self.parameters.geometry_triso_graphite_block_radius),
        #     divisions_x=(0.25 * self.parameters.divisions_circumferential) / 2,
        #     divisions_y=(0.25 * self.parameters.divisions_circumferential) / 2,
        #     divisions_z=(0.5 * 0.25 * self.parameters.divisions_circumferential),
        #     region=self.parameters.region_graphite))

        # if doing a full triso compact (no cross-section)
        if self.parameters.model_symmetry_type == self.parameters.geometry_no_symmetry:
            psf_string_list.extend(self.copy(
                self.parameters.hex_void_particle_name,
                self.parameters.temporary_object_1_name))
            
            psf_string_list.extend(self.mirror(
                self.parameters.hex_void_particle_name,
                "Z"))
            
            # Snapping together --- result is full sphere
            psf_string_list.extend(self.snap_2_objects(
                self.parameters.hex_void_particle_name,
                self.parameters.temporary_object_1_name,
                self.parameters.hex_void_particle_name))
            
            # delete 
            psf_string_list.append(self.delete_list_of_objects(
                [self.parameters.temporary_object_1_name]))
        return psf_string_list

    def todo_create_triso_particle_corner(self):
        """

        :return:
        """
        psf_string_list = []
        return psf_string_list

    def todo_create_triso_particle_edge(self):
        """

        :return:
        """
        psf_string_list = []
        # psf_string_list.extend()

        return psf_string_list

    def todo_create_void_corner(self):
        """

        :return psf_string_list: psf formatted list of strings for creating a rounded edge cube hex mesh
        """
        psf_string_list = []
        p1_x = self.triso_particle_radius * self.num_particles_across_pellet_diameter
        p1_y = p1_x
        p2_x = self.pellet_radius * math.cos(math.pi / 4)
        p2_y = p2_x
        theta_3 = self.parameters.divisions_circumferential * math.pi / 180
        p3_x = self.pellet_radius * math.cos(theta_3)
        p3_y = self.pellet_radius * math.cos(theta_3)
        p4_x = p3_x
        p4_y = p1_y

        hex_name = "HEX_VOID_CORNER"
        psf_string_list.extend(self.create_circular_polyhedron(
            hex_name_output=hex_name,
            point_1=[p1_x, p1_y],
            point_2=[p2_x, p2_y],
            point_3=[p3_x, p3_y],
            point_4=[p4_x, p4_y],
            extrude_height=(self.parameters.geometry_triso_graphite_block_radius),
            divisions_circumferential=1,
            divisions_radial=1,
            divisions_axial=1,
            curvature_radii=[0.0, self.pellet_radius, 0.0, 0.0],
            region=self.parameters.region_graphite,
            mark=True,
            torus_radius=self.pellet_radius)
        )

        return psf_string_list

    def create_void_ring(self):
        """
        
        This function creates a 3D void annulus (ring) hex mesh to be used in `create_triso_compact`.
        
        Supporting Methods Used:
            :method: `create_circular_polyhedron`,
            :method: `auto_revolve`
        :return psf_string_list: psf formatted list of strings for creating a ring hex mesh
        """
        psf_string_list = []
        p1_x = -1 * self.pellet_bounding_box_width / 2
        p1_y = p1_x
        p2_x = -2 * self.pellet_radius * math.cos(math.pi / 4)
        p2_y = p2_x
        p3_x = -1 * p2_x
        p3_y = p2_x
        p4_x = -1 * p1_x
        p4_y = p1_x
        div_circum = self.num_particles_across_pellet_diameter

        print(
            f"point_1: {[p1_x, p1_y]}\n"
            f"point_2: {[p2_x, p2_y]}\n"
            f"point_3: {[p3_x, p3_y]}\n"
            f"point_4: {[p4_x, p4_y]}\n"
        )

        hex_name = "HEX_VOID_RING"
        psf_string_list.extend(self.create_circular_polyhedron(
            hex_name_output=hex_name,
            point_1=[p1_x, p1_y],
            point_2=[p2_x, p2_y],
            point_3=[p3_x, p3_y],
            point_4=[p4_x, p4_y],
            extrude_height=2*(self.particle_bounding_box_half_width),
            divisions_circumferential=24,
            divisions_radial=5,
            divisions_axial=2,
            curvature_radii=[0.0, 2*self.pellet_radius, 0.0, 0.0],
            region=self.parameters.region_graphite,
            mark=True,
            torus_radius=2*self.pellet_radius))

        # rotate and copy
        psf_string_list.extend(self.auto_revolve(
            hex_name,
            3,
            "z",
            90))
        return psf_string_list

    def todo_create_void_edge(self):
        """

        :return:
        """
        psf_string_list = []
        psf_string_list.extend()

        return psf_string_list

    def create_triso_compact_core(self):
        """
        
        This function creates a 3D cuboid (box) hex mesh of embedded triso particles that is to be used in `create_triso_compact`.
        
        Supporting Methods Used:
            :method: `create_triso_particle`,
            :method: `create_void_block`,
            :method: `auto_triso_grid_gen`,
            :method: `create_void_block`

        :return psf_string_list: psf formatted list of strings for creating a core (rectangular shape) distribution of triso particles hex mesh
        """
        psf_string_list = []
        direction = "XYZ"
        # print(f"self.parameters.model_flag_triso_plot_d3: {self.parameters.model_flag_triso_plot_d3}")

        # create single triso and single void mesh
        
        psf_string_list.extend(self.parameters.create_header_comment("Creating Initial Triso Particle", level=2))
        psf_string_list.extend(self.create_triso_particle())
        
        psf_string_list.extend(self.parameters.create_header_comment("Creating Initial Void Block", level=2))
        psf_string_list.extend(self.create_void_block())

        # create dist
        self.compute_simple_random_grid()

        # Particles, Voids and final output
        pv_hex_names = [
            self.parameters.hex_triso_particle_name,
            self.parameters.hex_void_particle_name,
            self.parameters.hex_rod_name]

        psf_string_list.extend(self.parameters.create_header_comment("Begining Auto-Grid-Generation", level=2))
        pv_translation_vectors_string, pv_copy_object_list = self.auto_triso_grid_gen(
            pv_hex_names,
            self.number_of_pv,
            direction,
            self.pv_trans_coor)

        # self.print_dict(self.creation_dict_hex_mesh) 
        psf_string_list.extend(pv_translation_vectors_string)
        psf_string_list.extend(self.delete_list_of_objects(pv_hex_names[0:2]))
        return psf_string_list

    def todo_create_triso_compact_ring(self):
        """

        :return:
        """
        psf_string_list = []

        return psf_string_list

    def create_triso_compact(self):
        """

        :return psf_string_list: psf formatted list of strings for creating a pellet shaped distribution of triso particles hex mesh
        """
        psf_string_list = []
        psf_string_list.extend(self.create_triso_compact_core())
        psf_string_list.extend(self.create_void_ring())

        return psf_string_list

    def create_bc_sets_for_triso_particle(self):
        """

        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_triso_particle
        """
        psf_string_list = []

        ####################### Fixed #######################
        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_origin,
            self.parameters.position_coordinate_origin))

        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_x,
            self.parameters.position_coordinate_x))

        psf_string_list.append(self.create_single_node(
            self.parameters.nodes_clad_od_pos_y,
            self.parameters.position_coordinate_y))

        if self.parameters.model_symmetry_type == self.parameters.geometry_eigth_symmetry:
            circ_divisions = int(self.parameters.divisions_circumferential / 4)
        elif self.parameters.model_symmetry_type == self.parameters.geometry_quarter_symmetry:
            circ_divisions = int(self.parameters.divisions_circumferential / 2)
        elif self.parameters.model_symmetry_type == self.parameters.geometry_half_symmetry:
            circ_divisions = self.parameters.divisions_circumferential
        elif self.parameters.model_symmetry_type == self.parameters.geometry_no_symmetry:
            circ_divisions = self.parameters.divisions_circumferential

        sel_vol_radius = round(self.triso_particle_outer_radius - self.parameters.geometry_tolerance,
                               self.parameters.tolerance_rounding - 1)
        
        ####################### outer surface of particle #######################
        psf_string_list.append(self.create_sphere_sel_vol(
            selection_volume_name=self.parameters.selection_volume_temp_name,
            radius=sel_vol_radius,
            circumferntial_divisions=self.parameters.divisions_circumferential))

        psf_string_list.append(self.create_element_set_from_selection_volume(
            element_set_name=self.parameters.elements_triso_particle_outer_surface,
            boundary_or_continuum=self.parameters.set_type_element_boundary,
            phys_mesh=self.parameters.physical_mesh_name,
            coverage_flag=self.parameters.word_completely,
            side_flag=self.parameters.word_outside,
            selection_volume_name=self.parameters.selection_volume_temp_name))
        return psf_string_list

    def todo_create_bc_sets_for_triso_compact(self):
        """

        :return psf_string_list: psf formatted list of strings for creating boundary condition element/node sets for create_triso_compact
        """
        psf_string_list = []
        return psf_string_list

class MeshTool(HexMesh):
    """

    This class combines all classes in the `meshing` module and can be imported into other python files to 
    easily access all meshing features.

    Supporting Classes Used:
        :class: `QuadPatch`,
        :class: `HexMesh`,
        :class: `Rz2dRod`,
        :class: `Rtheta2D`,
        :class: `Rtheta3D`,
        :class: `Triso`
    """

    def __init__(self, input_file_path=None):
        super(MeshTool, self).__init__(input_file_path)
        self.mesh_quad_patch_tool = QuadPatch(input_file_path)
        self.mesh_hex_mesh_tool = HexMesh(input_file_path)
        self.mesh_rz_2d_rod_tool = Rz2dRod(input_file_path)
        self.mesh_rtheta_2d_rod_tool = Rtheta2D(input_file_path)
        self.mesh_rtheta_3d_rod_tool = Rtheta3D(input_file_path)
        self.mesh_triso_tool = Triso(input_file_path)
        self.dict_regions = {}

    def get_regions_dict(self):
        """

        Gets the regions dictionary for a specified mesh type

        """
        # r_z_2d dict
        if len(self.mesh_rz_2d_rod_tool.creation_dict_region) != 0:
            self.dict_regions = self.mesh_rz_2d_rod_tool.creation_dict_region

        # r_theta_2d dict
        elif len(self.mesh_rtheta_2d_rod_tool.creation_dict_region) != 0:
            self.dict_regions = self.mesh_rtheta_2d_rod_tool.creation_dict_region

        # r_theta_3d dict
        elif len(self.mesh_rtheta_3d_rod_tool.creation_dict_region) != 0:
            self.dict_regions = self.mesh_rtheta_3d_rod_tool.creation_dict_region

        # triso dict
        elif len(self.mesh_triso_tool.creation_dict_region) != 0:
            self.dict_regions = self.mesh_triso_tool.creation_dict_region
        else:
            pass


if __name__ == "__main__":
    # param_file_test = "/home/projects/griffinpy/scrip_gen_tests/input/generator_input.yaml"
    # rz_2d_rod_tool = Mesh(param_file_test)
    out_file_test = "/home/projects/shared_folders/cases/triso/deffects/testing_eccentric2.psf"
    mt_triso = Triso()
    psf_string_list = []
    print("\n================================================================")

    psf_string_list.extend(mt_triso.create_eccentric_spheres())

    f = open(f"{out_file_test}", "w+")
    f.truncate()
    for string in range(0, len(psf_string_list)):
        f.write(psf_string_list[string])
    f.close()

    # Poly line
    # point_1=[0.0,1.0,5.0]
    # point_2=[5.0,1.0,5.0]
    # num_pts=10
    # ints_string_list = params.create_linear_radial_polyline("test_poly",point_1,point_2,num_pts)
    # psf_string_list.extend(ints_string_list)
