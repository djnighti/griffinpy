# import template class from griffinpy
from template_creation import Template

if __name__ == "__main__":
    # generator input file and path
    param_file_test = "/home/projects/griffinpy/scrip_gen_tests/scrip_gen_input/generator_input_1_4.yaml"
    
    # creating Template class instance which is initialized with the new generator input
    template_example = Template(param_file_test)

    # creating list of strings which is the actual PSF! 
    create_template_string_list = template_example.create_psf()

    # creating PSF to write to
    output_psf = open(f"{template_example.parameters.file_pegasus_location}", "w+")
    
    # doing for loop over list to write one string at a time to the output
    for string in range(0, len(create_template_string_list)):
        try:
            output_psf.write(create_template_string_list[string])
        except TypeError:
            print(f"string: {string}")