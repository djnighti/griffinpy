import os
import yaml
import numpy as np
import copy

class Parameters:
    """

    """
    def __init__(self, input_file_path=None):

        self.tolerance_rounding = 8

        ################ Comment Sections ################
        self.level_header_0 = 0
        self.level_header_1 = 1
        self.level_header_2 = 2
        self.level_header_3 = 3

        ################ Prefixes Params ################
        self.prefix_model = "model"
        self.prefix_bc = "bc"
        self.prefix_file = "file"
        self.prefix_geometry = "geometry"
        self.prefix_divisions = "divisions"
        self.prefix_material_property = "material"
        self.prefix_flag = "flag"
        self.prefix_retrieve = "retrieve"
        self.prefix_inventory_gio = "inventory_gio"
        
        ################ Dictionary Params ################
        self.param_flags_dict = {}
        self.param_retrieve_dict = {}
        self.material_associations_dict = {}
        
        ################ Problem definition Params ################
        
        # Modeling Flags
        self.model_flag_dimmension_d2 = False
        self.model_flag_dimmension_d3 = False
        # solve type
        self.model_flag_solve_axisymmetric = False
        self.model_flag_solve_plane_strain = False
        self.model_flag_solve_plane_stress = False

        # 2D mesh flags
        self.model_flag_rod_rz_simple = False
        self.model_flag_rod_rz_detailed_smeared = False
        self.model_flag_rod_rz_detailed_discrete = False
        self.model_flag_rod_rz_detailed_plenum = False
        self.model_flag_rod_rz_detailed_spring = False
        self.model_flag_rod_rz_detailed_blanket_pellet = False
        self.model_flag_rod_r_theta_simple = False
        self.model_flag_rod_r_theta_detailed_crack = False
        self.model_flag_rod_r_theta_detailed_mps = False
        self.model_flag_rod_r_theta_detailed_crack_and_mps = False

        # 3D mesh flags
        self.model_flag_rod_r_theta_z_simple = False
        self.model_flag_rod_r_theta_z_detailed_smeared = False
        self.model_flag_rod_r_theta_z_detailed_discrete = False
        
        # triso mesh flags
        self.model_flag_triso_particle = False
        self.model_flag_triso_pellet = False
        self.model_flag_multi_clad_layers = False
        self.model_flag_triso_plot_design = False
        self.model_flag_triso_plot_d3 = False
        self.model_flag_triso_plot_build = False
        
        # symmetry flags
        self.model_flag_symmetry_eigth= False
        self.model_flag_symmetry_quarter= False
        self.model_flag_symmetry_half= False
        self.model_flag_enable_contact = False
        
        # Boundary condition flags
        self.bc_flag_gtd_create = False
        self.bc_flag_function_gravity = False
        self.bc_flag_function_heat_flux = False
        self.bc_flag_function_temperature_cladding = False
        self.bc_flag_function_temperature_plenum = False
        self.bc_flag_function_pressure = False
        self.bc_flag_function_power = False
        self.bc_flag_gtd_heat_flux = False
        self.bc_flag_gtd_temperature_cladding = False
        self.bc_flag_gtd_temperature_plenum = False
        self.bc_flag_gtd_pressure = False
        self.bc_flag_gtd_power = False
        self.bc_flag_symmetry_xy = False
        self.bc_flag_symmetry_xz = False
        self.bc_flag_symmetry_yz = False
        self.bc_flag_inventory_gas = False
        self.bc_flag_inventory_fuel = False
        
        ################ Meshing Params ################
        
        # Geometric Parameters
        self.geometry_fuel_fuel_gap_height = 1.0E-3
        self.geometry_fuel_endcap_gap_thickness = 1.0E-3
        self.geometry_fuel_radius = 1.0E-3
        self.geometry_fuel_height_smeared = 1.0E-3
        self.geometry_fuel_height_discrete = 1.0
        self.geometry_fuel_mps_length = 1.0E-3
        self.geometry_cladding_inner_radius = 1.0E-3
        self.geometry_cladding_outer_radius = 1.0E-3
        self.geometry_fuel_diameter = 1.0E-3        
        self.geometry_gap_thickness = 1.0E-3   
        self.geometry_plenum_height_top = 1.0E-3
        self.geometry_plenum_height_bottom = 1.0E-3
        self.geometry_end_cap_thickness = 1.0E-3
        self.geometry_number_of_pellets = None
        self.geometry_cladding_thickness_list = [1.0E-3]
        self.geometry_tolerance = 1.0E-7
        self.geometry_selection_volume_threshold = 1.0E-6
        self.geometry_deformation_scale = 1.0
        self.geometry_dimmension_2 = int(2)
        self.geometry_dimmension_3 = int(3)
        self.geometry_eigth_symmetry = 45
        self.geometry_quarter_symmetry = 90
        self.geometry_half_symmetry = 180
        self.geometry_no_symmetry = 360
        self.geometry_triso_kernal_radius = 1.00e-3
        self.geometry_triso_buffer_radius = 1.50e-3
        self.geometry_triso_inner_pyrolytic_radius = 1.80e-3
        self.geometry_triso_silicon_carbide_radius = 2.00e-3
        self.geometry_triso_outer_pyrolytic_radius = 2.10e-3
        self.geometry_triso_graphite_block_scalar = 1.80e-3
        self.geometry_triso_num_particles_across_diameter = 1.80e-3
        self.geometry_triso_particles_density_percent = 1.80e-3       
        self.geometry_triso_graphite_block_radius  = 1.80e-3
        self.geometry_triso_thickness_list = [1]
        self.geometry_mesh_positions_dict = {}
        
        # Element divisions
        self.divisions_radial_fuel = 1
        self.divisions_radial_gap = 1
        self.divisions_radial_cladding_list = [1]
        self.divisions_circumferential = 1
        self.divisions_axial_fuel_discrete = 1
        self.divisions_axial_fuel_smeared = 1
        self.divisions_axial_plenum_top = 1
        self.divisions_axial_plenum_bottom = 1
        self.divisions_axial_fuel_fuel_gap = 1
        self.divisions_axial_end_cap = 1
        self.divisions_radial_triso_list = [1]

        # object names
        self.fuel_name = "FUEL"
        self.fuel_center_name = "FUEL_CENTER"
        self.fuel_cladding_gap_name = "FUEL_CLADDING_GAP"
        self.cladding_name = "CLADDING"
        self.end_cap_top_name = "END_CAP_TOP_ASSEMBLY"
        self.end_cap_bottom_name = "END_CAP_BOTTOM_ASSEMBLY"
        self.gap_top_name = "GAP_TOP"
        self.gap_bottom_name = "GAP_BOTTOM"
        self.plenum_top_name = "PLENUM_ASSEMBLY_TOP"
        self.plenum_bottom_name = "PLENUM_ASSEMBLY_BOTTOM"
        self.plenum_name = "PLENUM"
        self.plenum_assembly_name = "PLENUM_ASSEMBLY"
        self.fuel_fuel_gap_name = "FUEL_FUEL_GAP"
        self.fuel_fuel_gap_assembly_name = "FUEL_FUEL_GAP_ASSEMBLY"
        self.spring_name = "SPRING"
        self.rod_name = "ROD"
        self.unit_rod_name = "UNIT_ROD"
        self.rectangle_name = "RECTANGLE"
        self.arc_name = "ARC"
        self.center_disk_name = "CENTER_DISK"
        self.disk_name = "DISK"
        self.ring_name = "RING"
        self.rod_end_caps_name = "ROD_END_CAPS"
        self.rod_plenum_name = "ROD_PLENUM"


        self.triso_particle_name = "TRISO_PARTICLE"
        self.triso_particle_layer_name = "TRISO_PARTICLE_LAYER"
        self.void_particle_name = "VOID_PARTICLE"
        self.triso_matrix_name = "TRISO_MATRIX"

        # Quad patch names
        self.qp_name = "QP_"
        self.qp_fuel_name =  self.qp_name+self.fuel_name
        self.qp_fuel_center_name = self.qp_name+self.fuel_center_name
        self.qp_fuel_cladding_gap_name = self.qp_name+self.fuel_cladding_gap_name
        self.qp_cladding_name = self.qp_name+self.cladding_name
        self.qp_end_cap_top_name = self.qp_name+self.end_cap_top_name
        self.qp_end_cap_bottom_name = self.qp_name+self.end_cap_bottom_name
        self.qp_gap_top_name = self.qp_name+self.gap_top_name
        self.qp_gap_bottom_name = self.qp_name+self.gap_bottom_name
        self.qp_plenum_top_name = self.qp_name+self.plenum_top_name
        self.qp_plenum_bottom_name = self.qp_name+self.plenum_bottom_name
        self.qp_fuel_fuel_gap_name = self.qp_name+self.fuel_fuel_gap_name
        self.qp_fuel_fuel_gap_assembly_name = self.qp_name+self.fuel_fuel_gap_assembly_name
        self.qp_spring_name = self.qp_name+self.spring_name
        self.qp_rod_name = self.qp_name+self.rod_name
        self.qp_unit_rod_name = self.qp_name+self.unit_rod_name
        self.qp_rectangle_name = self.qp_name+self.rectangle_name
        self.qp_arc_name = self.qp_name+self.arc_name
        self.qp_center_disk_name = self.qp_name+self.center_disk_name
        self.qp_disk_name = self.qp_name+self.disk_name
        self.qp_ring_name = self.qp_name+self.ring_name

        # temporary object names
        self.temporary_object_1_name = 'TMP_OBJ_1'
        self.temporary_object_2_name = 'TMP_OBJ_2'
        self.temporary_object_auto_stack_name = 'TMP_OBJ_AS'
        self.temporary_object_auto_grid_gen_name = 'TMP_OBJ_AGG'
        self.temporary_object_void_name = 'TMP_OBJ_VOID'
        self.temporary_object_triso_name = 'TMP_OBJ_TRISO'
        self.temporary_object_auto_revolve_name = 'TMP_OBJ_AR'

        # Mark names
        self.mark_cladding_outer_surface_name = "CLAD_OUTER_SURFACE_MARK"
        self.mark_bottom_surface_name = "BOTTOM_SURFACE_MARK"
        
        # Region names
        self.region_dict = {}
        self.region_prefix_name = "REGION_"
        self.region_fuel = "REGION_FUEL"
        self.region_fuel_cladding_gap = "REGION_FUEL_CLADDING_GAP"
        self.region_cladding = "REGION_CLADDING"
        self.region_fuel_end_cap_gap = "REGION_FUEL_END_CAP_GAP"
        self.region_fuel_fuel_gap = "REGION_FUEL_FUEL_GAP"
        self.region_end_cap = "REGION_END_CAP"
        self.region_plenum = "REGION_PLENUM"
        self.region_default = "REGION_DEFAULT"
        self.region_kernal = "REGION_KERNAL"
        self.region_buffer = "REGION_BUFFER"
        self.region_inner_pyrolytic = "REGION_INNER_PYROLYTIC"
        self.region_silicon_carbide = "REGION_SILICON_CARBIDE"
        self.region_outer_pyrolytic = "REGION_OUTER_PYROLYTIC"
        self.region_graphite = "REGION_GRAPHITE"
        self.region_triso_layer = "REGION_TRISO_LAYER"
        
        # Hex mesh names
        self.hex_name = "HEX_"
        self.hex_cylinder_name = "HEX_CYLINDER"
        self.hex_pipe_name = "HEX_PIPE"
        self.hex_sphere_name = "HEX_SPHERE"
        self.hex_cube_name = "HEX_CUBE"
        self.hex_cuboid_name = "HEX_CUBOID"
        self.hex_cross_name = "HEX_CROSS"

        self.hex_fuel_name =  self.hex_name+self.fuel_name
        self.hex_fuel_center_name = self.hex_name+self.fuel_center_name
        self.hex_fuel_cladding_gap_name = self.hex_name+self.fuel_cladding_gap_name
        self.hex_cladding_name = self.hex_name+self.cladding_name
        self.hex_end_cap_top_name = self.hex_name+self.end_cap_top_name
        self.hex_end_cap_bottom_name = self.hex_name+self.end_cap_bottom_name
        self.hex_gap_top_name = self.hex_name+self.gap_top_name
        self.hex_gap_bottom_name = self.hex_name+self.gap_bottom_name
        self.hex_plenum_top_name = self.hex_name+self.plenum_top_name
        self.hex_plenum_bottom_name = self.hex_name+self.plenum_bottom_name
        self.hex_fuel_fuel_gap_name = self.hex_name+self.fuel_fuel_gap_name
        self.hex_fuel_fuel_gap_assembly_name = self.hex_name+self.fuel_fuel_gap_assembly_name
        self.hex_spring_name = self.hex_name+self.spring_name
        self.hex_rod_name = self.hex_name+self.rod_name
        self.hex_unit_rod_name = self.hex_name+self.unit_rod_name
        self.hex_rectangle_name = self.hex_name+self.rectangle_name
        self.hex_arc_name = self.hex_name+self.arc_name
        self.hex_center_disk_name = self.hex_name+self.center_disk_name
        self.hex_disk_name = self.hex_name+self.disk_name
        self.hex_ring_name = self.hex_name+self.ring_name
        self.hex_triso_particle_name = self.hex_name+self.triso_particle_name
        self.hex_triso_particle_layer_name = self.hex_name+self.triso_particle_layer_name
        self.hex_void_particle_name = self.hex_name+self.void_particle_name
        self.hex_triso_matrix_name = self.hex_name+self.triso_matrix_name
        
        # Selection Volume names
        self.selection_volume_temp_name = "TEMPORARY_SELECTION_VOLUME"
        
        # Key words
        self.word_completely = "COMPLETELY"
        self.word_partially = "PARTIALLY"
        self.word_boundary_only = "BOUNDARY_ONLY"
        self.word_surface_only = "SURFACE_ONLY"
        self.word_boundary_and_surface = "BOUNDARY_AND_SURFACE"
        self.word_inside = "INSIDE"
        self.word_outside = "OUTSIDE"
        self.word_with = "WITH"
        self.word_without = "WITHOUT"
        
        # Physical mesh names
        self.physical_mesh_name = "ROD"

        # Element/Node set type names
        self.set_type_element_boundary = "BOUNDARY_ELEMENT_SET"
        self.set_type_element_continuum = "CONTINUUM_ELEMENT_SET"
        self.set_type_node = "NODE_SET"

        # Elements names
        self.elements_prefix_name = "ELEMENTS_"
        self.elements_all_name = "ELEMENTS_ALL"
        self.elements_fuel = "ELEMENTS_FUEL"
        self.elements_fuel_cladding_gap = "ELEMENTS_FUEL_CLADDING_GAP"
        self.elements_cladding = "ELEMENTS_CLADDING"
        self.elements_plenum = "ELEMENTS_PLENUM"
        self.elements_cladding_outer_surface = "ELEMENTS_CLADDING_OUTER_SURFACE"
        self.elements_triso_particle_outer_surface = "ELEMENTS_TRISO_PARTICLE_OUTER_SURFACE"
        self.elements_triso_compact_outer_surface = "ELEMENTS_TRISO_COMPACT_OUTER_SURFACE"
        
        # Nodes names
        self.nodes_all_name = "NODES_ALL"
        self.nodes_fuel = "NODES_FUEL"
        self.nodes_fuel_cladding_gap = "NODES_FUEL_CLADDING_GAP"
        self.nodes_cladding = "NODES_CLADDING"
        self.nodes_origin = "NODES_ORIGIN"
        self.nodes_clad_od_pos_y = "NODES_CLADDING_OD_POSITIVE_Y"
        self.nodes_clad_od_pos_x = "NODES_CLADDING_OD_POSITIVE_X"
        self.nodes_clad_od_neg_y = "NODES_CLADDING_OD_NEGATIVE_Y"
        self.nodes_clad_od_neg_x = "NODES_CLADDING_OD_NEGATIVE_Y"
        self.nodes_plane_xz = "NODES_PLANE_XZ"
        self.nodes_plane_yz = "NODES_PLANE_YZ"
        self.nodes_bottom_of_rod = "NODES_BOTTOM_OF_ROD"
        self.nodes_bottom_of_fuel = "NODES_BOTTOM_OF_FUEL"
        self.nodes_top_of_rod = "NODES_TOP_OF_ROD"
        
        # Capture Data names
        self.cd_elements_all_name = "CD_ELEMENTS_ALL"
        self.cd_nodes_all_name = "CD_NODES_ALL"
        
        ################ Materials Params ################
        
        # Materials names and alias
        self.material_prefix_name = "MATERIAL_"
        self.material_alias_name_fuel = "MATERIAL_FUEL"
        self.material_alias_name_fuel_cladding_gap = "MATERIAL_FUEL_CLADDING_GAP"
        self.material_alias_name_cladding_list = "MATERIAL_CLADDING"
        self.material_alias_name_gas = "MATERIAL_GAS"
        self.material_alias_name_plenum = "MATERIAL_PLENUM"
        self.material_alias_name_end_cap = "MATERIAL_END_CAP"
        self.material_alias_name_general = "MATERIAL_J2"
        self.material_alias_name_triso_list = "MATERIAL_TRISO_LAYER"
        self.material_fuel_class = "Uo2Fuel"
        self.material_fuel_cladding_gap_class = "PelletCladGapPseudo"
        self.material_cladding_list_class = ["GenericZircNonfuel"]
        self.material_plenum_class = "Gas"
        self.material_end_cap_class = "J2ThermoElasticPlastic"
        self.material_gas_class = "Gas"
        self.material_general_class = "J2ThermoElasticPlastic"
        self.material_triso_list_class = []
        self.material_fuel_retrieve_state_variables = None
        self.material_fuel_retrieve_coordinates = None
        self.material_fuel_cladding_gap_retrieve_state_variables = None
        self.material_fuel_cladding_gap_retrieve_coordinates = None
        self.material_cladding_list_retrieve_state_variables = None
        self.material_cladding_list_retrieve_coordinates = None
        self.material_gas_retrieve_state_variables = None
        self.material_gas_retrieve_coordinates = None
        self.material_plenum_retrieve_state_variables = None
        self.material_plenum_retrieve_coordinates = None
        self.material_end_cap_retrieve_state_variables = None
        self.material_end_cap_retrieve_coordinates = None
        self.material_general_retrieve_state_variables = None
        self.material_general_retrieve_coordinates = None
        self.material_triso_list_retrieve_state_variables = None
        self.material_triso_list_retrieve_coordinates = None
        
        # Materials values
        #------------------ GENERIC_ZIRC_NONFUEL ------------------#
        self.material_property_zirc_density = 6560.0
        self.material_property_zirc_default_internal_energy = 0.0
        self.material_property_zirc_default_temperature = 298.0
        self.material_property_zirc_poisson_ratio = 0.37
        self.material_property_zirc_SIG_C_0 = 0.0
        self.material_property_zirc_ALPHA_C = 0.0
        self.material_property_zirc_EPS_C_0 = 1.0
        self.material_property_zirc_SIG_P_0 = 0.0
        self.material_property_zirc_ALPHA_P = 0.0
        self.material_property_zirc_EPS_P_0 = 1.0
        self.material_property_zirc_INITIAL_YIELD_STRESS = 400.0E6
        self.material_property_zirc_INITIAL_YIELD_SLOPE_MULTIPLIER = 1.0
        self.material_property_zirc_HARD_EXP = 0.25
        self.material_property_zirc_cold_work_factor = 0.0
        self.material_property_zirc_initial_oxide_layer_thickness = 0.0
        self.material_property_zirc_initial_additional_oxygen_concentration = 0.0
        self.material_property_zirc_oxide_conductivity = 1.7
        self.material_property_zirc_surface_roughness = 1.8E-05
        self.material_property_zirc_material_axis_origin_x = 0.0
        self.material_property_zirc_material_axis_origin_y = 0.0
        self.material_property_zirc_material_axis_origin_z = 0.0
        self.material_property_zirc_material_axis_direction_x = 0.0
        self.material_property_zirc_material_axis_direction_y = 0.0
        self.material_property_zirc_material_axis_direction_z = 1.0
        self.material_property_zirc_creep_rate_multiplier = 1.2
        self.material_property_zirc_irradiation_growth_strain_multiplier = 1.0
        self.material_property_zirc_reactor_type = "PWR"
        self.material_property_zirc_creep_rate_model = "LIMBACK_SECONDARY"

        #------------------ UO2_FUEL ------------------#
        self.material_property_uo2_density = 11000.0
        self.material_property_uo2_default_internal_energy = 0.0
        self.material_property_uo2_default_temperature = 298.0
        self.material_property_uo2_poisson_ratio = 0.33
        self.material_property_uo2_initial_fission_density = 0.0
        self.material_property_uo2_reference_pressure = 500.0E6
        self.material_property_uo2_dil_temperature_rate = 0.002
        self.material_property_uo2_dil_temperature_factor = 0.01
        self.material_property_uo2_dil_creep_exponent = 3.0
        self.material_property_uo2_fracture_strain_ratio = -0.2
        self.material_property_uo2_closure_factor = 0.5
        self.material_property_uo2_open_porosity_ratio = 0.05
        self.material_property_uo2_manufactured_porosity = 0.015
        self.material_property_uo2_initial_gadolinium_enrichment = 0.04
        self.material_property_uo2_initial_PuO2_weight_fraction = 0.0
        self.material_property_uo2_initial_Oxygen_to_metal_ratio = 2.0
        self.material_property_uo2_grain_size = 1.0E-5
        self.material_property_uo2_surface_roughness = 1.0E-5
        self.material_property_uo2_material_axis_origin_x = 0.0
        self.material_property_uo2_material_axis_origin_y = 0.0
        self.material_property_uo2_material_axis_origin_z = 0.0
        self.material_property_uo2_material_axis_direction_x = 0.0
        self.material_property_uo2_material_axis_direction_y = 0.0
        self.material_property_uo2_material_axis_direction_z = 1.0
        self.material_property_uo2_as_manufactured_gap_width = self.geometry_gap_thickness
        self.material_property_uo2_as_manufactured_pellet_diameter = self.geometry_fuel_diameter
        self.material_property_uo2_max_densification_strain = 0.01
        self.material_property_uo2_swelling_multiplier = 1.0
        self.material_property_uo2_relocation_threshold = 2.0
        self.material_property_uo2_relocation_strain_multiplier = 1.0
        self.material_property_uo2_fast_flux_multiplier = 1.0
        self.material_property_uo2_fgr_parameter = 1.0
        self.material_property_uo2_creep_strain_multiplier = 1.0
        self.material_property_uo2_initial_fuel_composition = "DEFAULT_UO2"
        self.material_property_uo2_fission_gas_release_model = "SIMPLE_FGR"
        
        #------------------ PELLET_CLAD_GAP_PSEUDO_MAT ------------------#
        self.material_property_gap_density = 0.1
        self.material_property_gap_default_internal_energy = 0.0
        self.material_property_gap_default_temperature = 298.0
        self.material_property_gap_closed_gap_stiffness = 72.0E9
        self.material_property_gap_gap_stiffness_rate = 1.0
        self.material_property_gap_friction_coefficient = 0.25
        self.material_property_gap_gas_heat_capacity = 5.0E3
        self.material_property_gap_clad_surface_roughness = 1.0E-6
        self.material_property_gap_fuel_surface_roughness = 1.0E-6
        self.material_property_gap_clad_oxide_layer_thickness = 0.0
        self.material_property_gap_gap_condutance_multiplier = 1.0
        
        #------------------ GAP_PSEUDO_MATERIAL ------------------#
        self.material_property_gap_density = 0.1
        self.material_property_gap_default_internal_energy = 0.0
        self.material_property_gap_default_temperature = 298.0
        self.material_property_gap_closed_gap_stiffness = 72.0E9
        self.material_property_gap_gap_stiffness_rate = 1.0
        self.material_property_gap_friction_coefficient = 0.25
        self.material_property_gap_open_conductance = 1.5E3
        self.material_property_gap_closed_conductance = 6.0E3
        self.material_property_gap_gas_heat_capacity = 5.0E3
        
        #------------------ GAS ------------------#
        self.material_property_gas_density = 0.1
        self.material_property_gas_default_internal_energy = 0.0
        self.material_property_gas_default_temperature = 298.0
        
        #------------------ J2_THERMO_ELASTIC_PLASTIC ------------------#
        self.material_property_j2_density = 6560.0
        self.material_property_j2_default_internal_energy = 0.0
        self.material_property_j2_default_temperature = 298.0
        self.material_property_j2_elastic_modulus = 20.0E9
        self.material_property_j2_poisson_ratio = 0.37
        self.material_property_j2_hardening_exponent = 0.4
        self.material_property_j2_yield_stress_coeff_0 = 300.0E12
        self.material_property_j2_yield_stress_coeff_1 = 800.0
        self.material_property_j2_reference_strain = 1.0
        self.material_property_j2_heat_capacity = 500.0
        self.material_property_j2_conductivity = 200.0
        self.material_property_j2_thermal_expansivity = 1.0E-6
        
        #------------------ UCO_FUEL ------------------#
        self.material_property_uco_density = 3200.0
        self.material_property_uco_default_internal_energy = 0.0
        self.material_property_uco_default_temperature = 298.0
        self.material_property_uco_thermal_expansivity = 15.0e-6
        self.material_property_uco_porosity = 0.05
        self.material_property_uco_initial_oxygen_to_uranium_ratio = 1.5
        self.material_property_uco_initial_carbon_to_uranium_ratio = 0.4
        self.material_property_uco_initial_uo2_enrichment_wt_percent = 0.155
        self.material_property_uco_reference_pressure = 500.0E6
        self.material_property_uco_dil_temperature_rate = 0.002
        self.material_property_uco_dil_temperature_factor = 0.01
        self.material_property_uco_dil_creep_exponent = 3.0
        self.material_property_uco_creep_multiplier = 1.0
        self.material_property_uco_swelling_multiplier = 1.0
        self.material_property_uco_material_axis_origin_x = 0.0
        self.material_property_uco_material_axis_origin_y = 0.0
        self.material_property_uco_material_axis_origin_z = 0.0
        self.material_property_uco_material_axis_direction_x = 0.0
        self.material_property_uco_material_axis_direction_y = 0.0
        self.material_property_uco_material_axis_direction_z = 1.0
        self.material_property_uco_fission_gas_release_model = "NO_FGR"
        
        #------------------ SIC_NONFUEL ------------------#
        self.material_property_sic_density = 3200.0
        self.material_property_sic_default_internal_energy = 0.0
        self.material_property_sic_default_temperature = 298.0
        self.material_property_sic_poisson_ratio = 0.13
        self.material_property_sic_thermal_expansivity = 4.9e-6
        self.material_property_sic_initial_yield_stress = 400.0e6
        self.material_property_sic_initial_yield_slope_multiplier = 1.0
        self.material_property_sic_hard_exp = 0.25
        self.material_property_sic_fast_flux_multiplier = 1.0
        self.material_property_sic_creep_rate_model = "DEFAULT"
        
        #------------------ PYC_NONFUEL ------------------#
        self.material_property_pyc_density = 1900.0
        self.material_property_pyc_default_internal_energy = 0.0
        self.material_property_pyc_default_temperature = 298.0
        self.material_property_pyc_poisson_ratio = 0.13
        self.material_property_pyc_thermal_conductivity = 4.0
        self.material_property_pyc_specific_heat_capacity = 720.0
        self.material_property_pyc_as_fab_bacon_anisotropy_factor = 1.05 
        self.material_property_pyc_crystallite_diameter = 30.0
        self.material_property_pyc_initial_yield_stress = 400.0e6
        self.material_property_pyc_initial_yield_slope_multiplier = 1.0
        self.material_property_pyc_hard_exp = 0.25
        self.material_property_pyc_irradiation_strain_multiplier = 1.0
        self.material_property_pyc_creep_rate_multiplier = 1.0
        self.material_property_pyc_fast_flux_value = 0.0
        self.material_property_pyc_creep_rate_model = "DEFAULT"
        
        #------------------ BUFFER_NONFUEL ------------------#
        self.material_property_buffer_density = 2250.0
        self.material_property_buffer_default_internal_energy = 0.0
        self.material_property_buffer_default_temperature = 298.0
        self.material_property_buffer_poisson_ratio = 0.13
        self.material_property_buffer_thermal_conductivity_theoretical = 4.0
        self.material_property_buffer_thermal_conductivity_initial = 0.5
        self.material_property_buffer_specific_heat_capacity = 720.0
        self.material_property_buffer_density_initial = 1000.0
        self.material_property_buffer_initial_yield_stress = 400.0e6
        self.material_property_buffer_initial_yield_slope_multiplier = 1.0
        self.material_property_buffer_hard_exp = 0.25
        self.material_property_buffer_irradiation_strain_multiplier = 1.0
        self.material_property_buffer_creep_rate_multiplier = 1.0
        self.material_property_buffer_fast_flux_value = 0.0
        self.material_property_buffer_creep_rate_model = "DEFAULT"

        ################ Retrieve Params ################
        self.state_variable_type_node = "node"
        self.state_variable_type_element = "element"
        
        # common retrievable state variables
        self.standard_retrieve_heat_flux_x = self.state_variable_type_element
        self.standard_retrieve_heat_flux_y = self.state_variable_type_element
        self.standard_retrieve_heat_flux_z = self.state_variable_type_element
        self.standard_retrieve_internal_energy = self.state_variable_type_element
        self.standard_retrieve_cauchy_stress_xx = self.state_variable_type_element
        self.standard_retrieve_cauchy_stress_yy = self.state_variable_type_element
        self.standard_retrieve_cauchy_stress_zz = self.state_variable_type_element
        self.standard_retrieve_cauchy_stress_thetatheta = self.state_variable_type_element
        self.standard_retrieve_cauchy_stress_rr = self.state_variable_type_element
        self.standard_retrieve_temperature = self.state_variable_type_node
        self.standard_retrieve_displacement_x = self.state_variable_type_node
        self.standard_retrieve_displacement_y = self.state_variable_type_node
        self.standard_retrieve_displacement_z = self.state_variable_type_node

        # uo2
        # zirc
        # pellet clad psuedo gap
        # gas
        # J2
        # uco
        # sic
        # pyc
        # buffer

        
        ################ Inventory objects Params ################

        # inventory object names
        self.name_gio = "all_mesh_gio"
        self.name_fio = "all_mesh_fio"

        # Gas inventory object
        self.inventory_gio_initial_gas_pressure = 1.99948E6
        self.inventory_gio_initial_gas_composition = "DEFAULT_HE_GAS"
        self.inventory_gio_external_volume_for_calculations = 0.0
        self.inventory_gio_ease_time_for_pressure_bc = 1.0
        self.inventory_gio_plenum_bc_htc = 1000.0

        # Fuel inventory object
        self.inventory_fio_initial_burnup = "ZERO_BURNUP"
        self.inventory_fio_initial_fluence = "ZERO_FLUENCE"
        
        ################ Boundary conditions Params ################

        # Boundary types
        self.bc_boundary_type_segment = "segment"
        self.bc_boundary_type_facet = "facet"
        self.bc_boundary_type_nodal = "nodal"
        self.bc_boundary_type = None
        
        # Boundary condition names
        self.bc_name_fixed_x = "fixed_x_bc"
        self.bc_name_fixed_y = "fixed_y_bc"
        self.bc_name_fixed_z = "fixed_z_bc"
        self.bc_name_heat_flux = "coolant_heat_flux_bc"
        self.bc_name_pressure = "pressure_bc"
        self.bc_name_temperature = "temperature_bc"
        self.bc_name_heat_source = "ROD_POWER"
        self.bc_name_power_distribution = "rod_power_distribution"
        self.bc_name_motionless_isothermal = "MOTIONLESS ISOTHERMAL"

        # Boundary conditions value names
        self.bc_val_name_htc = "HTC"
        self.bc_val_name_room_temp = "ROOM_TEMP"
        self.bc_val_name_delta_start_temp = "DELTA_START_TEMP"
        self.bc_val_name_coolant_pressure = "COOLANT_PRESSURE"
        self.bc_val_name_atmospheric_pressure = "ATMOSPHERIC_PRESSURE"
        self.bc_val_name_one_hour = "ONE_HOUR"

        # Boundary condition Physical Qualities 
        self.bc_phys_qual_cauchy_pressure = "CAUCHY_PRESSURE"
        self.bc_phys_qual_heat_flux = f'HEAT_FLUX + TEMPERATURE * "{self.bc_val_name_htc}"'
        self.bc_phys_qual_disp_x = "DISPLACEMENT_1"
        self.bc_phys_qual_disp_y = "DISPLACEMENT_2"
        self.bc_phys_qual_disp_z = "DISPLACEMENT_3"
        self.bc_phys_qual_temperature = "TEMPERATURE"
        
        # Boundary condition function names
        self.bc_function_name_pressure = "PRESSURE_FUNCTION"
        self.bc_function_name_heat_flux = "HEAT_FLUX_FUNCTION"
        self.bc_function_name_temperature = "TEMPERATURE_FUNCTION"
        self.bc_function_name_power_history = "POWER_HISTORY_FUNCTION"
        self.bc_function_name_axial_power = "AXIAL_POWER_FUNCTION"
        self.bc_function_name_gio_temp_history = "GIO_VOLUMETRIC_TEMPERATURE_FUNCTION"
        self.bc_function_name_gravity = "GRAVITY"
        self.bc_function_name_fixed_x = "FIXED_X"
        self.bc_function_name_fixed_y = "FIXED_Y"
        self.bc_function_name_fixed_z = "FIXED_Z"

        # Boundary condition gtd names
        self.bc_gtd_name_pressure_history = "pressure_history.gtd"
        self.bc_gtd_name_temperature_history = "temperature_history.gtd"
        self.bc_gtd_name_heat_flux_history = "heat_flux_history.gtd"
        self.bc_gtd_name_power_history = "power_history.gtd"
        self.bc_gtd_name_axial_power_shapes = "axial_power_shapes.gtd"
        self.bc_gtd_name_plenum_temperature_history = "plenum_temperature_history.gtd"
        
        # Boundary condition power dist object names
        self.bc_power_fraction_for_coolant_channel = 0.0
        self.bc_internal_tu_burnup_distribution = "TU_BURNUP_RAD_PWR_DIST"
        self.bc_interpolation_piecewise_linear = "PIECEWISE_LINEAR"
        self.bc_interpolation_piecewise_constant = "PIECEWISE_CONSTANT"

        # Boundary conditions values
        self.bc_val_initial_temperature = 298.0
        self.bc_val_peak_power = 18000.0
        self.bc_val_htc = 39626.8
        self.bc_val_room_temp = 298.15
        self.bc_val_delta_start_temp = 305.0
        self.bc_val_coolant_temp = 600.0
        self.bc_val_pressure_coolant = 15.4132E6
        self.bc_val_pressure_atmospheric = 0.101325E6
        self.bc_val_minute = 60.0
        self.bc_val_hour = self.bc_val_minute * 60.0
        self.bc_val_day = self.bc_val_hour * 24.0
        self.bc_val_week = self.bc_val_day * 7.0
        self.bc_val_year = self.bc_val_day * 365.0
        self.bc_val_time_power_up_start = self.bc_val_hour 
        self.bc_val_time_power_down_start = self.bc_val_week + 2*self.bc_val_hour  
        self.bc_val_time_power_down_end = self.bc_val_time_power_down_start + self.bc_val_hour
        
        # Boundary conditions functions
        self.bc_function_expression_power_ramp = "(LINEAR_RAMP((T-TIME_POWER_UP_START) / HOUR) - LINEAR_RAMP((T - (TIME_POWER_DOWN_START)) / HOUR))"
        self.bc_function_expression_pt_ramp = "(LINEAR_RAMP(T / HOUR) - LINEAR_RAMP((T-(TIME_POWER_DOWN_END)) / HOUR))"
        self.bc_function_expression_gas_ramp = "(LINEAR_RAMP(T / HOUR) + 1.5*LINEAR_RAMP((T-HOUR) / (23.0*HOUR)) - 2.5*LINEAR_RAMP((T-(TIME_POWER_DOWN_END)) / HOUR))"
        self.bc_function_expression_pressure = "PRESSURE_ATMOSPHERIC + PRESSURE_COOLANT * "+self.bc_function_expression_pt_ramp
        self.bc_function_expression_heat_flux = "ROOM_TEMP + DELTA_START_TEMP * "+self.bc_function_expression_pt_ramp
        self.bc_function_expression_temperature = "ROOM_TEMP + DELTA_START_TEMP * "+self.bc_function_expression_pt_ramp
        self.bc_function_expression_power_function = "PEAK_POWER * "+self.bc_function_expression_power_ramp
        self.bc_function_expression_axial_power_shapes = "1.0"
        self.bc_function_expression_gio_temp_history = "ROOM_TEMP + DELTA_START_TEMP * "+self.bc_function_expression_gas_ramp
        self.bc_function_expression_gravity = "9.8 * LINEAR_RAMP(T)"
        self.bc_function_expression_fixed_x_bc = 0.0
        self.bc_function_expression_fixed_y_bc = 0.0
        self.bc_function_expression_fixed_z_bc = 0.0
        
        # Directions/coordinates
        self.position_vector_origin = [0.0, 0.0, 0.0]
        self.position_vector_x = [1.0, 0.0, 0.0]
        self.position_vector_y = [0.0, 1.0, 0.0]
        self.position_vector_z = [0.0, 0.0, 1.0]
        self.position_vector_neg_z = [0.0, 0.0, -1.0]
        self.position_coordinate_origin = [0.0, 0.0, 0.0]
        self.position_coordinate_x = [1.0, 0.0, 0.0]
        self.position_coordinate_y = [0.0, 1.0, 0.0]
        self.position_coordinate_z = [0.0, 0.0, 1.0]
        self.bc_gravity_direction = [0.0, 0.0, -1.0]


        
        ################ Solution control Params ################
        
        # Tolerance
        self.sol_ctrl_deformable_quasistatic_finite_deformation = [2.0e3, 10.0]
        self.sol_ctrl_transient_thermal = [100.0, 0.1]
        self.sol_ctrl_maximum_number_of_line_search_per_newton = 12
        self.sol_ctrl_maximum_number_of_newton_pass_per_step = 32
        self.sol_ctrl_maximum_number_of_time_cuts = 8
        self.sol_ctrl_time_cut_factor = 0.25
        self.sol_ctrl_slow_convergence_residual_norm_ratio = 0.9
        self.sol_ctrl_fast_convergence_residual_norm_ratio = 0.1
        self.sol_ctrl_stab_heat_source_coeff = 1.0e6
        self.sol_ctrl_stab_body_force_coeff = 1.0e6
        
        # contact
        self.sol_ctrl_contact_friction_coefficient = 0.2
        self.sol_ctrl_contact_maximum_interpenetration_distance = 1.0E-5
        self.sol_ctrl_contact_maximum_gap_distance = 0.5E-4
        self.sol_ctrl_contact_maximum_slip_distance = 1.0E-5
        self.sol_ctrl_contact_thermal_conductivity_coefficients = [1.87E-8, 6.45E+3, 4.0E+3] 
        
        
        ################ Initialize Params ################
        self.model_flag_dimmension = None 
        self.model_flag_embed_triso = None
        self.model_symmetry_type = None
        self.model_solve_type = None

        self.file_pegasus_output_location= None
        self.file_pegasus_input_location= None
        self.file_pegasus_input_data= None
        self.file_pegasus_location= None
        self.file_pegasus_output_pics_relative_location= None
        self.file_pegasus_output_tabular_data_relative_location= None
        self.file_pegasus_output_database_relative_location= None
        self.file_pegasus_output_pics_location = None 
        self.file_pegasus_output_tabular_data_location = None 
        self.file_pegasus_output_database_location = None 

        self.position_z_top_end_cap = None 
        self.position_z_bottom_end_cap = None 
        self.position_z_top_plenum = None
        self.position_z_bottom_plenum = None

        self.geometry_cladding_inner_radius = None 
        self.geometry_cladding_outer_radius = None 
        self.geometry_cladding_thickness = None    
        self.geometry_fuel_diameter = None         
        self.geometry_triso_graphite_block_radius = None 
        self.geometry_negative_selection_volume_threshold = None         
        self.geometry_times_2_selection_volume_threshold = None          
        self.geometry_negative_times_2_selection_volume_threshold = None 
        self.geometry_outside_outer_clad_radius = None                   
        self.geometry_inside_outer_clad_radius = None                    
        self.geometry_times_2_outside_outer_clad_radius = None           
        self.geometry_negative_outside_outer_clad_radius = None          
        self.geometry_outside_top_rod_height = None                      
        self.geometry_outside_bottom_rod_height = None  



        ################ File management ################
        
        # Custom/Template Pegasus file
        self.psf_type_template = False
        self.psf_type_mesh_only = False
        
        # Files
        self.file_parameter_input_dictionary = {}
        self.file_pegasus_name = "default"
        self.file_database_name = "rod_db"
        self.file_directory_template_location = "default"
        self.file_directory_output_name = "output"
        self.file_directory_input_name = "input"
        self.file_input_data_name = "default"
        self.file_input_data_location = None
        self.file_create_database = False
        if input_file_path is not None:
            self.file_parameter_input_path = input_file_path
            self.update_parameters()
            self.update_model_flags()
            self.update_file_properties()
            self.update_geometry()
            self.create_geometry_mesh_positions_dict()
            self.update_selection_volumes()
            self.create_directory_template()
            self.create_material_associations_dict()
            
            # self.create_params_retrieve()

    ################ modeling flag properties ################ 

    def update_model_flags(self):
        # dimmension
        if self.model_flag_dimmension_d2:
            self.model_flag_dimmension = self.geometry_dimmension_2
        elif self.model_flag_dimmension_d3:
            self.model_flag_dimmension = self.geometry_dimmension_3

        # embed triso
        if self.model_flag_triso_particle:
            self.model_flag_embed_triso = False
        elif self.model_flag_triso_pellet:
            self.model_flag_embed_triso =  True
        
        # symmetry
        if self.model_flag_symmetry_eigth:
            self.model_symmetry_type = self.geometry_eigth_symmetry
        elif self.model_flag_symmetry_quarter:
            self.model_symmetry_type = self.geometry_quarter_symmetry
        elif self.model_flag_symmetry_half:
            self.model_symmetry_type = self.geometry_half_symmetry
        else:
            self.model_symmetry_type = self.geometry_no_symmetry
        
        # solve type
        if self.model_flag_solve_axisymmetric:
            self.model_solve_type = "AXISYMMETRIC"
        elif self.model_flag_solve_plane_strain:
            self.model_solve_type = "PLANE_STRAIN"
        elif self.model_flag_solve_plane_stress:
            self.model_solve_type = "PLANE_STRESS"
        else:
            self.model_solve_type = None

    ################ geometry properties ################ 

    def update_geometry(self):
        """

        """
        self.geometry_cladding_inner_radius = round(self.geometry_fuel_radius + self.geometry_gap_thickness, self.tolerance_rounding)
        self.geometry_cladding_outer_radius = self.geometry_cladding_inner_radius + float(np.sum(self.geometry_cladding_thickness_list))
        self.geometry_cladding_thickness =    round(self.geometry_cladding_outer_radius - self.geometry_cladding_inner_radius, self.tolerance_rounding)
        self.geometry_fuel_diameter =         round(2 * self.geometry_fuel_radius, self.tolerance_rounding)
        
        # triso
        if self.geometry_triso_graphite_block_scalar < 1.05:
            geometry_triso_graphite_block_scalar = 1.05
        else:
            geometry_triso_graphite_block_scalar = self.geometry_triso_graphite_block_scalar
        self.geometry_triso_graphite_block_radius = round(geometry_triso_graphite_block_scalar * self.geometry_triso_outer_pyrolytic_radius, self.tolerance_rounding)        

    ################ File properties ################ 

    def update_file_properties(self):
        self.file_pegasus_output_location = str(self.file_directory_template_location+"/"+self.file_directory_output_name)
        self.file_pegasus_input_location = str(self.file_directory_template_location+"/"+self.file_directory_input_name)

        # file_pegasus_input_data
        if self.file_input_data_location is not None:
            self.file_pegasus_input_data = str(self.file_input_data_location+"/"+self.file_input_data_name+".xlsx")
        else:
            self.file_pegasus_input_data = str("/"+self.file_input_data_name+".xlsx")

        # psf file location
        self.file_pegasus_location = str(self.file_pegasus_input_location+"/"+self.file_pegasus_name+".psf")

        # output directory locations
        # relative
        self.file_pegasus_output_pics_relative_location = str(self.file_directory_output_name+"/pics")
        self.file_pegasus_output_tabular_data_relative_location = str(self.file_directory_output_name+"/tabular_data")
        self.file_pegasus_output_database_relative_location = str(self.file_directory_output_name+"/database")
        # absolute
        self.file_pegasus_output_pics_location = str(self.file_directory_template_location+"/"+self.file_pegasus_output_pics_relative_location)
        self.file_pegasus_output_tabular_data_location = str(self.file_directory_template_location+"/"+self.file_pegasus_output_tabular_data_relative_location)
        self.file_pegasus_output_database_location = str(self.file_directory_template_location+"/"+self.file_pegasus_output_database_relative_location)

    ################ mesh position properties ################ 

    def create_geometry_mesh_positions_dict(self):
        # top end cap
        if self.geometry_number_of_pellets is None:
            self.position_z_top_end_cap = round(self.geometry_fuel_height_smeared + self.geometry_plenum_height_top, self.tolerance_rounding)
        else:
            self.position_z_top_end_cap = round((self.geometry_number_of_pellets - 1) * self.geometry_fuel_fuel_gap_height + self.geometry_plenum_height_top, self.tolerance_rounding)
        
        # bottom end cap
        self.position_z_bottom_end_cap = round(-1*(self.geometry_plenum_height_bottom+self.geometry_end_cap_thickness), self.tolerance_rounding)
        
        # top plenum
        if self.geometry_number_of_pellets is None:
            self.position_z_top_plenum = round(self.geometry_fuel_height_smeared, self.tolerance_rounding)
        else:
            self.position_z_top_plenum = round(self.geometry_plenum_height_bottom + (self.geometry_number_of_pellets - 1) * self.geometry_fuel_fuel_gap_height, self.tolerance_rounding)
        
        # bottom plenum
        self.position_z_bottom_plenum = round((-1*self.geometry_plenum_height_bottom), self.tolerance_rounding)

        self.geometry_mesh_positions_dict[self.end_cap_top_name] = self.position_z_top_end_cap
        self.geometry_mesh_positions_dict[self.end_cap_bottom_name] = self.position_z_bottom_end_cap
        self.geometry_mesh_positions_dict[self.plenum_top_name] = self.position_z_top_plenum
        self.geometry_mesh_positions_dict[self.plenum_bottom_name] = self.position_z_bottom_plenum

    ################ Selection volume geometric properties ################ 
    
    def update_selection_volumes(self):
        self.geometry_negative_selection_volume_threshold =         round((-1 * self.geometry_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_times_2_selection_volume_threshold =          round((2 * self.geometry_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_negative_times_2_selection_volume_threshold = round((-1 * self.geometry_times_2_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_outside_outer_clad_radius =                   round((self.geometry_cladding_outer_radius + self.geometry_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_inside_outer_clad_radius =                    round((self.geometry_cladding_outer_radius - self.geometry_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_times_2_outside_outer_clad_radius =           round((2 * self.geometry_outside_outer_clad_radius), self.tolerance_rounding)
        self.geometry_negative_outside_outer_clad_radius =          round((-1 * self.geometry_outside_outer_clad_radius), self.tolerance_rounding)
        self.geometry_outside_top_rod_height =                      round((self.position_z_top_end_cap + self.geometry_end_cap_thickness + self.geometry_selection_volume_threshold), self.tolerance_rounding)
        self.geometry_outside_bottom_rod_height =                   round((-(self.geometry_end_cap_thickness + self.geometry_plenum_height_bottom + self.geometry_selection_volume_threshold)), self.tolerance_rounding)

    ################ Region, Material, and Element Association ################ 
    
    def todo_create_region_mat_ele_assoc_dict(self):
        pass
    
    ################ Methods ################ 

    def update_parameters(self):
        """

        """
        with open(self.file_parameter_input_path, "r") as input_parameter_file:
            # pegasus_inputs = yaml.load(input_parameter_file, Loader=yaml.FullLoader)
            pegasus_inputs = yaml.safe_load(input_parameter_file)
            try:
                self.material_number_dynamics_mats = len(input_parameter_file['MATERIAL']['CLASS']['DYNAMIC']['INDEX'])
            except TypeError:
                pass

            flat_pegasus_inputs = self.flatten_params(pegasus_inputs)
            flat_pegasus_inputs = {key.replace('_name', ''): value for key, value in flat_pegasus_inputs.items()}
            
            for key in flat_pegasus_inputs:
                value = flat_pegasus_inputs[key]
                setattr(self, key.lower(), value)
                # print(
                #     f"yaml--- (key, value): ({key.lower()}, {value})"
                # )
    
    def flatten_params(self, params, parent_key=''):
            items = []
            # try:
            for key in params:
                if isinstance(key, int):
                    new_key = parent_key if parent_key else str(key)
                else:
                    new_key = parent_key + '_' + str(key) if parent_key else str(key)
                value = params[key]
                if isinstance(value, dict):
                    items.extend(self.flatten_params(value, new_key).items())
                    
                elif isinstance(value, list) and isinstance(value[0], dict):
                    # print(
                    #     f"is list and dict --- :\n"
                    #     f"  new_key: {new_key}\n"
                    #     f"  value: {value}\n"
                    #     f"  value[0]: {value[0]}\n"
                    #     f"  value[1]: {value[1]}\n"
                    #     f"  value[1][0]: {next(iter(value[0].keys()))}, {type(next(iter(value[0].keys())))}\n"
                    # )
                    val_first = next(iter(value[0].keys()))
                    val_is_int = isinstance(val_first, int)
                    # print(
                    #     f"result: {val_first}, {val_is_int}"
                    # )
                
                    if not val_is_int:
                        # print(
                        #     f"if isinstance ---: {val_first}, {val_is_int}, {type(val_first)}"
                        # )
                        for i, v in enumerate(value):
                            items.extend(self.flatten_params(v, new_key + f'_{i+1}').items())
                elif isinstance(value, list) and isinstance(value, list):
                    # print(
                    #     f"is list ---{new_key}, {value}, {type(value)}"
                    # )
                    items.append((new_key, value))
                elif isinstance(key, int):
                    # pass
                    # print(f"is int --- key, value: {key}, {value}")
                    if isinstance(value, list):
                        # print(f"is int --- key, new_key, params[key]: {key}, {new_key}, {value}")
                        items.append((new_key, value))
                        # for i, v in enumerate(value):
                        #     print(f"i,v: {i}, {v}")
                        #     items.extend(self.flatten_params(v, new_key).items())                    

                else:
                    items.append((new_key, value))
            # except TypeError:
            #     for key in params:
            #         print(
            #             f"error --- key and type: {key}, {type(key)}"
            #             f"error --- params[key]: {params[key]}"
            #         )
            return dict(items)

    def print_atts(self):
        """

        """
        params_dict = vars(self)
        for key in params_dict:
            print(f"{key} : {params_dict[key]}")

    def create_directory_template(self):
        """

        """
        # Input directory
        if not os.path.isdir(self.file_pegasus_input_location):
            os.makedirs(self.file_pegasus_input_location)
        # Output directory
        if not os.path.isdir(self.file_pegasus_output_location):
            os.makedirs(self.file_pegasus_output_location)
        # Output directory: Pictures  
        if not os.path.isdir(self.file_pegasus_output_pics_location):
            os.makedirs(self.file_pegasus_output_pics_location)
        # Output directory: Tabular data
        if not os.path.isdir(self.file_pegasus_output_tabular_data_location):
            os.makedirs(self.file_pegasus_output_tabular_data_location)
        # Output directory: Database sub directories
        if not os.path.isdir(self.file_pegasus_output_database_location):
            os.makedirs(self.file_pegasus_output_database_location)

    def create_params_flags(self):
        """

        """
        params_dict = vars(self)
        for key in params_dict:
            if type(params_dict[key])==bool:
                key_prefix = key.lower().split('_', 2)[1]
                # print(f"flags key_prefix: {key}, {key_prefix}")
                if key_prefix == self.prefix_flag:
                    model_value = params_dict[key]
                    if model_value:
                        self.param_flags_dict[key] = model_value

    def create_params_retrieve(self):
        """
        MATERIAL_FUEL_RETRIEVE_STATE_VARIABLES
        MATERIAL_FUEL_RETRIEVE_COORDINATES

        """
        params_dict = vars(self)
        # for key in params_dict:
            # print(
            #     f"{key}, {params_dict[key]}"
            #     )
            
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            # if key_prefix == self.prefix_retrieve:
            if key_prefix == self.prefix_retrieve:
                # print(
                #     f"create retrieve key ==================== \n"
                #     f"{key}, {params_dict[key]}\n"
                #     )
                key_suffix = key.lower().split('_', 1)[1]
                # Modify keys to match material alias names (strip prefix)
                print(
                    f"  key_suffix: {key_suffix}\n"
                    f"  key_suffix value: {params_dict[key_suffix]}\n"
                    f"  key: {key}\n"
                    f"  params_dict[key]: {params_dict[key]}\n"
                )
                # print(
                #     f"  self.param_retrieve_dict[key_suffix]: {self.param_retrieve_dict[key_suffix]}\n"
                #     )

                self.param_retrieve_dict[key_suffix] = params_dict[key]
                # for value in params_dict[key]:
                #     print(f"    prefix key: {key_suffix}, value: {value}")
                #     pass
        # print(
        #     f"\n"
        #     f"done creating\n"
        #     f"===================================================="
        #     )

    def create_material_associations_dict(self):
        create_material_associations_dict_temp = {}
        params_dict = vars(self)
        # print("params dict\n")
        # for value in params_dict:
        #     print(
        #         f"params dict --- {value}: {params_dict[value]}"
        #     )
        for var_name, var_value in params_dict.items():
            if var_name.endswith("_class"):
                var_name_pre = var_name.split("_class")[0].split('_', 1)[-1]
                retrieve_state_variable_name = var_name.split("_class")[0]+"_retrieve_state_variables"
                retrieve_coordinate_name = var_name.split("_class")[0]+"_retrieve_coordinates"
                    
                alias_name = "material_alias_name_" + var_name_pre.lower()
                # print(f"\ncreate assoc --- alias_name: {alias_name}, {retrieve_coordinate_name}\n")
                if hasattr(self, alias_name):
                    value_list = []
                    alias_name_value = getattr(self, alias_name)
                    
                    if hasattr(self, retrieve_state_variable_name):
                        state_vars_to_retrieve = getattr(self, retrieve_state_variable_name)
                        if state_vars_to_retrieve is not None:
                            value_list.append(state_vars_to_retrieve)
                            if hasattr(self, retrieve_coordinate_name):
                                value_list.append([getattr(self, retrieve_coordinate_name)])
                    # self.alias_to_class[getattr(self, alias_name)] = value_list

                    # print(
                    #     f"      pre value_list: {value_list}\n"
                    # )
                    
                    if isinstance(var_value, list):
                        # alias_name_key = alias_name.split("_list")[0]
                        for alias_index in range(len(var_value)):
                            mat_type = var_value[alias_index]
                            copy_value_list = copy.deepcopy(value_list)
                            copy_value_list.insert(0, mat_type)
                            # print(f"        during value_list: {copy_value_list}")
                            create_material_associations_dict_temp[alias_name_value+"_"+str(alias_index+1)] = copy_value_list
                            # value_list.pop(0)
                    else:
                        # value = value_list.insert(0, var_value)
                        value_list.insert(0, var_value)
                        # print(f"during value_list: {value_list}")
                        create_material_associations_dict_temp[alias_name_value] = value_list

                    # print(
                    #     f"post insert value_list: {value_list}"
                    # )
        
        # print(f"\n====== printing create_mat_class_alias ======\n")
        for key in create_material_associations_dict_temp:
            value = create_material_associations_dict_temp[key]
            try:
                if value[1] is not None:
                    self.material_associations_dict[key] = create_material_associations_dict_temp[key]
                    # print(
                    #     f"  unit test {key}: {self.material_associations_dict[key]}"
                    # )
            except IndexError:
                pass
        # print("\n====== done create_mat_class_alias ======\n")

    def create_model_params(self):
        """

        """
        for title in self.title_list:
            prefix = title
            self.prefix_list.append(prefix)  

    def create_params_geometry(self):
        """

        :return:
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        for key in params_dict:
            if type(params_dict[key])==float:
                if key.lower().split('_', 1)[0] == self.prefix_geometry:
                    create_an_real, _ = self.create_real(key.upper(), params_dict[key])
                    create_params_string_list.extend(create_an_real)
        create_params_string_list.extend(new_line)
        return create_params_string_list

    def create_params_resolution(self):
        """

        :return:
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        for key in params_dict:
            if type(params_dict[key])==int:
                if key.lower().split('_', 1)[0] == self.prefix_divisions:
                    create_an_int, _ = self.create_int(key.upper(), params_dict[key])
                    create_params_string_list.extend(create_an_int)
        create_params_string_list.extend(new_line)
        return create_params_string_list

    def create_params_material(self):
        """

        :return:
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        for key in params_dict:
            if type(params_dict[key])==float:
                if key.lower().split('_', 1)[0] == self.prefix_material_property:
                    mat_prop_suffix = key.lower().split('_', 2)[-1]
                    create_a_val, _ = self.create_real(mat_prop_suffix.upper(), params_dict[key])
                    create_params_string_list.extend(create_a_val)
        create_params_string_list.extend(new_line)
        return create_params_string_list

    def create_params_gio(self):
        """

        :return:
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        for key in params_dict:
            if type(params_dict[key])==float:
                if key.lower().split('_', 1)[0] == self.prefix_inventory_gio:
                    create_a_val, _ = self.create_real(key.upper(), params_dict[key])
                    create_params_string_list.extend(create_a_val)
        create_params_string_list.extend(new_line)
        return create_params_string_list
        
    def create_int(self, name, value):
        """

        :param name:
        :param value:
        :return:
        """
        int_name_string = name.upper()
        create_int_string = f"CREATE INTEGER {int_name_string} = {int(value)} \n"
        return create_int_string, int_name_string

    def create_real(self, name, value):
        """

        :param name:
        :param value:
        :return:
        """
        real_name_string = name.upper()
        create_real_string = f"CREATE REAL {real_name_string} = {np.format_float_scientific(value,precision=6)} \n"
        return create_real_string, real_name_string
      
    def create_header_comment(self,header_text,level=None):
        """
        
        A method for creating a section comment in pegasus.
        
        style level 1: =====
        style level 2: -----
        style level 3: - - -

        :param header_text: test to insert into pegasus comment
        :param level: style level options: (`1`,`2`,`3`)
        :return:
        """
        if level is None:
            level = self.level_header_1
        header_string = []
        if level==self.level_header_0:
            header_string = (
            f"# {header_text} \n"
            f"\n"
            )
        elif level==self.level_header_1:
            header_string = (
            f"#======================================================================= \n"
            f"# {header_text} \n"
            f"#=======================================================================\n"
            f"\n"
            )
        elif level==self.level_header_2:
            header_string = (
            f"#----------------------------------------------------------------------- \n"
            f"# {header_text} \n"
            f"#-----------------------------------------------------------------------\n"
            f"\n"
            )
        elif level==self.level_header_3:
            header_string = (
            f"#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   - \n"
            f"# {header_text} \n"
            f"#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -\n"
            f"\n"
            )
        elif level==self.level_header_4:
            header_string = (
            f"#~  ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ \n"
            f"# {header_text} \n"
            f"#~  ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~\n"
            f"\n"
            )
        return header_string

    def create_pause(self):
        """

        :return psf_string: psf formatted string for creating pause
        """
        psf_string = (
            "PAUSE \n"
            "\n")
        return psf_string

if __name__ == "__main__":
    param_file_test = "/home/projects/griffinpy/scrip_gen_tests/scrip_gen_input/generator_input.yaml"
    out_file_test = "/home/projects/griffinpy/scrip_gen_tests/output/testing_new.psf"
    params = Parameters()
    params.print_atts()
    print("\n================================================================")
    psf_string_list = []
    upd_params = Parameters(param_file_test)
    params.print_atts()
    print("\n================================================================")
    upd_params.create_params_retrieve()
    print(upd_params.param_retrieve_dict)