from parameters import Parameters


class BoundaryConditionsUtilities:
    """
    
    This class contains the methods for creating the inventory objects.
    
    """
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)
        self.prefix_bc_val = "val"
        self.prefix_function = "function"
        self.prefix_property = "property"
        self.object_inventory = "inventory"

    def create_inventory_object(self):
        """

        :return: psf formatted list of strings for creating an inventory object
        """
        props_dict = self.create_inventory_props_dict()
        num_props = len(props_dict)
        return_string = []
        string_post = []

        # Begining part of creating inventory bc
        if self.object_type == self.object_inventory:
            string_pre = (
                f"CREATE {self.inventory_type.upper()} \\ \n"
                f"  {self.inventory_name.upper()} \\ \n")
        
        # Last part of string (can vary size)
        count = 1
        # print(props_dict)
        for key in props_dict:
            if type(props_dict[key])==float:
                _, inv_val_real = self.parameters.create_real(key, props_dict[key])
                if count!=num_props:
                    single_string_post = (
                    f"  {inv_val_real} \\ # {key} \n")
                else:
                    single_string_post = (
                    f"  {inv_val_real}  # {key} \n"
                    f"\n")
                string_post.append(single_string_post)
            
            elif type(props_dict[key])==str:
                if count!=num_props:
                    single_string_post = (
                    f'  "{props_dict[key]}" \\ # {key} \n')
                else:
                    single_string_post = (
                    f'  "{props_dict[key]}" # {key} \n'
                    f"\n")
                string_post.append(single_string_post)
            
            # adding this quick fix for power dist object (PDO) becasue its supposed to be an object and not string
            # so putting PDO as type=list
            elif type(props_dict[key])==list:
                if count!=num_props:
                    single_string_post = (
                    f"  {props_dict[key][0]} \\ # {key} \n")
                else:
                    single_string_post = (
                    f"  {props_dict[key][0]} # {key} \n"
                    f"\n")
                string_post.append(single_string_post)
            count+=1
        
        return_string.extend(string_pre)
        return_string.extend(string_post)
        return return_string
    
    def create_inventory_props_dict(self):
        """

        :return: a dictionary with keys -> iventory object property name and values -> iventory object property value
        """
        params_dict = vars(self)
        props_dict = {}
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            if key_prefix == self.prefix_property:
                key_suffix = key.lower().split('_', 1)[1]
                props_dict[key_suffix] = params_dict[key]
        return props_dict

    def create_reals(self):
        """

        :return: psf formatted list of strings for creating real variables
        """
        params_dict = vars(self)
        create_params_string_list = []
        new_line = f"\n"
        for key in params_dict:
            if type(params_dict[key])==float:
                if key.lower().split('_', 2)[1] == self.prefix_bc_val:
                    create_an_real, _ = self.parameters.create_real(key.lower().split('_', 2)[-1], params_dict[key])
                    create_params_string_list.extend(create_an_real)
                # inventory object
                elif key.lower().split('_', 1)[0] == self.prefix_property:
                    create_an_real, _ = self.parameters.create_real(key.lower().split('_', 1)[1], params_dict[key])
                    create_params_string_list.extend(create_an_real)
                
        create_params_string_list.extend(new_line)
        return create_params_string_list

    def print_atts(self):
        """

        """
        params_dict = vars(self)
        for key in params_dict:
            print(f"{key} : {params_dict[key]}")

class BoundaryCondition(BoundaryConditionsUtilities):
    """
    
    This class has all the boundary condition types that exist in Pegasus.

    """
    def __init__(self, input_file_path=None):
        super(BoundaryCondition, self).__init__(input_file_path)
        
    def import_gtd(self, gtd_file_name, interpolation_type, function_name):
        """

        :param gtd_file_name: name of gtd file to import
        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :param function_name: name to give new function
        :return: psf formatted string for importing a gtd file
        """
        import_string = f"IMPORT GTD \\ \n" \
                        f'    "{gtd_file_name}" \\ \n' \
                        f"    {interpolation_type} \\ \n" \
                        f"    {function_name} \n" \
                        f"\n"
        return import_string

    def create_function(self, function_name, function_string):
        """

        :param function_name: name to give new function
        :param function_string: value of function given as string ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`
        :return: psf formatted string for creating a function
        """
        function_string = (
            f'CREATE FUNCTION {function_name.upper()} "{str(function_string).upper()}" \n' \
            f'\n')
        return function_string

    def create_bc(self, boundary_type, bc_name, function_string):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param bc_name: name of the boundary condition to be created
        :param function_string: an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied
        :return: psf formatted string for creating the boundary condition
        """
        bc_string = (
            f'CREATE {boundary_type.upper()}_BOUNDARY_CONDITION {bc_name.upper()} "{function_string.upper()}" \n'
            f'\n')
        return bc_string

    def create_qhys_qual_bc(self, boundary_type, bc_name, physical_quality, function_string):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param bc_name: name of the boundary condition to be created
        :param function_string: an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied
        :return: psf formatted string for creating the boundary condition
        """
        bc_string = (
            f'CREATE {boundary_type.upper()}_BOUNDARY_CONDITION \\ \n' 
            f'  {bc_name.upper()} \\ \n' 
            f'  {physical_quality.upper()} = "{function_string.upper()}" \n'
            f'\n')
        return bc_string

    def create_body_force(self, bc_name, function_string, direction):
        """

        :param bc_name: name of the boundary condition to be created for body force
        :param function_string: an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied as body force
        :param direction: direction vector to apply body force [x,y,z]
        :return: psf formatted string for creating a body force
        """
        body_force_string = f'CREATE BODY_FORCE {bc_name.upper()} {direction} = "{function_string.upper()}" \n' \
                            f"\n"
        return body_force_string

    def create_heat_source(self, function_string):
        """

        :param function_string: an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied as heat source
        :return: psf formatted string for creating a heat source
        """
        heat_source_string = (
            f'CREATE HEAT_SOURCE {self.parameters.bc_name_heat_source.upper()} = "{function_string.upper()}" \n'
            f'\n')
        return heat_source_string

    def create_gio_temperature(self):
        """

        :return: psf formatted string for creating gio temperature function
        """
        bc_string_list = []
        
        bc_string_list.append(self.create_function(
            self.parameters.bc_function_name_gio_temp_history, 
            self.parameters.bc_function_expression_gio_temp_history))
        return bc_string_list

    def create_gio_temperature_from_gtd(self, interpolation_type):
        """

        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :return: psf formatted string that imports plenum temperature history from a gtd file
        """
        bc_string_list = []
        
        bc_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_plenum_temperature_history, 
            interpolation_type, 
            self.parameters.bc_function_name_gio_temp_history))
        return bc_string_list

    def create_fixed(self, direction):
        """

        :param direction: direction to apply fixed boundary condition options: (`"x"`,`"y"`,`"z"`)
        :return: psf formatted string for creating a fixed boundary condition
        """
        fixed_bc_string_list = []
        direction = direction.upper()
        if direction == 'X':
            # create bc function
            fixed_bc_string_list.append(self.create_function(
                self.parameters.bc_function_name_fixed_x, 
                self.parameters.bc_function_expression_fixed_x_bc))
            # create displacement bc
            fixed_bc_string_list.append(self.create_qhys_qual_bc(
                self.parameters.bc_boundary_type_nodal, 
                self.parameters.bc_name_fixed_x, 
                self.parameters.bc_phys_qual_disp_x,
                self.parameters.bc_function_name_fixed_x))
        elif direction == 'Y':
            # create bc function
            fixed_bc_string_list.append(self.create_function(
                self.parameters.bc_function_name_fixed_y, 
                self.parameters.bc_function_expression_fixed_y_bc))
            # create displacement bc
            fixed_bc_string_list.append(self.create_qhys_qual_bc(
                self.parameters.bc_boundary_type_nodal, 
                self.parameters.bc_name_fixed_y, 
                self.parameters.bc_phys_qual_disp_y,
                self.parameters.bc_function_name_fixed_y))
        elif direction == 'Z':
            # create bc function
            fixed_bc_string_list.append(self.create_function(
                self.parameters.bc_function_name_fixed_z, 
                self.parameters.bc_function_expression_fixed_z_bc))
            # create displacement bc
            fixed_bc_string_list.append(self.create_qhys_qual_bc(
                self.parameters.bc_boundary_type_nodal, 
                self.parameters.bc_name_fixed_z, 
                self.parameters.bc_phys_qual_disp_z,
                self.parameters.bc_function_name_fixed_z))
        return fixed_bc_string_list

    def create_gravity(self):
        """

        :return: psf formatted string for creating a gravity body force [m/s^2]
        """
        gravity_string = self.create_body_force(
            self.parameters.bc_function_name_gravity, 
            self.parameters.bc_function_expression_gravity, 
            self.parameters.position_vector_neg_z)
        return gravity_string

    def create_power_distribution(self, height_of_active_rod, or_of_fuel, ir_of_fuel=0.0):
        """

        :param height_of_active_rod: height of active rod [m]
        :param or_of_fuel: outer radius of fuel [m]
        :param ir_of_fuel: inner radius of fuel [m]
        :return: psf formatted string for creating power distribution object
        """
        # power_distribution_string_list = []
        power_distribution_string_list = (
            f"CREATE POWER_DISTRIBUTION \\ \n"
            f"    {self.parameters.bc_name_power_distribution.upper()} \\ # output power distribution object \n"
            f"    {self.parameters.bc_function_name_axial_power} \\ # axial distribution (shape) function \n"
            f"    TU_BURNUP_RAD_PWR_DIST \\ # radial shape function \n"
            f"    {self.parameters.bc_function_name_power_history} \\ # linear rod average power time-history [ W / m ]\n"
            f"    {ir_of_fuel} \\ # inner radius of pellets [ m ]\n"
            f"    {or_of_fuel} \\ # outer radius of pellets [ m ]\n"
            f"    {height_of_active_rod} \\ # height of the powered rod [ m ] \n"
            f"    {self.parameters.bc_power_fraction_for_coolant_channel} # fraction of power that is reserved for coolant channel \n"
            f"    \n")
        return power_distribution_string_list

    def create_power_from_gtd(self, interpolation_type, height_of_active_rod, or_of_fuel=None, ir_of_fuel=0.0):
        """

        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :param height_of_active_rod:height of active rod [m]
        :param or_of_fuel: outer radius of fuel [m]
        :param ir_of_fuel: inner radius of fuel [m]
        :return: psf formatted string for creating power distribution object from gtd files
        """
        if or_of_fuel is None:
            or_of_fuel = self.parameters.geometry_fuel_radius

        create_rod_power_from_gtd_string_list = []
        create_rod_power_from_gtd_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_power_history, 
            interpolation_type, 
            self.parameters.bc_function_name_power_history))
        
        create_rod_power_from_gtd_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_axial_power_shapes, 
            interpolation_type, 
            self.parameters.bc_function_name_axial_power))

        create_rod_power_from_gtd_string_list.append(
            self.create_power_distribution(
                height_of_active_rod=height_of_active_rod,
                or_of_fuel=or_of_fuel, 
                ir_of_fuel=ir_of_fuel
                ))
        create_rod_power_from_gtd_string_list.append(self.create_heat_source(self.parameters.bc_name_power_distribution))
        return create_rod_power_from_gtd_string_list

    def create_temperature_from_gtd(self, interpolation_type):
        """

        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :return: psf formatted string for creating temperature distribution profile from a gtd file
        """
        bc_string_list = []
        bc_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_temperature_history, 
            interpolation_type, 
            self.parameters.bc_function_name_temperature))
        
        bc_string_list.append(self.create_qhys_qual_bc(
            self.parameters.bc_boundary_type_nodal,
            self.parameters.bc_name_temperature,
            self.parameters.bc_phys_qual_temperature,
            self.parameters.bc_function_name_temperature))
        return bc_string_list

    def create_pressure_from_gtd(self, boundary_type, interpolation_type):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :return: psf formatted string for creating pressure distribution profile from a gtd file
        """
        temperature_history_bc_string_list = []
        temperature_history_bc_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_pressure_history, 
            interpolation_type, 
            self.parameters.bc_function_name_pressure))

        temperature_history_bc_string_list.append(self.create_bc(
            boundary_type,
            self.parameters.bc_name_pressure,
            self.parameters.bc_function_name_pressure))
        return temperature_history_bc_string_list

    def create_heat_flux_from_gtd(self, boundary_type, interpolation_type):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param interpolation_type: options:(`"PIECEWISE_LINEAR"`, `"PIECEWISE_CONSTANT"`)
        :return: psf formatted string for creating heat flux distribution profile from a gtd file
        """
        bc_string_list = []
        bc_string_list.append(self.import_gtd(
            self.parameters.bc_gtd_name_heat_flux_history, 
            interpolation_type, 
            self.parameters.bc_function_name_heat_flux))
        
        bc_string_list.append(self.create_bc(
            boundary_type,
            self.parameters.bc_name_heat_flux,
            self.parameters.bc_function_name_heat_flux))
        return bc_string_list
    
    def create_power(self, shape_function=None, mag_function=None, height_of_active_rod=None, or_of_fuel=None, ir_of_fuel=0.0):
        """

        :param shape_function: an existing functions name to be applied as axial shape
        :param mag_function:
        :param height_of_active_rod:
        :param or_of_fuel:
        :param ir_of_fuel:
        :return: psf formatted list of strings for creating power all power components
        """
        create_rod_power_string_list = []
        
        if shape_function is None:
            shape_function = self.parameters.bc_function_expression_axial_power_shapes
        if mag_function is None:
            mag_function = self.parameters.bc_function_expression_power_function
        if height_of_active_rod is None:
            height_of_active_rod = self.parameters.geometry_active_rod_height
        if or_of_fuel is None:
            or_of_fuel = self.parameters.geometry_fuel_radius
        
        # Axial function
        create_rod_power_string_list.append(self.create_function(
            self.parameters.bc_function_name_axial_power, 
            shape_function))

        # Power Magnitude function
        create_rod_power_string_list.append(self.create_function(
            self.parameters.bc_function_name_power_history, 
            mag_function))
        
        # Power distribution
        create_rod_power_string_list.append(
             self.create_power_distribution(
                height_of_active_rod=height_of_active_rod,
                or_of_fuel=or_of_fuel, 
                ir_of_fuel=ir_of_fuel
                ))

        # Heat source
        create_rod_power_string_list.append(
            self.create_heat_source(self.parameters.bc_name_power_distribution))
        
        # print(f"create_rod_power_string_list: {create_rod_power_string_list}")
        return create_rod_power_string_list

    def create_temperature(self, temperature_function=None):
        """
        
        :param temperature_function: an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied as temperature bc.
        :return: psf formatted string for creating a temperature boundary condition
        """
        if temperature_function is None:
            temperature_function = self.parameters.bc_function_expression_temperature
        bc_string_list = []
        
        bc_string_list.append(self.create_function(
            self.parameters.bc_function_name_temperature, 
            self.parameters.bc_function_expression_temperature))
        
        bc_string_list.append(self.create_qhys_qual_bc(
            self.parameters.bc_boundary_type_nodal,
            self.parameters.bc_name_temperature,
            self.parameters.bc_phys_qual_temperature,
            self.parameters.bc_function_name_temperature))
        return bc_string_list

    def create_pressure(self, boundary_type, pressure_function=None):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param pressure_function:  an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied as pressure bc.
        :return: psf formatted string for creating a pressure boundary condition
        """
        if pressure_function is None:
            pressure_function = self.parameters.bc_function_expression_pressure
        pressure_bc_string_list = []
        
        pressure_bc_string_list.append(self.create_function(
            self.parameters.bc_function_name_pressure, 
            pressure_function))
        
        pressure_bc_string_list.append(self.create_qhys_qual_bc(
            boundary_type,
            self.parameters.bc_name_pressure,
            self.parameters.bc_phys_qual_cauchy_pressure,
            self.parameters.bc_function_name_pressure))
        return pressure_bc_string_list

    def create_heat_flux(self, boundary_type, heat_flux_function=None):
        """

        :param boundary_type: options:(`"FACET_BOUNDARY_CONDITION"`, `"SEGMENT_BOUNDARY_CONDITION"`)
        :param heat_transfer_coefficient: heat transfer coefficient
        :param heat_flux_function:  an existing functions name **or** string value of function (ex. `"1.0 * (LINEAR_RAMP(T / 3600.0)"`) to be applied as heat flux bc.
        :return: psf formatted string for creating a heat flux boundary condition
        """
        if heat_flux_function is None:
            heat_flux_function = self.parameters.bc_val_name_htc+" * ("+self.parameters.bc_function_expression_heat_flux+")"
        heat_transfer_bc_string_list = []
        heat_transfer_bc_string_list.append(self.create_function(
            self.parameters.bc_function_name_heat_flux, 
            heat_flux_function))

        heat_transfer_bc_string_list.append(self.create_qhys_qual_bc(
            boundary_type,
            self.parameters.bc_name_heat_flux,
            self.parameters.bc_phys_qual_heat_flux,
            self.parameters.bc_function_name_heat_flux))
        return heat_transfer_bc_string_list

    def todo_create_bf_func_for_2drz_rod(self):
        pass

class GasInventoryObject(BoundaryConditionsUtilities):
    """
    
    This class is for managing the gas inventory object.

    """
    def __init__(self, input_file_path=None, inventory_name=None):
        super(GasInventoryObject, self).__init__(input_file_path)
        self.object_type = "inventory"
        if inventory_name is None:
            self.inventory_name = self.parameters.name_gio.upper()
        self.inventory_type = "GAS_INVENTORY_OBJ"
        self.property_gio_applied_element_set = [self.parameters.elements_all_name]
        self.property_gio_initial_gas_pressure = self.parameters.inventory_gio_initial_gas_pressure
        self.property_gio_initial_gas_composition = (self.parameters.inventory_gio_initial_gas_composition).upper()
        self.property_gio_external_volume_for_calculations = self.parameters.inventory_gio_external_volume_for_calculations
        self.property_gio_external_volume_tmperature_time_history_name = [self.parameters.bc_function_name_gio_temp_history.upper()]
        self.property_gio_ease_time_for_pressure_bc = self.parameters.inventory_gio_ease_time_for_pressure_bc
        self.property_gio_plenum_bc_htc = self.parameters.inventory_gio_plenum_bc_htc

class FuelInventoryObject(BoundaryConditionsUtilities):
    """
    
    This class is for managing the fuel inventory object.

    """
    def __init__(self, input_file_path=None, inventory_name=None):
        super(FuelInventoryObject, self).__init__(input_file_path)
        self.object_type = "inventory"
        if inventory_name is None:
            self.inventory_name = self.parameters.name_fio.upper()
        self.inventory_type = "FUEL_INVENTORY_OBJ"
        self.property_fio_applied_element_set = [self.parameters.elements_all_name]
        self.property_fio_power_distribution = [self.parameters.bc_name_power_distribution.upper()]
        self.property_fio_initial_burnup = [self.parameters.inventory_fio_initial_burnup.upper()]
        self.property_fio_initial_fluence = [self.parameters.inventory_fio_initial_fluence.upper()]

class BoundaryConditionTool(BoundaryConditionsUtilities):
    """

    This class combines all classes in the meshing module and can be imported into other python files to 
    easily access all boundary condition features.

    Supporting Classes Used::
        :class: `BoundaryCondition`,
        :class: `GasInventoryObject`,
        :class: `FuelInventoryObject`
    """
    def __init__(self, input_file_path=None):
        super(BoundaryConditionTool, self).__init__(input_file_path)
        self.bc = BoundaryCondition(input_file_path)
        self.inventory_gas = GasInventoryObject(input_file_path)
        self.inventory_fuel = FuelInventoryObject(input_file_path)
