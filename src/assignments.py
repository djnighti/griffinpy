from parameters import Parameters

class AssignmentsTool:
    """
    
    This simple class contains all the methods for assigning boundary conditions, materials and some preset
    assigns for specific geometries.

    """
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)


    def assign_materials(self, material_name, element_set_name):
        """
        
        This function assigns a material to an element set.

        :param material_name: alias name of existing of material to assign
        :param element_set_name: name of exisiting element set to apply material to
        :return psf_string: formatted string that creates a material assignement 
        """
        try:
            # print(f"test: {element_set_name}")
            psf_string = (
                f"ASSIGN {material_name} \\ \n" 
                f"  TO {element_set_name} \n"
                f"\n")
            return psf_string
        except KeyError:
            print('Error: Element set has not been created yet.')

    def assign_boundary_conditions(self, boundary_condition_name, ele_node_set_name):
        """
        
        This function assigns a boundary condition to an element set.

        :param boundary_condition_name: name of exisiting boundary condition to assign
        :param ele_node_set_name: name of exisiting element/node set to apply boundary condition to
        :return psf_string: psf formatted string that creates a boundary condition assignement
        """
        if type(ele_node_set_name) is list:
            sep = ", "
            ele_node_set_name = sep.join(ele_node_set_name)
        try:
            psf_string = (
                f"ASSIGN {boundary_condition_name.upper()} \\ \n"
                f"  TO \\ \n"
                f"  {{{ele_node_set_name.upper()}}} \n"
                f"\n")
            return psf_string
        except KeyError:
            print('Error: Element set has not been created yet.')

    def assign_inital_temperature(self):
        """
        
        This function assigns an initial temperature condition to all nodes in mesh.

        :return psf_string: psf formatted string that creates an initial temperature condition assignement
        """
        psf_string = (
            f'ASSIGN INITIAL TEMPERATURE "{self.parameters.bc_val_initial_temperature}" \\ \n'
            f"  TO {self.parameters.nodes_all_name} \n"
            f"\n")
        return psf_string

    def assign_rod_materials(self, regions_dict=None):
        """
        
        This function assigns all materials to `meshing.Rz2dRod, meshing.Rtheta2D, meshing.Rtheta3D`. This is done by keeping 
        track of the regions created during the meshing process. The convention assumes that the regions, 
        materials, and element sets will all have the same suffix name.
        
        ie. REGION_FUEL : ELEMENTS_FUEL : MATERIAL_FUEL
        where the suffix is "fuel" and the prefix is either "region", "elements", or "material"

        :param regions_dict: a dictionary with keys -> integers and values -> strings of region names
        :return psf_string_list: psf formatted list of strings that creates a material assignements for all regions in mesh
        """
        psf_string_list = []
        if regions_dict is not None:
            for key in regions_dict:
                value_suffix = (regions_dict[key].lower().split('_', 1)[-1]).upper()
                psf_string_list.extend(self.assign_materials(
                    self.parameters.material_prefix_name+value_suffix,
                    self.parameters.elements_prefix_name+value_suffix))
        return psf_string_list
    
    def assign_triso_materials(self, regions_dict=None):
        """
        
        This function (similar to `assign_rod_materials`) assigns materials through the region name convention
        but forces the first region to always be fuel because that is first mesh set created in `meshing.Triso.create_triso_particle`

        :param regions_dict: a dictionary with keys -> integers and values -> strings of region names
        :return psf_string_list: psf formatted list of strings that creates a material assignements for all regions in mesh
        """
        psf_string_list = []
        if regions_dict is not None:
            for key in regions_dict:
                value_suffix = (regions_dict[key].lower().split('_', 1)[-1]).upper()
                # print(f"value_suffix: {value_suffix}")
                if key==1:
                    element_name = self.parameters.elements_fuel
                else:
                    element_name = self.parameters.elements_prefix_name+value_suffix
                psf_string_list.extend(self.assign_materials(
                    self.parameters.material_prefix_name+value_suffix,
                    element_name))
        return psf_string_list

    def assign_2drz_rod_bc(self, model_flag_detailed=False):
        """
        
        This function assigns all boundary conditions to `meshing.Rz2dRod`. This is done by assuming
        the node sets were created during the meshing process.

        :param model_flag_detailed: a flag for including additonal bc's options - (True, False)
        :return psf_string_list: psf formatted list of strings that creates all boundary condition assignements
        """
        psf_string_list = []
        if model_flag_detailed==self.parameters.model_flag_rod_rz_detailed_smeared:
            model_flag_detailed=True
        
        # Fixed
        fixed_nodes_list = [self.parameters.nodes_bottom_of_rod]
        if model_flag_detailed:
            fixed_nodes_list.append(self.parameters.nodes_bottom_of_fuel)
        
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_z, 
            fixed_nodes_list))
        
        # motionless isothermal
        if model_flag_detailed:
            psf_string_list.extend(self.assign_boundary_conditions(
                self.parameters.bc_name_motionless_isothermal, 
                self.parameters.elements_plenum))

        # Pressure
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_pressure, 
            self.parameters.elements_cladding_outer_surface))

        # Heat flux 
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_flux, 
            self.parameters.elements_cladding_outer_surface))
        
        # Rod Power
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_source, 
            self.parameters.elements_fuel))

        return psf_string_list

    def assign_2drtheta_rod_bc(self):
        """

        This function assigns all boundary condition for `meshing.Rtheta2D`. This is done by assuming
        the node sets were created during the meshing process.

        :return psf_string_list: psf formatted list of strings that creates all boundary condition assignements
        """
        psf_string_list = []
        # Fixed x
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_x, 
            [self.parameters.nodes_clad_od_pos_y, self.parameters.nodes_origin]))

        # Fixed y
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_y, 
            [self.parameters.nodes_clad_od_pos_x, self.parameters.nodes_origin]))

        # Pressure
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_pressure, 
            self.parameters.elements_cladding_outer_surface))

        # Heat flux 
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_flux, 
            self.parameters.elements_cladding_outer_surface))
        
        # Rod Power
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_source, 
            self.parameters.elements_fuel))

        return psf_string_list

    def assign_3drtheta_rod_bc(self, model_flag_detailed=False):
        """

        This function assigns all boundary condition for `meshing.Rtheta3D`. This is done by assuming
        the node sets were created during the meshing process.

        :return psf_string_list: psf formatted list of strings that creates all boundary condition assignements
        """
        psf_string_list = []

        if model_flag_detailed==self.parameters.model_flag_rod_rz_detailed_smeared:
            model_flag_detailed=True
        
        # Fixed x
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_x, 
            [self.parameters.nodes_plane_yz]))

        # Fixed y
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_y, 
            [self.parameters.nodes_plane_xz]))

        # Fixed z
        fixed_nodes_list = [self.parameters.nodes_bottom_of_rod]
        if model_flag_detailed:
            fixed_nodes_list.append(self.parameters.nodes_bottom_of_fuel)
        
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_z, 
            fixed_nodes_list))
        
        # motionless isothermal
        if model_flag_detailed:
            psf_string_list.extend(self.assign_boundary_conditions(
                self.parameters.bc_name_motionless_isothermal, 
                self.parameters.elements_plenum))

        # Pressure
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_pressure, 
            self.parameters.elements_cladding_outer_surface))

        # Heat flux 
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_flux, 
            self.parameters.elements_cladding_outer_surface))
        
        # Rod Power
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_source, 
            self.parameters.elements_fuel))

        return psf_string_list

    def assign_triso_particle_bc(self):
        """

        This function assigns all boundary condition for `meshing.Triso`. This is done by assuming
        the node sets were created during the meshing process.

        :return psf_string_list: psf formatted list of strings that creates all boundary condition assignements
        """
        psf_string_list = []
        # Fixed
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_x, 
            [self.parameters.nodes_clad_od_pos_y, self.parameters.nodes_origin]))
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_fixed_y, 
            [self.parameters.nodes_clad_od_pos_x, self.parameters.nodes_origin]))

        # Pressure
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_pressure, 
            self.parameters.elements_triso_particle_outer_surface))

        # Heat flux 
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_flux, 
            self.parameters.elements_triso_particle_outer_surface))
        
        # Rod Power
        psf_string_list.extend(self.assign_boundary_conditions(
            self.parameters.bc_name_heat_source, 
            self.parameters.elements_fuel))

        return psf_string_list