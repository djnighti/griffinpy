import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import TextBox, Button
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib import animation
import pandas as pd
import os
import sys
from itertools import groupby
from tkinter import *
import tkinter.filedialog as fdialog
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkVideoPlayer import TkinterVideo
import imageio
from moviepy.editor import *
import glob
# from ttkthemes import themed_tk as tk2
from ttkthemes import ThemedStyle
import time
from matplotlib import rcParams
from plotting_tool import PictureToMovie, GraphAnimation, SpatialPlotting
import traceback
from template_creation import Template
import copy

LARGEFONT =("Courier New", 35)
  
class GriffinpyGUI(tk.Tk):
    """

    """
    # __init__ function for class GriffinpyGUI
    def __init__(self, *args, **kwargs):
         
        # __init__ function for class Tk
        tk.Tk.__init__(self, *args, **kwargs)
        # self.tk2.ThemedTk(theme="ubuntu")
        # self.update()
        # style = ttk.Style()
        style = ThemedStyle(self)
        style.set_theme("ubuntu")

        # Configure the theme with style
        # style.theme_use('clam')

        # self.winfo_toplevel().title("Griffinpy")
        self.width= self.winfo_toplevel().winfo_screenwidth()
        self.height= self.winfo_toplevel().winfo_screenheight()
        win_width = 1840
        win_height = 670
        # self.win_size = str(int(win_width))+"x"+str(int(win_height))
        self.win_size = str(int(self.width))+"x"+str(int(self.height))
        self.winfo_toplevel().geometry(self.win_size)
        self.winfo_toplevel().title('Griffinpy')
         
        # creating a container
        container = tk.Frame(self) 
        container.pack(side = "top", fill = "both", expand = True)
  
        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)
  
        # initializing frames to an empty array
        self.frames = {} 
  
        # iterating through a tuple consisting
        # of the different page layouts
        for F in (HomePage, PsfGenPage, PlottingPage):
  
            frame = F(container, self)
  
            # initializing frame of that object from
            # HomePage, PsfGenpage, page2 respectively with
            # for loop
            self.frames[F] = frame
            frame.grid(row = 0, column = 0, sticky ="nsew")
  
        self.show_frame(HomePage)
    # to display the current frame passed as parameter
    def show_frame(self, cont):
        """

        :param cont:
        """
        frame = self.frames[cont]
        frame.tkraise()
 
class HomePage(tk.Frame):
    """

    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.frame_start_page = ttk.Frame(self)
        self.frame_start_page.pack(expand=True, side=LEFT, pady=3, fill="both")
        self.width= self.winfo_toplevel().winfo_screenwidth()
        self.height= self.winfo_toplevel().winfo_screenheight()
        # print(
        #     f"self.width: {self.width}\n"
        #     f"self.height: {self.height}\n"
        # )
        # button_height = int(self.height/4)
        # button_width = int(self.width/2)
        button_height = 10
        button_width = 10
        # frames
        self.title_frame = ttk.Frame(self.frame_start_page)
        self.button_frame = ttk.Frame(self.frame_start_page)
        # self.title_frame.grid(row=0, column=0)
        # self.button_frame.grid(row=1, column=0)
        self.title_frame.pack(expand=True, side=TOP, pady=3, padx=3, fill="both")
        self.button_frame.pack(expand=True, side=BOTTOM, pady=3, padx=3, fill="both")

        # label of home page
        label = ttk.Label(self.title_frame, text ="Griffinpy", font = LARGEFONT)


        # psf_gen_button button to show psf generation page
        label.pack(expand=True, side=TOP, pady=3, padx=3)
        psf_gen_button = ttk.Button(
            self.button_frame, 
            text ="Create A PSF",
            style='bb.TButton',
            command = lambda : controller.show_frame(PsfGenPage))
        psf_gen_button.pack(expand=True, side=LEFT, pady=3, padx=3, fill="x")
        
        # psf_gen_button.pack(expand=True, side=LEFT, pady=3, padx=3, fill="both")
        # psf_gen_button.grid(row=0,column=0, sticky=(N, S, E, W), pady=5, padx=5)
        
        # post_process button to show post processor page
        post_process_button = ttk.Button(
            self.button_frame, 
            text="Post Processor",
            style='bb.TButton',
            command = lambda : controller.show_frame(PlottingPage))
        post_process_button.pack(expand=True, side=RIGHT, pady=3, padx=3, fill="x")
        # post_process_button.grid(row=0,column=1, rowspan=10)
        # post_process_button.grid(row=0,column=1, sticky=(N, S, E, W), pady=5, padx=5)
        # post_process_button.pack(expand=True, side=RIGHT, pady=[40,0], padx=[20,0], anchor=S)
        # post_process_button.pack(expand=True, side=RIGHT, padx=[20,0], anchor=S)
        # post_process_button.pack(side=RIGHT, padx=[200,200], anchor=S)
        # post_process_button.pack(expand=True, side=RIGHT, pady=3, padx=3, ipady=button_height, ipadx=button_width,fill="both")
        # post_process_button.pack(expand=True, side=RIGHT, pady=3, padx=3, fill="x")
        # post_process_button.configure(height=int(self.height/4),width=int(self.width/2))
        # self.update()

class PsfGenPage(tk.Frame):
    """

    """
    def __init__(self, parent, controller):
         
        tk.Frame.__init__(self, parent)
        self.frame_psf_gen_page = ttk.Frame(self)
        self.frame_yaml_input = ttk.Frame(self)
        self.frame_psf_output = ttk.Frame(self)
        self.frame_psf_gen_page.pack(expand=True, side=LEFT, pady=3, fill="both")
        self.frame_yaml_input.pack(expand=True, side=LEFT, pady=3, fill="both")
        self.frame_psf_output.pack(expand=True, side=RIGHT, pady=3, fill="both")
        
        # self.frame_psf_gen_page.grid(row=0, column=1)
        # self.frame_yaml_input.grid(row=1, column=1)
        # self.frame_psf_output.grid(row=0, column=2)

        label = ttk.Label(self.frame_psf_gen_page, text ="Create PSF", font = LARGEFONT)
        label.grid(row = 0, column = 1, padx = 10, pady = 10, sticky=(N, S, E, W))
        # label.pack(expand=True, side=TOP, pady=3, padx=3)
        self.controller = controller

        
        
        self.text_file_explorer = "File Explorer"
        self.text_file_selected = "File Selected"
        self.text_q1 = "Select where generator input is located: "
        self.text_show_yaml = "YAML Input"
        self.text_show_psf = "PSF Output"
        self.text_save = "Save"

        self.text_file_button = StringVar()
        self.text_file_button.set(self.text_file_explorer)
        self.row_0=0
        self.row_1=1
        self.row_2=2
        self.row_3=3
        self.col_0=0
        self.col_1=1
        self.col_2=2
  
        self.create_psf_widgets()
    
    def browseParamFiles(self):
        """

        """
        self.input_param_file_and_path = fdialog.askopenfilename()
        self.text_file_button.set(self.text_file_selected)
        # self.create_psf()
        read_file = open(self.input_param_file_and_path,'r')
        self.yaml_view.delete('1.0',END)
        file_contents = read_file.read()
        self.yaml_view.insert(tk.END, file_contents)
        param_file_and_path = os.path.split(self.input_param_file_and_path)
        self.input_param_path = param_file_and_path[0]
        self.input_param_file = param_file_and_path[1]

    def create_psf(self):
        """

        """
        try:
            template_example = Template(self.input_param_file_and_path)
            self.psf_name_path = template_example.parameters.file_pegasus_location
            self.psf_path = template_example.parameters.file_pegasus_input_location

            # creating list of strings which is the actual PSF! 
            create_template_string_list = template_example.create_psf()

            # creating PSF to write to
            with open(f"{self.psf_name_path}", "w") as output_psf:
                # doing for loop over list to write one string at a time to the output
                for string in range(0, len(create_template_string_list)):
                    try:
                        output_psf.write(create_template_string_list[string])
                    except TypeError:
                        print(f"string: {string}")

            # send success message
            result_msg = (
                f"The PSF has been saved and can be found here: {self.psf_path}\n"
            )
            messagebox.showinfo("Success!", result_msg)

            # read new psf and display in gui
            self.psf_view.delete('1.0',END)
            with open(self.psf_name_path,'r') as read_file:
                file_contents = read_file.read()
                self.psf_view.insert(tk.END, file_contents)

            # save copy of yaml file to input directory specified by user
            copy_param_file = self.psf_path+"/"+ self.input_param_file
            txt_box_contents = self.yaml_view.get('1.0',END)
            with open(f"{copy_param_file}",'w') as write_file:
                write_file.write(txt_box_contents)

            # give success message for saving updated yaml
            result_msg = (
                f"A copy of the YAML can be found here: {copy_param_file}\n"
            )
            messagebox.showinfo("Success!", result_msg)
            
        except Exception as e:
            error_str = traceback.format_exc()
            # messagebox.showerror("Error", str(error_str))
            self.create_error_msg(error_str)
    
    def create_error_msg(self, msg):
        """

        :param msg:
        """
        self.frame_psf_message_box = Toplevel(self.frame_psf_gen_page)
        self.frame_psf_message_box.title("Error")
        error_text = tk.Text(self.frame_psf_message_box, wrap=WORD, yscrollcommand=True)
        error_text.insert(INSERT, msg)
        error_text.pack()
        ok_button = ttk.Button(self.frame_psf_message_box, text="OK", command=self.frame_psf_message_box.destroy)
        ok_button.pack()
        self.frame_psf_message_box.lift()
        self.frame_psf_message_box.grab_set()
      
    def update_psf_contents(self):
        """

        """
        txt_box_contents = self.psf_view.get('1.0',END)
        write_file = open(f"{self.psf_name_path}",'w')
        write_file.write(txt_box_contents)
        write_file.close
        result_msg = (
            f"The PSF has been updated and can be found here: {self.psf_path}\n"
        )
        messagebox.showinfo("Success!", result_msg)
            
    def update_yaml_contents(self):
        """

        """
        txt_box_contents = self.yaml_view.get('1.0',END)
        write_file = open(f"{self.input_param_file_and_path}",'w')
        write_file.write(txt_box_contents)
        write_file.close

    def create_psf_widgets(self):
        """

        """
        # Get param file Label
        label_explore = ttk.Label(self.frame_psf_gen_page, text=self.text_q1)
        label_explore.grid(row=self.row_1, column=self.col_1, sticky=E, pady=5, padx=5)
        # label_explore.pack(expand=True, side=TOP, pady=3, padx=3)
        
        # Get param file button
        button_explore = ttk.Button(self.frame_psf_gen_page, textvariable=self.text_file_button, command=self.browseParamFiles)
        button_explore.grid(row=self.row_1, column=self.col_2, sticky=W, pady=5, padx=5)
        # button_explore.pack(expand=True, side=LEFT, pady=3, padx=3, fill=BOTH)

        # Create PSF button
        button_create_psf = ttk.Button(self.frame_psf_gen_page, text="Create PSF", command=self.create_psf)
        button_create_psf.grid(row=self.row_2, column=self.col_2, sticky=W, pady=5, padx=5)
        # button_create_psf.pack(expand=True, side=RIGHT, pady=3, padx=3, fill=BOTH)

        # Home button
        button_home = ttk.Button(self.frame_psf_gen_page, text ="Home", command = lambda : self.controller.show_frame(HomePage))
        button_home.grid(row=self.row_3, column=self.col_2, sticky=W, pady=5, padx=5)
        # button_home.pack(anchor="s", expand=True, side=BOTTOM, pady=3, padx=3, fill=BOTH)

        # File viewer
        # win_height = int(0.2 * self.winfo_toplevel().winfo_screenheight())
        # print(win_height)
        win_height = 50
        # YAML Editor
        label_yaml = ttk.Label(self.frame_yaml_input, text=self.text_show_yaml)
        self.yaml_view = tk.Text(self.frame_yaml_input) # added one text box
        button_update_psf = ttk.Button(self.frame_yaml_input, text=self.text_save, command=self.update_yaml_contents)
        label_yaml.grid(row=self.row_0,column=self.col_1, sticky=NSEW, pady=5, padx=5)
        self.yaml_view.grid(row=self.row_1,column=self.col_1, sticky=NSEW, pady=5, padx=5)
        button_update_psf.grid(row=self.row_2, column=self.col_1, sticky=NSEW, pady=5, padx=5)

        # PSF Editor
        label_psf = ttk.Label(self.frame_psf_output, text=self.text_show_psf)
        self.psf_view = tk.Text(self.frame_psf_output) # added one text box
        button_update_yaml = ttk.Button(self.frame_psf_output, text=self.text_save, command=self.update_psf_contents)
        label_psf.grid(row=self.row_0,column=self.col_1, sticky=NSEW, pady=5, padx=5)
        self.psf_view.grid(row=self.row_1,column=self.col_1, sticky=NSEW, pady=5, padx=5)
        button_update_yaml.grid(row=self.row_2, column=self.col_1, sticky=NSEW, pady=5, padx=5)


        for x in range(1,2):
            # self.frame_psf_gen_page.columnconfigure(x, weight=1)
            # self.frame_psf_gen_page.rowconfigure(x, weight=1)
            self.frame_yaml_input.columnconfigure(x, weight=1)
            self.frame_yaml_input.rowconfigure(x, weight=1)
            self.frame_psf_output.columnconfigure(x, weight=1)
            self.frame_psf_output.rowconfigure(x, weight=1)
    
class PlottingPage(tk.Frame, PictureToMovie, GraphAnimation, SpatialPlotting):
    """

    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        # super(PlottingPage, self).__init__()
        # /home/projects/pegasus_cases/JRC/runs/08_15_22/tabular_data/ID/BOT
        # /home/projects/pegasus_cases/JRC/runs/08_15_22/pics
        # colors
        self.plot_x_data_type = None
        self.red = "tomato"
        self.white = "white"
        self.light_grey = "grey80"
        self.dark_grey = "grey60"
        self.no_offset_line_color = 'blue'
        self.offset_line_color = 'red'
        self.color_1 = self.light_grey
        self.color_2 = self.dark_grey
        self.controller = controller

        # Window properties
        # self.window = Tk()
        # self.window = tk2.ThemedTk(theme="ubuntu")
        # # self.window.grid_columnconfigure(0, weight=1)
        # # self.window.grid_rowconfigure(0, weight=1)
        # # ['arc', 'plastik', 'scidgrey', 'itft1', 'clearlooks', 'black', 'yaru', 'clam', 'aquativo', 'keramik', 'equilux', 'scidblue', 
        # # 'blue', 'breeze', 'default', 'winxpblue', 'classic', 'scidpurple', 'kroc', 'elegance', 'scidpink', 'scidgreen', 
        # # 'radiance', 'adapta', 'smog', 'ubuntu', 'alt', 'scidmint', 'scidsand']
        # self.window.update()
        # self.window.config(bg=self.white)
        # self.width= self.winfo_toplevel().winfo_screenwidth()
        # self.height= self.winfo_toplevel().winfo_screenheight()
        # win_width = 1834
        # win_height = 571
        # self.win_size = str(int(win_width))+"x"+str(int(win_height))
        # self.winfo_toplevel().geometry(self.win_size)
        # self.winfo_toplevel().title('Griffinpy Plotting Tool')
        # self.window.attributes('-alpha', 0.7)
        self.notebook_base = ttk.Notebook(self)
        self.notebook_base.pack(expand=1,fill=BOTH)

        ################################# Post-Processing Layers ################################# 

        # sub notebooks
        # create frames to put sub notebooks
        self.notebook_tab_main = ttk.Frame(self.notebook_base)
        self.notebook_tab_animations = ttk.Frame(self.notebook_base)
        
        # add frames to base notebook
        self.notebook_base.add(self.notebook_tab_main, text="Plots")
        self.notebook_base.add(self.notebook_tab_animations, text="Animations")
        
        # adding sub notebooks
        self.notebook_main = ttk.Notebook(self.notebook_tab_main)
        self.notebook_animations = ttk.Notebook(self.notebook_tab_animations)

        # background image
        # bg_pegasus = PhotoImage(file='/home/projects/griffinpy/test/plotting_tools/pegasus_logo.png')

        # adding sub notebooks frames and tabs
        self.notebook_tab_data_questions = ttk.Frame(self.notebook_main)
        self.notebook_tab_data = ttk.Frame(self.notebook_main)
        self.notebook_main.add(self.notebook_tab_data_questions, text="Data Processing")
        # self.notebook_main.add(self.notebook_tab_data_questions, text="Data Processing", image=bg_pegasus)
        self.notebook_main.add(self.notebook_tab_data, text="Figures")
        self.notebook_data = ttk.Notebook(self.notebook_tab_data)
        self.notebook_main.pack(expand=True, fill="both")
        self.notebook_data.pack(expand=True, fill="both")
        self.notebook_animations.pack(expand=1, fill="both")


        # background image
        # bg_pegasus = PhotoImage(file='/home/projects/griffinpy/plotting_tools/pegasus_logo.png')
        # self.canvas = Canvas(self.window, height=win_height, width=win_width)
        # self.canvas.pack() #place(), etc.
        # self.Canvas_Image = self.canvas.create_image(0,0, image=bg_pegasus, anchor="s")

        # add frames for questions and spatial data on main notebook
        self.frame_notebook_main_questions = ttk.LabelFrame(self.notebook_tab_data_questions, text="Options")
        self.frame_notebook_main_sd = ttk.LabelFrame(self.notebook_tab_data_questions, text="Spatial Coordinates")
        self.frame_notebook_main_logo = Label(self.notebook_tab_data_questions)
        self.frame_notebook_main_questions.pack(expand=True, side=LEFT, pady=3, fill="both")
        self.frame_notebook_main_sd.pack(expand=True, side=RIGHT, pady=3, fill="both")
        # self.frame_notebook_main_questions.pack_propagate(False)

        # add frames for questions and spatial data on animations notebook
        self.frame_notebook_animations_questions = ttk.LabelFrame(self.notebook_animations, text="Options")
        self.frame_notebook_animations_movie = ttk.LabelFrame(self.notebook_animations, text="Movie")
        self.frame_notebook_animations_questions.pack(expand=False, side=LEFT, pady=3, fill="both")
        self.frame_notebook_animations_movie.pack(expand=True, side=RIGHT, pady=3, fill="both")
        # self.frame_notebook_animations_logo = Label(self.notebook_animations)
        # self.frame_notebook_animations_logo.pack(expand=True, side=BOTTOM, pady=3, fill="both")

        # Set the Menu initially
        self.text_creating_movie ="Creating Movie..."
        self.text_create_movie ="Create Movie"
        self.text_file_explorer = "File Explorer"
        self.text_directory_selected = "Directory Selected"
        self.text_file_selected = "File Selected"
        self.text_analyze_data =  "Analyze Data"
        self.text_update_x_data =  "Update X Data"
        self.text_update_y_data =  "Update Y Data"
        self.text_begin_plotting =  "Begin Plotting"
        self.text_close_plots = "Close All Plots and Plot Different Data"
        self.text_axis_scale_factor = "Enter axis scale factor"
        self.text_axis_label = "Enter axis label"
        self.text_select_x_axis_data = "Select X-axis data to be used to plot against all data"
        self.text_select_y_axis_data = "Select Y-axis data for updating plotting options"
        self.text_file_explorer_comparison_data = "Load comparison data"
        self.text_select_y_axis_pegasus_data = "Select Pegasus Variable"
        self.text_select_y_axis_comparison_data = "Select Comparison Variable"
        self.text_add_comparison = "Add"
        self.text_yes = "YES"
        self.text_no = "NO"
        self.text_together = "TOGETHER"
        self.text_separate = "SEPARATE"
        self.text_csv = "CSV"
        self.text_xlsx = "XLSX"
        
        
        self.text_file_button = StringVar()
        self.text_compare_button = StringVar()
        self.text_picture_button = StringVar()
        self.text_make_movie = StringVar()
        self.q1_resp = StringVar()
        self.q2_resp = StringVar()
        self.q3_resp = StringVar()
        self.q4_resp = StringVar()
        self.q5_a_resp = StringVar()
        self.q5_b_resp = StringVar()
        self.q5_c_resp = StringVar()
        self.q6_a_resp = StringVar()
        self.q6_b_resp = StringVar()
        self.q6_c_resp = StringVar()
        self.q7_resp = StringVar()
        self.q8_resp = StringVar()
        self.q9_resp = StringVar()
        self.q10_pegasus_resp = StringVar()
        self.q10_compare_resp = StringVar()
        
        self.q1_ani_resp = StringVar()
        self.q2_ani_resp = StringVar()
        self.q3_ani_resp = StringVar()

        # Setting initial values
        self.init_button_labels()

        # row and column indicies
        self.row_1 = 1
        self.row_2 = self.row_1 + 1
        self.row_3 = self.row_2 + 1
        self.row_4 = self.row_3 + 1
        self.row_5 = self.row_4 + 1
        self.row_6 = self.row_5 + 1
        self.row_7 = self.row_6 + 1
        self.row_8 = self.row_7 + 1
        self.row_9 = self.row_8 + 1
        self.row_10 = self.row_9 + 1
        self.row_11 = self.row_10 + 1
        self.row_12 = self.row_11 + 1
        self.row_13 = self.row_12 + 1
        self.row_14 = self.row_13 + 1
        self.row_15 = self.row_14 + 1
        self.row_16 = self.row_15 + 1
        self.row_17 = self.row_16 + 1
        self.col_1 = 1
        self.col_2 = 2
        self.col_3 = 3
        self.col_4 = 4
        self.col_5 = 5

        # Button dicts
        self.radio_button_dict_q2 = {
            1 : "CSV",
            2 : "XLSX"}
        self.radio_button_dict_q8 = {
            1 : "YES",
            2 : "NO"}

        # Question labels
        self.text_q1 = "Question 1: Select where the files to be processed are located: "
        self.text_q2 = "Question 2: Do you want to process a set of CSV files or a single XLSX file? "
        self.text_q3 = "Question 3: Enter the name of the xlsx to be created/used (exclude .xlsx extension): "
        self.text_q4 = "Question 4: Enter the node offset number (or enter 0 for no offset): "
        self.text_q5 = "Question 5: X-axis data options: "
        self.text_q6 = "Question 6: Y-axis data options: "
        self.text_q7 = "Question 7: Enter desired node ID('s) to plot data (or leave blank for all nodes): "
        self.text_q8 = "Question 8: Do you want to plot nodes together or separately: "
        self.text_q9 = "Question 9: Would you like to make graphical animations? "
        self.text_q10 = "Question 10: Comparison data options: "

        self.text_q1_ani = "Question 1: Select where the pictures to be processed are located: "
        self.text_q2_ani = "Question 2: Enter Length of Movie (seconds): "
        self.text_q3_ani = "Question 3: Enter Name of Movie: "

        self.data_type_label = None
        self.canvas_node = None
        self.create_all_widgets()

    def init_button_labels(self):
        """

        """
        self.q5_b_resp.set(self.text_axis_scale_factor)
        self.q5_c_resp.set(self.text_axis_label)
        self.q6_b_resp.set(self.text_axis_scale_factor)
        self.q6_c_resp.set(self.text_axis_label)

        self.text_file_button.set(self.text_file_explorer)
        self.text_compare_button.set(self.text_file_explorer_comparison_data)
        self.text_picture_button.set(self.text_file_explorer)
        self.text_make_movie.set(self.text_create_movie)
        self.q1_resp.set(" ")
        self.q1_ani_resp.set(" ")

    def restart_init_labels(self):
        """

        """
        # Loading data
        self.q2_resp.set(None)
        self.q3_resp.set("")
        self.q4_resp.set("")
        
        # Axis options
        self.init_data_menus()
        self.q5_b_resp.set(self.text_axis_scale_factor)
        self.q5_c_resp.set(self.text_axis_label)
        self.q6_b_resp.set(self.text_axis_scale_factor)
        self.q6_c_resp.set(self.text_axis_label)
        self.q8_resp.set(None)

    def create_error_msg(self, msg):
        """

        :param msg:
        """
        self.frame_plot_message_box = Toplevel(self.notebook_tab_main)
        self.frame_plot_message_box.title("Error")
        error_text = tk.Text(self.frame_plot_message_box, wrap=WORD, yscrollcommand=True)
        error_text.insert(INSERT, msg)
        error_text.pack()
        ok_button = ttk.Button(self.frame_plot_message_box, text="OK", command=self.frame_plot_message_box.destroy)
        ok_button.pack()
        self.frame_plot_message_box.lift()
        self.frame_plot_message_box.grab_set()
    
    def browse_compare_button(self):
        """

        """
        self.file_comp_data = fdialog.askopenfilename()
        print(f"file: {self.file_comp_data}")
        self.text_file_button.set(self.text_file_selected)
        self.file_comp_data_name = os.path.basename(self.file_comp_data).split('.', 1)[0]
        self.file_comp_data_path = os.path.dirname(self.file_comp_data)
        self.file_comp_data_name_and_path = self.file_comp_data
        if self.file_comp_data.endswith(".csv"):
            self.compare_convert_to_xlsx()
        elif self.file_comp_data.endswith(".xlsx"):
            self.file_comp_data = self.file_comp_data[:-5]
        self.get_comparison_data()
        self.compare_data_dict_copy = copy.deepcopy(self.compare_data_dict)
        self.update_q_10_menus()

    def browse_files_button(self):
        """

        """
        self.reset_spatial_plotting()
        self.file_location = fdialog.askdirectory()
        self.text_file_button.set(self.text_directory_selected)
        self.enable_q_234()
        self.enable_q_5()
        self.enable_q_6()
        self.restart_init_labels()
        self.disable_q_5()
        self.disable_q_6()
        self.disable_q_78bp()

    def browse_pictures_button(self):
        """

        """
        self.picture_location = fdialog.askdirectory()
        # self.q1_ani_resp.set(self.picture_location)
        self.text_picture_button.set(self.text_directory_selected)

        # Create path for movie
        griffinpy_output_directory = 'griffinpy_output'
        output_movies_directory = 'movies'
        self.path_to_output = os.path.join(self.picture_location, griffinpy_output_directory)
        self.path_to_output_movies = os.path.join(self.path_to_output, output_movies_directory)
        if not os.path.isdir(self.path_to_output):
            os.mkdir(self.path_to_output)
        if not os.path.isdir(self.path_to_output_movies):
            os.mkdir(self.path_to_output_movies)
        
        self.text_box_q2_ani['state'] = NORMAL
        self.text_box_q3_ani['state'] = NORMAL
        self.button_make_movie['state'] = NORMAL
         
    def create_movie_button(self):
        """

        """
        try:
            self.text_make_movie.set(self.text_creating_movie)
            self.q2_ani_val = int(self.q2_ani_resp.get())
            self.q3_ani_val = str(self.q3_ani_resp.get())+'.mp4'
            self.picture_directory = self.picture_location 
            self.movie_length = self.q2_ani_val
            self.movie_name = self.q3_ani_val
            self.text_box_q2_ani['state'] = DISABLED
            self.text_box_q3_ani['state'] = DISABLED
            self.button_make_movie['state'] = DISABLED
            self.movie_maker()
            result_msg = (f"The Movie has been saved and can be found here: {self.path_to_output_movies}\n")
            messagebox.showinfo("Success!", result_msg)
        except Exception as e:
            self.button_make_movie['state'] = DISABLED
            self.text_box_q2_ani['state'] = DISABLED
            self.text_box_q3_ani['state'] = DISABLED
            self.text_make_movie.set(self.text_create_movie)
            error_str = traceback.format_exc()
            self.create_error_msg(error_str)

    def analysis_data_button(self):
        """

        """
        try:
            self.q2_val = self.q2_resp.get()
            self.q3_val = self.q3_resp.get()
            # self.q4_val = self.q4_resp.get()
            self.q4_val = 0
            
            griffinpy_output_directory = 'griffinpy_output'
            output_figures_directory = 'figures'
            self.path_to_output = os.path.join(self.file_location, griffinpy_output_directory)
            self.path_to_output_figures = os.path.join(self.path_to_output, output_figures_directory)
            if not os.path.isdir(self.path_to_output):
                os.mkdir(self.path_to_output)
            if not os.path.isdir(self.path_to_output_figures):
                os.mkdir(self.path_to_output_figures)

            self.csv_or_xlsx = self.q2_val
            self.name_of_xlsx_file = self.q3_val + '.xlsx'
            self.node_id_offset = int(self.q4_val)

            if self.node_id_offset > 0:
                self.line_color = self.offset_line_color
                self.line_color_opp = self.no_offset_line_color
            else:
                self.line_color = self.no_offset_line_color
                self.line_color_opp = self.offset_line_color
            
            # Sort data
            self.analyze_data()
            self.x_axis_data_dict_copy = copy.deepcopy(self.x_axis_data_dict)

            # Updating buttons
            self.disable_q_234()
            self.button_analyze_data['state'] = DISABLED
            self.enable_q_5()
            self.update_x_axis_data_menu()
            
            # fill spatial data fame with spatial data plot or label indicating regular csv
            if not self.reg_csv_files:
                
                # if plotting new (spatial) data, remove label (if it exists)
                if self.data_type_label is not None:
                    self.data_type_label.destroy()
                # if plotting new (spatial) data, remove old plot (if it exists)
                if self.canvas_node is not None:
                    self.toolbar.destroy()
                    self.canvas_node.get_tk_widget().destroy()
                
                self.plot_nodes()
                self.canvas_node = FigureCanvasTkAgg(self.fig_nodes, master=self.frame_notebook_main_sd)
                self.canvas_node.draw()
                
                self.canvas_node.get_tk_widget().pack(side='top', fill='x', expand=0)
                self.toolbar = NavigationToolbar2Tk(self.canvas_node, self.frame_notebook_main_sd)
                self.toolbar.update()
                self.toolbar.pack(side='bottom', fill='x')
                self.fig_nodes.savefig(self.path_to_output_figures+'/node_id.png')

            else:
                self.data_type_label = ttk.Label(self.frame_notebook_main_sd, text=self.data_type_str).grid(row=self.row_1, column=self.col_1, sticky=E, pady=5, padx=5)
                
                # if plotting new (regular) data, remove spatial data plot (if it exists)
                if self.canvas_node is not None:
                    self.canvas_node.get_tk_widget().destroy()
        except Exception as e:
            error_str = traceback.format_exc()
            self.create_error_msg(error_str)

    def begin_plotting_button(self):
        """

        """
        self.determine_x_axis_data()
        points = self.q7_resp.get().replace(" ", "").split(",")
        try:
            self.point_selection = list(map(int, points))[:len(points)]
        except ValueError:
            self.point_selection = list(range(1, self.num_spatial_points+1))
        
        self.point_overlay_input = self.q8_resp.get()
        self.continue_plotting_input = "Y"
        self.create_animations_input = self.q9_resp.get()

        # comparison data
        data_name = self.q10_pegasus_resp.get()
        # print(f"data_name:{data_name}")
        if data_name != self.text_select_y_axis_pegasus_data:
            self.compare_data = True
            self.compare_data_index = self.x_axis_data_dict[str(data_name)][0]
            # print("here")

        # Gui
        self.plotting_iteration += 1 
        self.create_plots()
        self.disable_q_5()
        self.disable_q_6()
        self.disable_q_78bp()
        self.button_close_plots['state'] = NORMAL

        if self.create_animations_input == "Y":
            self.create_animation()

        # screen_width = self.window.winfo_width()
        # screen_height = self.window.winfo_height()
        result_msg = (
            f"All Figures have been saved and can be found here: {self.path_to_output_figures}\n"
            f"\n"
            f"Switch to the Figures tab to view results.")
        messagebox.showinfo("Success!", result_msg)
        
    def close_plots_button(self):
        """

        """
        plt.close('all')
        self.enable_q_5()
        self.enable_q_78()
        self.button_close_plots['state'] = DISABLED
        if self.q8_resp.get() !=0:
            self.button_begin_plotting['state'] = NORMAL
        for item in self.notebook_data.winfo_children():
            item.destroy()

    def update_x_data_button(self):
        """

        """
        q_a_val = self.q5_a_resp.get()
        q_b_val = self.q5_b_resp.get()
        q_c_val = self.q5_c_resp.get()

        # incase user does not update scale factor
        if q_b_val==self.text_axis_scale_factor or len(q_b_val)==0:
            self.x_data_scale_factor = 1.0
        else:
            self.x_data_scale_factor = float(q_b_val)
        
        # incase user does not update axis label
        if q_c_val==self.text_axis_label or len(q_c_val)==0:
            self.plot_x_data_type = q_a_val
        else:
            self.plot_x_data_type = q_c_val
        
        # if updating time varible, this throws index error
        try:
            self.out_label_list[self.x_axis_data_dict[str(q_a_val)][0]] = self.plot_x_data_type
        except IndexError:
            pass
        
        # update key
        self.x_axis_data_dict[str(self.plot_x_data_type)] = self.x_axis_data_dict.pop(str(q_a_val))

        # get index to assign new value that contains [index, multiplier]
        data_index = self.x_axis_data_dict[str(self.plot_x_data_type)][0]
        self.x_axis_data_dict[str(self.plot_x_data_type)] = [data_index, self.x_data_scale_factor]

        self.update_x_axis_data_menu()
        self.q5_a_resp.set(self.plot_x_data_type)
        self.enable_q_6()
        self.update_y_axis_data_menu()
        if not self.reg_csv_files:
            # allowing plotting options
            self.enable_q_78()
        self.enable_q_10()
        
        # enable plotting for regular csv (enable plotting after selecting T/S in )
        if self.reg_csv_files:
            self.button_begin_plotting['state'] = NORMAL

    def update_y_data_button(self):
        """

        """
        q_a_val = self.q6_a_resp.get()
        q_b_val = self.q6_b_resp.get()
        q_c_val = self.q6_c_resp.get()

        # incase user does not update scale factor
        if q_b_val==self.text_axis_scale_factor or len(q_b_val)==0:
            self.x_data_scale_factor = 1.0
        else:
            self.x_data_scale_factor = float(q_b_val)
        
        # incase user does not update axis label
        if q_c_val==self.text_axis_label or len(q_c_val)==0:
            self.plot_y_data_type = q_a_val
        else:
            self.plot_y_data_type = q_c_val
        
        # update labels
        self.out_label_list[self.x_axis_data_dict[str(q_a_val)][0]] = self.plot_y_data_type
        
        # update key
        self.x_axis_data_dict[str(self.plot_y_data_type)] = self.x_axis_data_dict.pop(str(q_a_val))

        # get index to assign new value that contains [index, multiplier]
        data_index = self.x_axis_data_dict[str(self.plot_y_data_type)][0]
        self.x_axis_data_dict[str(self.plot_y_data_type)] = [data_index, self.x_data_scale_factor]

        # update menu in gui
        self.update_y_axis_data_menu()
        self.q6_a_resp.set(self.plot_y_data_type)
        
        # update data scale from plotting tool class
        self.update_data_scale()
        print(f"self.x_axis_data_dict: {self.x_axis_data_dict}")

    def update_comp_data_button(self):
        self.compare_data = True
        pegasus_data = self.q10_pegasus_resp.get()
        comp_data = self.q10_compare_resp.get()
        data_index = self.x_axis_data_dict[str(pegasus_data)][0]

        print(
            f"data_index: {data_index}"
        )

        # update index so that comparison index will match pegasus data
        self.compare_data_dict[comp_data][0] = data_index

        # update the comparison flag to true
        self.compare_data_dict[comp_data][1] = True

        # update menus to exclude the selected comparison
        # for key in self.x_axis_data_dict_copy:
        #     print(
        #         f"x_axis_data_dict_copy {key}: {self.x_axis_data_dict_copy[key]}"
        #     )
        # for key in self.compare_data_dict_copy:
        #     print(
        #         f"compare_data_dict_copy {key}: {self.compare_data_dict_copy[key]}"
        #     )
        self.x_axis_data_dict_copy.pop(str(pegasus_data))
        self.compare_data_dict_copy.pop(comp_data)
        self.update_q_10_menus()

        self.q10_pegasus_resp.set(pegasus_data)
        self.q10_compare_resp.set(comp_data)

    def rb_t_pressed(self):
        """

        """
        if self.q8_resp.get() !=0:
            self.button_begin_plotting['state'] = NORMAL
            self.enable_q_10()

    def rb_s_pressed(self):
        """

        """
        if self.q8_resp.get() !=0:
            self.button_begin_plotting['state'] = NORMAL
            self.enable_q_10()
       
    def create_all_widgets(self):
        """

        """
        ##################################################################################################
        ######################################### Data Processing ########################################
        ##################################################################################################
        ################################# Question 1: (File explorer) get data directory #################################
        # Label
        label_explore = ttk.Label(self.frame_notebook_main_questions, text=self.text_q1)
        label_explore.grid(row=self.row_1, column=self.col_1, sticky=E, pady=5, padx=5)

        # File explorer button for getting data
        button_explore = ttk.Button(self.frame_notebook_main_questions, textvariable=self.text_file_button, command=self.browse_files_button)
        button_explore.grid(row=self.row_1, column=self.col_2, sticky=W, pady=5, padx=5)

        ################################# Question 2: (Radio Buttons) process xlsx or csv #################################
        # Label
        label_radio_button = ttk.Label(self.frame_notebook_main_questions, text=self.text_q2)
        label_radio_button.grid(row=self.row_2, column=self.col_1, sticky=E, pady=5, padx=5)
        
        # Radio buttons
        self.rb_c_q2 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_csv, variable=self.q2_resp, value=self.text_csv, state="disabled")
        self.rb_x_q2 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_xlsx, variable=self.q2_resp, value=self.text_xlsx, state="disabled")
        self.rb_c_q2.grid(row=self.row_2, column=self.col_2, sticky=W, pady=5, padx=5)
        self.rb_x_q2.grid(row=self.row_2, column=self.col_3, sticky=W, pady=5, padx=5)

        ################################# Question 3: (Create text box) name of xlsx #################################
        # Label
        label_q3 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q3)
        label_q3.grid(row=self.row_3, column=self.col_1, sticky=E, pady=5, padx=5)
        
        # Text box for entering name of new/existing xlsx workbook
        self.text_box_q3 = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q3_resp)
        self.text_box_q3.grid(row=self.row_3, column=self.col_2, sticky=W, pady=5, padx=5)
        self.text_box_q3['state'] = DISABLED

        ################################# Question 4: (Create text box) node offset (CURRENTLY DISABLED) #################################
        # Label
        label_q4 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q4)
        label_q4.grid(row=self.row_4, column=self.col_1, sticky=E, pady=5, padx=5)

        # Text box for entering a node offset (ultimately will change node numbers when plotting several poly lines and change plot colors)
        self.text_box_q4 = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q4_resp)
        self.text_box_q4.grid(row=self.row_4, column=self.col_2, sticky=W, pady=5, padx=5)
        self.text_box_q4['state'] = DISABLED

        ################################# Analyze Data Button (q6-8 will now be enabled) #################################
        # Button to begin analyzing data
        self.button_analyze_data = ttk.Button(self.frame_notebook_main_questions, text=self.text_analyze_data, command=self.analysis_data_button)
        self.button_analyze_data.grid(row=self.row_5, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)
        
        ################################# Question 5: x axis data options ################################# 
        # Label
        label_q5 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q5)
        label_q5.grid(row=self.row_6, column=self.col_1, sticky=E, pady=5, padx=5)

        # Question 5a: (dropdown menu) axis data
        self.list_q5_a = [self.text_select_x_axis_data]
        self.menu_drop_q5_a = ttk.OptionMenu(self.frame_notebook_main_questions, self.q5_a_resp, *self.list_q5_a, command=self.q5_selected)
        self.menu_drop_q5_a.grid(row=self.row_7, column=self.col_1, sticky=E, pady=5, padx=5)
        self.menu_drop_q5_a['state'] = DISABLED

        # Question 5b: (Entry box) axis data scale factor
        self.text_box_q5_b = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q5_b_resp)
        self.text_box_q5_b.grid(row=self.row_7, column=self.col_2, sticky=W, pady=5, padx=5)
        self.text_box_q5_b.bind("<FocusIn>", self.del_q5b)
        self.text_box_q5_b['state'] = DISABLED
        
        # Question 5c: (Entry box) axis data label
        self.text_box_q5_c = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q5_c_resp)
        self.text_box_q5_c.grid(row=self.row_7, column=self.col_3, sticky=W, pady=5, padx=5)
        self.text_box_q5_c.bind("<FocusIn>", self.del_q5c)
        self.text_box_q5_c['state'] = DISABLED

        # update data button
        self.button_upd_x_data = ttk.Button(self.frame_notebook_main_questions, text=self.text_update_x_data, command=self.update_x_data_button)
        self.button_upd_x_data.grid(row=self.row_7, column=self.col_4, sticky=(N, S, E, W), pady=5, padx=5)
        self.button_upd_x_data['state'] = DISABLED

        ################################# Question 6: Y axis data options ################################# 
        # Label
        label_q6 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q6)
        label_q6.grid(row=self.row_8, column=self.col_1, sticky=E, pady=5, padx=5)

        # Question 6a: (dropdown menu) axis data
        self.list_q6_a = [self.text_select_y_axis_data]
        self.menu_q6_a = ttk.OptionMenu(self.frame_notebook_main_questions, self.q6_a_resp, *self.list_q6_a, command=self.q6_selected)
        self.menu_q6_a.grid(row=self.row_9, column=self.col_1, sticky=E, pady=5, padx=5)
        self.menu_q6_a['state'] = DISABLED

        # Question 6b: (Entry box) data scale factor
        self.text_box_q6_b = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q6_b_resp)
        self.text_box_q6_b.grid(row=self.row_9, column=self.col_2, sticky=W, pady=5, padx=5)
        self.text_box_q6_b.bind("<FocusIn>", self.del_q6b)
        self.text_box_q6_b['state'] = DISABLED

        # Question 6c: (Entry box) data label
        self.text_box_q6_c = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q6_c_resp)
        self.text_box_q6_c.grid(row=self.row_9, column=self.col_3, sticky=W, pady=5, padx=5)
        self.text_box_q6_c.bind("<FocusIn>", self.del_q6c)
        self.text_box_q6_c['state'] = DISABLED

        # update data button
        self.button_upd_y_data = ttk.Button(self.frame_notebook_main_questions, text=self.text_update_y_data, command=self.update_y_data_button)
        self.button_upd_y_data.grid(row=self.row_9, column=self.col_4, sticky=(N, S, E, W), pady=5, padx=5)
        self.button_upd_y_data['state'] = DISABLED

        ################################# Question 7: (Create text box) Node numbers ################################# 
        # Label
        label_q7 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q7)
        label_q7.grid(row=self.row_10, column=self.col_1, sticky=E, pady=5, padx=5)
        
        # Text box for entering which nodes to plot
        self.text_box_q7 = ttk.Entry(self.frame_notebook_main_questions, textvariable=self.q7_resp)
        self.text_box_q7.grid(row=self.row_10, column=self.col_2, sticky=W, pady=5, padx=5)
        self.text_box_q7['state'] = DISABLED

        ################################# Question 8: (Radio Buttons) Plot together or separate ################################# 
        # Label
        label_q8 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q8)
        label_q8.grid(row=self.row_11, column=self.col_1, sticky=E, pady=5, padx=5)
        
        # Radio buttons
        self.rb_t_q8 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_together, variable=self.q8_resp, value="T", command=self.rb_t_pressed)
        self.rb_s_q8 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_separate, variable=self.q8_resp, value="S", command=self.rb_s_pressed)
        self.rb_t_q8.grid(row=self.row_11, column=(self.col_2), sticky=W, pady=5, padx=5)
        self.rb_s_q8.grid(row=self.row_11, column=(self.col_3), sticky=W, pady=5, padx=5)
        self.rb_t_q8['state'] = DISABLED
        self.rb_s_q8['state'] = DISABLED

        ################################# Question 9: (Radio Buttons) create animations (CURRENTLY DISABLED) ################################# 
        # Label
        label_q9 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q9)
        label_q9.grid(row=self.row_12, column=self.col_1, sticky=E, pady=5, padx=5)
        
        # Radio buttons
        self.rb_y_q9 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_yes, variable=self.q9_resp, value="Y")
        self.rb_n_q9 = ttk.Radiobutton(self.frame_notebook_main_questions, text=self.text_no, variable=self.q9_resp, value="N")
        self.rb_y_q9.grid(row=self.row_12, column=(self.col_2), sticky=W, pady=5, padx=5)
        self.rb_n_q9.grid(row=self.row_12, column=(self.col_3), sticky=W, pady=5, padx=5)
        self.rb_y_q9['state'] = DISABLED
        self.rb_n_q9['state'] = DISABLED

        ################################# Question 10: (file explorer, option menu) comparison data ################################# 
        # Label
        label_q10 = ttk.Label(self.frame_notebook_main_questions, text=self.text_q10)
        label_q10.grid(row=self.row_13, column=self.col_1, sticky=E, pady=5, padx=5)

        # (File explorer) get comparison data directory
        self.button_compare = ttk.Button(self.frame_notebook_main_questions, textvariable=self.text_compare_button, command=self.browse_compare_button)
        self.button_compare.grid(row=self.row_14, column=self.col_1, sticky=E, pady=5, padx=5)
        self.button_compare['state'] = DISABLED

        # (dropdown menu) select pegasus data to compare to
        self.list_q10 = [self.text_select_y_axis_pegasus_data]
        # self.menu_q10 = ttk.OptionMenu(self.frame_notebook_main_questions, self.q10_pegasus_resp, *self.list_q10, command=self.pegasus_data_flag)
        self.menu_q10 = ttk.OptionMenu(self.frame_notebook_main_questions, self.q10_pegasus_resp, *self.list_q10)
        self.menu_q10.grid(row=self.row_14, column=self.col_2, sticky=W, pady=5, padx=5)
        self.menu_q10['state'] = DISABLED

        # (dropdown menu) select variable from comparison data
        self.list_comp_q10 = [self.text_select_y_axis_comparison_data]
        self.menu_comp_q10 = ttk.OptionMenu(self.frame_notebook_main_questions, self.q10_compare_resp, *self.list_comp_q10, command=self.comparison_data_flag)
        self.menu_comp_q10.grid(row=self.row_14, column=self.col_3, sticky=W, pady=5, padx=5)
        self.menu_comp_q10['state'] = DISABLED

        # (button) update comparison data list 
        self.button_upd_comp_data = ttk.Button(self.frame_notebook_main_questions, text=self.text_add_comparison, command=self.update_comp_data_button)
        self.button_upd_comp_data.grid(row=self.row_14, column=self.col_4, sticky=(N, S, E, W), pady=5, padx=5)
        self.button_upd_comp_data['state'] = DISABLED

        ################################# Buttons ################################# 
        # Begin Plotting
        self.button_begin_plotting = ttk.Button(self.frame_notebook_main_questions, text=self.text_begin_plotting, command=self.begin_plotting_button, state="disabled")
        self.button_begin_plotting.grid(row=self.row_15, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)

        # Close all figures
        self.button_close_plots = ttk.Button(self.frame_notebook_main_questions, text=self.text_close_plots, command=self.close_plots_button, state="disabled")
        self.button_close_plots.grid(row=self.row_16, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)

        # Griffinpy Home button
        button_home = ttk.Button(self.frame_notebook_main_questions, text ="Home", command= lambda : self.controller.show_frame(HomePage))
        button_home.grid(row=self.row_17, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)

        ##################################################################################################
        ######################################### Animations Tab #########################################
        ##################################################################################################
        ################################# Question 1: (Buttons) get pictures directory #################################
        # Label
        label_explore_ani = ttk.Label(self.frame_notebook_animations_questions, text=self.text_q1_ani)
        label_explore_ani.grid(row=self.row_1, column=self.col_1, sticky=E)

        # File explorer for getting pictures
        button_explore_ani = ttk.Button(self.frame_notebook_animations_questions, textvariable=self.text_picture_button, command=self.browse_pictures_button)
        button_explore_ani.grid(row=self.row_1, column=self.col_2, sticky=W, pady=5, padx=5)

        ################################# Question 2: (Create text box) Length of movie #################################
        # Label
        label_q3 = ttk.Label(self.frame_notebook_animations_questions, text=self.text_q2_ani).grid(row=self.row_2, column=self.col_1, sticky=E)
        self.text_box_q2_ani = ttk.Entry(self.frame_notebook_animations_questions, textvariable=self.q2_ani_resp, state="disabled")
        self.text_box_q2_ani.grid(row=self.row_2, column=self.col_2, sticky=W, pady=5, padx=5)

        ################################# Question 3: (Create text box) name of movie #################################
        # Label
        label_q3 = ttk.Label(self.frame_notebook_animations_questions, text=self.text_q3_ani).grid(row=self.row_3, column=self.col_1, sticky=E)
        self.text_box_q3_ani = ttk.Entry(self.frame_notebook_animations_questions, textvariable=self.q3_ani_resp, state="disabled")
        self.text_box_q3_ani.grid(row=self.row_3, column=self.col_2, sticky=W, pady=5, padx=5)


        ################################# Buttons ################################# 
        # Make movie
        self.button_make_movie = ttk.Button(self.frame_notebook_animations_questions, textvariable=self.text_make_movie, command=self.create_movie_button, state="disabled")
        self.button_make_movie.grid(row=self.row_4, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)

        # Griffinpy Home button
        button_home = ttk.Button(self.frame_notebook_animations_questions, text ="Home", command= lambda : self.controller.show_frame(HomePage))
        button_home.grid(row=self.row_5, column=self.col_1, sticky=(N, S, E, W), pady=5, padx=5)

    def init_data_menus(self):
        """

        """
        menu_x = self.menu_drop_q5_a['menu']
        menu_y = self.menu_q6_a['menu']
        menu_x.delete(0, 'end')
        menu_y.delete(0, 'end')
        menu_x.add_command(label=self.text_select_x_axis_data, command=lambda name=self.text_select_x_axis_data: self.q5_a_resp.set(self.text_select_x_axis_data))
        menu_y.add_command(label=self.text_select_y_axis_data, command=lambda name=self.text_select_y_axis_data: self.q5_a_resp.set(self.text_select_y_axis_data))
        
    def update_x_axis_data_menu(self):
        """

        """
        menu_x = self.menu_drop_q5_a['menu']
        menu_x.delete(0, 'end')
        for key, value in self.x_axis_data_dict.items():
            name = key
            menu_x.add_command(label=key, command=lambda name=name: self.q5_a_resp.set(name))

    def update_y_axis_data_menu(self):
        """

        """
        menu_y = self.menu_q6_a['menu']
        menu_y.delete(0, 'end')
        for key, value in self.x_axis_data_dict.items():
            name = key
            menu_y.add_command(label=key, command=lambda name=name: self.q6_a_resp.set(name))

    def update_q_10_pegasus_data_menu(self):
        """

        """
        # print(f"self.q10_pegasus_resp.get(): {self.q10_pegasus_resp.get()}")
        menu = self.menu_q10['menu']
        menu.delete(0, 'end')
        
        for key in self.x_axis_data_dict_copy:
            menu.add_command(label=key, command=lambda name=key: self.q10_pegasus_resp.set(name))
        
        # self.q10_pegasus_resp.set(self.q10_pegasus_resp.get())
    
    def update_q_10_compare_data_menu(self):
        """

        """
        menu = self.menu_comp_q10['menu']
        menu.delete(0, 'end')
        for key in self.compare_data_dict_copy:
            menu.add_command(label=key, command=lambda name=key: self.q10_compare_resp.set(name))

    def update_q_10_menus(self):
        self.update_q_10_pegasus_data_menu()
        self.update_q_10_compare_data_menu()

    def pegasus_data_flag(self, event):
        """

        :param event:
        """
        print(self.q10_pegasus_resp.get())

    def comparison_data_flag(self, event):
        """

        :param event:
        """
        print(self.q10_compare_resp.get())

    def enable_q_234(self):
        """

        """
        self.button_analyze_data['state'] = NORMAL
        self.rb_c_q2['state'] = NORMAL
        self.rb_x_q2['state'] = NORMAL
        self.text_box_q3['state'] = NORMAL
        # self.text_box_q4['state'] = NORMAL
    
    def enable_q_5(self):
        """

        """
        self.menu_drop_q5_a['state'] = NORMAL
        self.text_box_q5_b['state'] = NORMAL
        self.text_box_q5_c['state'] = NORMAL
        self.button_upd_x_data['state'] = NORMAL
       
    def enable_q_6(self):
        """

        """
        self.menu_q6_a['state'] = NORMAL
        self.text_box_q6_b['state'] = NORMAL
        self.text_box_q6_c['state'] = NORMAL
        self.button_upd_y_data['state'] = NORMAL

    def enable_q_78(self):
        """

        """
        self.text_box_q7['state'] = NORMAL
        if not self.reg_csv_files:
            self.rb_t_q8['state'] = NORMAL
            self.rb_s_q8['state'] = NORMAL
        # self.button_begin_plotting['state'] = NORMAL

    def enable_q_10(self):
        """

        """
        self.button_compare['state'] = NORMAL
        self.menu_q10['state'] = NORMAL
        self.menu_comp_q10['state'] = NORMAL
        self.button_upd_comp_data['state'] = NORMAL

    def disable_q_234(self):
        """

        """
        self.button_analyze_data['state'] = DISABLED
        self.rb_c_q2['state'] = DISABLED
        self.rb_x_q2['state'] = DISABLED
        self.text_box_q3['state'] = DISABLED
        self.text_box_q4['state'] = DISABLED

    def disable_q_5(self):
        """

        """
        self.menu_drop_q5_a['state'] = DISABLED
        self.text_box_q5_b['state'] = DISABLED
        self.text_box_q5_c['state'] = DISABLED
        self.button_upd_x_data['state'] = DISABLED

    def disable_q_6(self):
        """

        """
        self.menu_q6_a['state'] = DISABLED
        self.text_box_q6_b['state'] = DISABLED
        self.text_box_q6_c['state'] = DISABLED
        self.button_upd_y_data['state'] = DISABLED
    
    def disable_q_78bp(self):
        """

        """
        self.text_box_q7['state'] = DISABLED
        self.rb_t_q8['state'] = DISABLED
        self.rb_s_q8['state'] = DISABLED
        self.rb_y_q9['state'] = DISABLED
        self.rb_n_q9['state'] = DISABLED
        self.button_begin_plotting['state'] = DISABLED
        self.disable_q_10()

    def disable_q_10(self):
        """

        """

        self.button_compare['state'] = DISABLED
        self.menu_q10['state'] = DISABLED
        self.menu_comp_q10['state'] = DISABLED
        self.button_upd_comp_data['state'] = DISABLED
 
    def q5_selected(self, event):
        """

        :param event:
        """
        print(self.q5_a_resp.get())

    def q6_selected(self, event):
        """

        :param event:
        """
        print(self.q6_a_resp.get())

    def del_q5b(self, event):
        """

        :param event:
        """
        self.text_box_q5_b.delete(0,"end")

    def del_q5c(self, event):
        """

        :param event:
        """
        self.text_box_q5_c.delete(0,"end")

    def del_q6b(self, event):
        """

        :param event:
        """
        self.text_box_q6_b.delete(0,"end")

    def del_q6c(self, event):
        """

        :param event:
        """
        self.text_box_q6_c.delete(0,"end")

def create_griffinpy_gui():
    """

    """
    try:
        griffinpy_gui = GriffinpyGUI()
        griffinpy_gui.mainloop()
    except KeyboardInterrupt:
        print("\nExiting Griffinpy")
        griffinpy_gui.window.destroy()

if __name__ == '__main__':
    create_griffinpy_gui()