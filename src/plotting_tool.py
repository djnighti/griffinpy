from multi_csv_to_xlsx import MultiCsvToXlsx
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import TextBox, Button
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib import animation
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from decimal import Decimal
import os
import sys
from itertools import groupby
from tkinter import *
import tkinter.filedialog as fdialog
from tkinter import ttk
from tkinter import messagebox
from tkVideoPlayer import TkinterVideo
import imageio
from moviepy.editor import *
import glob
from ttkthemes import themed_tk as tk2
import time
from matplotlib import rcParams
import traceback

rcParams.update({'figure.autolayout': True})
np.set_printoptions(threshold=sys.maxsize)
# sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# /home/projects/pegasus_cases/JRC/runs/08_15_22/tabular_data/ID/BOT
 

class SpatialPlotting:
    def __init__(self):
        super(SpatialPlotting, self).__init__()
        self.reset_spatial_plotting()

    def reset_spatial_plotting(self):
        self.time_repeated_data = []
        self.x_repeated_coordinate_data = []
        self.y_repeated_coordinate_data = []
        self.z_repeated_coordinate_data = []
        self.output_unsorted_data = []
        self.t_sorted = []
        self.x_sorted = []
        self.y_sorted = []
        self.z_sorted = []
        self.output_sorted = []
        self.csv_or_xlsx = None
        self.file_location = None
        self.path_to_output_figures = None
        self.name_of_xlsx_file = None
        self.sheet_name = None
        self.compare_data_index = None
        self.compare_data = False
        self.compare_data_dict = {}
        self.t_lim_list = []
        self.x_lim_list = []
        self.y_lim_list = []
        self.z_lim_list = []
        self.output_lim_list = []
        self.time_label_list = []
        self.x_label_list = []
        self.y_label_list = []
        self.z_label_list = []
        self.out_label_list = []
        self.title_list = []
        self.prefix_list = []
        self.dict_title_list = {}
        self.num_of_data_points = None
        self.length_of_movie = 10
        self.calc_fps = None
        self.iteration_count = 0
        self.num_spatial_points = 0
        self.node_id_offset = 9
        self.number_of_output_variables = 0
        self.number_of_splines = 1
        self.max_x_coord = 0
        self.time_scale = 0
        self.time_shift = 0
        self.node_label_id = []
        self.point_selection = None
        self.reg_csv_files = False
        self.plotting_iteration = 1
        self.pegasus_out_label = "Pegasus Output"
        self.comparison_label = "Comparison Data"
        self.compare_data_style = {'color':'r','marker': 'o', 'markeredgecolor': 'r', 'markerfacecolor': 'r', 'linestyle':'dashed', 'linewidth':'2', 'markersize':'2'}
        # self.pegasus_data_style = {self.pegasus_out_label: 'o', 'markeredgecolor': 'b', 'markerfacecolor': 'b', 'linestyle':'dashed', 'linewidth':'1', 'markersize':'2'}
        self.fig_num = 1
        self.file_found = True
        self.continue_search_for_file = True
        self.multi_csv_to_xlsx = MultiCsvToXlsx(external_call=True)

    def compare_convert_to_xlsx(self):
        self.multi_csv_to_xlsx.file_location = self.file_comp_data_path
        self.multi_csv_to_xlsx.name_of_xlsx_file = self.file_comp_data_name+'.xlsx'
        self.multi_csv_to_xlsx.name_of_csv_file = self.file_comp_data_name
        self.multi_csv_to_xlsx.name_and_path_of_csv_file = self.file_comp_data_name_and_path
        self.multi_csv_to_xlsx.single_csv_to_xlsx_converter()

    def convert_to_xlsx(self):
        self.multi_csv_to_xlsx.file_location = self.file_location
        self.multi_csv_to_xlsx.name_of_xlsx_file = self.name_of_xlsx_file
        self.multi_csv_to_xlsx.multi_csv_to_xlsx_converter()
    
    def get_comparison_data(self):
        compare_column_x = []
        compare_column_y = []
        self.compare_title_list = []
        self.compare_column_x_label_list = []
        self.compare_column_y_label_list = []
        xlsx_dict = pd.read_excel(
            f"{self.file_comp_data_path}/{self.file_comp_data_name}.xlsx",
            sheet_name=None)
        index=0
        
        for sheet_name, sheet_data in xlsx_dict.items():
            index = index+1
            headers_list = xlsx_dict[sheet_name].columns.ravel()
            # print(
            #     f"headers_list: {headers_list}"
            # )
            compare_column_x = (xlsx_dict[sheet_name][headers_list[0]].values.tolist())
            compare_column_y = (xlsx_dict[sheet_name][headers_list[1]].values.tolist())
            self.compare_title_list.append(sheet_name)
            # dict with key:sheet name, value: list[compare:false, np array of xy data]
            self.compare_data_dict[sheet_name] = [index, False, [np.array(compare_column_x), np.array(compare_column_y)]]

            # print(
            #     # f"np.array(compare_column_y): {np.array(compare_column_y[0])}"
            #     f"(x0,y0): ({self.compare_data_dict[sheet_name][2][0][0]}, {self.compare_data_dict[sheet_name][2][1][0]})\n"
            # )

        print("\ncomparison data loaded\n")

    def determine_data_type(self, data_x, data_y, data_z ):
        # analyze coordinates to determine number of nodes
        xcoor_linear = data_x.flat
        ycoor_linear = data_y.flat
        zcoor_linear = data_z.flat
        
        xcoor_circ = np.diff(xcoor_linear)
        ycoor_circ = np.diff(ycoor_linear)
        zcoor_circ = np.diff(zcoor_linear)
        
        # circumferential
        xcoor_sorted_circ, unique_xspline_index_circ = np.unique(xcoor_circ, axis=0, return_index=True)
        ycoor_sorted_circ, unique_yspline_index_circ = np.unique(ycoor_circ, axis=0, return_index=True)
        zcoor_sorted_circ, unique_zspline_index_circ = np.unique(zcoor_circ, axis=0, return_index=True)
        
        # radial/axial
        xcoor_sorted_linear, unique_xspline_index_linear = np.unique(xcoor_linear, axis=0, return_index=True)
        ycoor_sorted_linear, unique_yspline_index_linear = np.unique(ycoor_linear, axis=0, return_index=True)
        zcoor_sorted_linear, unique_zspline_index_linear = np.unique(zcoor_linear, axis=0, return_index=True)
        
        print(f"zcoor_sorted_linear: {zcoor_sorted_linear}\n")
        print(
            f"x linear: {xcoor_sorted_linear.shape[0]} circ: {xcoor_sorted_circ.shape[0]}\n"
            f"y linear: {ycoor_sorted_linear.shape[0]} circ: {ycoor_sorted_circ.shape[0]}\n"
            f"z linear: {zcoor_sorted_linear.shape[0]} circ: {zcoor_sorted_circ.shape[0]}\n"
            )
        
        if xcoor_sorted_linear.shape[0] > xcoor_sorted_circ.shape[0]:
            x_spatial_num = xcoor_sorted_linear.shape[0]
        else:
            x_spatial_num = xcoor_sorted_circ.shape[0]
        
        if ycoor_sorted_linear.shape[0] > ycoor_sorted_circ.shape[0]:
            y_spatial_num = ycoor_sorted_linear.shape[0]
        else:
            y_spatial_num = ycoor_sorted_circ.shape[0]
        
        if zcoor_sorted_linear.shape[0] > zcoor_sorted_circ.shape[0]:
            z_spatial_num = zcoor_sorted_linear.shape[0]
        else:
            z_spatial_num = zcoor_sorted_circ.shape[0]
        
        # defining number of spatial points
        self.num_spatial_points = max([x_spatial_num, y_spatial_num, z_spatial_num])
    
        print(
            f"x_spatial_num: {x_spatial_num}\n"
            f"y_spatial_num: {y_spatial_num}\n"
            f"z_spatial_num: {z_spatial_num}\n"
            f"num_spatial_points: {self.num_spatial_points}\n"
            )
                
    def get_tabular_data(self):
        time_repeated_data = []
        x_repeated_coordinate_data = []
        y_repeated_coordinate_data = []
        z_repeated_coordinate_data = []
        output_unsorted_data = []
        xlsx_dict = pd.read_excel(
            f"{self.file_location}/{self.name_of_xlsx_file}",
            sheet_name=None)
        
        for sheet_name, sheet_data in xlsx_dict.items():
            headers_list = xlsx_dict[sheet_name].columns.ravel()
            time_repeated_data.append(xlsx_dict[sheet_name][headers_list[0]].values.tolist())
            x_repeated_coordinate_data.append(xlsx_dict[sheet_name][headers_list[1]].values.tolist())
            y_repeated_coordinate_data.append(xlsx_dict[sheet_name][headers_list[2]].values.tolist())
            z_repeated_coordinate_data.append(xlsx_dict[sheet_name][headers_list[3]].values.tolist())
            output_unsorted_data.append(xlsx_dict[sheet_name][headers_list[4]].values.tolist())
            self.time_label_list.append(headers_list[0])
            self.x_label_list.append(headers_list[1])
            self.y_label_list.append(headers_list[2])
            self.z_label_list.append(headers_list[3])
            self.out_label_list.append(headers_list[4])
            self.title_list.append(sheet_name)

        self.time_repeated_data = np.array(time_repeated_data)
        self.x_repeated_coordinate_data = np.array(x_repeated_coordinate_data)
        self.y_repeated_coordinate_data = np.array(y_repeated_coordinate_data)
        self.z_repeated_coordinate_data = np.array(z_repeated_coordinate_data)
        self.output_unsorted_data = np.array(output_unsorted_data)

        # analyze coordinates to determine number of nodes
        self.determine_data_type(
            self.x_repeated_coordinate_data, 
            self.y_repeated_coordinate_data, 
            self.z_repeated_coordinate_data)

        num_spatial_points_time = np.argmax(np.diff(self.time_repeated_data.flat) != 0) + 1
        print(f"num_spatial_points_time: {num_spatial_points_time}")

        # total number of output variables
        self.number_of_output_variables = self.output_unsorted_data.shape[0]
        
        # data set contains same state variable at multiple coordinates (not using polyline)
        if self.num_spatial_points >= num_spatial_points_time and num_spatial_points_time==1:
            self.data_type_str = "Regular csv files detected"
            self.reg_csv_files = True
            self.num_spatial_points = 1
            self.sort_regular_data()
        
        # unique state variables retrieved for polyline data
        else:
            self.data_type_str = "Spatial csv files detected"
            self.reg_csv_files = False
            self.sort_spatial_data()
        
        
        for out_var in range(0, len(self.out_label_list)):
            self.out_label_list[out_var] = self.out_label_list[out_var].replace(" ", "")

        for out_var in range(0, len(self.time_label_list)):
            self.time_label_list[out_var] = self.time_label_list[out_var].replace(" ", "").upper()
            
        
        # print(f"\nget_tabular_data --- self.out_label_list: {self.out_label_list}")
        
        
        self.update_data_labels()

        for title in self.title_list:
            prefix = title.split('_', 1)[0]
            self.prefix_list.append(prefix)
  
    def sort_spatial_data(self):
        self.num_of_data_points = self.time_repeated_data.shape[1] / self.num_spatial_points
        num_data_sets = self.output_unsorted_data.shape[0]
        self.t_sorted = self.time_repeated_data[0].reshape(int(self.num_of_data_points), int(self.num_spatial_points)).transpose()[0]
        self.x_sorted = self.x_repeated_coordinate_data[0].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]
        self.y_sorted = self.y_repeated_coordinate_data[0].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]
        self.z_sorted = self.z_repeated_coordinate_data[0].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]

        # print(
        #     f"self.t_sorted: {self.t_sorted.shape}"
        # )

        # Determine if multiple splines
        unique_x_splines = []
        unique_y_splines = []
        unique_z_splines = []
        for data_set in range(0, num_data_sets):
            x_n_sorted = self.x_repeated_coordinate_data[data_set].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]
            y_n_sorted = self.y_repeated_coordinate_data[data_set].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]
            z_n_sorted = self.z_repeated_coordinate_data[data_set].reshape(int(self.num_of_data_points), int(self.num_spatial_points))[0]
            unique_x_splines.append(x_n_sorted)
            unique_y_splines.append(y_n_sorted)
            unique_z_splines.append(z_n_sorted)
        
        self.x_sorted, unique_spline_index = np.unique(unique_x_splines, axis=0, return_index=True)
        self.y_sorted = np.array(unique_y_splines)[unique_spline_index]
        self.z_sorted = np.array(unique_z_splines)[unique_spline_index]
        self.number_of_splines = self.x_sorted.shape[0]
        self.coordinates_xyz = np.concatenate((self.x_sorted, self.y_sorted, self.z_sorted), axis=0)

        # print(
        #     f"self.coordinates_xyz: {self.coordinates_xyz}"
        # )

        for output in range(0, self.output_unsorted_data.shape[0]):
            self.output_sorted.append(self.output_unsorted_data[output].reshape(int(self.num_of_data_points), int(self.num_spatial_points)).transpose())

    def sort_regular_data(self):
        self.num_of_data_points = self.time_repeated_data.shape[1]
        num_data_sets = self.output_unsorted_data.shape[0]
        self.t_sorted = self.time_repeated_data[0]
        self.x_sorted = self.x_repeated_coordinate_data
        self.y_sorted = self.y_repeated_coordinate_data
        self.z_sorted = self.z_repeated_coordinate_data

        # Determine if multiple splines
        self.number_of_splines = 1
        self.output_sorted = self.output_unsorted_data

    def update_data_labels(self):
        self.x_axis_data_list = []
        self.x_axis_data_list.extend(self.out_label_list)
        self.x_axis_data_list.extend(self.time_label_list[0])
        self.update_data_labels_test()
        # self.x_axis_data_dict = dict((i,j) for i,j in enumerate(self.x_axis_data_list))
        # print(f"self.x_axis_data_list: {self.x_axis_data_list}\n")
        # print(f"\nBEFORE update_data_labels ---: \n")
        # y = enumerate(self.x_axis_data_list)
        # # for out_label in self.x_axis_data_list:
        #     # print(f"{out_label}")
        # for out_label in y:
        #     print(f"{out_label}")
        # self.x_axis_data_dict = {x: [index, 1.0] for index, x in enumerate(self.x_axis_data_list)}
        self.x_axis_data_dict_time_index = len(self.x_axis_data_dict) - 1
        # print(f"\nAFTER update_data_labels ---: \n")
        # for key, value in self.x_axis_data_dict.items():
        #     print(f"{key} {value}")
            # pass
    
    def update_data_labels_test(self):
        self.x_axis_data_list = []
        self.x_axis_data_list.extend(self.out_label_list)
        self.x_axis_data_list.extend(self.time_label_list[0])
        
        # print(f"\nBEFORE update_data_labels_test ---: \n")
        # print(f"self.x_axis_data_list: {self.x_axis_data_list}")
        # y = enumerate(self.x_axis_data_list)
        # for out_label in self.x_axis_data_list:
        #     print(f"{out_label}")
        # for out_label in y:
        #     print(f"{out_label}")
            
        # ['STRESS_22', 'FAST_NEUTRON_FLUX', 'STRESS_22', 'STRESS_22', 'STRESS_22', 'TEMPERATURE', 'FAST_NEUTRON_FLUENCE', 'STRESS_22', 'STRESS_22', 'T']
        
        self.x_axis_data_dict = {}
        duplicates = {}
        for item in enumerate(self.x_axis_data_list):
            # print(f"item: {item}")
            if item[1] not in duplicates:
                # print(f"item not: {item}")
                if item[1] in self.x_axis_data_dict:
                    duplicates[item] = 1
                    index = 1
                    key = item[1] + '_' + str(index)
                    while key in self.x_axis_data_dict:
                        index += 1
                        key = item[1] + '_' + str(index)
                    self.x_axis_data_dict[key] = [item[0], 1.0]
                else:
                    self.x_axis_data_dict[item[1]] = [item[0], 1.0]
            else:
                # print(f"item is in: {item}, {duplicates}")
                index = duplicates[item[1]]
                key = item[1] + '_' + str(index)
                while key in self.x_axis_data_dict:
                    index += 1
                    key = item[1] + '_' + str(index)
                self.x_axis_data_dict[key] = [item[0], 1.0]
                duplicates[item[1]] += 1
        
        # print(f"\nAFTER update_data_labels_test ---: \n")
        # for key, value in self.x_axis_data_dict.items():
        #     print(f"key: {key}, value: {value}")
    
    def update_data_scale(self):
        data_index = self.x_axis_data_dict[self.plot_y_data_type][0]
        scale_factor = self.x_axis_data_dict[self.plot_y_data_type][1]

    def determine_x_axis_data(self):
        plot_x_data_type = self.plot_x_data_type
        # print(
        #     f"self.x_axis_data_dict: {self.x_axis_data_dict}\n"
        #     f"\n"
        #     f"self.x_axis_data_dict[plot_x_data_type][0]: {self.x_axis_data_dict[plot_x_data_type][0]}\n"
        #     f"self.x_axis_data_dict_time_index: {self.x_axis_data_dict_time_index}\n"
        # )
        # print(f"\ndetermine_x_axis_data --- self.x_axis_data_dict: {self.x_axis_data_dict} \n")
        if self.x_axis_data_dict[plot_x_data_type][0]==self.x_axis_data_dict_time_index:
            self.x_label = plot_x_data_type
            self.x_data = self.x_axis_data_dict[plot_x_data_type][1] * self.t_sorted
        else:
            self.x_label = self.out_label_list[self.x_axis_data_dict[plot_x_data_type][0]]
            self.x_data = self.x_axis_data_dict[plot_x_data_type][1] * self.output_sorted[self.x_axis_data_dict[plot_x_data_type][0]]

    def format_coordinates(self, x_coor, y_coor, z_coor):
        point_x = Decimal(float(x_coor))
        point_y = Decimal(float(y_coor))
        point_z = Decimal(float(z_coor))
        point_x_f = f"{point_x:.3E}" if point_x != 0 else f"{point_x:.1f}"
        point_y_f = f"{point_y:.3E}" if point_y != 0 else f"{point_y:.1f}"
        point_z_f = f"{point_z:.3E}" if point_z != 0 else f"{point_z:.1f}"
        xyz_string = f"(x,y,z) = ({point_x_f}, {point_y_f}, {point_z_f})m"
        return xyz_string

    def plot_spatial_together(self, point_select=None):
        if point_select is None or len(point_select) == 0:
            point_list = list(range(1, self.num_spatial_points+1))
            title_str_end = 'Comparing all captured points'
            png_id_str = '_comparing_all_nodes'
        else:
            point_list = point_select
            title_str_end = "Comparing "+str(len(point_list))+" nodes"
            png_id_str = '_comparing_'+str(len(point_list))+'_nodes'
        
        node_id_str = "_".join([str(num) for num in point_list])
        
        self.fig_num=1
        for output_variable in range(0, self.number_of_output_variables):
            # update data scaling
            data_index = self.x_axis_data_dict[self.out_label_list[output_variable]][0]
            scale_factor = self.x_axis_data_dict[self.out_label_list[output_variable]][1]

            # initializing figure
            fig = Figure()
            title_str = f"Data set: {self.title_list[output_variable]} \n{title_str_end}"
            plot = fig.add_subplot(111, title=title_str, xlabel=self.x_label, ylabel=self.out_label_list[output_variable])
            for spatial_point in point_list:
                node_i_label = "Node ID: "+str(spatial_point)
                try:
                    plot.plot(
                        self.x_data[spatial_point-1-self.node_id_offset], 
                        scale_factor * self.output_sorted[output_variable][spatial_point-1], 
                        label=node_i_label
                        )
                except:
                    plot.plot(
                        self.x_data, 
                        scale_factor * self.output_sorted[output_variable][spatial_point-1], 
                        label=node_i_label
                        )
            # print(
            #     f"self.compare_data_index: {self.compare_data_index}"
            #     f"self.compare_data: {self.compare_data}"
            # # )
            # if self.compare_data_index is not None:
            #     if data_index==self.compare_data_index:
            #         if self.compare_data:
            #             compare_label="Comparison Data"
            #             plot.plot(
            #                 self.compare_column_x, 
            #                 self.compare_column_y, 
            #                 label=compare_label
            #                 )
            #             # print(
            #             #     f"ploting comparison data,  data_index: {data_index}"
            #             #     f"compare_column_x: {self.compare_column_x}\n"
            #             #     f"compare_column_y: {self.compare_column_y}\n"
            #             # )
            # plot.legend()

            for key in self.compare_data_dict:
                # check if compare data index is same as pegasus and user did want to compare
                if data_index==self.compare_data_dict[key][0] and self.compare_data_dict[key][1]:
                    compare_label="Comparison Data"
                    plot.plot(
                        self.compare_data_dict[key][2][0],
                        self.compare_data_dict[key][2][1], 
                        label=self.comparison_label,
                        **self.compare_data_style
                        )
            plot.legend()
                            
            # add plot to gui tab
            self.add_plot_to_new_tab(("Fig_"+str(self.fig_num)), fig)
            
            # save figure
            fig.savefig(self.path_to_output_figures+'/'+'spatial_together_'+str(node_id_str)+'_fig_num_'+str(self.fig_num)+'.png', bbox_inches='tight')
            self.fig_num += 1

    def plot_spatial_separate(self, point_select=None):
        if point_select is None or len(point_select) == 0:
            point_list = list(range(1, self.num_spatial_points+1))
        else:
            point_list = point_select
        self.fig_num=1
        for output_variable in range(0, self.number_of_output_variables):
            # update data scaling
            data_index = self.x_axis_data_dict[self.out_label_list[output_variable]][0]
            scale_factor = self.x_axis_data_dict[self.out_label_list[output_variable]][1]

            for spatial_point in point_list:
                node_i_label = "Node ID: "+str(spatial_point)

                # initializing figure
                fig = Figure()
                
                xyz_coor = self.format_coordinates(
                    self.x_sorted[0][spatial_point-1-self.node_id_offset],
                    self.y_sorted[0][spatial_point-1-self.node_id_offset],
                    self.z_sorted[0][spatial_point-1-self.node_id_offset])
                
                title_str = f"Data set: {self.title_list[output_variable]} \n{node_i_label} \nCoordinate: {xyz_coor} "
                plot = fig.add_subplot(111, title=title_str, xlabel=self.x_label, ylabel=self.out_label_list[output_variable])
                try:
                    plot.plot(
                        self.x_data[spatial_point-1-self.node_id_offset], 
                        scale_factor * self.output_sorted[output_variable][spatial_point-1-self.node_id_offset], 
                        color=self.line_color, 
                        label=node_i_label)
                except:
                    plot.plot(
                        self.x_data, 
                        scale_factor * self.output_sorted[output_variable][spatial_point-1-self.node_id_offset], 
                        color=self.line_color, 
                        label=node_i_label)

                # plot comparison data if provided
                if self.compare_data_index is not None:
                    if data_index==self.compare_data_index:
                        if self.compare_data:
                            compare_label="Comparison Data"
                            plot.plot(
                                self.compare_column_x, 
                                self.compare_column_y, 
                                label=compare_label
                                )
                            plot.legend()
                            
                # add plot to gui tab
                self.add_plot_to_new_tab(("Fig_"+str(self.fig_num)), fig)

                # save figure
                
                # fig.savefig(self.path_to_output_figures+'/'+self.title_list[output_variable]+'_fig_num_'+str(self.fig_num)+'_node_'+str(spatial_point)+'.png', bbox_inches='tight')
                
                fig.savefig(self.path_to_output_figures+'/'+'spatial_separate'+'_fig_num_'+str(self.fig_num)+'_node_'+str(spatial_point)+'.png', bbox_inches='tight')
                self.fig_num+=1
   
    def plot_reg_csv(self):
        self.fig_num=1
        for output_variable in range(0, self.number_of_output_variables):
            # update data scaling
            data_index = self.x_axis_data_dict[self.out_label_list[output_variable]][0]
            scale_factor = self.x_axis_data_dict[self.out_label_list[output_variable]][1]

            # print(
            #     f"output_variable:{output_variable}"
            #     f"self.title_list[output_variable]: {self.title_list[output_variable]}"
            # )
            
            # initializing figure
            fig = Figure()
            xyz_coor = self.format_coordinates(self.x_sorted[output_variable][0],self.y_sorted[output_variable][0],self.z_sorted[output_variable][0])
            title_str = f"Data set: {self.title_list[output_variable]} \nCoordinate: {xyz_coor} "
            
            plot = fig.add_subplot(111, title=title_str, xlabel=self.x_label, ylabel=self.out_label_list[output_variable])
            plot.plot(
                self.x_data, 
                self.output_sorted[output_variable], 
                color=self.line_color,
                label=self.pegasus_out_label)

            # plot comparison data if provided
            # if self.compare_data_index is not None:
            #     if data_index==self.compare_data_index:
            #         if self.compare_data:
            #             compare_label="Comparison Data"
            #             plot.plot(
            #                 self.compare_column_x, 
            #                 self.compare_column_y,
            #                 label=self.comparison_label,
            #                 **self.compare_data_style
            #                 )
            #             plot.legend()
            for key in self.compare_data_dict:
                # check if compare data index is same as pegasus and user did want to compare
                if data_index==self.compare_data_dict[key][0] and self.compare_data_dict[key][1]:
                    compare_label="Comparison Data"
                    plot.plot(
                        self.compare_data_dict[key][2][0],
                        self.compare_data_dict[key][2][1], 
                        label=self.comparison_label,
                        **self.compare_data_style
                        )
                    plot.legend()
                    # print(
                    #     f"data_index: {data_index}\n"
                    #     f"self.compare_data_dict[key][0]: {self.compare_data_dict[key][0]}\n"
                    #     f"(x0,y0): ({self.compare_data_dict[key][2][0][0]}, {self.compare_data_dict[key][2][1][0]})\n"
                    # )

            # add plot to gui tab
            self.add_plot_to_new_tab(("Fig_"+str(self.fig_num)), fig)
            
            # save figure
            fig.savefig(self.path_to_output_figures+'/'+'figure_'+str(self.fig_num)+'.png', bbox_inches='tight')
            self.fig_num += 1

    def plot_nodes(self):
        self.fig_nodes = Figure()
        self.fig_nodes.tight_layout()
        title_str = "Use Node ID Definition Below For Plotting"
        x_label_str = "x coordinate (m)"
        y_label_str = "y coordinate (m)"
        z_label_str = "z coordinate (m)"
        # plot = self.fig_nodes.add_subplot(111, title=title_str, xlabel=x_label_str, ylabel=y_label_str, aspect="equal")
        plot = self.fig_nodes.add_subplot(111, projection='3d', title=title_str, xlabel=x_label_str, ylabel=y_label_str)
        node_num = 1 + self.node_id_offset
        for spline in range(0, self.number_of_splines):
            plot.plot(
                self.x_sorted[spline], 
                self.y_sorted[spline], 
                self.z_sorted[spline], 
                color=self.line_color)
            plot.scatter(
                self.x_sorted[spline], 
                self.y_sorted[spline],  
                self.z_sorted[spline],
                s=100, 
                color=self.line_color_opp)
            for point in range(0, self.num_spatial_points):
                node_i_label = "Node ID: " + str(node_num)
                plot.text(
                     self.x_sorted[spline][point], 
                     self.y_sorted[spline][point], 
                     self.z_sorted[spline][point],
                     node_i_label)
                node_num+=1

    def add_plot_to_new_tab(self, tab_name_text, figure_name, notebook_to_put=None):
        if notebook_to_put is None:
            tab = ttk.Frame(self.notebook_data)
            self.notebook_data.add(tab, text=tab_name_text)
        else:
            tab = ttk.Frame(notebook_to_put)
            notebook_to_put.add(tab, text=tab_name_text)
        canvas = FigureCanvasTkAgg(figure_name, master=tab)
        canvas.draw()
        canvas.get_tk_widget().pack()

    def analyze_data(self):
        if self.csv_or_xlsx=="CSV":
            self.convert_to_xlsx()
        self.get_tabular_data()
    
    def create_plots(self):
        # continue_plotting = True
        # while continue_plotting:
        if not self.reg_csv_files:
            if len(self.point_selection) == 1:
                self.plot_spatial_separate(point_select=self.point_selection)
                # print("Here1")
            else:
                # print("Here2")
                if self.point_overlay_input == "T":
                    self.plot_spatial_together(point_select=self.point_selection)
                elif self.point_overlay_input == "S":
                    self.plot_spatial_separate(point_select=self.point_selection)
        else:
            self.plot_reg_csv()
            # self.continue_plotting_input = "N"
            # if self.continue_plotting_input != "Y":
            #     continue_plotting = False

class GraphAnimation:
    def __init__(self):
        super(GraphAnimation, self).__init__()
        self.movie_path = f'/home/projects/griffinpy/test_anim.mp4'
        # print("anim init")


    def animate_all(self, i):
        self.ax_anim.cla() # clear the previous image
        for output_variable in range(0, self.number_of_output_variables):
            for spatial_point in self.point_selection:
                node_i_label = "Node ID: "+str(spatial_point)
                try:
                    x_data = self.x_data[spatial_point-1-self.node_id_offset:i]
                    y_data = self.output_sorted[output_variable][spatial_point-1:i]
                except:
                    x_data = self.x_data[:i]
                    y_data = self.output_sorted[output_variable][spatial_point-1:i]
                self.ax_anim.plot(x_data, y_data)
                self.ax_anim.set_xlim([np.min(x_data), np.max(x_data)]) # fix the x axis
                self.ax_anim.set_ylim([1.1*np.min(y_data), 1.1*np.max(y_data)]) # fix the y axis
            self.ax_anim.legend()


    def animate(self, i):
        try:
            self.ax_anim.cla() # clear the previous image
            for output_variable in range(0, self.number_of_output_variables):
                try:
                    x_data = self.x_data[:i]
                    y_data = self.output_sorted[output_variable][0][:i]
                except:
                    x_data = self.x_data[:i]
                    y_data = self.output_sorted[output_variable][0][:i]
                self.ax_anim.plot(x_data, y_data)
                self.ax_anim.set_xlim([np.min(x_data), np.max(x_data)]) # fix the x axis
                self.ax_anim.set_ylim([1.1*np.min(y_data), 1.1*np.max(y_data)]) # fix the y axis
                # self.ax_anim.legend()
        except ValueError:
            pass


    def create_animation(self):
        self.fig_anim, self.ax_anim = plt.subplots()
        # print(
        #         f"self.x_data: {self.x_data}"
        #         f"self.output_sorted[0][0]: {self.output_sorted[0][0][0:5]}"
        #     )
        self.anim = animation.FuncAnimation(self.fig_anim, self.animate, interval=1)
        self.anim.save(
            self.movie_path,
            writer='ffmpeg',
            fps=30)
        print(
            f"saving movie to: {self.movie_path}"
        )

class PictureToMovie:
    def __init__(self):
        super(PictureToMovie, self).__init__()
        self.picture_directory = "test"
        self.movie_length = 1 #input("Enter Length of Movie (seconds): ")
        self.movie_name = "test" #input("Enter Name of Movie: ")
        # print("pic init")

    def movie_maker(self):
    
        # Setting frame rate
        fps = 30

        # Get all the pictures from specified directory
        file_list = glob.glob(f'{self.picture_directory}/*.png')
        number_of_pics = float(len(file_list))

        image_duration = float(self.movie_length) / number_of_pics

        # Sort photos by time stamp
        # file_list.sort(key=os.path.getmtime)
        file_list.sort(key = lambda x: os.path.getmtime(x))

        # Setting duration for each picture
        clips = [ImageClip(m).set_duration(image_duration)
                for m in file_list]

        # Concatenate all pictures together
        concat_clip = concatenate_videoclips(clips, method="compose")
        # print(type(concat_clip))
        # test = mpimg.imread(concat_clip)

        # Create final video and specify name and location
        concat_clip.write_videofile(f'{self.path_to_output_movies}/{self.movie_name}', fps=fps)
        video_name = self.path_to_output_movies+'/'+self.movie_name #This is your video file path

        self.add_movie_to_new_tab(
            tab_name_text="movie_test",
            video_name=video_name,
            notebook_to_put=self.frame_notebook_animations_movie
        )
       
        
    def add_movie_to_new_tab(self, tab_name_text, video_name, notebook_to_put=None):
        self.videoplayer = TkinterVideo(notebook_to_put, scaled=True, keep_aspect=True)
        self.videoplayer.load(video_name)
        self.videoplayer.pack(expand=True, fill="both")
        # contour movie controls
        self.playbtn = ttk.Button(notebook_to_put, text="Pause", command=self.play_pause)
        self.playbtn.pack(side=BOTTOM, pady=5, padx=5)
        self.text_make_movie.set(self.text_create_movie)
        self.videoplayer.play()
        

    def play_pause(self):
        """ pauses and plays """
        if self.videoplayer.is_paused():
            self.videoplayer.play()
            self.playbtn["text"] = "Pause"

        else:
            self.videoplayer.pause()
            self.playbtn["text"] = "Play"


def plot_data_example():
    try:
        my_spatial_plot = SpatialPlotting()
        my_spatial_plot.create_all_widgets()
    except KeyboardInterrupt:
        print("\nExiting plotting tool")
        my_spatial_plot.window.destroy()

if __name__ == '__main__':
    plot_data_example()
