from parameters import Parameters


class RetrieveTool:
    """
    
    This simple class contains all the methods for retrieving tabular data and creating database files.

    """
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)
        self.parameters.create_params_retrieve()
        self.prefix_attribute = "attribute"
        self.prefix_elemental = "element"
        self.prefix_nodal = "node"
        self.parameters.create_material_associations_dict()
      
    def create_retrieve_poly(self, material_name, state_var, capture_data, poly_name, out_name):
        """
        
        Method for creating a retrieve for a single polyline.

        :param material_name: name (alias) of material to retrieve data for
        :param state_var: name of state variable to retrieve
        :param capture_data: capture data set to retrieve data from
        :param poly_name: name of polyline where to retrieve data from
        :param out_name: name of output .csv file
        :return psf_string: psf formatted string that creates a single retrieve command
        """
        psf_string = (
            f"RETRIEVE \\ \n"
            f"  {material_name.upper()} \\ \n" 
            f"  {state_var.upper()} \\ \n"
            f"  {capture_data} \n"
            f"  ALL_CAPTURE_TIMES \n"
            f"  {poly_name} \n"
            f"  R \n"
            f"  CSV \n"
            f'  "{out_name}" \n'
            f" \n")
        return psf_string

    def create_retrieve_single_coor(self, material_name, state_var, capture_data, coordinate, out_name):
        """
        
        Method for creating a retrieve for a single coordinate.

        :param material_name: name (alias) of material to retrieve data for
        :param state_var: name of state variable to retrieve
        :param capture_data: capture data set to retrieve data from
        :param coordinate: coordinate to retrieve data from ex. (`[x,y,z]`)
        :param out_name: name of output .csv file
        :return psf_string: psf formatted string that create a retrieve command
        """
        x = coordinate[0]
        y = coordinate[1]
        z = coordinate[2]
        psf_string = (
            f"RETRIEVE \\ \n"
            f"  {material_name.upper()} \\ \n" 
            f"  {state_var.upper()} \\ \n"
            f"  {capture_data} \\ \n"
            f"  ALL_CAPTURE_TIMES \\ \n"
            f"  [[{x} , {y}, {z}]] \\ \n"
            f"  NSPF \\ \n"
            f"  CSV \\ \n"
            f'  "{out_name}" \n'
            f" \n")
        return psf_string
    
    def create_database(self, db_name="my_db"):
        """
        
        Method for creating a pegasus database file.

        :param db_name: name to give new database file
        :return psf_string: psf formatted string that creates a database
        """
        db_name = self.parameters.file_database_name.upper()
        psf_string = (
            f"OPEN NEW PEGASUS_DB \\ \n"
            f"  {db_name} \\ \n" 
            f'  "../+{self.parameters.file_pegasus_output_database_relative_location}" \\ \n'
            f"  OVERWRITE \n"
            f" \n"
            f"SAVE {{{self.parameters.physical_mesh_name}}} TO {db_name} \n"
            f"CLOSE PEGASUS_DB {db_name} \n"
            f" \n")
        return psf_string

    def create_retrieve_from_dict(self, material_ret_dict):
        """
        
        Method for creating multiple retrieve commands from a dictionary in the form
            key: ex. (`"MATERIAL_CLADDING"`)
            value: `["GenericZircNonfuel", "TEMPERATURE", [x,y,z]]`

        :param material_ret_dict: keys-> material alias name and values -> list of: material class, state variable and coordinate
        :return psf_string_list: psf formatted list of strings that creates all user requested retrieve commands
        """
        
        requested_retrieve_dict = self.parameters.material_associations_dict
        psf_string_list = []
        
        for key in requested_retrieve_dict:
            mat_alias_name = key
            value_list = requested_retrieve_dict[mat_alias_name]
            mat_class_name = value_list[0]
            state_vars = value_list[1]
            coordinates = value_list[2][0]
            for a_state_var in state_vars:
                try:
                    if material_ret_dict[a_state_var.upper()]==self.prefix_elemental:
                        capture_data=self.parameters.cd_elements_all_name
                    elif material_ret_dict[a_state_var.upper()]==self.prefix_nodal:
                        capture_data=self.parameters.cd_nodes_all_name
                except TypeError:
                    return
                
                count=1
                for a_coordinate in coordinates:
                    out_name = ("../"+self.parameters.file_pegasus_output_tabular_data_relative_location+"/"+mat_alias_name+"_"+a_state_var+"_"+str(count)+".csv").lower()
                    a_single_retrieve = self.create_retrieve_single_coor(
                        mat_alias_name, 
                        a_state_var, 
                        capture_data, 
                        a_coordinate, 
                        out_name)
                    psf_string_list.extend([a_single_retrieve])
                    count+=1
        return psf_string_list
