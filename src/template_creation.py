import os
import yaml
from parameters import Parameters
from tools import FileTool
from meshing import MeshTool
from boundary_conditions import BoundaryConditionTool
from materials import RodMaterialsTool
from retrieve import RetrieveTool
from assignments import AssignmentsTool
from simulation_control import SimulationControlTool


class Template:
    """

    """
    def __init__(self, input_file_path=None):
        # initialize variables
        self.bc_pressure = None
        self.bc_heat_flux = None
        self.bc_temperature_cladding = None
        self.bc_temperature_plenum = None
        self.power_rod_height = None
        self.template_mesh = None
        self.create_bc_sets = None
        self.assign_bcs = None
        self.assign_mats = None
        

        # parameters
        self.parameters = Parameters(input_file_path)
        # self.parameters.print_atts()
        
        # meshing tools
        self.mesh_tool = MeshTool(input_file_path)

        # material tools
        self.material_tool = RodMaterialsTool(input_file_path)

        # boundary condition tools
        self.bc_tool = BoundaryConditionTool(input_file_path)
        
        # assign tool
        self.assign_tool = AssignmentsTool(input_file_path)
        
        # simulation control tools
        self.sim_ctrl_tool = SimulationControlTool(input_file_path)

        # retrieval tools
        self.retrieval_tool = RetrieveTool(input_file_path)

        # file tools
        self.file_tool = FileTool(input_file_path)
        
        # Headers for PSF
        self.prefix_header = "header"
        self.prefix_headersub1 = "headersub1"
        self.header_params_str = "Model Parameters"
        self.headersub1_params_geo_str = "Model Geometry"
        self.headersub1_params_res_str = "Model Resolution"
        self.headersub1_params_mat_str = "Material parameters"
        self.headersub1_params_inv_str = "Inventory parameters"
        self.headersub1_params_bc_str = "Boundary Condition parameters"
        self.header_mesh_str = "Meshing"
        self.headersub1_mesh_model_str = "Create Mesh"
        self.headersub1_mesh_phymesh_str = "Create Physical Mesh"
        self.header_mat_str = "Materials"
        self.header_elenodepoly_str = "Element/node sets and Polylines"
        self.headersub1_elenodepoly_all_str = "All nodes/elements"
        self.headersub1_elenodepoly_mats_str = "Material elements"
        self.headersub1_elenodepoly_bc_str = "Boundary condition elements"
        self.header_bc_str = "Boundary Conditions"
        self.headersub1_bc_gravity_str = "Gravity"
        self.headersub1_bc_fixed_str = "Fixed"
        self.headersub1_bc_pressure_str = "Pressure"
        self.headersub1_bc_heat_str = "Heat"
        self.headersub1_bc_power_str = "Power"
        self.headersub1_bc_gio_str = "Gas inventory object"
        self.headersub1_bc_fio_str = "Fuel inventory object"
        self.header_assign_str = "Assign materials & boundary conditions to Nodes/Elements"
        self.headersub1_assign_mat_str = "Materials"
        self.headersub1_assign_bc_str = "Boundary/Initial Conditions"
        self.header_simctrl_str = "Setting simulation solution parameters"
        self.headersub1_simctrl_contact_str = "Mechanical Contact"
        self.headersub1_simctrl_conv_str = "Convergence Tolerances"
        self.headersub1_simctrl_scp_str = "Solution Control Parameters"
        self.headersub1_simctrl_init_str = "Initialize Simulation"
        self.headersub1_simctrl_cd_str = "Capture Data"
        self.header_walk_str = "Commence with simulation"
        self.header_database_str = "Database"
        self.header_retrieve_str = "Data retrieval"
        self.level_header_0 = 0
        self.level_header_1 = 1
        self.parameters.level_header_2 = 2
        self.level_header_3 = 3
        self.model_flag_rod = False
        self.model_flag_triso = False
        self.new_line = "\n"
        self.model_flag_detailed_rod = False
        self.parameters.create_directory_template()
        self.determine_problem_type()
        print("\nParameters updated and directories created.\n")

    def determine_problem_type(self):
        """

        """
        # Model dimension and name
        if self.parameters.model_flag_dimmension==self.parameters.geometry_dimmension_2:
            self.bc_boundary_type = self.parameters.bc_boundary_type_segment
            self.template_mesh_name = self.parameters.qp_rod_name
        elif self.parameters.model_flag_dimmension==self.parameters.geometry_dimmension_3:
            self.bc_boundary_type = self.parameters.bc_boundary_type_facet
            self.template_mesh_name = self.parameters.hex_rod_name
        
        # Model geometry / Assigns
        if self.parameters.psf_type_template:
            # 2D mesh checking 
            if self.parameters.model_flag_rod_rz_simple:
                self.model_flag_rod = True
                self.template_mesh = self.mesh_tool.mesh_rz_2d_rod_tool.create_simple_smeared_rod()
                self.create_bc_sets = self.mesh_tool.mesh_rz_2d_rod_tool.create_bc_sets_for_2drz_rod()
                self.assign_bcs = self.assign_tool.assign_2drz_rod_bc()
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared
            
            elif self.parameters.model_flag_rod_rz_detailed_smeared:
                self.model_flag_rod = True
                self.model_flag_detailed_rod = True
                self.template_mesh = self.mesh_tool.mesh_rz_2d_rod_tool.create_detailed_smeared_rod()
                self.create_bc_sets = self.mesh_tool.mesh_rz_2d_rod_tool.create_bc_sets_for_detailed_2drz_rod()
                self.assign_bcs = self.assign_tool.assign_2drz_rod_bc(model_flag_detailed=self.model_flag_detailed_rod)
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared
            
            elif self.parameters.model_flag_rod_r_theta_simple:
                self.model_flag_rod = True
                self.template_mesh = self.mesh_tool.mesh_rtheta_2d_rod_tool.create_rod_disk()
                self.create_bc_sets = self.mesh_tool.mesh_rtheta_2d_rod_tool.create_bc_sets_for_2d_rtheta_rod()
                self.assign_bcs = self.assign_tool.assign_2drtheta_rod_bc()
                self.power_rod_height = 1.0
            
            elif self.parameters.model_flag_rod_r_theta_detailed_mps:
                self.model_flag_rod = True
                self.template_mesh = self.mesh_tool.mesh_rtheta_2d_rod_tool.create_mps()
                self.create_bc_sets = self.mesh_tool.mesh_rtheta_2d_rod_tool.create_bc_sets_for_2d_rtheta_rod()
                self.assign_bcs = self.assign_tool.assign_2drtheta_rod_bc()
                self.power_rod_height = 1.0

            # 3D mesh checking
            elif self.parameters.model_flag_rod_r_theta_z_simple:
                self.model_flag_rod = True
                self.model_flag_detailed_rod = True
                self.template_mesh = self.mesh_tool.mesh_rtheta_3d_rod_tool.create_simple_smeared_rod()
                self.create_bc_sets = self.mesh_tool.mesh_rtheta_3d_rod_tool.create_bc_sets_for_simple_3d_rtheta_rod()
                self.assign_bcs = self.assign_tool.assign_3drtheta_rod_bc()
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared
            
            elif self.parameters.model_flag_rod_r_theta_z_detailed_smeared:
                self.model_flag_rod = True
                self.model_flag_detailed_rod = True
                self.template_mesh = self.mesh_tool.mesh_rtheta_3d_rod_tool.create_detailed_smeared_rod()
                self.create_bc_sets = self.mesh_tool.mesh_rtheta_3d_rod_tool.create_bc_sets_for_detailed_3d_rtheta_rod()
                self.assign_bcs = self.assign_tool.assign_3drtheta_rod_bc(model_flag_detailed=self.model_flag_detailed_rod)
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared

            # triso mesh checking
            elif self.parameters.model_flag_triso_particle:
                self.template_mesh = self.mesh_tool.mesh_triso_tool.create_triso_particle()
                self.create_bc_sets = self.mesh_tool.mesh_triso_tool.create_bc_sets_for_triso_particle()
                self.assign_bcs = self.assign_tool.assign_triso_particle_bc()
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared
                self.model_flag_triso = True
            
            elif self.parameters.model_flag_triso_pellet:
                self.template_mesh = self.mesh_tool.mesh_triso_tool.create_triso_compact()
                self.create_bc_sets = self.mesh_tool.mesh_triso_tool.create_bc_sets_for_triso_particle()
                self.assign_bcs = self.assign_tool.assign_triso_particle_bc()
                self.power_rod_height = self.parameters.geometry_fuel_height_smeared
                self.model_flag_triso = True
        else:
            self.template_mesh = None
            self.create_bc_sets = None
            self.assign_bcs = None
            self.assign_mats = None
            self.power_rod_height = self.parameters.geometry_fuel_height_smeared

        self.mesh_tool.get_regions_dict()
        
        # Mechanical contact
        if self.parameters.model_flag_enable_contact:
            self.set_contact = self.sim_ctrl_tool.ccp_tool.create_solution_params()
        else:
            self.set_contact = None

        # Materials
        self.determine_materials()

        # Boundary conditions
        self.determine_boundary_conditions()

    def determine_boundary_conditions(self):
        """

        """
        ## Create GTD files
        if self.parameters.bc_flag_gtd_create:
            self.file_tool.convert_xlsx_to_gtd()
        
        ## Gravity
        self.bc_gravity = self.parameters.bc_flag_function_gravity

        ## Pressure
        if self.parameters.bc_flag_function_pressure:
            self.bc_pressure = self.bc_tool.bc.create_pressure(self.bc_boundary_type)
        elif self.parameters.bc_flag_gtd_pressure:
            self.bc_pressure = self.bc_tool.bc.create_pressure_from_gtd(
                self.bc_boundary_type,
                self.parameters.bc_interpolation_piecewise_linear)
        else:
            self.bc_pressure = None
        
        ## Heat Flux
        if self.parameters.bc_flag_function_heat_flux:
            self.bc_heat_flux = self.bc_tool.bc.create_heat_flux(self.bc_boundary_type)
        elif self.parameters.bc_flag_gtd_heat_flux:
            self.bc_heat_flux = self.bc_tool.bc.create_heat_flux_from_gtd(
                self.bc_boundary_type,
                self.parameters.bc_interpolation_piecewise_linear)
        else:
            self.bc_heat_flux = None

        ## Cladding Temperature
        if self.parameters.bc_flag_function_temperature_cladding:
            self.bc_temperature_cladding = self.bc_tool.bc.create_temperature()
        elif self.parameters.bc_flag_gtd_temperature_cladding:
            self.bc_temperature_cladding = self.bc_tool.bc.create_temperature_from_gtd(self.parameters.bc_interpolation_piecewise_linear)
        else:
            self.bc_temperature_cladding = None

        ## Plenum Temperature
        if self.parameters.bc_flag_function_temperature_plenum:
            self.bc_temperature_plenum = self.bc_tool.bc.create_gio_temperature()
        elif self.parameters.bc_flag_gtd_temperature_plenum:
            self.bc_temperature_plenum = self.bc_tool.bc.create_gio_temperature_from_gtd(self.parameters.bc_interpolation_piecewise_linear)
        else:
            self.bc_temperature_plenum = None
        
        ## Power
        if self.parameters.bc_flag_function_power:
            self.bc_power = self.bc_tool.bc.create_power(
                height_of_active_rod=self.power_rod_height
            )
        elif self.parameters.bc_flag_gtd_power:
            self.bc_power = self.bc_tool.bc.create_power_from_gtd(
                self.parameters.bc_interpolation_piecewise_linear,
                height_of_active_rod=self.power_rod_height)
        else:
            self.bc_power = None

        ## Inventory objects
        self.bc_gio = self.parameters.bc_flag_inventory_gas
        self.bc_fio = self.parameters.bc_flag_inventory_fuel

    def determine_materials(self):
        """

        """
        # this will store the input variables for all materials
        self.materials_all_real_string_list = []

        # this will store the actual material creation commands
        self.materials_all_string_list = []

        # keep track of material objects to be able to retrieve state variables for them.
        # the key will be the materials alias and the value will be the material object
        self.materials_all_object_dict = {}

        ################################# Rod type #################################
        if self.model_flag_rod:
            ## Fuel
            self.material_fuel, self.material_fuel_reals = self.material_tool.determine_create_material(
                material_class_name=self.parameters.material_fuel_class, 
                material_alias=self.parameters.material_alias_name_fuel)
            
            # Material values (reals) 
            self.materials_all_real_string_list.extend(self.material_fuel_reals)
            # Actual materials    
            self.materials_all_string_list.extend(self.material_fuel)
            # material object storage
            self.materials_all_object_dict[self.parameters.material_alias_name_fuel] = self.parameters.material_fuel_class
            
            ## Gap
            self.material_fuel_cladding_gap, self.material_fuel_cladding_gap_reals = self.material_tool.determine_create_material(
                material_class_name=self.parameters.material_fuel_cladding_gap_class, 
                material_alias=self.parameters.material_alias_name_fuel_cladding_gap)

            # Material values (reals) 
            self.materials_all_real_string_list.extend(self.material_fuel_cladding_gap_reals)
            # Actual materials
            self.materials_all_string_list.extend(self.material_fuel_cladding_gap)
            # material object storage
            self.materials_all_object_dict[self.parameters.material_alias_name_fuel_cladding_gap] = self.parameters.material_fuel_cladding_gap_class
            
            ## Cladding
            if len(self.parameters.material_cladding_list_class) > 1:
                self.material_cladding = []
                self.material_cladding_reals = []
                num_cladding_layers = len(self.parameters.material_cladding_list_class)
                # print(
                #     f"cladding list: {self.parameters.material_cladding_list_class}"
                # )
                for layer in range(0, num_cladding_layers):
                    mat_index = "_"+str(layer+1)
                    material_cladding, material_cladding_reals = self.material_tool.determine_create_material(
                        material_class_name=self.parameters.material_cladding_list_class[layer], 
                        material_alias=self.parameters.material_alias_name_cladding_list+mat_index,
                        material_index=mat_index)
                    self.material_cladding.extend(material_cladding)
                    self.material_cladding_reals.extend(material_cladding_reals)

                    # material object storage
                    self.materials_all_object_dict[self.parameters.material_alias_name_cladding_list+mat_index] = self.parameters.material_cladding_list_class[layer]

            else:
                self.material_cladding, self.material_cladding_reals = self.material_tool.determine_create_material(
                    layer = 0,
                    mat_index = "_"+str(layer+1),
                    material_class_name=self.parameters.material_cladding_list_class[layer], 
                    material_alias=self.parameters.material_alias_name_cladding_list+mat_index)
            
                # material object storage
                self.materials_all_object_dict[self.parameters.material_alias_name_cladding_list+mat_index] = self.parameters.material_cladding_list_class[layer]    

            # Material values (reals) 
            self.materials_all_real_string_list.extend(self.material_cladding_reals) 
            # Actual materials
            self.materials_all_string_list.extend(self.material_cladding) 

            if self.model_flag_detailed_rod:
                # plenum
                self.material_plenum, self.material_plenum_reals = self.material_tool.determine_create_material(
                    material_class_name=self.parameters.material_plenum_class, 
                    material_alias=self.parameters.material_alias_name_plenum)
                
                # Material values (reals) 
                self.materials_all_real_string_list.extend(self.material_plenum_reals) 
                # Actual materials
                self.materials_all_string_list.extend(self.material_plenum) 
                # material object storage
                self.materials_all_object_dict[self.parameters.material_alias_name_plenum] = self.parameters.material_plenum_class
                
                # end cap
                self.material_end_cap, self.material_end_cap_reals = self.material_tool.determine_create_material(
                    material_class_name=self.parameters.material_end_cap_class, 
                    material_alias=self.parameters.material_alias_name_end_cap)
                
                # Material values (reals) 
                self.materials_all_real_string_list.extend(self.material_end_cap_reals) 
                # Actual materials
                self.materials_all_string_list.extend(self.material_end_cap) 
                # material object storage
                self.materials_all_object_dict[self.parameters.material_alias_name_end_cap] = self.parameters.material_end_cap_class
              

            # Material sets
            self.mesh_material_sets = self.mesh_tool.create_material_sets_for_rod(self.mesh_tool.dict_regions)
            self.assign_mats = self.assign_tool.assign_rod_materials(self.mesh_tool.dict_regions)
        
        ################################# Triso type #################################   
        ## UCO 
        ## Buffer
        ## Inner PyC
        ## SiC
        ## Outer PyC
        
        elif self.model_flag_triso:
            self.material_triso = []
            self.material_triso_reals = []
            num_layers = len(self.parameters.material_triso_list_class)
            for layer in range(0, num_layers):
                mat_index = "_"+str(layer)
                material_triso, material_triso_reals = self.material_tool.determine_create_material(
                    material_class_name=self.parameters.material_triso_list_class[layer], 
                    material_alias=self.parameters.material_alias_name_triso_list+mat_index,
                    material_index=mat_index)
                self.material_triso.extend(material_triso)
                self.material_triso_reals.extend(material_triso_reals)
            
            # Material values (reals) 
            self.materials_all_real_string_list.extend(self.material_triso_reals)
            
            # Actual materials    
            self.materials_all_string_list.extend(self.material_triso)   

            # Material sets
            self.mesh_material_sets = self.mesh_tool.create_material_sets_for_triso_particle(self.mesh_tool.dict_regions)
            self.assign_mats = self.assign_tool.assign_triso_materials(self.mesh_tool.dict_regions)

    def create_dynamic_toc(self):
        """

        :return:
        """
        params_dict = vars(self)
        toc_dict = {}
        count_toc = 1
        toc_string = []
        for key in params_dict:
            prefix_key = key.lower().split('_', 1)[0]
            if prefix_key == self.prefix_header:
                toc_dict[key] = [count_toc]
                count_toc+=1

        for key in params_dict:
            prefix_key = key.lower().split('_', 1)[0]
            if prefix_key == self.prefix_headersub1:
                section_subkey = key.lower().split('_', 2)[1]
                header_str = self.prefix_header+"_"+key.lower().split('_', 2)[1]+"_str"
                toc_dict[header_str].append([key, params_dict[key]])

        # print(f"create toc\n")
        template_dict = {}
        
        for key in toc_dict:
            value_list = toc_dict[key]
            value_list_len = len(value_list)
            section_str_list = []
            section_header_str = str(value_list[0])+". "+params_dict[key]
            # main section
            toc_string.extend(
                f"# {section_header_str} \n"
                f"# \n"
            )
            # updating main section index number
            setattr(self, key.lower(), section_header_str)

            section_str_list.append([section_header_str])
            
            # loop over list to set/update sections
            if value_list_len > 1:
                section_single_str_list = []
                for item in range(1, value_list_len):
                    section_single_str = str(value_list[0])+"."+str(item)+". "+value_list[item][1]
                    # sub section
                    toc_string.extend(
                        f"#     {section_single_str} \n"
                        f"# \n"
                    )
                    # updating sub section index number
                    setattr(self, value_list[item][0], section_single_str)
                    section_single_str_list.append([section_single_str])
                section_str_list.append(section_single_str_list)
            
            template_dict[key] = section_str_list

            # print(
            #     f"{key}: {template_dict[key]}"
            # )

        
        self.toc_dict = template_dict
        return toc_string

    def create_table_of_contents(self):
        """

        :return:
        """
        self.params_dict = vars(self)
        toc_string = []
        toc_post_string = []
        header_str = self.parameters.create_header_comment("Model Explaination")
        toc_header_str = self.parameters.create_header_comment("Model Construction Plan")
        toc_pre_string = (
            f"{header_str}"
            f"{toc_header_str}")
        new_line = f"\n"
        
        toc_post_string = self.create_dynamic_toc()
                
        toc_string.extend(toc_pre_string)
        toc_string.extend(toc_post_string)
        toc_string.extend(new_line)
        
        return toc_string
    
    def create_empty_body_template(self):
        """

        :return:
        """
        template_string = []
        for key in self.toc_dict:
            template_string.extend(self.parameters.create_header_comment(self.toc_dict[key][0][0]))
            # print(
            #     f"test      {key}: {self.toc_dict[key][0][0]}"
            # )
            if len(self.toc_dict[key]) > 1:
                for value in range(len(self.toc_dict[key][1])):
                    template_string.append(self.parameters.create_header_comment(self.toc_dict[key][1][value][0], level=self.parameters.level_header_2))
                #     print(
                #     f"{key}: {self.toc_dict[key][1][value][0]}"
                # )
        return template_string
    
    def create_psf(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.create_table_of_contents())
        
        # If using template, then build its corresponding PSF
        if self.parameters.psf_type_template:
            template_string.extend(self.create_section_params())
            template_string.extend(self.create_section_meshing())
            
            if not self.parameters.psf_type_mesh_only:
                template_string.extend(self.create_section_materials())
                template_string.extend(self.create_section_ele_node_poly())

                template_string.extend(self.create_section_bc())
                template_string.extend(self.create_section_assign())
                template_string.extend(self.create_section_sim_params())
                
                template_string.extend(self.create_section_walk())
                template_string.extend(self.create_section_database())
                template_string.extend(self.create_section_retrieve())

        # Else, create empty template 
        elif not self.parameters.psf_type_template:
            template_string.extend(self.create_empty_body_template())
            print(
                f"should make empty body"
            )
        # print(
        #     f"template_string: {template_string}"
        # )
        return template_string
         
    def create_section_params(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_params_str))
        
        # Model Geometry Parameters
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_geo_str,level=self.parameters.level_header_2))
        template_string.extend(self.parameters.create_params_geometry())
        
        # Model Resolution Parameters
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_res_str,level=self.parameters.level_header_2))
        template_string.extend(self.parameters.create_params_resolution())
        
        # Create Remaining PSF Parameters
        if not self.parameters.psf_type_mesh_only:
            # Materials
            template_string.extend(self.parameters.create_header_comment(self.headersub1_params_mat_str,level=self.parameters.level_header_2))
            template_string.extend(self.materials_all_real_string_list)

            # Inventory objects
            if self.bc_gio or self.bc_fio:
                template_string.extend(self.parameters.create_header_comment(self.headersub1_params_inv_str,level=self.parameters.level_header_2))
            # Gas Inventory Object
            if self.bc_gio:
                template_string.extend(self.bc_tool.inventory_gas.create_reals())

            # Boundary Conditions
            template_string.extend(self.parameters.create_header_comment(self.headersub1_params_bc_str,level=self.parameters.level_header_2))
            template_string.extend(self.bc_tool.bc.create_reals())

        return template_string
    
    def create_section_meshing(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_mesh_str))
        
        # Create Mesh
        template_string.extend(self.parameters.create_header_comment(self.headersub1_mesh_model_str,level=self.parameters.level_header_2))
        template_string.extend(self.template_mesh)
        
        # Create Physical Mesh 
        template_string.extend(self.parameters.create_header_comment(self.headersub1_mesh_phymesh_str,level=self.parameters.level_header_2))
        template_string.extend(self.mesh_tool.create_phys_mesh(self.template_mesh_name))
        return template_string

    def create_section_materials(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_mat_str))
        template_string.extend(self.materials_all_string_list)
        return template_string
    
    def create_section_ele_node_poly(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_elenodepoly_str))

        # All
        template_string.extend(self.parameters.create_header_comment(self.headersub1_elenodepoly_all_str, level=self.parameters.level_header_2))
        template_string.extend(self.mesh_tool.create_all_elements_set())
        template_string.extend(self.mesh_tool.create_all_nodes_set())

        # Materials
        template_string.extend(self.parameters.create_header_comment(self.headersub1_elenodepoly_mats_str, level=self.parameters.level_header_2))
        template_string.extend(self.mesh_material_sets)
        
        # Boundary conditions
        template_string.extend(self.parameters.create_header_comment(self.headersub1_elenodepoly_bc_str, level=self.parameters.level_header_2))
        template_string.extend(self.create_bc_sets)
        return template_string
    
    def create_section_bc(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_bc_str))

        # Gravity
        if self.bc_gravity:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_gravity_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_tool.bc.create_gravity())
        
        # Fixed
        template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_fixed_str, level=self.parameters.level_header_2))
        template_string.extend(self.bc_tool.bc.create_fixed("x"))
        template_string.extend(self.bc_tool.bc.create_fixed("y"))
        template_string.extend(self.bc_tool.bc.create_fixed("z"))
        
        # Pressure
        if self.bc_pressure is not None:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_pressure_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_pressure)
        
        # Heat: Flux and/or Temperature
        if self.bc_heat_flux is not None:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_heat_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_heat_flux)
        if self.bc_temperature_cladding is not None:
            template_string.extend(self.bc_temperature_cladding)

        # Power
        if self.bc_power is not None:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_power_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_power)
        
        # Gas Inventory Object
        if self.bc_gio:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_gio_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_temperature_plenum)
            template_string.extend(self.bc_tool.inventory_gas.create_inventory_object())

        # Fuel Inventory Object
        if self.bc_fio:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_bc_fio_str, level=self.parameters.level_header_2))
            template_string.extend(self.bc_tool.inventory_fuel.create_inventory_object())
        
        return template_string
    
    def create_section_assign(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_assign_str))

        # materials
        template_string.extend(self.parameters.create_header_comment(self.headersub1_assign_mat_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.assign_mats)

        # init/bc conditions
        template_string.extend(self.parameters.create_header_comment(self.headersub1_assign_bc_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.assign_tool.assign_inital_temperature())  
        template_string.extend(self.assign_bcs)

        return template_string
    
    def create_section_sim_params(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_simctrl_str))

        # contact
        if self.set_contact is not None:
            template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_contact_str, level=self.parameters.level_header_2)) 
            template_string.extend(self.set_contact)

        # Convergence Tolerances 
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_conv_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sim_ctrl_tool.conv_tool.create_solution_params())
        
        # Solution Control Parameters 
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_scp_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sim_ctrl_tool.scp_tool.create_solution_params())
       
        # Init sim
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_init_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sim_ctrl_tool.init_tool.create_all_init_string())
 
        # Capture data
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_cd_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sim_ctrl_tool.cd_tool.create_default_capture_data())

        # Show ROD and Pause
        template_string.extend(self.sim_ctrl_tool.init_tool.create_hide_show())
        template_string.extend(self.sim_ctrl_tool.init_tool.create_pause())
        
        return template_string

    def create_section_walk(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_walk_str))
        if self.parameters.file_input_data_location is not None:
            template_string.extend(self.sim_ctrl_tool.walk_tool.walk_from_xlsx())
        else:
            template_string.extend(self.sim_ctrl_tool.walk_tool.walk_stable_start_sequence())
        return template_string

    def create_section_database(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_database_str))
        if self.parameters.file_create_database:
            template_string.extend(self.retrieval_tool.create_database())
        return template_string

    def create_section_retrieve(self):
        """

        :return:
        """
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_retrieve_str))
        for mat_alias in self.parameters.material_associations_dict:
            mat_class_name = self.parameters.material_associations_dict[mat_alias][0]
            ret_dict = self.material_tool.all_mat_dict[mat_class_name].create_material_state_var_dict()
            template_string.extend(self.retrieval_tool.create_retrieve_from_dict(ret_dict))
        return template_string        


if __name__ == "__main__":
    param_file_test = "/home/projects/griffinpy/scrip_gen_tests/input/generator_input.yaml"
    out_file_test = "/home/projects/griffinpy/scrip_gen_tests/output/testing_new.psf"
    psf_string_list = []
    upd_params = Template(param_file_test)
    # upd_params = Template()
    create_template_string_list = upd_params.create_empty_body_sub_template()
    psf_string_list.extend(create_template_string_list)

    f = open(f"{upd_params.parameters.file_pegasus_location}", "w+")
    for string in range(0, len(psf_string_list)):
        f.write(psf_string_list[string])
