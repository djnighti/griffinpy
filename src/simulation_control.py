import openpyxl
from parameters import Parameters
from tools import FileTool
import numpy as np


class SimulationControl:
    """

    This class helps organize and create all the various classes in `simulataion_control`

    """
    
    def __init__(self, input_file_path=None):
        self.parameters = Parameters(input_file_path)
        self.solution_control_conv_type = "convergence_tolerances"
        self.solution_control_scp_type = "solution_control_parameter"
        self.solution_control_ccp_type = "contact_control_parameter"
        self.prefix_property = "property"
        self.file_tool = FileTool(input_file_path)

    def create_solution_params(self):
        """
        
        This method creates the base string for any simulation control parameter.

        Supporting Methods Used:
            :method: `create_props_dict`
        :return output_string: psf formatted string for creating any solution control parameter
        """
        params_dict = vars(self)
        props_dict = self.create_props_dict()
        num_props = len(props_dict)
        output_string = []
        for key in props_dict:
            if self.solution_control_type == self.solution_control_conv_type:
                sc_string = self.create_conv_string(key, props_dict[key])
            elif self.solution_control_type == self.solution_control_scp_type:
                sc_string = self.create_scp_string(key, props_dict[key])
            elif self.solution_control_type == self.solution_control_ccp_type:
                sc_string = self.create_ccp_string(key, props_dict[key])
            output_string.extend(sc_string)
        return output_string
    
    def create_props_dict(self):
        """

        :return props_dict: dictionary with keys-> attribute name and values-> attribute value
        """
        params_dict = vars(self)
        props_dict = {}
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            if key_prefix == self.prefix_property:
                key_suffix = key.lower().split('_', 1)[1]
                props_dict[key_suffix] = params_dict[key]
        return props_dict

    def create_reals(self):
        """
        
        This method create the real strings for any float/intger value in `simulataion_control`
        
        Supporting Methods Used:
            :method: `parameters.create_real`
        :return create_params_string_list: psf formatted list of strings for creating all the real variables for sc
        """
        params_dict = vars(self)
        create_params_string_list = []
        create_an_real = []
        new_line = f"\n"
        for key in params_dict:
            key_prefix = key.lower().split('_', 1)[0]
            if key_prefix == self.prefix_property:
                key_suffix = key.lower().split('_', 1)[1]
                if type(params_dict[key])==float:
                    create_an_real, _ = self.parameters.create_real(key_suffix, params_dict[key])
                elif type(params_dict[key])==list:
                    for item in params_dict[key]:
                        create_an_real_itt, _ = self.parameters.create_real(key_suffix, params_dict[key][item])
                        create_an_real.extend(create_an_real_itt)
                create_params_string_list.extend(create_an_real)
                
        create_params_string_list.extend(new_line)
        return create_params_string_list

class ConvergenceTolerance(SimulationControl):
    """

    This class manages the convergence tolerance commands.
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)
        self.solution_control_type = "convergence_tolerances"
        self.property_deformable_quasistatic_finite_deformation = self.parameters.sol_ctrl_deformable_quasistatic_finite_deformation
        self.property_transient_thermal = self.parameters.sol_ctrl_transient_thermal

    def create_conv_string(self, key, value):
        """

        :param key: attribute name
        :param value: attribute value
        :return psf_string: psf formatted string for creating a convergence tolerance
        """
        psf_string = (
            f"SET CONVERGENCE_TOLERANCES \\ \n"
            f"  {key.upper()} \\ \n" 
            f"  {self.parameters.physical_mesh_name} \\ \n"
            f"  {value[0]} {value[1]} \n"
            f" \n")
        return psf_string

class MechanicalContact(SimulationControl):
    """

    This class manages the mechanical contact commands.
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)
        self.solution_control_type = "contact_control_parameter"
        self.property_friction_coefficient = self.parameters.sol_ctrl_contact_friction_coefficient
        self.property_maximum_interpenetration_distance = self.parameters.sol_ctrl_contact_maximum_interpenetration_distance
        self.property_maximum_gap_distance = self.parameters.sol_ctrl_contact_maximum_gap_distance
        self.property_maximum_slip_distance = self.parameters.sol_ctrl_contact_maximum_slip_distance
        self.property_thermal_conductivity_coefficients = self.parameters.sol_ctrl_contact_thermal_conductivity_coefficients

    def create_ccp_string(self, key, value):
        """

        :param key: attribute name
        :param value: attribute value
        :return psf_string: psf formatted string for creating a contact parameter
        """
        psf_string = (
            f"SET CONTACT_CONTROL_PARAMETER \\ \n"
            f"  {key.upper()} \\ \n" 
            f"  {self.parameters.physical_mesh_name} \\ \n"
            f"  {value} \n"
            f" \n")
        return psf_string

class SolutionControlParameter(SimulationControl):
    """

    This class manages all the simulation parameter commands.
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)
        self.solution_control_type = "solution_control_parameter"
        self.property_maximum_number_of_line_search_per_newton = self.parameters.sol_ctrl_maximum_number_of_line_search_per_newton
        self.property_maximum_number_of_newton_pass_per_step = self.parameters.sol_ctrl_maximum_number_of_newton_pass_per_step
        self.property_maximum_number_of_time_cuts = self.parameters.sol_ctrl_maximum_number_of_time_cuts
        self.property_time_cut_factor = self.parameters.sol_ctrl_time_cut_factor
        self.property_slow_convergence_residual_norm_ratio = self.parameters.sol_ctrl_slow_convergence_residual_norm_ratio
        self.property_fast_convergence_residual_norm_ratio = self.parameters.sol_ctrl_fast_convergence_residual_norm_ratio
        self.property_stab_heat_source_coeff = self.parameters.sol_ctrl_stab_heat_source_coeff
        self.property_stab_body_force_coeff = self.parameters.sol_ctrl_stab_body_force_coeff

    def create_scp_string(self,key,value):
        """

        :param key: attribute name
        :param value: attribute value
        :return psf_string: psf formatted string for creating a solution control parameter
        """
        psf_string = (
            f"SET SOLUTION_CONTROL_PARAMETER \\ \n"
            f"  {key.upper()} \\ \n" 
            f"  {self.parameters.physical_mesh_name} \\ \n"
            f"  {value} \n"
            f" \n")
        return psf_string

class InitializeSimulation(SimulationControl):
    """

    This class manages the initialization commands as well as some other useful start up commands.
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)
        self.solution_control_type = "initialization_parameter"
        self.property_deformation_visualization_scale = 1.0
        self.initialize_simulation = 0.0
    
    def create_init_rod_string(self):
        """

        :return psf_string_list: psf formatted list of strings for initializing simulation
        """
        psf_string_list = []
        init_rod_pre_string = (
            f"INITIALIZE_SIMULATION \\ \n"
            f"  {self.parameters.physical_mesh_name.upper()} \\ \n")
        if self.parameters.model_solve_type is not None:
            init_rod_post_string = (
                f"  0.0 \\ \n"
                f"  {self.parameters.model_solve_type} \n"
                f" \n")
        else:
            init_rod_post_string = (
                f"  0.0 \n"
                f" \n")
        psf_string_list.append(init_rod_pre_string)
        psf_string_list.append(init_rod_post_string)
        return psf_string_list
        
    def create_set_def_scale(self):
        """

        :return psf_string: psf formatted string for setting deformation visualization scale
        """
        psf_string = (
            f"SET DEFORMATION_VISUALIZATION_SCALE \\ \n"
            f"  {self.parameters.physical_mesh_name.upper()} \\ \n"
            f"  {self.parameters.geometry_deformation_scale} \n"
            f" \n")
        return psf_string

    def create_pause(self):
        """

        :return psf_string: psf formatted string for creating pause
        """
        psf_string = (
            "PAUSE \n"
            "\n")
        return psf_string

    def create_hide_show(self, set_to_show=None):
        """

        :param set_to_show: name of object to show (default: `parameters.physical_mesh_name`)
        :return psf_string: psf formatted string for creating hide/show
        """
        if set_to_show is None:
            set_to_show = self.parameters.physical_mesh_name
        psf_string = (
            "HIDE ALL \n"
            "\n"
            f"SHOW {{{set_to_show.upper()}}} \n"
            "\n")
        return psf_string

    def create_all_init_string(self):
        """

        :return psf_string_list: psf formatted list of strings for creating all initializations
        """
        psf_string_list = []
        psf_string_list.extend(self.create_init_rod_string())
        psf_string_list.extend(self.create_set_def_scale())
        return psf_string_list

class CaptureData(SimulationControl):
    """

    This class manages the capture data commands.
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)

    def create_capture_data(self, data_set):
        """

        :param data_set: name of data set to convert to captured data type
        :return psf_string: psf formatted string for creating all captured data sets
        """
        psf_string = (
            f"CREATE CAPTURED_DATA CD_{data_set.upper()} {{{data_set.upper()}}}\n"
            f" \n")
        return psf_string

    def create_default_capture_data(self):
        """

        This method is for creating the following default capture data sets: 
            1: `parameters.elements_all_name` 
            2: `parameters.nodes_all_name`
        
        Supporting Methods Used:
            :method: `create_capture_data`
        :return psf_string: psf formatted string for creating all captured data sets
        """
        data_elements = self.parameters.elements_all_name
        data_nodes = self.parameters.nodes_all_name
        cd_list = [data_elements, data_nodes]
        # print(cd_list)
        psf_string_list = []
        for cd_set in range(0, len(cd_list)):
            psf_string_list.append(self.create_capture_data(cd_list[cd_set]))
        return psf_string_list

class Walks(SimulationControl):
    """

    This class manages the walk commands from various possible sources as well as 
    calculating capture strides (cds)
        
    Supporting Classes Used:
    :class: `SimulationControl`
    
    """
    
    def __init__(self, input_file_path=None):
        super().__init__(input_file_path)
        self.starter = 1
        self.generate_gtd_files = False
        self.capture_data_object = CaptureData()
        self.init_walk_time = [1.0, 10.0, 100.0, 1000.0]
        self.init_walk_steps = [0.1, 1.0, 10.0, 100.0]
        self.init_walk_cds = [10, 9, 9, 9]

    def single_walk(self, end_time, time_step_size, cd_stride=None, cap_pics=True):
        """
        
        This method creates single walk command.

        :param end_time: final time
        :param time_step_size: size of time step
        :param cd_stride: size of time step
        :param cap_pics: capture picture options: (Default-`True`, `False`)
        :return psf_string_list: psf formatted list of strings for creating a walk
        """
        if cd_stride is None:
            capture_stride = int(time_step_size)
        else:
            capture_stride = int(cd_stride)
            
        psf_string_list = []
        single_walk_pre_string = (
            f"WALK {self.parameters.physical_mesh_name} \\ \n" 
            f"  TO {float(end_time)} BY {float(time_step_size)} \\ \n "
            f"  {{ \\ \n " 
            f"     {self.parameters.cd_nodes_all_name}, \\ \n")
        
        if cap_pics:
            single_walk_post_string = (
            f"      {self.parameters.cd_elements_all_name}, \\ \n" 
            f'      "../{self.parameters.file_pegasus_output_pics_relative_location}/rod.png" \\ \n' 
            f"   }} \\ \n " 
            f"  {capture_stride} \n"
            f"\n")
            
        else:
            single_walk_post_string = (
            f"      {self.parameters.cd_elements_all_name} \\ \n" 
            f"   }} \\ \n " 
            f"  {capture_stride} \n"
            f"\n")
            
        psf_string_list.extend(single_walk_pre_string)
        psf_string_list.extend(single_walk_post_string)
        return psf_string_list

    def calculate_cds(self, walk_time_i, walk_time_f, step_size):
        """
        
        This method will calculate the capture data strides integer value.

        :param walk_time_i: initial time
        :param walk_time_f: final time
        :param step_size: step size between inital and final times
        :return cds: capture data stride 
        """
        cds = int((walk_time_f - walk_time_i) / step_size)
        return cds

    def walk_from_list(self, walk_time, step_size, cd_stride=None):
        """
        
        Method for creating walk sequence from 2 lists where
            list 1: final time
            list 2: step size

        Supporting Methods Used:
            :method: `walk_stable_start_sequence`,
            :method: `walk_from_list`

        :param time_and_step_list:
        :return psf_string_list: psf formatted list of strings for creating multiple walks
        """
        cds = cd_stride
        length_of_data = len(walk_time)
        start_index = 0
        if cd_stride is None:
            start_index = 1
            if type(walk_time) is list:
                walk_time.insert(0, self.init_walk_time[-1])
                step_size.insert(0, self.init_walk_steps[-1])
            else:
                walk_time = np.insert(walk_time, 0, self.init_walk_time[-1])
                step_size = np.insert(step_size, 0, self.init_walk_steps[-1])
                length_of_data = walk_time.shape[0]
        psf_string_list = []
        # print(f"walk_time: {walk_time}, step_size: {step_size}")
        for a_walk in range(start_index, length_of_data):
            if cd_stride is not None:
                cds = cd_stride[a_walk]
            else:
                cds = self.calculate_cds(walk_time[a_walk-1], walk_time[a_walk], step_size[a_walk])
                
            psf_string_list.extend(self.single_walk(
                walk_time[a_walk], 
                step_size[a_walk], 
                cds
                )
            )
        return psf_string_list

    def walk_from_xlsx(self):
        """
        
        Method for creating walk sequence from an xlsx worksheet where
            column 1: final time
            column 2: step size

        Supporting Methods Used:
            :method: `walk_stable_start_sequence`,
            :method: `walk_from_list`
            
        :param file_location:
        :param data_file_name:
        :param sheet_name:
        :return psf_string_list: psf formatted list of strings for creating multiple walks
        """
        self.file_tool.get_time_steps()
        psf_string_list = []
        psf_string_list.extend(self.walk_stable_start_sequence())
        psf_string_list.extend(self.walk_from_list(
            self.file_tool.data_walk, 
            self.file_tool.data_step_size))
        return psf_string_list

    def walk_stable_start_sequence(self):
        """
        
        This method creates a stable start up sequence of walk commands.

        Supporting Methods Used:
            :method: `walk_from_list`
        :return psf_string_list: psf formatted list of strings for creating initial-stable walk sequence
        """
        walk_string_list = self.walk_from_list(self.init_walk_time, self.init_walk_steps, self.init_walk_cds)
        return walk_string_list

class SimulationControlTool(SimulationControl):
    """

    This class combines all classes in the `simulataion_control` module and can be imported into other python files to 
    easily access all simulataion control features.

    Supporting Classes Used:
    :class: `ConvergenceTolerance`,
    :class: `SolutionControlParameter`,
    :class: `MechanicalContact`,
    :class: `InitializeSimulation`,
    :class: `CaptureData`,
    :class: `Walks`
    
    """
    def __init__(self, input_file_path=None):
        super(SimulationControlTool, self).__init__(input_file_path)
        self.conv_tool = ConvergenceTolerance(input_file_path)
        self.scp_tool = SolutionControlParameter(input_file_path)
        self.ccp_tool = MechanicalContact(input_file_path)
        self.init_tool = InitializeSimulation(input_file_path)
        self.cd_tool = CaptureData(input_file_path)
        self.walk_tool = Walks(input_file_path)
