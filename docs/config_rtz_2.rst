Rod R-Theta-Z Detailed
======================

.. literalinclude:: ../script_generation/examples/scrip_gen_input/generator_input_rtz_2.yaml
   :language: yaml
   :linenos:
