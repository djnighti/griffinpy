GUI
===

Griffinpy has its own built in GUI that has both pre and post processing capabilites for completeing tasks in Pegasus.

Running Griffinpy
^^^^^^^^^^^^^^^^^

To run the GUI there are a few options:
1. The docker container has a built in command that can be used

.. code-block:: bash
    
    griffinpy_gui

2. running the source code directly in the `src` folder of griffinpy

.. code-block:: bash
    
    python3 griffinpy_gui

3. Finally, can be ran using the Desktop icon mentioned on `Installation`


Pre-Processing
--------------

Creating a Pegasus Script File (PSF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/7-Gh8w6RZvM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>


Triso Grid Generation
~~~~~~~~~~~~~~~~~~~~~

Here is a video showing how to generate triso particle distribution that can be used for modeling on the pellet-compact scale.

Mesh Builder
~~~~~~~~~~~~

Here is a video example of a mesh that was designed in Griffinpy and built step-by-step in Pegasus

Post-Processing
---------------

Plotting Pegasus Output Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pegasus provides 2 different data types that can be retrieved. Below are examples of how to use
Griffinpy to plot either one and the differences between them.

Single point data type
""""""""""""""""""""""

The video below shows the various options that can be used for plotting this data type.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/f32812hP__U" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

Multi point (spatial) data type
"""""""""""""""""""""""""""""""

The video below shows the various options that can be used for plotting this data type.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/f4ZvjdJklYM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

Loading Comparison Data
"""""""""""""""""""""""

The video below shows how to easily load in comparison/validation data to plot directly against
either of Pegasus's data types.

Creating Animations
~~~~~~~~~~~~~~~~~~~

The video below shows how to create an animation using the pictures that were taken during a Pegasus
simulation as well as being able to get a preview of the animation in Griffinpy.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/JpRooBcR_FM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
