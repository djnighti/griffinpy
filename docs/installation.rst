************
Installation
************
Griffinpy would ideally be converted to a pip package for the easiest installation 
but for now it can be accessed from source via gitlab or from the docker image (recommended). Has been tested using python3.7 on Ubuntu19

Method 1: Source
================
Create a virtual environment and install all dependencies.

.. code-block:: bash

    git clone https://gitlab.com/djnighti/griffinpy
    cd griffinpy
    python3 -m venv myenv
    source myenv/bin/activate 
    pip3 install -r requirements.txt


Method 2: Docker
================
Simply pull docker image and start using.

.. code-block:: bash

    docker pull djnighti/griffinpy:latest

Support Files for Docker
------------------------
Create a new bash script that will launch the container and create a shared folder between the docker container and the users local files. 
Files in this folder sync both ways, ie. any new content that's created in the docker container and is put into this shared folder will be available in the users local files and vice versa. 
This can be done by creating a new bash script OR by creating a function in the ~/.bashrc script. Both Methods shown below.

Bash script
^^^^^^^^^^^

.. code-block:: bash

    gedit ~/.bashrc

Then scroll down to the bottom and copy and paste the following:

.. code-block:: bash

    run_docker() 
    { 
        RED='\033[1;31m'
        RED_HIGH='\033[2;91m'
        GREEN='\033[1;32m'
        YELLOW='\033[1;33m'
        BLUE='\033[1;34m'
        PURPLE='\033[1;35m'
        CYAN='\033[1;36m'
        WHITE='\033[1;37m'
        ORANGE='\033[1;38;5;214m'
        NC='\033[0m'
        TEMP=`getopt --long -o "i:t:e:n:d:lh" "$@"`
        eval set -- "$TEMP"
        func_name=${FUNCNAME[0]}
        # unset varibales
        unset shared_directory
        unset tag_id
        unset container_name
        unset image_name
        unset exec_container_name
        unset run_exec_command
    
        # get user inputs
        while true ; do
          case "$1" in
            -i ) 
                image_name=$2
                shift 2
            ;;
            -t ) 
                tag_id=$2
                shift 2
            ;;
            -n ) 
                container_name=$2
                shift 2
            ;;
            -e ) 
                exec_container_name=$2
                shift 2
            ;;
            -l ) 
                # list out all available images and tags via for loop on list
                docker_image_list=()
                
                # get available images in form of string list
                docker_image_str_list=$(docker images --format="{{ .Repository }}" | sort | uniq)
                for a_docker_image in ${docker_image_str_list}; do
                    docker_image_list+=(${a_docker_image})
                done
                
                num_images=${#docker_image_list[@]}
                echo -e "There are ${GREEN}${num_images}${NC} available images"
    
                # loop over each image to show all its associated tags
                for a_docker_image in ${docker_image_list[@]}; do
                    docker_tags_list=()
                    
                    # get available tags in form of string list
                    docker_tags_str_list=$(docker images ${a_docker_image} --format="{{ .Tag }}")
                    
                    # format string into list
                    for a_docker_tag in ${docker_tags_str_list}; do
                        docker_tags_list+=(${a_docker_tag})
                    done
    
                    num_images=${#docker_image_list[@]}
                    # echo -e "str ${GREEN}${docker_tags_str_list}${NC}"
                    # echo -e "list ${GREEN}${docker_tags_list}${NC}"
                    
                    # show available tags
                    num_tags=${#docker_tags_list[@]}
                    echo -e "There are ${GREEN}${num_tags}${NC} available tags for the ${YELLOW}${a_docker_image}${NC} image:"
                    for a_docker_tag in ${docker_tags_list[@]}; do
                        echo -e "  * ${BLUE}$a_docker_tag${NC}"
                    done
                    unset docker_tags_list
                done
                return 0
            ;;
            -d ) 
                shared_directory=$2
                shift 2
            ;;
            -h ) 
                echo -e " "
                echo -e "Usage: ${YELLOW}${func_name} -[i|t|e|n|d|l|h]${NC}"
                echo -e " "
                echo -e "${PURPLE}Flags for creating new container${NC}:"
                echo -e "   ${RED}Required${NC}:"
                echo -e "       ${RED}-i${NC}: Specify the docker ${BLUE}image${NC} to create a container from"
                echo -e "       ${RED}-t${NC}: Specify the ${BLUE}tag${NC} associated with the selected docker image"
                echo -e "   ${ORANGE}Optional${NC}:"
                echo -e "       ${ORANGE}-n${NC}: Specify the ${BLUE}name${NC} of new container to create"
                echo -e "       ${ORANGE}-d${NC}: Specify a ${BLUE}directory${NC} to share with the new docker container"
                echo -e " "
                echo -e "${CYAN}Flags for entering existing container${NC}:"
                echo -e "   ${RED}Required${NC}:"
                echo -e "       ${RED}-e${NC}: Specify the ${BLUE}name${NC} of an existing container to enter"
                echo -e " "
                echo -e "${GREEN}General helper flags${NC}:"
                echo -e "   ${GREEN}-l${NC}: Display all available ${BLUE}tag ids${NC} associated with the selected docker image"
                echo -e "   ${GREEN}-h${NC}: Display this help message"
                echo -e " "
                return 0
            ;;
            *)
                break
            ;;
          esac
        done
    
        run_exec_command="false"
    
        if [ -z "$image_name" ] | [ -z "$tag_id" ]; then
            if [ -z "$exec_container_name" ]; then
                echo "Image and tag arguements (-i, -t) are both required."
                return 0
            else
                run_exec_command="true"
            fi
        fi
    
        #  echo -e "$run_exec_command"
    
        if [ "$run_exec_command" == "true" ]; then
            echo -e " "
            echo -e "Now entering ${BLUE}${exec_container_name}${NC} docker container..."
            echo -e " "
            docker exec -it $exec_container_name bash
    
        else
            # Set default values
            tag_id=${tag_id:-latest}
            container_name=${container_name:-"default_container"}
            current_time=$(date +"%Y-%m-%d %H:%M:%S")
            container_info="image and tag name: $image_name:$tag_id \ncontainer name: $container_name \ncreation time: $current_time"
    
            # printenv var_container_info
            # echo -e $var_container_info
    
            # Check if shared directory is not provided
            if [ -z "$shared_directory" ]; then
                
                # now run docker command without sharing a directory
                echo -e " "
                echo -e "${RED}No directory${NC} is being shared with ${BLUE}${container_name}${NC}, manage files cautiously."
                echo -e "Now entering ${BLUE}${container_name}${NC} docker container from ${BLUE}${image_name}:${tag_id}${NC}..."
                echo -e " "
                docker run \
                    --name $container_name \
                    -it \
                    --privileged \
                    --net=host \
                    --env="DISPLAY" \
                    -e var_container_info="$container_info" \
                    ${image_name}:$tag_id
            
            # If shared directory is provided, check if user wants current directory or some path
            else
                # Use current directory as shared folder
                if [ "$shared_directory" == "here" ]; then
                    shared_directory=$(pwd)
                # Use provided input 
                else
                    # make directory incase it doesnt exist yet
                    mkdir -p $(dirname $shared_directory)
                fi
                container_info="$container_info \nshared directory: $shared_directory"
                
                # now run docker command with sharing a directory
                echo -e " "
                echo -e "The directory: ${GREEN}${shared_directory}${NC} is being shared with ${BLUE}${container_name}${NC}."
                echo -e "Files in this directory can be modified on the host and the changes will be reflected inside ${BLUE}${container_name}${NC}."
                echo -e "Now entering ${BLUE}${container_name}${NC} docker container from ${BLUE}${image_name}:${tag_id}${NC}..."
                echo -e " "
                docker run \
                    --name $container_name \
                    -it \
                    --privileged \
                    --net=host \
                    --env="DISPLAY" \
                    --volume "$shared_directory:/home/projects/shared_folders" \
                    -e var_container_info="$container_info" \
                    ${image_name}:$tag_id
            fi
            # unset varibales
            unset shared_directory
            unset tag_id
            unset container_name
            unset image_name
            unset exec_container_name
            unset run_exec_command
            unset container_info
            unset current_time
        fi
    }

Then source it

.. code-block:: bash

    source ~/.bashrc

Then run the `-h` flag to see how to use it.

.. code-block:: bash

    run_docker -h


Desktop Icon
^^^^^^^^^^^^

This section covers how to easily create a desktop shortcut that will launch the docker container.
One package will need to be installed for this on the local machine (not in docker container)

.. code-block:: bash

    sudo apt-get install --no-install-recommends gnome-pane
    gnome-desktop-item-edit --create-new ~/Desktop

1. Then fill in the entries

- Type: Application

- Name: Griffinpy

- Command: bash /home/dnightingale/projects/docker/dockerPegasus.sh

- Comment: (can leave blank)

2. Add a picture (here is the griffinpy logo)

3. Then click ok to create desktop shortcut

4. Now click on the griffinpy desktop icon to launch!

