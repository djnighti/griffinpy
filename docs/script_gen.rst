*****************
Script Generation
*****************

Griffinpy has a built in automated process for creating a suite of predefined problems to be run directly in Pegasus.
Each template has its own set of hyper-parameters that can be tuned by the user to suit their needs via a .yaml file (`generator_input`). 
More simple examples of using this `generator_input` can be seen in unit_tests.

.. toctree::
   :maxdepth: 1
   :caption: PSF Template Suite:
   
   config_rt_1
   config_rt_2
   config_rtz_1
   config_rtz_2
   config_rz_1
   config_rz_2
   config_t_1
   config_t_1

General Idea of the Generator Input File
========================================
This configuration file can be used for selecting which predefined meshes to use, updating geometric constraints, 
material inputs and boundary conditions. It is also where to define the name and location of the output PSF

There are several categories of different configuration options available which follow a naming convention so that all the 
parameters are updated correctly as well as for the sake of maintainability.

The categories are as follows:

· Input/Output Files

· Modeling Flags

· Model Geometry

· Model Resolution

· Boundary Condition Flags

· Boundary Condition Constraints

· Data Retrieval

Each category has a set of parameters that can be updated by the user. The flag categories have default values set to “False” 
which means that the user does not need to include all the flag options when creating new inputs and only need to update the 
flag they wish to use to “True”.

Directory Structure
===================

To keep things organized, a directory scheme is also automatically generated when using Griffinpy to help keep things organized. 
The scheme created and its location is dependent on the `DIRECTORY_TEMPLATE_LOCATION` parameter in the config file which is 
discussed later. Each directory location is tracked and the corresponding data types will be put into their respective folders. 
The scheme is shown below.

**Example input:**

.. code-block:: yaml
    :linenos:

    FILE:
        PEGASUS_NAME: "testing_2D_rz" # Name of Pegasus file
        DIRECTORY_TEMPLATE_LOCATION: "/home/projects/testing_script_gen" # Path to directory to create template in
        INPUT:
            DATA_NAME: "rod_data_simple" # dataset that (could) contain: power_history, axial_power_shapes, temperature_history, walks
            DATA_LOCATION: "/home/projects/input_data" # path to input `DATA_NAME`

**Example output:**

.. code-block:: bash
    
    home
        user_name
            ...
                testing_script_gen
                    input
                        testing_2D_rz.psf
                        power_history.gtd
                        axial_power_shapes.gtd
                    output
                        database
                        pictures
                        tabular_data