# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import sys
import os

project = 'griffinpy'
copyright = '2023, Dominic Nightingale'
author = 'Dominic Nightingale'
release = '1.0'

# add files to path so readthedocs can find them and do automodules
sys.path.insert(0, os.path.abspath('../src'))
sys.path.insert(0, os.path.abspath('../unit_tests'))
sys.path.insert(0, os.path.abspath('../script_generation/examples/scrip_gen_input'))

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# extensions = ['sphinx.ext.autodoc']

extensions = [
    'sphinx.ext.autodoc',
    'sphinx_rtd_theme',
    'sphinx.ext.intersphinx',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
]

templates_path = ['_templates']
# exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
exclude_patterns = ['Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

autodoc_default_options = {
    'member-order': 'bysource',
    'inherited-members': True ,
    'show-inheritance': True
}

autodoc_mock_imports = [
    "numpy",
    "control",
    "descartes",
    "imageio",
    "matplotlib",
    "moviepy",
    "mss",
    "numpy",
    "openpyxl",
    "pandas",
    "Pillow",
    "PyQt5",
    "PyYAML",
    "scipy",
    "Shapely",
    "tkVideoPlayer",
    "ttkthemes",
    "ttkwidgets"
]