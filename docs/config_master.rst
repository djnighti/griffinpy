Master Config
=============

.. literalinclude:: ../script_generation/examples/scrip_gen_input/generator_input_master.yaml
   :language: yaml
   :linenos:
