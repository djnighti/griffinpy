.. griffinpy documentation master file, created by
   sphinx-quickstart on Thu Jan 26 18:40:16 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to griffinpy's documentation!
=====================================
The structure of Griffinpy mirrors that of Pegasus when it comes to the key elements of creating a simulation but branches out and 
becomes extremely powerful when considering all the automated procedures that are built in such as mesh generation, extracting datasets, 
creating animations, and plotting results. The following sections will discuss the inner workings of Griffinpy which are split 
into two main categories of pre-processing and post-processing tools. Following this overview, the the code API 
will discuss more detail about all the available functions and how to use them. Then checkout the suite of available 
templates the user can have immediate access to use to start doing problems in Pegasus. Finally, using the gui will go into 
more detail about how to use the pre and post processing tools in Griffinpy that streamline the simulation process.

The sequence of how any PSF must be written is shown below and is the same process that's built into Griffinpy. 
This allows for conformity when creating new problems and adding new templates to the suite of problems that are currently in Griffinpy. 
The general sequence is provided below.

1. Model Parameters

2. Meshing

3. Materials

4. Element/node sets and Polylines

5. Boundary Conditions

6. Assign materials & boundary conditions to Nodes/Elements

7. Setting simulation solution parameters

8. Commence with simulation

9. Database

10. Data retrieval


:doc:`installation`
    How to install griffinpy.

:doc:`code`
    Code API.

:doc:`script_gen`
    Suite of templates and example of PSF generated files using .yaml files.

:doc:`unit_tests`
    Examples of using the API to create new problems or add new materials.

:doc:`gui`
    Using the GUI in griffinpy for pre/post-processing.

.. Hidden TOCs

.. toctree::
   :caption: Documentation
   :maxdepth: 2
   :hidden:

   installation
   code
   script_gen
   unit_tests
   gui