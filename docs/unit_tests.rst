Unit Tests
==========
A suite of unit tests were created to show how to interface with the API of griffinpy for when creating new problems.
The `Helper` is the manager of all the unit tests. It searches for other python files that are pre-fixed with "unit" and will run
the functions inside them that are also pre-fixed with "unit" and print either simple results (ex. test passed/failed) or detailed results
that are defined in each unit test. It's recommended that if the user is just starting to learn the API, these unit tests are the place to start
as they break down the API into each of the core elements for learning and testing purposes. This unit testing framework sets up the idea that as source
code is being updated, the unit tests can be ran to ensure nothing breaks but also as a place to develop new functionality into griffinpy without the potential 
risk of again breaking certain aspects in griffinpy. 

Helper
----------
.. automodule:: unit_test_helper
    :members:

Parameters
----------
.. automodule:: unit_test_parameters
    :members:

Meshing
-------
.. automodule:: unit_test_meshing
    :members:

Materials
---------
.. automodule:: unit_test_materials
    :members:

Boundary Condtions
------------------
.. automodule:: unit_test_boundary_conditions
    :members:

Assignments
-----------
.. automodule:: unit_test_assignments
    :members:

Simulation Control
------------------
.. automodule:: unit_test_simulation_control
    :members:

Retrieve
--------
.. automodule:: unit_test_retrieve
    :members:

Tools
-----
.. automodule:: unit_test_tools
    :members: