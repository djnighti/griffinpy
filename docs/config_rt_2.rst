Rod R-Theta MPS
===============

.. literalinclude:: ../script_generation/examples/scrip_gen_input/generator_input_rt_2.yaml
   :language: yaml
   :linenos:
