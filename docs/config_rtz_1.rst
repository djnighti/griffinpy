Rod R-Theta-Z Standard
======================

.. literalinclude:: ../script_generation/examples/scrip_gen_input/generator_input_rtz_1.yaml
   :language: yaml
   :linenos:
