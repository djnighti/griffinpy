GUI
===
Here is where examples of using the GUI in Griffinpy will go.


pre-processing
--------------

Creating a Pegasus Script File (PSF)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


post-processing
---------------

Plotting Pegasus Output Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pegasus provides 2 different data types that can be retrieved. Below are examples of how to use
Griffinpy to plot either one and the differences between them.

Single point data type
""""""""""""""""""""""
The video below shows the various options that can be used for plotting this data type.

Multi point (spatial) data type
"""""""""""""""""""""""""""""""
The video below shows the various options that can be used for plotting this data type.

Loading Comparison Data
"""""""""""""""""""""""
The video below shows how to easily load in comparison/validation data to plot directly against
either of Pegasus's data types.

Creating Animations
~~~~~~~~~~~~~~~~~~~
The video below shows how to create an animation using the pictures that were taken during a Pegasus
simulation as well as being able to get a preview of the animation in Griffinpy.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/JpRooBcR_FM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
