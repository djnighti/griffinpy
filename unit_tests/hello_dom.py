import sys


class Test:
    def __init__(self):
        self.greet = "hello world"

    def hello(self, msg=None):
        """
        This function ....

        :param msg: A message to be displayed on the terminal
        :return: 1: result 2: msg
        """
        if msg is None:
            msg = self.greet

        result = "Test Passed"
        return result, msg

    def get_user_input(self):
        print(f"sys.argv: {sys.argv}")
        if len(sys.argv) > 1:       # no inputs tell
            new_msg = sys.argv[1]
        else:
            new_msg = None

        result, msg = self.hello(new_msg)
        print(f"result: {result} {msg}")

    def test(self):
        print("testing")


if __name__ == "__main__":   # use to create class instance to sp
    greet_test = Test()   # instance of class
    greet_test.hello()
