import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from materials import RodMaterialsTool
from unit_test_helper import UnitTestsHelper


class UnitTestsMaterials(UnitTestsHelper):
    """

    unit_test_1 is designed to be able to return psf formatted lists of real variables for materials
    and psf formatted strings of created materials without the need for a yaml file input, but is also set up to be able to do so.

    unit_test_2 will read its respective yaml file and update the material parameters accordingly

    yaml files can be listed at the very end of this script, which are then added to a list and are then indexed 
    by their respective functions

    unit_test_hello is used to indicate which unit test reuslts are being displayed

    To run this script with detailed results,    use: python3 unit_test_materials.py True
    To run this script without detailed results, use: python3 unit_test_materials.py False
    """
    def __init__(self, input_file_path=None):
        super(UnitTestsMaterials, self).__init__()
        self.input_yaml_list = input_file_path

     
    def update_config(self, input_file_path):
        self.parameters = Parameters(input_file_path)
        self.material_tool = RodMaterialsTool(input_file_path)
        self.all_mat_dicts = self.material_tool.create_all_mat_dictionary()
        self.materials_all_object_dict = {}

    def create_material_dictionary(self, mat_class_name):
        """
        This function:
         - creates a dictionary (props_dict) of material class names found in materials.py (material_tool)

        :return: result: Test Passed
        """
        props_dict= self.material_tool.all_mat_dict[mat_class_name].create_material_dicts()
        print(f"\n================== Material Properties for: {mat_class_name} ==================\n")
        for key in props_dict:
            print(f"{key}: {props_dict[key]}")
        result = "Test passed"
        return result

    def create_material_alias_class_dict(self):
        """
        This function:
         - creates a dictionary (materials_all_object_dict) of material aliases and class names for fuel, gap, and cladding materials only
            which are found in paramaters.py (parameters)

        :return: materials_all_object_dict (key: material alias name, value: material class name)
        """
        materials_all_object_dict = {}
        layer = 0
        mat_index = "_"+str(layer+1)
        materials_all_object_dict[self.parameters.material_alias_name_fuel] = self.parameters.material_fuel_class
        materials_all_object_dict[self.parameters.material_alias_name_fuel_cladding_gap] = self.parameters.material_fuel_cladding_gap_class
        materials_all_object_dict[self.parameters.material_alias_name_cladding_list+mat_index] = self.parameters.material_cladding_list_class[layer]
        return materials_all_object_dict

    def unit_test_1(self):
        """
        This function:
         - indexes the first input from the list of yaml files, which is currently set to "None".
         - will not use a yaml file to update parameters, but it is set up to be able to do so.

         - calls create_material_alias_class_dict() that generates a dictionary (materials_all_object_dict)
            of material aliases and class names for fuel, gap, and cladding materials only
        
         - calls determine_create_material() from materials.py (material_tool), which creates 2 string lists:
              - 1. the psf format strings for the creation of the material
              - 2. the psf format strings to create all the real variables that will be used
        
         - :return 1: result: Test Passed
         - :return 2: msg: psf formatted list of real variables for material, psf formatted string of created material
        """
        self.update_config(self.input_yaml_list[0])
        msg_list = []
        materials_all_object_dict = self.create_material_alias_class_dict()

        for key_mat_alias in materials_all_object_dict:
            psf_material_string_list, material_reals_string_list = self.material_tool.determine_create_material(
                    material_class_name=materials_all_object_dict[key_mat_alias], 
                    material_alias=key_mat_alias)
            
            msg_list.extend([f"\n#      ---------- Real Variables for Material: {materials_all_object_dict[key_mat_alias]} ----------\n"])
            msg_list.extend(material_reals_string_list)
            
            msg_list.extend([f"\n#      ---------- Creating Material: {materials_all_object_dict[key_mat_alias]} ----------\n"])
            msg_list.extend(psf_material_string_list)
            msg = ''.join([item for sublist in msg_list for item in sublist])
            
        result = "Test Passed"
        return result, msg

    def unit_test_2(self):
        """
        This function:
         - indexes the second input from the list of yaml files
         - calls determine_create_material() from materials.py (material_tool) to specify the fuel, gap, and cladding
            material alias and class names which are then added to a list (msg_list)
         - yaml file is used to update material parameters using specified alias and class names 

        :return 1: result: Test passed
        :return 2: msg: psf formatted list of real variables for material, psf formatted string of created material
        """
        self.update_config(self.input_yaml_list[1])
        msg_list = []
        
        #---------------------- Fuel -----------------------#
        material_fuel, material_fuel_reals = self.material_tool.determine_create_material(
            material_class_name=self.parameters.material_fuel_class, 
            material_alias=self.parameters.material_alias_name_fuel)
            
        # Material values (reals) 
        msg_list.extend(material_fuel_reals)
        # Actual materials    
        msg_list.extend(material_fuel)

        #---------------------- Gap -------------------------#
        material_fuel_cladding_gap, material_fuel_cladding_gap_reals = self.material_tool.determine_create_material(
            material_class_name=self.parameters.material_fuel_cladding_gap_class, 
            material_alias=self.parameters.material_alias_name_fuel_cladding_gap)

        # Material values (reals) 
        msg_list.extend(material_fuel_cladding_gap_reals)
        # Actual materials
        msg_list.extend(material_fuel_cladding_gap)

        #---------------------- Cladding ----------------------#
        if len(self.parameters.material_cladding_list_class) > 1:
            material_cladding = []
            material_cladding_reals = []
            num_cladding_layers = len(self.parameters.material_cladding_list_class)
            for layer in range(0, num_cladding_layers):
                mat_index = "_"+str(layer+1)
                a_material_cladding, a_material_cladding_reals = self.material_tool.determine_create_material(
                    material_class_name=self.parameters.material_cladding_list_class[layer], 
                    material_alias=self.parameters.material_alias_name_cladding_list+mat_index,
                    material_index=mat_index)
                material_cladding.extend(a_material_cladding)
                material_cladding_reals.extend(a_material_cladding_reals)

        # Material values (reals) 
        msg_list.extend(material_cladding_reals) 
        # Actual materials
        msg_list.extend(material_cladding)


        # output results
        msg = ''.join([item for sublist in msg_list for item in sublist])            
        result = "Test passed"
        return result, msg

    def unit_test_hello(self):
        """

        :return 1: result: Test Passed
        :return 2: msg: Hello From Materials :)
        """
        result = "Test passed"
        msg = "Hello from Materials :)"
        return result, msg

if __name__ == "__main__":
    param_file_test_1 = None
    param_file_test_2 = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_dynamic_mat.yaml"
    param_file_list = [param_file_test_1, param_file_test_2]
    unit_test = UnitTestsMaterials(param_file_list)
    unit_test.test_psf_out(detailed_results=sys.argv[1])