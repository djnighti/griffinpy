import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from boundary_conditions import BoundaryConditionTool
from boundary_conditions import GasInventoryObject
from boundary_conditions import BoundaryCondition
from unit_test_helper import UnitTestsHelper


class UnitTestsParameters(UnitTestsHelper):
    """
    This unit test displays a psf formatted list of strings for parameters defined in a pegasus script file

    To run this script with detailed results,    use: python3 unit_test_parameters.py True
    To run this script without detailed results, use: python3 unit_test_parameters.py False
    """
    def __init__(self):
        super(UnitTestsParameters, self).__init__()
        self.parameters = Parameters()    
        self.bc_tool = BoundaryConditionTool() 
        self.inventory_gas = GasInventoryObject()
        self.bc = BoundaryCondition()
    
    def unit_test_1(self):
        """
        
        :return 1: result: Test passed
        :return 2: msg: psf formatted list of strings for parameters defined in a pegasus script file
        """
        self.header_params_str         = "Model Parameters"
        self.headersub1_params_geo_str = "Model Geometry"
        self.headersub1_params_res_str = "Model Resolution"
        self.headersub1_params_mat_str = "Material parameters"
        self.headersub1_params_inv_str = "Inventory parameters"
        self.headersub1_params_bc_str  = "Boundary Condition parameters"

        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_params_str))
        
        # Model Geometry Parameters
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_geo_str,level=self.parameters.level_header_2))
        template_string.extend(self.parameters.create_params_geometry())
        
        # Model Resolution Parameters
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_res_str,level=self.parameters.level_header_2))
        template_string.extend(self.parameters.create_params_resolution())
        
        # Create Remaining PSF Parameters

        # Materials
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_mat_str,level=self.parameters.level_header_2))
        template_string.extend(self.parameters.create_params_material())

        # Inventory objects
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_inv_str,level=self.parameters.level_header_2))
        # Gas Inventory Object
        template_string.extend(self.inventory_gas.create_reals())


        # Boundary Conditions
        template_string.extend(self.parameters.create_header_comment(self.headersub1_params_bc_str,level=self.parameters.level_header_2))
        template_string.extend(self.bc.create_reals())


        result = "Test passed"
        msg = ''.join([item for sublist in template_string for item in sublist])
        return result, msg

    def unit_test_hello(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Hello from Parameters :)
        """
        result = "Test passed"
        msg = "Hello from Parameters :)"
        return result, msg

if __name__ == "__main__":
    unit_test = UnitTestsParameters()
    unit_test.test_psf_out(detailed_results=sys.argv[1])