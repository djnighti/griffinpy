import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from simulation_control import SimulationControlTool
from unit_test_helper import UnitTestsHelper


class UnitTestsSimulationControl(UnitTestsHelper):
    """
    This unit test will display a .psf formatted output for simulation control parameters and walk commands using separate functions.

    The "hello" unit test function is used to indicate which unit test results are being displayed

    To run this script with detailed results,    use: python3 unit_test_simulation_control.py True
    To run this script without detailed results, use: python3 unit_test_simulation_control.py False
    """
    def __init__(self):
        super(UnitTestsSimulationControl, self).__init__()
        self.parameters = Parameters()
        self.sc_tool = SimulationControlTool()
        
    def unit_test_sim_control(self):
        self.header_simctrl_str = "Setting simulation solution parameters"
        self.headersub1_simctrl_contact_str = "Mechanical Contact"
        self.headersub1_simctrl_conv_str = "Convergence Tolerances"
        self.headersub1_simctrl_scp_str = "Solution Control Parameters"
        self.headersub1_simctrl_init_str = "Initialize Simulation"
        self.headersub1_simctrl_cd_str = "Capture Data"
        self.header_walk_str = "Commence with simulation"

        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_simctrl_str))

        # contact
        self.set_contact = self.sc_tool.ccp_tool.create_solution_params()
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_contact_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.set_contact)

        # Convergence Tolerances 
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_conv_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sc_tool.conv_tool.create_solution_params())
        
        # Solution Control Parameters 
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_scp_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sc_tool.scp_tool.create_solution_params())
       
        # Init sim
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_init_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sc_tool.init_tool.create_all_init_string())
 
        # Capture data
        template_string.extend(self.parameters.create_header_comment(self.headersub1_simctrl_cd_str, level=self.parameters.level_header_2)) 
        template_string.extend(self.sc_tool.cd_tool.create_default_capture_data())

        # Show ROD and Pause
        template_string.extend(self.sc_tool.init_tool.create_hide_show())
        template_string.extend(self.sc_tool.init_tool.create_pause())


        result = "Test passed"
        msg = ''.join([item for sublist in template_string for item in sublist])
        return result, msg

    def unit_test_walk(self):
        template_string = []
        template_string.extend(self.parameters.create_header_comment(self.header_walk_str))
        template_string.extend(self.sc_tool.walk_tool.walk_stable_start_sequence())

        result = "Test passed"
        msg = ''.join([item for sublist in template_string for item in sublist])
        return result, msg

    def unit_test_hello(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Hello From Simulation Control :)
        """
        result = "Test passed"
        msg = "Hello from Simulation Control :)"
        return result, msg

if __name__ == "__main__":
    unit_test = UnitTestsSimulationControl()
    unit_test.test_psf_out(detailed_results=sys.argv[1])