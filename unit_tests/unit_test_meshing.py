import os
import sys
import copy 
import importlib
import numpy as np
import math
from control.matlab import *
import __init__
importlib.reload(__init__)
from parameters import Parameters
from meshing import MeshTool
from meshing import Triso
from unit_test_helper import UnitTestsHelper



class UnitTestsMeshing(UnitTestsHelper):
    """
    This unit test is composed of functions corresponding to different mesh types.
    These functions will read their respective yaml files and update the parameters accordingly.

    The yaml files can be listed at the very end of this script, which are then added to a list and are then indexed by their
    respective functions. 
    
    This unit test also consists of two functions for polyline creation 

    The "hello" unit test function is used to indicate which unit test results are being displayed

    To run this script with detailed results,    use: python3 unit_test_meshing.py True
    To run this script without detailed results, use: python3 unit_test_meshing.py False

    """
    def __init__(self, input_file_path=None):
        super(UnitTestsMeshing, self).__init__()
        self.input_yaml_list = input_file_path
        
    
    def update_config(self, input_file_path):
        self.parameters = Parameters(input_file_path)
        self.mesh_tool = MeshTool(input_file_path)
        self.mesh_triso_tool = Triso(input_file_path)
        
        
    def unit_test_1_simple_rz(self):
        """
        This function creates a simple 2D RZ smeared rod mesh

        :return 1: result: Test passed
        :return 2: msg: psf formatted list of strings for creating a simple 2D RZ smeared rod mesh
        """
        self.update_config(self.input_yaml_list[1])
        
        # self.template_mesh = self.mesh_tool.mesh_rz_2d_rod_tool.create_simple_smeared_rod()
        fuel_region = self.parameters.region_fuel
        gap_region = self.parameters.region_fuel_cladding_gap
        cladding_region = self.parameters.region_cladding
        result_mesh_name = self.parameters.qp_rod_name
        mesh_name_fuel = self.parameters.qp_fuel_name
        mesh_name_gap = self.parameters.qp_fuel_cladding_gap_name
        mesh_name_cladding = self.parameters.qp_cladding_name
        
        flag_assembly=False
        
        # Multi-layer cladding support
        if len(self.parameters.geometry_cladding_thickness_list) > 1:
            thickness_list = self.parameters.geometry_cladding_thickness_list
            radial_divisions_list = self.parameters.divisions_radial_cladding_list
        else:
            thickness_list = [self.parameters.geometry_cladding_thickness_list[0]]
            radial_divisions_list = [self.parameters.divisions_radial_cladding_list[0]]
        psf_string_list = []
        
        height = self.parameters.geometry_fuel_height_smeared
        axial_divisions = self.parameters.divisions_axial_fuel_smeared
        
        # Fuel region
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_fuel(
            height,
            axial_divisions,
            region=fuel_region,
            result_qp_name=mesh_name_fuel))
        
        # Gap region
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_gap(
            height,
            axial_divisions,
            region=gap_region,
            result_qp_name=mesh_name_gap))
        
        # Cladding region
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_cladding(
            thickness_list_in=thickness_list,
            divisions_radial_list=radial_divisions_list,
            divisions_axial=axial_divisions,
            height=height,
            region=cladding_region,
            name=mesh_name_cladding))
        
        # auto snapping
        assembly_list = []
        
        # snap component wise assemblies, not everything thats been created yet
        if not flag_assembly:
            for key in self.mesh_tool.mesh_rz_2d_rod_tool.creation_dict_quad_patch:
                assembly_list.append(key)
        else:
            for key in self.mesh_tool.mesh_rz_2d_rod_tool.creation_dict_quad_patch:
                if result_mesh_name in key:
                    assembly_list.append(key)
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.snap_list_objects(
                object_list=assembly_list,
                child_object_name=result_mesh_name))
        
        # deleting
        psf_string_list.append(self.mesh_tool.mesh_rz_2d_rod_tool.delete_list_of_objects(object_list=assembly_list))
        
        # bring to xz plane
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.rotate(
            result_mesh_name,
            90, 
            "x"))
    
        result = "Test passed"
        msg = ''.join([item for sublist in psf_string_list for item in sublist])
        return result, msg
    
    def unit_test_2_detailed_rz(self):
        """
        This function creates a detailed 2D RZ smeared rod mesh

        :return 1: result: Test passed
        :return 2: msg: psf formatted list of strings for creating a detailed 2D RZ smeared rod mesh
        """
        self.update_config(self.input_yaml_list[2])
         # self.template_mesh = self.mesh_tool.mesh_rz_2d_rod_tool.create_detailed_smeared_rod()
        self.mesh_tool.mesh_rz_2d_rod_tool.creation_dict_hex_mesh
        psf_string_list = []
        # smeared rod assembly
        psf_string_list.extend(self.parameters.create_header_comment(
            "Smeared Fuel rod assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_unit_rod_assembly(
            self.parameters.geometry_fuel_height_smeared,
            self.parameters.divisions_axial_fuel_smeared))
        # Top plenum
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_top,
            divisions=self.parameters.divisions_axial_plenum_top,
            name=self.parameters.qp_plenum_top_name))
        # Bottom plenum
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom plenum assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_plenum_assembly(
            height=self.parameters.geometry_plenum_height_bottom,
            divisions=self.parameters.divisions_axial_plenum_bottom,
        name=self.parameters.qp_plenum_bottom_name))
        # Top end cap
        psf_string_list.extend(self.parameters.create_header_comment(
            "Top end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.create_end_cap_assembly())
        # Bottom end cap
        psf_string_list.extend(self.parameters.create_header_comment(
            "Bottom end cap assembly", 
            level=self.parameters.level_header_2))
        psf_string_list.append(self.mesh_tool.mesh_rz_2d_rod_tool.copy(
            self.parameters.qp_end_cap_top_name,
            self.parameters.qp_end_cap_bottom_name))
        # auto translate and snap all assemblies to correct position and delete parent objects
        psf_string_list.extend(self.mesh_tool.mesh_rz_2d_rod_tool.auto_translate_assemblies(
            object_dictionary=self.mesh_tool.mesh_rz_2d_rod_tool.creation_dict_quad_patch,
            position_dictionary=self.parameters.geometry_mesh_positions_dict,
            result_object_name=self.parameters.qp_rod_name))
        result = "Test passed"
        msg = ''.join([item for sublist in psf_string_list for item in sublist]) 
        return result, msg

    def unit_test_3_triso_mesh_grid_gen(self):
        """
        This function calls compute_simple_random_grid() from meshing.py (mesh_tool) which will output figures for an 
        automated grid of triso pellets

        :return 1: result: Test passed
        :return 2: msg: "Figures generated"
        """
        self.update_config(self.input_yaml_list[3])

        self.mesh_triso_tool.compute_simple_random_grid()
        

        result = "Test passed"
        msg = "Figures generated"
        return result, msg

    def unit_test_4_polyline_radial(self):
        """
        This function calls create_linear_radial_polyline() from meshing.py (mesh_tool)

        :return 1: result: Test passed
        :return 2: msg: psf formatted list of strings for creating a radial polyline given a set of points
        """
        self.update_config(self.input_yaml_list[0])
        
        poly_name = "Fuel_Mid"
        start_point = [0.0, 0.0, 0.0]
        end_point = [0.0002195]
        num_points = 5

        self.point_1_x = start_point[0]
        self.point_1_y = start_point[1]
        self.point_1_z = start_point[2]
        self.point_n_x = end_point[0]

        psf_string_list = [self.parameters.create_header_comment("Polyline"),
            self.mesh_tool.create_linear_radial_polyline(poly_name, start_point, end_point, num_points)]

        result = "Test passed"
        msg = ''.join([item for sublist in psf_string_list for item in sublist]) 
        return result, msg

    def unit_test_5_polyline_axial(self):
        """
        This function calls create_linear_axial_polyline() from meshing.py (mesh_tool)

        :return 1: result: Test passed
        :return 2: msg: psf formatted list of strings for creating a polyline in the axial direction given a set of points
        """
        self.update_config(self.input_yaml_list[0])
        
        poly_name   = "Fuel_Axial_OD"
        start_point = [0.0002195, 0.0, 0.0]
        end_point   = [0.0002195, 0.0, 0.508]
        num_points  = 10

        self.point_1_x = start_point[0]
        self.point_1_y = start_point[1]
        self.point_1_z = start_point[2]
        self.point_n_z = end_point[2]

        psf_string_list = [self.parameters.create_header_comment("Polyline"),
            self.mesh_tool.create_linear_axial_polyline(poly_name, start_point, end_point, num_points)]

        result = "Test passed"
        msg = ''.join([item for sublist in psf_string_list for item in sublist]) 
        return result, msg

    def unit_test_hello(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Hello from Meshing :)
        """
        result = "Test passed"
        msg = "Hello from Meshing :)"
        return result, msg

if __name__ == "__main__":
    param_file_test_1 = None
    param_file_test_2 = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_rz_1.yaml"
    param_file_test_3 = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_rz_2.yaml"
    param_file_test_4 = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_t_2.yaml"
    param_file_list = [param_file_test_1, param_file_test_2, param_file_test_3, param_file_test_4]
    unit_test = UnitTestsMeshing(param_file_list)
    unit_test.test_psf_out(detailed_results=sys.argv[1])