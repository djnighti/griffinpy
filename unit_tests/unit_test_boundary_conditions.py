import os
import sys
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from boundary_conditions import BoundaryConditionTool
from unit_test_helper import UnitTestsHelper


class UnitTestsBoundaryConditions(UnitTestsHelper):
    """
    This unit test will display a psf formatted output for multiple boundary condition types along with 
    the power distribution object, gas inventory object, and fuel inventory object.
    
    unit_test_hello is used to indicate which unit test reuslts are being displayed

    To run this script with detailed results,    use: python3 unit_test_boundary_conditions.py True
    To run this script without detailed results, use: python3 unit_test_boundary_conditions.py False
    """
    def __init__(self):
        super(UnitTestsBoundaryConditions, self).__init__()
        self.parameters = Parameters()
        self.bc_tool = BoundaryConditionTool()
        
    def unit_test_bc(self):
        """
        calls on several functions from boundary_conditions.py (bc_tool)

        :return 1: result: Test passed
        :return 2: msg: psf formatted output for multiple boundary condition types along with the 
         power distribution object, gas inventory object, and fuel inventory object.
        """
        self.headersub1_bc_gio_str = "Gas Inventory Object"
        self.headersub1_bc_fio_str = "Fuel Inventory Object"
        bc_string_list = []

        ## Gravity
        self.bc_gravity = self.bc_tool.bc.create_gravity()
        bc_string_list.extend(self.bc_gravity)

        ## Pressure
        self.bc_pressure = self.bc_tool.bc.create_pressure(boundary_type="FACET_BOUNDARY_CONDITION")
        bc_string_list.extend(self.bc_pressure)

        ## Heat Flux
        self.bc_heat_flux = self.bc_tool.bc.create_heat_flux(boundary_type="FACET_BOUNDARY_CONDITION")
        bc_string_list.extend(self.bc_heat_flux)

        ## Cladding Temperature
        self.bc_temperature_cladding = self.bc_tool.bc.create_temperature()
        bc_string_list.extend(self.bc_temperature_cladding)

        ## Plenum Temperature
        self.bc_temperature_plenum = self.bc_tool.bc.create_gio_temperature()
        bc_string_list.extend(self.bc_temperature_plenum)
        
        ## Power
        self.bc_power = self.bc_tool.bc.create_power(
            height_of_active_rod=self.parameters.geometry_fuel_height_smeared
            )
        bc_string_list.extend(self.bc_power)

        ## Inventory objects
        bc_string_list.extend(self.parameters.create_header_comment(self.headersub1_bc_gio_str, level=self.parameters.level_header_2))
        bc_string_list.extend(self.bc_temperature_plenum)
        bc_string_list.extend(self.bc_tool.inventory_gas.create_inventory_object())

        # Fuel Inventory Object
        bc_string_list.extend(self.parameters.create_header_comment(self.headersub1_bc_fio_str, level=self.parameters.level_header_2))
        bc_string_list.extend(self.bc_tool.inventory_fuel.create_inventory_object())
        

        result = "Test passed"
        msg    = ''.join([item for sublist in bc_string_list for item in sublist])
        return result, msg

    def unit_test_hello(self):
        """

        :return 1: result: Test Passed
        :return 2: msg: Hello from Boundary Conditions :)
        """
        result = "Test passed"
        msg = "Hello from Boundary Conditions :)"
        return result, msg

if __name__ == "__main__":
    unit_test = UnitTestsBoundaryConditions()
    unit_test.test_psf_out(detailed_results=sys.argv[1])