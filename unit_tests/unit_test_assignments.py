import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from assignments import AssignmentsTool
from unit_test_helper import UnitTestsHelper


class UnitTestsAssignments(UnitTestsHelper):
    """
    This unit test displays a psf formatted output of the Assign command for assigning materials and boundary conditions

    unit_test_hello is used to indicate which unit test reuslts are being displayed

    To run this script with detailed results,    use: python3 unit_test_assignments.py True
    To run this script without detailed results, use: python3 unit_test_assignments.py False

    :return 1: result: Test Passed
    :return 2: msg: psf formatted list of ASSIGN parameters
    """
    def __init__(self):
        super(UnitTestsAssignments, self).__init__()
        self.parameters = Parameters()
        self.assign_tool = AssignmentsTool()
        self.regions_dict = {}
        self.create_assign_dicts()
    
    def create_assign_dicts(self):
        region_fuel = "REGION_FUEL"
        region_gap = "REGION_GAP"
        region_cladding = "REGION_CLADDING"
        self.regions_dict[1] = region_fuel
        self.regions_dict[2] = region_gap
        self.regions_dict[3] = region_cladding
        
    def unit_test_assign(self):
        template_string = []
        template_string.extend(self.parameters.create_header_comment("Assign Materials"))
        template_string.extend(self.assign_tool.assign_rod_materials(self.regions_dict))
        template_string.extend(self.parameters.create_header_comment("Assign Boundary Conditions"))
        template_string.extend(self.assign_tool.assign_2drz_rod_bc())
        
        result = "Test passed"
        msg = ''.join([item for sublist in template_string for item in sublist])
        return result, msg
        
    def unit_test_hello(self):
        """

        :return 1: result: Test Passed
        :return 2: msg: Hello from Assignments :)
        """
        result = "Test passed"
        msg = "Hello from Assignments :)"
        return result, msg
        

if __name__ == "__main__":
    unit_test = UnitTestsAssignments()
    unit_test.test_psf_out(detailed_results=sys.argv[1])