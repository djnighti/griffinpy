import os
import sys

source_code_dir = os.path.abspath(os.path.join(os.getcwd(),"../"))
sys.path.append(source_code_dir+"/src")