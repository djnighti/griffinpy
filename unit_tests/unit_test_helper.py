import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
import traceback
import fnmatch
import argparse

class UnitTestsHelper:
    """
    This class is imported in each one of the unit_tests to allow for individual runs of the unit tests

    This script can also be used to run all unit tests at once with the option to view detailed psf formatted outputs
    or simple "unit_test pass/fail outputs"

    To run this script, use: python3 unit_test_helper.py
    """
    def __init__(self, run_master=False):
        self.test_results_dict = {}
        self.test_class_name = self.__class__.__name__
        if run_master:
            self.update_show_details()
    
    def update_show_details(self):
        user_input = input("Do you want to see detailed results from all unit tests? (y/n): ")
        if user_input.lower() == "y":
            self.show_details = True
        elif user_input.lower() == "n":
            self.show_details = False
        else:
            print("Sorry, only [y/n] options are available")
        

    def run_unit_tests(self):
        """
        Run all unit tests in this directory with prefix unit_test. while omitting unit_test_helper.py
        """
        test_files = [file for file in os.listdir() if file.endswith(".py") and file.startswith("unit_test") and not file.endswith("unit_test_helper.py")]
        for file in test_files:
            os.system(f"python3 {file} {self.show_details}")

    def print_unit_tests_summary(self, detailed=False):
        print(f"\n================== Unit Test Summary for: {self.test_class_name} ==================\n")
        for a_test in self.test_results_dict:
            print(
                f"Result for {a_test}: {self.test_results_dict[a_test][0]}\n")
            if detailed=="True":
                print(
                    f"Test output: \n"
                    f"{self.test_results_dict[a_test][1]}\n")
        
    def test_psf_out(self, detailed_results=False):
        test_name_list = []
        result_list = []
        msg_list = []
        for method_name in dir(self):
            if method_name.startswith("unit"):
                try:
                    method = getattr(self, method_name)
                    a_result, a_msg = method()
                    result_list.extend([a_result])
                    msg_list.extend([a_msg])
                    test_name_list.append(method_name)
                
                    result = ''.join([item for sublist in result_list for item in sublist])
                    msg = ''.join([item for sublist in msg_list for item in sublist])
                    self.test_results_dict[method_name] = [a_result, a_msg]   
                except Exception as e:
                    result = "Test Failed"
                    msg = traceback.format_exc()
                    test_name_list = []
                    self.test_results_dict[method_name] = [result, msg]
        
        self.print_unit_tests_summary(detailed=detailed_results)

if __name__ == "__main__":
    unit_test = UnitTestsHelper(run_master=True)
    unit_test.run_unit_tests()