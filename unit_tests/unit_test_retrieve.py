import os
import sys
import copy 
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from materials import RodMaterialsTool
from retrieve import RetrieveTool
from unit_test_helper import UnitTestsHelper


class UnitTestsRetrieve(UnitTestsHelper):
    """
    This unit test reads a yaml file that is used to update materials and parameters and specifies the state variables for the 
    "Retrieve" command
    
    This unit test will display a .psf formatted output for the "Retrieve" commands

    The "hello" unit test function is used to indicate which unit test results are being displayed

    To run this script with detailed results,    use: python3 unit_test_retrieve.py True
    To run this script without detailed results, use: python3 unit_test_retrieve.py False
    """
    def __init__(self, input_file_path=None):
        super(UnitTestsRetrieve, self).__init__()
        self.parameters = Parameters(input_file_path)
        self.mat_tool = RodMaterialsTool(input_file_path)
        self.ret_tool = RetrieveTool(input_file_path)
        self.all_mat_dicts = self.mat_tool.create_all_mat_dictionary()
        self.parameters.create_material_associations_dict()

    def unit_test_1_retrieve(self):
        retrieve_string_list = []
        for mat_alias in self.parameters.material_associations_dict:
            mat_class_name = self.parameters.material_associations_dict[mat_alias][0]
            ret_dict = self.all_mat_dicts[mat_class_name].create_material_state_var_dict()
            retrieve_string_list.extend(self.ret_tool.create_retrieve_from_dict(ret_dict))
        result = "Test passed"
        msg=''.join([item for sublist in retrieve_string_list for item in sublist])
        return result, msg
        
    def unit_test_hello(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Hello From Retrieve :)
        """
        result = "Test passed"
        msg = "Hello from Retrieve :)"
        return result, msg

if __name__ == '__main__':
    param_file_test = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_ret.yaml"
    unit_test = UnitTestsRetrieve(param_file_test)
    unit_test.test_psf_out(detailed_results=sys.argv[1])
