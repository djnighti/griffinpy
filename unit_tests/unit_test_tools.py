import os
import sys
import copy 
import openpyxl
import pandas as pd
import numpy as np
import importlib
import __init__
importlib.reload(__init__)
from parameters import Parameters
from tools import FileTool
from unit_test_helper import UnitTestsHelper


class UnitTestsTools(UnitTestsHelper):
    """
    This unit test consits of two functions that test the tools used to convert excel files to gtd
    and generating time steps from given data

    These functions will read their respective yaml files that specify the path to the excel input data

    The yaml files can be listed at the very end of this script, which are then added to a list and are then indexed by their
    respective functions

    The "hello" unit test function is used to indicate which unit test results are being displayed

    To run this script with detailed results,    use: python3 unit_test_tools.py True
    To run this script without detailed results, use: python3 unit_test_tools.py False

    """
    def __init__(self, xlsx_file_path=None):
        super(UnitTestsTools, self).__init__()
        self.xlsx_file_path = xlsx_file_path
        
     
    def update_config(self, xlsx_file_path):
        self.parameters = Parameters(xlsx_file_path)
        self.file_tool = FileTool(xlsx_file_path)
        
        
    def unit_test_convert_xlsx_gtd(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Tools working
        """
        self.update_config(self.xlsx_file_path[1])

        self.file_tool.convert_xlsx_to_gtd()
        


        result = "Test passed"
        msg = "Tools working"
        return result, msg


    def unit_test_get_time_steps(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Tools working
        """
        self.update_config(self.xlsx_file_path[1])

        self.file_tool.get_time_steps()
        

        result = "Test passed"
        msg = "Tools working"
        return result, msg


    def unit_test_hello(self):
        """

        :return 1: result: Test passed
        :return 2: msg: Hello from Tools :)
        """
        result = "Test passed"
        msg = "Hello from Tools :)"
        return result, msg

if __name__ == "__main__":
    param_file_test_1 = None
    param_file_test_2 = "/home/projects/shared_folders/griffinpy/test/script_generation/scrip_gen_input/generator_input_rz_1.yaml"
    param_file_list = [param_file_test_1, param_file_test_2]
    unit_test = UnitTestsTools(param_file_list)
    unit_test.test_psf_out(detailed_results=sys.argv[1])