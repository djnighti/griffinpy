#======================================================================= 
# Model Explaination 
#=======================================================================

#======================================================================= 
# Model Construction Plan 
#=======================================================================

# 1. Model Parameters 
# 
#     1.1. Model Geometry 
# 
#     1.2. Model Resolution 
# 
#     1.3. Material parameters 
# 
#     1.4. Inventory parameters 
# 
#     1.5. Boundary Condition parameters 
# 
# 2. Meshing 
# 
#     2.1. Create Mesh 
# 
#     2.2. Create Physical Mesh 
# 
# 3. Materials 
# 
# 4. Element/node sets and Polylines 
# 
#     4.1. All nodes/elements 
# 
#     4.2. Material elements 
# 
#     4.3. Boundary condition elements 
# 
# 5. Boundary Conditions 
# 
#     5.1. Parameters 
# 
#     5.2. Gravity 
# 
#     5.3. Fixed 
# 
#     5.4. Pressure 
# 
#     5.5. Heat 
# 
#     5.6. Power 
# 
#     5.7. Gas inventory object 
# 
#     5.8. Fuel inventory object 
# 
# 6. Assign materials & boundary conditions to Nodes/Elements 
# 
#     6.1. Materials 
# 
#     6.2. Boundary/Initial Conditions 
# 
# 7. Setting simulation solution parameters 
# 
#     7.1. Mechanical Contact 
# 
#     7.2. Convergence Tolerances 
# 
#     7.3. Solution Control Parameters 
# 
#     7.4. Initialize Simulation 
# 
#     7.5. Capture Data 
# 
# 8. Commence with simulation 
# 
# 9. Database 
# 
# 10. Data retrieval 
# 

#======================================================================= 
# 1. Model Parameters 
#=======================================================================

#----------------------------------------------------------------------- 
# 1.1. Model Geometry 
#-----------------------------------------------------------------------

CREATE REAL GEOMETRY_FUEL_FUEL_GAP_HEIGHT = 1.e-03 
CREATE REAL GEOMETRY_FUEL_ENDCAP_GAP_THICKNESS = 1.e-03 
CREATE REAL GEOMETRY_FUEL_RADIUS = 6.25e-03 
CREATE REAL GEOMETRY_FUEL_HEIGHT_SMEARED = 1.e-03 
CREATE REAL GEOMETRY_FUEL_HEIGHT_DISCRETE = 1.e+00 
CREATE REAL GEOMETRY_FUEL_MPS_LENGTH = 1.e-03 
CREATE REAL GEOMETRY_CLADDING_INNER_RADIUS = 7.25e-03 
CREATE REAL GEOMETRY_CLADDING_OUTER_RADIUS = 8.25e-03 
CREATE REAL GEOMETRY_FUEL_DIAMETER = 1.25e-02 
CREATE REAL GEOMETRY_GAP_THICKNESS = 1.e-03 
CREATE REAL GEOMETRY_PLENUM_HEIGHT_TOP = 1.e-03 
CREATE REAL GEOMETRY_PLENUM_HEIGHT_BOTTOM = 1.e-03 
CREATE REAL GEOMETRY_END_CAP_THICKNESS = 1.e-03 
CREATE REAL GEOMETRY_TOLERANCE = 1.e-07 
CREATE REAL GEOMETRY_SELECTION_VOLUME_THRESHOLD = 1.e-06 
CREATE REAL GEOMETRY_DEFORMATION_SCALE = 1.e+00 
CREATE REAL GEOMETRY_TRISO_KERNAL_RADIUS = 1.e-03 
CREATE REAL GEOMETRY_TRISO_BUFFER_RADIUS = 1.5e-03 
CREATE REAL GEOMETRY_TRISO_INNER_PYROLYTIC_RADIUS = 1.8e-03 
CREATE REAL GEOMETRY_TRISO_SILICON_CARBIDE_RADIUS = 2.e-03 
CREATE REAL GEOMETRY_TRISO_OUTER_PYROLYTIC_RADIUS = 2.1e-03 
CREATE REAL GEOMETRY_TRISO_GRAPHITE_BLOCK_SCALAR = 1.1e+00 
CREATE REAL GEOMETRY_TRISO_PARTICLES_DENSITY_PERCENT = 4.e-01 
CREATE REAL GEOMETRY_TRISO_GRAPHITE_BLOCK_RADIUS = 2.31e-03 
CREATE REAL GEOMETRY_CLADDING_THICKNESS = 1.e-03 
CREATE REAL GEOMETRY_NEGATIVE_SELECTION_VOLUME_THRESHOLD = -1.e-06 
CREATE REAL GEOMETRY_TIMES_2_SELECTION_VOLUME_THRESHOLD = 2.e-06 
CREATE REAL GEOMETRY_NEGATIVE_TIMES_2_SELECTION_VOLUME_THRESHOLD = -2.e-06 
CREATE REAL GEOMETRY_OUTSIDE_OUTER_CLAD_RADIUS = 8.251e-03 
CREATE REAL GEOMETRY_INSIDE_OUTER_CLAD_RADIUS = 8.249e-03 
CREATE REAL GEOMETRY_TIMES_2_OUTSIDE_OUTER_CLAD_RADIUS = 1.6502e-02 
CREATE REAL GEOMETRY_NEGATIVE_OUTSIDE_OUTER_CLAD_RADIUS = -8.251e-03 
CREATE REAL GEOMETRY_OUTSIDE_TOP_ROD_HEIGHT = 3.001e-03 
CREATE REAL GEOMETRY_OUTSIDE_BOTTOM_ROD_HEIGHT = -2.001e-03 

#----------------------------------------------------------------------- 
# 1.2. Model Resolution 
#-----------------------------------------------------------------------

CREATE INTEGER DIVISIONS_RADIAL_FUEL = 1 
CREATE INTEGER DIVISIONS_RADIAL_GAP = 1 
CREATE INTEGER DIVISIONS_CIRCUMFERENTIAL = 16 
CREATE INTEGER DIVISIONS_AXIAL_FUEL_DISCRETE = 1 
CREATE INTEGER DIVISIONS_AXIAL_FUEL_SMEARED = 1 
CREATE INTEGER DIVISIONS_AXIAL_PLENUM_TOP = 1 
CREATE INTEGER DIVISIONS_AXIAL_PLENUM_BOTTOM = 1 
CREATE INTEGER DIVISIONS_AXIAL_FUEL_FUEL_GAP = 1 
CREATE INTEGER DIVISIONS_AXIAL_END_CAP = 1 

#======================================================================= 
# 2. Meshing 
#=======================================================================

#----------------------------------------------------------------------- 
# 2.1. Create Mesh 
#-----------------------------------------------------------------------

CREATE SPHERE_HEX_BLOCK \ 
  HEX_CORE \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.0 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.0 \ # Inner radius of block
  0.0003 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.002 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  3 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_0 # Region associated with quad patch

CREATE SPHERE_HEX_BLOCK \ 
  HEX_ROD \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.0003 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.002 \ # Inner radius of block
  0.001 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.001 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  3 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_0 # Region associated with quad patch

CREATE SPHERE_HEX_BLOCK \ 
  HEX_ROD_1 \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.001 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.001 \ # Inner radius of block
  0.0011 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.0011 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  1 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_1 # Region associated with quad patch

HEX_ROD = HEX_ROD SNAP HEX_ROD_1 

DELETE {HEX_ROD_1} 

CREATE SPHERE_HEX_BLOCK \ 
  HEX_ROD_2 \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.0011 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.0011 \ # Inner radius of block
  0.00114 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.00114 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  1 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_2 # Region associated with quad patch

HEX_ROD = HEX_ROD SNAP HEX_ROD_2 

DELETE {HEX_ROD_2} 

CREATE SPHERE_HEX_BLOCK \ 
  HEX_ROD_3 \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.00114 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.00114 \ # Inner radius of block
  0.001175 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.001175 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  1 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_3 # Region associated with quad patch

HEX_ROD = HEX_ROD SNAP HEX_ROD_3 

DELETE {HEX_ROD_3} 

CREATE SPHERE_HEX_BLOCK \ 
  HEX_ROD_4 \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.001175 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.001175 \ # Inner radius of block
  0.001215 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  0.001215 \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  1 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_4 # Region associated with quad patch

HEX_ROD = HEX_ROD SNAP HEX_ROD_4 

DELETE {HEX_ROD_4} 

CREATE SPHERE_HEX_BLOCK \ 
  HEX_CUBE \ # Arbitrary name of mesh
  [0.0,0.0,0.0] \ # Base Coordinate 
  0.001215 \ # Inner length of bounding cube (use 0.0 when making a core block)
  0.001215 \ # Inner radius of block
  0.001215 \ # Outer length of bounding cube (set to outer radius of block for perfect sphere)
  INFINITY \ # Outer radius of block 
  2 \ # Number of divisions along edges (circumferential) (that connect shell and core blocks) 
  1 \ # Number of divisions along height (radial) (only for shell blocks, not core blocks)
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_TRISO_LAYER_4 # Region associated with quad patch

HEX_ROD = HEX_CUBE SNAP HEX_ROD 

COPY HEX_ROD TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [1.0, 1.0, 1.0] 120.0

HEX_ROD = HEX_ROD SNAP TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [1.0, 1.0, 1.0] 120.0

HEX_ROD = HEX_ROD SNAP TMP_OBJ_AR 

DELETE {TMP_OBJ_AR} 

HEX_ROD = HEX_CORE SNAP HEX_ROD 

DELETE {HEX_CORE, HEX_CUBE} 

COPY HEX_ROD TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_ROD = HEX_ROD SNAP TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_ROD = HEX_ROD SNAP TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_ROD = HEX_ROD SNAP TMP_OBJ_AR 

DELETE {TMP_OBJ_AR} 

CREATE HEX_BLOCK \ 
  HEX_VOID_PARTICLE \ # Arbitrary name of mesh
  [-0.001155, -0.001155, 0.0] \ # Base Coordinate 
  0.00231 \ # Extrusion length along x direction
  0.00231 \ # Extrusion length along y direction
  0.00231 \ # Extrusion length along z direction
  2 \ # Number of divisions along x direction
  2 \ # Number of divisions along y direction
  2 \ # Number of divisions along z direction
  1e-07 \ # Geometric tolerance
  HEX_8_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_GRAPHITE # Region associated with quad patch

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_0 

TRANSLATE \ 
  TMP_OBJ_VOID_0 \ # Name of object to translate 
  [-0.012705, -0.012705, 0.0] # Vector representing translation

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_1 

TRANSLATE \ 
  TMP_OBJ_VOID_1 \ # Name of object to translate 
  [-0.010395, -0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = TMP_OBJ_VOID_0 SNAP TMP_OBJ_VOID_1 

DELETE {TMP_OBJ_VOID_0, TMP_OBJ_VOID_1} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_2 

TRANSLATE \ 
  TMP_OBJ_VOID_2 \ # Name of object to translate 
  [-0.008085, -0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_2 

DELETE {TMP_OBJ_VOID_2} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_3 

TRANSLATE \ 
  TMP_OBJ_VOID_3 \ # Name of object to translate 
  [-0.005775, -0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_3 

DELETE {TMP_OBJ_VOID_3} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_4 

TRANSLATE \ 
  TMP_OBJ_TRISO_4 \ # Name of object to translate 
  [-0.00231, -0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_4 

DELETE {TMP_OBJ_TRISO_4} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_5 

TRANSLATE \ 
  TMP_OBJ_TRISO_5 \ # Name of object to translate 
  [0.00231, -0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_5 

DELETE {TMP_OBJ_TRISO_5} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_6 

TRANSLATE \ 
  TMP_OBJ_TRISO_6 \ # Name of object to translate 
  [0.00693, -0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_6 

DELETE {TMP_OBJ_TRISO_6} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_7 

TRANSLATE \ 
  TMP_OBJ_TRISO_7 \ # Name of object to translate 
  [0.01155, -0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_7 

DELETE {TMP_OBJ_TRISO_7} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_8 

TRANSLATE \ 
  TMP_OBJ_VOID_8 \ # Name of object to translate 
  [-0.012705, -0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_8 

DELETE {TMP_OBJ_VOID_8} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_9 

TRANSLATE \ 
  TMP_OBJ_VOID_9 \ # Name of object to translate 
  [-0.010395, -0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_9 

DELETE {TMP_OBJ_VOID_9} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_10 

TRANSLATE \ 
  TMP_OBJ_VOID_10 \ # Name of object to translate 
  [-0.008085, -0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_10 

DELETE {TMP_OBJ_VOID_10} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_11 

TRANSLATE \ 
  TMP_OBJ_VOID_11 \ # Name of object to translate 
  [-0.005775, -0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_11 

DELETE {TMP_OBJ_VOID_11} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_12 

TRANSLATE \ 
  TMP_OBJ_VOID_12 \ # Name of object to translate 
  [-0.012705, -0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_12 

DELETE {TMP_OBJ_VOID_12} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_13 

TRANSLATE \ 
  TMP_OBJ_VOID_13 \ # Name of object to translate 
  [-0.010395, -0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_13 

DELETE {TMP_OBJ_VOID_13} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_14 

TRANSLATE \ 
  TMP_OBJ_TRISO_14 \ # Name of object to translate 
  [-0.00693, -0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_14 

DELETE {TMP_OBJ_TRISO_14} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_15 

TRANSLATE \ 
  TMP_OBJ_TRISO_15 \ # Name of object to translate 
  [-0.00231, -0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_15 

DELETE {TMP_OBJ_TRISO_15} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_16 

TRANSLATE \ 
  TMP_OBJ_TRISO_16 \ # Name of object to translate 
  [0.00231, -0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_16 

DELETE {TMP_OBJ_TRISO_16} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_17 

TRANSLATE \ 
  TMP_OBJ_TRISO_17 \ # Name of object to translate 
  [0.00693, -0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_17 

DELETE {TMP_OBJ_TRISO_17} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_18 

TRANSLATE \ 
  TMP_OBJ_TRISO_18 \ # Name of object to translate 
  [-0.01155, -0.00462, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_18 

DELETE {TMP_OBJ_TRISO_18} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_19 

TRANSLATE \ 
  TMP_OBJ_TRISO_19 \ # Name of object to translate 
  [0.01155, -0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_19 

DELETE {TMP_OBJ_TRISO_19} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_20 

TRANSLATE \ 
  TMP_OBJ_VOID_20 \ # Name of object to translate 
  [-0.008085, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_20 

DELETE {TMP_OBJ_VOID_20} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_21 

TRANSLATE \ 
  TMP_OBJ_TRISO_21 \ # Name of object to translate 
  [-0.00462, -0.00231, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_21 

DELETE {TMP_OBJ_TRISO_21} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_22 

TRANSLATE \ 
  TMP_OBJ_VOID_22 \ # Name of object to translate 
  [-0.001155, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_22 

DELETE {TMP_OBJ_VOID_22} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_23 

TRANSLATE \ 
  TMP_OBJ_VOID_23 \ # Name of object to translate 
  [0.001155, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_23 

DELETE {TMP_OBJ_VOID_23} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_24 

TRANSLATE \ 
  TMP_OBJ_VOID_24 \ # Name of object to translate 
  [0.003465, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_24 

DELETE {TMP_OBJ_VOID_24} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_25 

TRANSLATE \ 
  TMP_OBJ_VOID_25 \ # Name of object to translate 
  [0.005775, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_25 

DELETE {TMP_OBJ_VOID_25} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_26 

TRANSLATE \ 
  TMP_OBJ_VOID_26 \ # Name of object to translate 
  [0.008085, -0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_26 

DELETE {TMP_OBJ_VOID_26} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_27 

TRANSLATE \ 
  TMP_OBJ_TRISO_27 \ # Name of object to translate 
  [0.00231, 0.0, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_27 

DELETE {TMP_OBJ_TRISO_27} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_28 

TRANSLATE \ 
  TMP_OBJ_VOID_28 \ # Name of object to translate 
  [-0.012705, -0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_28 

DELETE {TMP_OBJ_VOID_28} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_29 

TRANSLATE \ 
  TMP_OBJ_VOID_29 \ # Name of object to translate 
  [-0.010395, -0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_29 

DELETE {TMP_OBJ_VOID_29} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_30 

TRANSLATE \ 
  TMP_OBJ_VOID_30 \ # Name of object to translate 
  [-0.008085, -0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_30 

DELETE {TMP_OBJ_VOID_30} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_31 

TRANSLATE \ 
  TMP_OBJ_VOID_31 \ # Name of object to translate 
  [-0.001155, -0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_31 

DELETE {TMP_OBJ_VOID_31} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_32 

TRANSLATE \ 
  TMP_OBJ_TRISO_32 \ # Name of object to translate 
  [0.00693, 0.0, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_32 

DELETE {TMP_OBJ_TRISO_32} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_33 

TRANSLATE \ 
  TMP_OBJ_TRISO_33 \ # Name of object to translate 
  [0.01155, -0.00231, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_33 

DELETE {TMP_OBJ_TRISO_33} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_34 

TRANSLATE \ 
  TMP_OBJ_VOID_34 \ # Name of object to translate 
  [-0.012705, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_34 

DELETE {TMP_OBJ_VOID_34} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_35 

TRANSLATE \ 
  TMP_OBJ_VOID_35 \ # Name of object to translate 
  [-0.010395, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_35 

DELETE {TMP_OBJ_VOID_35} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_36 

TRANSLATE \ 
  TMP_OBJ_TRISO_36 \ # Name of object to translate 
  [-0.00693, 0.00231, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_36 

DELETE {TMP_OBJ_TRISO_36} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_37 

TRANSLATE \ 
  TMP_OBJ_VOID_37 \ # Name of object to translate 
  [-0.003465, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_37 

DELETE {TMP_OBJ_VOID_37} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_38 

TRANSLATE \ 
  TMP_OBJ_VOID_38 \ # Name of object to translate 
  [-0.001155, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_38 

DELETE {TMP_OBJ_VOID_38} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_39 

TRANSLATE \ 
  TMP_OBJ_VOID_39 \ # Name of object to translate 
  [0.010395, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_39 

DELETE {TMP_OBJ_VOID_39} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_40 

TRANSLATE \ 
  TMP_OBJ_VOID_40 \ # Name of object to translate 
  [0.012705, 0.001155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_40 

DELETE {TMP_OBJ_VOID_40} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_41 

TRANSLATE \ 
  TMP_OBJ_TRISO_41 \ # Name of object to translate 
  [-0.01155, 0.00462, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_41 

DELETE {TMP_OBJ_TRISO_41} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_42 

TRANSLATE \ 
  TMP_OBJ_VOID_42 \ # Name of object to translate 
  [-0.003465, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_42 

DELETE {TMP_OBJ_VOID_42} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_43 

TRANSLATE \ 
  TMP_OBJ_VOID_43 \ # Name of object to translate 
  [-0.001155, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_43 

DELETE {TMP_OBJ_VOID_43} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_44 

TRANSLATE \ 
  TMP_OBJ_TRISO_44 \ # Name of object to translate 
  [0.00231, 0.00462, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_44 

DELETE {TMP_OBJ_TRISO_44} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_45 

TRANSLATE \ 
  TMP_OBJ_VOID_45 \ # Name of object to translate 
  [0.005775, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_45 

DELETE {TMP_OBJ_VOID_45} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_46 

TRANSLATE \ 
  TMP_OBJ_VOID_46 \ # Name of object to translate 
  [0.008085, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_46 

DELETE {TMP_OBJ_VOID_46} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_47 

TRANSLATE \ 
  TMP_OBJ_VOID_47 \ # Name of object to translate 
  [0.010395, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_47 

DELETE {TMP_OBJ_VOID_47} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_48 

TRANSLATE \ 
  TMP_OBJ_VOID_48 \ # Name of object to translate 
  [0.012705, 0.003465, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_48 

DELETE {TMP_OBJ_VOID_48} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_49 

TRANSLATE \ 
  TMP_OBJ_TRISO_49 \ # Name of object to translate 
  [-0.00693, 0.00693, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_49 

DELETE {TMP_OBJ_TRISO_49} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_50 

TRANSLATE \ 
  TMP_OBJ_VOID_50 \ # Name of object to translate 
  [-0.003465, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_50 

DELETE {TMP_OBJ_VOID_50} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_51 

TRANSLATE \ 
  TMP_OBJ_VOID_51 \ # Name of object to translate 
  [-0.001155, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_51 

DELETE {TMP_OBJ_VOID_51} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_52 

TRANSLATE \ 
  TMP_OBJ_VOID_52 \ # Name of object to translate 
  [0.005775, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_52 

DELETE {TMP_OBJ_VOID_52} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_53 

TRANSLATE \ 
  TMP_OBJ_VOID_53 \ # Name of object to translate 
  [0.008085, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_53 

DELETE {TMP_OBJ_VOID_53} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_54 

TRANSLATE \ 
  TMP_OBJ_VOID_54 \ # Name of object to translate 
  [0.010395, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_54 

DELETE {TMP_OBJ_VOID_54} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_55 

TRANSLATE \ 
  TMP_OBJ_VOID_55 \ # Name of object to translate 
  [0.012705, 0.005775, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_55 

DELETE {TMP_OBJ_VOID_55} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_56 

TRANSLATE \ 
  TMP_OBJ_VOID_56 \ # Name of object to translate 
  [-0.012705, 0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_56 

DELETE {TMP_OBJ_VOID_56} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_57 

TRANSLATE \ 
  TMP_OBJ_VOID_57 \ # Name of object to translate 
  [-0.010395, 0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_57 

DELETE {TMP_OBJ_VOID_57} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_58 

TRANSLATE \ 
  TMP_OBJ_VOID_58 \ # Name of object to translate 
  [-0.003465, 0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_58 

DELETE {TMP_OBJ_VOID_58} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_59 

TRANSLATE \ 
  TMP_OBJ_VOID_59 \ # Name of object to translate 
  [-0.001155, 0.008085, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_59 

DELETE {TMP_OBJ_VOID_59} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_60 

TRANSLATE \ 
  TMP_OBJ_TRISO_60 \ # Name of object to translate 
  [0.00231, 0.00924, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_60 

DELETE {TMP_OBJ_TRISO_60} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_61 

TRANSLATE \ 
  TMP_OBJ_TRISO_61 \ # Name of object to translate 
  [0.00693, 0.00924, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_61 

DELETE {TMP_OBJ_TRISO_61} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_62 

TRANSLATE \ 
  TMP_OBJ_TRISO_62 \ # Name of object to translate 
  [0.01155, 0.00924, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_62 

DELETE {TMP_OBJ_TRISO_62} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_63 

TRANSLATE \ 
  TMP_OBJ_VOID_63 \ # Name of object to translate 
  [-0.012705, 0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_63 

DELETE {TMP_OBJ_VOID_63} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_64 

TRANSLATE \ 
  TMP_OBJ_TRISO_64 \ # Name of object to translate 
  [-0.00924, 0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_64 

DELETE {TMP_OBJ_TRISO_64} 

COPY HEX_TRISO_PARTICLE TMP_OBJ_TRISO_65 

TRANSLATE \ 
  TMP_OBJ_TRISO_65 \ # Name of object to translate 
  [-0.00462, 0.01155, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_TRISO_65 

DELETE {TMP_OBJ_TRISO_65} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_66 

TRANSLATE \ 
  TMP_OBJ_VOID_66 \ # Name of object to translate 
  [-0.001155, 0.010395, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_66 

DELETE {TMP_OBJ_VOID_66} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_67 

TRANSLATE \ 
  TMP_OBJ_VOID_67 \ # Name of object to translate 
  [-0.012705, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_67 

DELETE {TMP_OBJ_VOID_67} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_68 

TRANSLATE \ 
  TMP_OBJ_VOID_68 \ # Name of object to translate 
  [-0.001155, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_68 

DELETE {TMP_OBJ_VOID_68} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_69 

TRANSLATE \ 
  TMP_OBJ_VOID_69 \ # Name of object to translate 
  [0.001155, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_69 

DELETE {TMP_OBJ_VOID_69} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_70 

TRANSLATE \ 
  TMP_OBJ_VOID_70 \ # Name of object to translate 
  [0.003465, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_70 

DELETE {TMP_OBJ_VOID_70} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_71 

TRANSLATE \ 
  TMP_OBJ_VOID_71 \ # Name of object to translate 
  [0.005775, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_71 

DELETE {TMP_OBJ_VOID_71} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_72 

TRANSLATE \ 
  TMP_OBJ_VOID_72 \ # Name of object to translate 
  [0.008085, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_72 

DELETE {TMP_OBJ_VOID_72} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_73 

TRANSLATE \ 
  TMP_OBJ_VOID_73 \ # Name of object to translate 
  [0.010395, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_73 

DELETE {TMP_OBJ_VOID_73} 

COPY HEX_VOID_PARTICLE TMP_OBJ_VOID_74 

TRANSLATE \ 
  TMP_OBJ_VOID_74 \ # Name of object to translate 
  [0.012705, 0.012705, 0.0] # Vector representing translation

HEX_TRISO_MATRIX = HEX_TRISO_MATRIX SNAP TMP_OBJ_VOID_74 

DELETE {TMP_OBJ_VOID_74} 

DELETE {HEX_TRISO_PARTICLE, HEX_VOID_PARTICLE} 

CREATE QUAD_PATCH \ 
  QP_POLY \ # Arbitrary name of quad patch
  [[-0.01386, -0.01386],\ # Point 1
  [-0.004419417382415923, -0.004419417382415923], \ # Point 2
  [0.004419417382415923, -0.004419417382415923], \ # Point 3
  [0.01386, -0.01386]] \ # Point 4
  5 \ # Divisions along edge 1 and 3 (radial) 
  24 \ # Divisions along edge 2 and 4 (axial) 
  [0.0, 0.00625, 0.0, 0.0] \ # Curvatures along each edge
  [1.0, 1.0, 1.0, 1.0] \ # Division ratios along each edge
  1e-07 \ # Geometric tolerance
  AS_IS \ # Orientation of provided points
  QUAD_4_NODE_ELEM_KIND \ # Element kind (4/9 node options)
  REGION_GRAPHITE # Region associated with quad patch

CREATE TORUS \ 
  TEMPORARY_SELECTION_VOLUME \ # name of selection volume 
  [0.0, 0.0, 0.0] \ # starting point 
  [0.0, 0.0, 1.0] \ # direction vector
  0.00625 \ # radius_major 
  4e-06 \ # radius_minor 
  24 \ # circumferential divisions major
  24 \ # circumferential divisions minor
  1e-06 # geometry tolerance 
 
MARK \ 
  QP_POLY \ # Name of object to mark 
  CLAD_OUTER_SURFACE_MARK \ # Name of marked surface
  BOUNDARY_ONLY \ # opts: BOUNDARY_ONLY, SURFACE_ONLY, BOUNDARY_AND_SURFACE 
  PARTIALLY \ # opts: COMPLETELY, PARTIALLY 
  INSIDE \ # opts: INSIDE, OUTSIDE 
  TEMPORARY_SELECTION_VOLUME # Name of selection volume to use for marking 
 


EXTRUDE PRISM\ 
  HEX_VOID_RING \ 
  [0.0, 0.0, 0.0] \ 
  [0.0, 0.0, 1.0] \ 
  0.00231 \ 
  2 \ 
  1.0 \ 
  1.0 \ 
  0.0 \ 
  QP_POLY 
DELETE {QP_POLY} 

COPY HEX_VOID_RING TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_VOID_RING = HEX_VOID_RING SNAP TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_VOID_RING = HEX_VOID_RING SNAP TMP_OBJ_AR 

ROTATE TMP_OBJ_AR [0.0,0.0,0.0] [0.0, 0.0, 1.0] 90.0

HEX_VOID_RING = HEX_VOID_RING SNAP TMP_OBJ_AR 

DELETE {TMP_OBJ_AR} 

#----------------------------------------------------------------------- 
# 2.2. Create Physical Mesh 
#-----------------------------------------------------------------------

CREATE PHYSICAL_MESH ROD HEX_ROD 
ASSIGN DEFORMABLE_QUASISTATIC_FINITE_DEFORMATION TRANSIENT_THERMAL TO ROD 

