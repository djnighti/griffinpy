# **griffinpy official repository**

A simple python wrapper for an FEA code called Grffin that will automate the script generation process as well create new problems and update old input decks.

<div>
<a href="https://griffinpy.readthedocs.io/en/latest/index.html" >Official documentation found on Readthedocs</a>
